\documentclass[11pt]{article}

\include{macros}
\include{enumerate}

\newcommand{\prob}[1]{\ensuremath{\text{{\bf Pr}$\left[#1\right]$}}}
\newcommand{\expct}[1]{\ensuremath{\text{{\bf E}$\left[#1\right]$}}}
\newcommand{\var}[1]{\ensuremath{\text{{\bf Var}$\left[#1\right]$}}}

\begin{document}

\scribe{HW2 Solutions}{11/28/2017}{Joel Atkins, Sam Lemley and Sam Vinitsky}

\section{} % 1
\subsection*{1(a)}

Suppose for the sake of contradiction that the statement holds for circuits with bounded fan-in. Let $\oplus$ denote the ``parity problem'' of determining the parity of the number of ones in an input. Clearly $\oplus \in \cc{NL}$, since we can solve it in deterministic constant space (just keep a parity bit, and flip it every time we see a 1 in the input). Then by assumption there exists a $c$ such that for all positive integers $d$, $\oplus_n$ is decided by some depth $d$ circuit with fan-in at most $b$ and size $2^{n^{c/d}}$ for all but finitely many $n$. 

Note that there are only a finite number of circuit of depth $d$ and bounded fan-in $b$ (we ignore the size restriction, since it will only lower the number of possible circuits). But for an infinite number of $n$, $\oplus_n$ is decided by some circuit of depth $d$ and fan in at most $b$. Since there are only finitely many such circuits, some circuit $C$ must decide $\oplus_n$ for an infinite number of $n$. But $C$ is a fixed circuit, and thus computes its output from $k$ input bits (for some fixed $k$). But then $C$ must compute $\oplus_m$ for some $m > k$, since it computes $\oplus_n$ for infinitely many $n$ and $k$ is fixed. But then $\oplus_m$ depends only on $k$ bits of the input. This means that we can compute the parity of a string $x$ of length $m$ by examining only $k < m = |x|$ bits of $x$. But it is impossible to compute the parity of $x$ without looking at all the bits of $x$. This is a contraction! Thus the statement fails to hold for circuits of bounded fan-in.

\subsection*{1(b)}

Let $L \in \cc{NL}$. Then $L$ is decided by some nondeterministic log-space (and poly-time) Turing machine $M$. Consider the computation tableau for $M$ on some input $x$. Let $t$ denote the time complexity of $M$. From our discussions in class, recall that for every $k$:

\begin{align}
x \in L \iff (\exists C_1,C_2,...,C_{k})(\forall b \in [k])(R_{M,x}^{t/k}(C_{b-1},C_{b})) \label{boo}
\end{align}

Where the $C_i$ denote configurations of $M$, $C_0$ is the initial configuration of $M$ on $x$, and $C_k$ is an accepting configuration. We use new notation $R_{M,x}^s(C_i,C_j)$ to denote that $M$ on input $x$ can reach $C_j$ from $C_i$ in $s$ steps. This statement then comes from splitting up the computation tableau of $M$ on $x$ into $k$ different chunks, and was discussed several times in class.

Let $d$ be given. Suppose $M$ runs in time $t = n^c$. Then set $k = n^{2c/d}$. Consider the circuit $D$ where the existential qualifier is replaced by a OR gate, the universal qualifiers are replaced by AND gates, and $R_{M,x}^{t/k}(C_{b-1},C_b)$ is computed recursively. 

We now describe $D$ in detail: The first layer of $D$ consists of ``many'' AND gates, each with fan-in $k$. The input to each AND gate is the output of a recursive circuit computing $R_{M,x}^{t/k}(C_{b-1},C_{b})$ for each $b \in [k]$. There is one such AND gate for every set of configurations of size $k$. There are $2^O(\log n)$ possible configurations, since $M$ is log space. Thus the total number of size $k$ whose members are configurations is $2^{k O (\log n)}$. Thus the total number of AND gates is $2^{k O (\log n)}$. The output of all of these AND gates is OR'd together in a single OR gate with fan-in $2^{k O (\log n)}$. 

\begin{claim}
$D$ has depth $d$, size $2^{n^{c'/d}}$, and decides $L$ (for some constant $c'$)
\end{claim}

\begin{proof}
The total depth of this circuit is 2 (one layer of AND gates, and one layer consisting of a single OR gate), plus the depth of the circuits recursively computing $R_{M,x}^{t/k}(C_{b-1},C_{b})$. Thus the depth of $D$ is equal to 2 times the number of recursions (since we add 2 circuits every time we recurse.) We recurse $\log_{k} t$ times, where $t= n^c$ is the time complexity of $M$. Then $\log_k t = \log{k} n^c = \log_{n^{2c/d}} n^c = d / 2$, and so the total number of recursions is $d/2$, meaning the depth of $d$ is $2 \cdot d /2 = d$ as desired.

This circuit has size $2^{k O (\log n)} = 2^{n^{2c/d} O(\log n)} \leq 2^{n^{c'/d}}$ for some $c'$ as desired.

Finally, $D$ decides $L$ by construction, since we're explicitly computing the formula from the right-hand side of Equation (\ref{boo}).
\end{proof}

Since we chose $d$ arbitrarily, this claim holds for all positive integers $d$. Since we chose $L$ arbitrarily, we know that all for all $L \in \cc{NL}$ there exists some $c'$ such that $L$ can be decided by some circuit of depth $d$ and size $2^{n^{c'/d}}$ for all positive integers $d$.

\section{} % 2
Some definitions:
\begin{itemize}
\item Let $f(x_1,...,x_n)$ denote the polynomial computed by $C$.
\item Let $N = |C|$, where the size of a constant is equal to its value. 
\item Let $p = (2^N + 1)^2$. 
\item Note: every monomial in $f$ has the sum of its degrees less than $2^N +1 < p^{1/2}$.
\item Note: every constant in $f$ has value less than $N^{2^N+1} < N^{p^{1/2}}$.
\item Let $g(y) = f(y^p, y^{p^2}, ..., y^{p^n})$. (i.e, $x_i = y^{p^i})$.
\end{itemize}

\begin{claim}
$g(y)$ is computable by a circuit $D$ whose size is polynomial in $N$ that can be constructed in polynomial time.
\end{claim}

\begin{proof}
$D$, on input $y$, computes $y_i = y^{p^i} = y^{(2^{|C|} + 1)^{2i}}$ by plugging $y$ into a repeated squaring circuit $S_i$ with some polynomial number of nodes, since the power is just $O(2^{\poly{|C|}})$ (since $i \leq n \leq N$). There are $n$ such circuits, (one for each input). The output from each of circuit $S_i$ goes into the input node $x_i$ for $C$. Thus $C$ takes inputs $x_i = y^{p^i}$ as desired, and thus by definition computes $g(x)$. Thus $g(N)$ is computed by plugging in $N$ as the input $D$. This whole construction takes polynomial time in $N=|C|$, since we simply made some extra circuits of polynomial size, and then hooked them into $C$.
\end{proof}

\begin{claim}
$C \equiv 0 \iff D(N) =0$.
\end{claim}
\begin{proof}
Note that $D(N) = C(N^p,...,N^{p^n})$. Thus if $C \equiv 0$, then it is zero on every input, including $(N^p,...,N^{p^n})$, meaning $D(N)$ must also be zero, since $D(N) = C(N^p,...,N^{p^n}) = 0$. 

It remains to show that if $C \not\equiv 0$, then $D(N) \neq 0$. Suppose that $C \not\equiv 0$. Then in the simplified and expanded form of $f$, there must be some monomial with nonzero coefficient. Suppose first that there is only one nonzero monomial. That is, $f$ is of the form $cx_1^{a_1}x_2^{a_2}...x_n^{a_n} + d$, where $c$ and $d$ are constants, and at least one $a_i$ is nonzero. Then $D(N) = g(N) = cN^{a_1p^1 + a_2p^2 + ... + a_np^n} + d$. This can only be 0 if $cN^{a_1p^1 + a_2p^2 + ... + a_np^n} = d$. But there must be some nonzero $a_i$, and thus the power of $N$ is at least $p$. Thus the left hand side is at least $N^p$. But from our above observations, any constant in $f$ is less than $N^p$. Thus $d < N^p$, meaning we cannot have $cN^{a_1p^1 + a_2p^2 + ... + a_np^n} = d$. Thus $D(N)$ is nonzero as desired.

Suppose now that there are several monomials with nonzero coefficients in the final expanded form of $f$. Then $f$ is of the form $c_1x_1^{a_{11}}x_2^{a_{12}}...x_n^{a_{1n}} + c_2x_1^{a_{21}}x_2^{a_{22}}...x_n^{a_{2n}} + ... + c_kx_1^{a_{k1}}x_2^{a_{k2}}...x_n^{a_{kn}} + d$, where the $c_i$'s and $d$ are all constants, and every term has total degree less than $p$. (i.e. for each $j$, there exists some $i$ such that $a_{ji} \neq 0$, and all $\sum_{i,j} a_{ij} < p$.). Then $D(N) = g(N) = c_1N^{a_{11}p+a_{12}p^2+...+a_{1n}p^n} + ... + c_kN^{a_{k1}p+a_{k2}p^2+...+a_{kn}p^n} + d$. Note that all the terms must have unique degree, since $a_{ij} < p$. Suppose that the $t$'th term in this polynomial has the largest degree (there is such a term since the degrees are unique.) Let $g_{-t}$ denote the polynomial $g$ without the $t$'th term. Then:

\begin{claim}
If the $t$'th term in $g(N)$ has largest degree, then $|c_tN^{a_{t1}p+a_{t2}p^2+...+a_{tn}p^n}| > |g_{-t}(N)|$.
\end{claim}
\begin{proof}
Note that the left hand side is at least $|N^{a_{t1}p+a_{t2}p^2+...+a_{tn}p^n}|$. Since $N=|C|>0$, we ignore the absolute value sign. Thus it suffices to show that $|g_{-t}(N)| < N^{a_{t1}p+a_{t2}p^2+...+a_{tn}p^n}$. Note that $|g_{-t}(N)| = |\sum_{i \neq t} c_iN^{a_{i1}p+...+a_{in}p^n}|$. The largest this sum could ever be is when all the coefficients $c_i$ have the same sign, and are as large as possible. From before, we know these must be less than $N^{p^{1/2}}$. Thus $|g_{-t}(N)| < N^{p^{1/2}} \sum_{i\neq t} N^{a_{i1}p+...+a_{in}p^n}$. If $s$ is the term with the second largest sum of its degrees, then $|g_{-t}(N)| < N^{p^{1/2}} \cdot k \cdot N^{a_{s1}p+...+a_{sn}p^n}$, where $k$ is the number of terms in the expanded polynomial for $f$. Note that since at each node we at most square the number of terms, the number of terms $k$ has $k < N^{2^{|C|}} < N^{p^{1/2}}$. Thus $|g_{-t}(N)| < N^{p^{1/2}} \cdot N^{p^{1/2}} \cdot N^{a_{s1}p+...+a_{sn}p^n} < N^{2p^{1/2} + a_{s1}p+...+a_{sn}p^n}$. But note that since the $t$'th term had degree bigger than the $s$'th term, $a_{t1}p+a_{t2}p^2+...+a_{tn}p^n > a_{s1}p+...+a_{sn}p^n$. But that means that the left hand side was at least $p$ bigger than the right hand side (because every term in the LHS has a factor of $p$, and $p^i > p$). But $p > 2 p^{1/2}$ since $p = (2^N + 1)^2$. Thus $|g_{-t}(N)| < N^{2p^{1/2} + a_{s1}p+...+a_{sn}p^n} < N^{a_{t1}p+a_{t2}p^2+...+a_{tn}p^n}$ as desired. 
\end{proof}

If $g(N) = c_tN^{a_{t1}p+a_{t2}p^2+...+a_{tn}p^n} + g_{-t}(N) = 0$, then $|c_tN^{a_{t1}p+a_{t2}p^2+...+a_{tn}p^n}| = |g_{-t}(N)|$, which contradicts this claim. Thus $D(N) = g(N) \neq 0$ as desired. 

\end{proof}

These two claims taken together imply that given a circuit $C$, we can construct a circuit $D$ and an input $N$ in time polynomial in $|C|$ such that $C$ is identically 0 if and only if $D(N) = 0$. Thus we have provided the polynomial time mapping reduction.

\section{} % 3
\subsection*{3(a)}

We will follow the setup of 3b, and prove every claim along the way. First, assume $\cc{NP} \subseteq \cc{BPP}$.

\begin{claim}
$\Sigma_2^p = \cc{NP}^{\cc{NP}}$
\end{claim}
\begin{proof}
This was proven in class, so we defer to that proof.
\end{proof}

\begin{claim} \label{npnpnpbpp}
$\cc{NP}^\cc{NP} \subseteq \cc{NP}^{\cc{BPP}}$
\end{claim}
\begin{proof}
Let $L \in \cc{NP}^\cc{NP}$. Then there exists some nondeterministic polynomial time TM $M$ that decides $L$ with oracle access to NP. But since $\cc{NP} \subseteq \cc{BPP}$, giving $M$ oracle access to BPP allows it to still makes queries to NP. Thus $L$ can be decided by a nondeterminsitic polynomial time TM with oracle access to BPP. Thus $L \in \cc{NP}^\cc{BPP}$.
\end{proof}

\begin{claim}\label{hmmm}
$\cc{NP}^{\cc{BPP}} \subseteq \cc{BPP}^{\cc{BPP}}$
\end{claim}
\begin{proof}
Let $L \in \cc{NP}^{\cc{BPP}}$. Then $L$ is decided by some NP machine $M$ that has oracle access to BPP. But since $\cc{NP} \subseteq \cc{BPP}$, $M$ is also a BPP machine (with oracle access to BPP), and so $L \in \cc{BPP}^{\cc{BPP}}$
\end{proof}

\label{TODO: } %Maybe these two claims will replace Claim \ref{hmmm}? IDK how to prove the first one, and I think the proof for Claim \ref{hmmm} is convincing enough. 

% \begin{claim}
% $\cc{NP}^\cc{BPP} \subseteq \cc{BPP}^{\cc{NP}}$
% \end{claim}
% \begin{proof}
% \end{proof}

% \begin{claim}
% $\cc{BPP}^\cc{NP} \subseteq \cc{BPP}^{\cc{BPP}}$
% \end{claim}
% \begin{proof}
% Suppose $L \in \cc{BPP}^\cc{NP}$ is decided by some BPP machine $M$ with oracle access to NP. As in Claim \ref{npnpnpbpp}, giving $M$ oracle access to BPP still allows to to make oracle calls to NP, and thus $M$ with oracle access to BPP still decides $L$. Thus $L \in \cc{BPP}^\cc{BPP}$.
% \end{proof}

\begin{claim}
$\cc{BPP}^\cc{BPP} = \cc{BPP}$
\end{claim}
\begin{proof}
First note that $\cc{BPP} \subseteq \cc{BPP}^\cc{BPP}$ as usual, since we can just ignore the oracle. Let $L \in \cc{BPP}^\cc{BPP}$. The $L$ is decided by some BPP $M$ machine with oracle access to BPP. If $M$ makes $k$ oracle calls to BPP, then we can simulate $M$ with a BPP machine (with no oracle) with some increased error (note that oracle calls have NO error!). In particular, our simulating machine $M'$ is correct when it correctly simulates all $k$ error calls (it is also sometimes correct if it messes up some calls in a particular so they cancel out, but we're just looking at the upper bound so we ignore that here). But each simulation is correct with probability at least $2/3$, since $M'$ is a BPP machine. Thus all $k$ are correct with probability $(2/3)^k$. Therefore the probability that $M'$ correctly decides some input is at least $(2/3)^{k+1}$, where $k$ is polynomial in $n$. This can be boosted to $2/3$ using any of our standard error reduction techniques. 
\end{proof}

\begin{claim}
$\cc{BPP} \subseteq \Pi_2^p$
\end{claim}
\begin{proof}
This was proven in class, so we defer to that proof.
\end{proof}

Stringing these all together, we get:

\begin{align}
\Sigma_2^p = \cc{NP}^{\cc{NP}} \subseteq \cc{NP}^{\cc{BPP}} \subseteq \cc{BPP}^{\cc{BPP}} = \cc{BPP} \subseteq \Pi_2^p
\end{align}

Therefore $\Sigma_2^p \subseteq \Pi_2^p$. A similar chain of claims gives us $\Pi_2^p \subseteq \Sigma_2^p$ by noticing that $\Pi_2^p = \cc{coNP}^{\cc{NP}}$ and $\cc{coBPP} = \cc{BPP}$. Therefore $\Sigma_2^p = \Pi_2^p$, and thus the polynomial time hierarchy collapses to $\Sigma_2^p$ by a theorem from class. This means $\cc{PH} = \Sigma_2^p$. But in this chain of claims, we also proved $\Sigma_2^p \subseteq \cc{BPP}$. Therefore $\cc{PH} = \Sigma_2^p \subseteq \cc{BPP}$ as desired.

\subsection*{3(b)}

This purported proof is incorrect because it doesn't justify any of its claims. Any proof must convince the reader of its correctness, and this proof does nothing of the sort. It is just a string of unjustified statements, none of which are obvious. All of the claims made here are true, but none of them are justified properly to constitute a ``proof''.

\section{} % 4

Call a function $f$ on $O(\log n)$ variables ``hard'' if it cannot be decided by circuits of size polynomial in $n$. We begin with a claim:

\begin{claim}\label{construct}
Let $L \in \cc{BPP}$, and let $f$ be a hard function on $O(\log n)$ variables. Then there exists a polynomial time Turing machine $A_f$ that decides $L$ on inputs of size $n$. Furthermore, we can construct $A_f$ in time polynomial in $n$. 
\end{claim}

\begin{proof}
This follows from our discussion of derandomizing BPP from class. We can use $f$ to produce a PRG that $1/6$-fools circuits of size $s$ that is computable in $2^O(\log n) = \text{poly}(n)$ time as desired. Then, as discussed in class, this means we can use this PRG to solve BPP problems in polynomial time. 
\end{proof}

We now give a ZPP machine that decides $L$. The following Turing machine $M$ decides $L$ in expected polynomial time:

\begin{enumerate}[ ]
\item $M$, on input $x$ of length $n$:
\begin{enumerate}[1. ]
\item Repeat the following until we get a hard $f$:
	\begin{enumerate}[a) ]
	\item Randomly pick an $f$ on $O(\log n)$ variables by guessing the truth table.
	\item Check if $f$ can be decided by a circuit of size $k$. If so, then move to Step 2.
	\end{enumerate}

\item Construct $A_f$ as in Claim \ref{construct}.
\item Run $A_f$ on $x$ and output the result.
\end{enumerate}
\end{enumerate}

In order to prove the running time of this TM, we will need the following claim:

\begin{claim}\label{expNum}
Let $f$ on $k$ variables be chosen at random. Then $\prob{f \text{ is hard}} \geq 1/2$.
\end{claim}
\begin{proof}
There are $2^k$ possible inputs for a function on $l$ boolean variables. For each of these, there are 2 possible output values. Thus there are $2^{2^k}$ possible functions on $k$ variables. Consider the number of boolean circuits of size $s$. This is certainly less than the number of directed graphs of size $s$ (since every circuit is a DAG, but some directed graphs have cycles). The number of directed graphs of size $s$ is at most $2^{s^2}$, since there are less than $s^2$ edges, and each one can either appear or not appear. Thus the number of boolean circuits of size $s$ is at most $2^{s^{2}}$. Therefore the number of boolean circuits of size polynomial in $k$ is at most $2^{\text{poly}(k)^2}$. Thus the number of functions on $k$ variables that have polynomial sized circuits is at most $2^{\text{poly}(k)^2}$. Therefore the probability of a randomly drawn function being ``easy'' is $2^{\text{poly}(k)^2} / 2^{2^k} < 1/2$. Thus the probability of a random $f$ being hard is at least $1 - 1/2 = 1/2$ as desired.
\end{proof}

We can now prove that this TM is indeed a ZPP machine:

\begin{claim}
$M$ decides $L$ in expected polynomial time.
\end{claim}
\begin{proof}

The fact that $M$ decides $L$ follows from Claim \ref{construct}, since $A_f$ decides $L$ if $f$ is hard, and $f$ is hard by construction. 

We now consider the running time of $M$. Step 1a takes polynomial time, since there are only $2^{O(\log n)} = n^c$ entries in the truth table (for some constant $c$). Step 1b takes time $2^{O(\log n)} = n^c$ by assumption, since $f$ is on $O(\log n)$ variables. The expected number of times that we repeat Step 1 is polynomial, since by Claim \ref{expNum}, the probability of picking a hard $f$ is at least $1/2$, so the expected number of trials it will to get such an $f$ is at most $2$. Step 2 takes polynomial time, since from Claim \ref{construct}, $A_f$ is constructible in time polynomial in $n$. Step 3 takes polynomial time since $A_f$ is polynomial time (again from Claim \ref{construct}). Since every step is either polynomial time or expected constant time, the entire algorithm runs in expected polynomial time as desired.
\end{proof}

Since $M$ decides $L$ in expected polynomial time, $L \in \cc{ZPP}$ by the alternate definition of ZPP. We just chose $L \in \cc{ZPP}$ arbitrary, so $\cc{BPP} \subseteq \cc{ZPP}$. From class we know $\cc{ZPP} \subseteq{BPP}$, and so we have $\cc{BPP} = \cc{ZPP}$ as desired.

\section{} % 5

The idea for this problem is that we will use the randomized error reduction expander to pick the starting point for the determininistic error reduction expander. Throughout, $f(x)$ is computed using the oracle for $f$. 

We first describe a deterministic subroutine:

\begin{enumerate}[ ]
\item Algorithm $D$, on inputs $f$ and $r$, does the following:
\begin{enumerate}[1.]
\item Construct a $\frac{1}{\epsilon^2}$-regular expander for $f$.
\item Return $\mu_D = \epsilon^2 \sum_{x\in \Gamma(r)} f(x)$. \label{oracle}
\end{enumerate}
\end{enumerate}

Now we describe the entire algorithm:

\begin{enumerate}[ ]
\item Algorithm $A$, on input $f:\{0,1\}^n \rightarrow \{0,1\}$:
\begin{enumerate}[1.]
\item Construct a $d$-regular expander graph $G=(V,E)$ for some constant $d$.
\item Pick $r_0 \in V$ at random.
\item Do a random walk of length $t = \log \frac{1}{\delta}$ to get $r_1,r_2,...,r_t$.
\item Compute $\mu_A = \frac{1}{t} \sum_{i=1}^t D(f;r_i)$ \label{Rsum}
\end{enumerate}
\end{enumerate}

Before we prove the accuracy of this $A$, we will prove it has desirable properties:

\begin{claim}
The algorithm $A$ uses $n + O(\log \frac{1}{\delta})$ random bits.
\end{claim}
\begin{proof}
$D$ uses no randomness. In $A$, we use $n$ random bits to choose our initial $r_0 \in V$ (since each element of $V$ is an element of $\{0,1\}^n$). Then we take $t=\log \frac{1}{\delta}$ steps of a random walk. Each of these steps takes $\log d$ bits to choose the next vertex. Since $d$ is a constant, the total number of random bits required for this walk is $O(\log \frac{1}{\delta})$ as desired.
\end{proof}

\begin{claim}
The algorithm $A$ makes $O(\frac{1}{\epsilon^2}\log \frac{1}{\delta})$ oracle calls.
\end{claim}
\begin{proof}
The only part of our algorithm that makes oracle calls is step \ref{oracle} of $D$. It makes one oracle call per neighbor of $r$, of which there are $\frac{1}{\epsilon^2}$ since the graph is $\frac{1}{\epsilon^2}$-regular. Thus each time $D$ is called, it makes $\frac{1}{\epsilon^2}$ oracle calls. $D$ is called $t = \log \frac{1}{\delta}$ in step \ref{Rsum} of $A$. Thus the total number of orcale calls is $O(\frac{1}{\epsilon^2} \log \frac{1}{\delta})$ as desired.
\end{proof}

\begin{claim}
The algorithm $A$ runs in polynomial time in $n$, $\frac{1}{\epsilon}$, and $\log \frac{1}{\delta}$.
\end{claim}
\begin{proof}
We can construct $d$-regular expanders in polynomial time in these constraints and in $d$. Thus clearly $D$ takes time polynomial in $n$ and $\frac{1}{\epsilon}$. Similarly, $A$ takes time polynomial time to construct the expander, plus $\log \frac{1}{\delta} \cdot T(D)$. But $T(A)$ was polynomial in the desired quantities, and so $A$ is as well.
\end{proof}

We now prove the accuracy of the algorithms $D$ and $A$ in turn.

\begin{claim} \label{d}
For any $\epsilon > 0$, we have the estimator $\mu_D \in (\mu \pm \epsilon)$ with constant nonzero probability $\delta'$.
\end{claim}

\begin{proof}
Recall $\mu_D = \epsilon^2 \sum_{x\in \Gamma(r)} f(x)$. Then $\mu_D$ is the average of $|\Gamma(r)| = \frac{1}{\epsilon^2}$ independent random variables $R_x = f(x)$. Note that $\expct{R_x} = \expct{f(x)} = \mu$. Thus $\expct{\mu_D} = \epsilon^2 \cdot \frac{1}{\epsilon^2} \cdot \mu = \mu$. We are interested in the probability that $\mu_D \in (\mu \pm \epsilon) = (\expct{\mu_D} \pm \epsilon)$. This is exactly $1 - \prob{|\mu_D - \expct{\mu_D}| > \epsilon}$. But $\mu_D$ is the average of independent random 0-1 variables, so we can apply the Hoeffding bound to this probability. Then $\prob{|\mu_D - \expct{\mu_D}| > \epsilon} \leq e^{-2\epsilon^2 N}$, where $N$ is the number of variables in the average. But $N=\frac{1}{\epsilon^2}$, so $e^{-2\epsilon^2 N} = e^{-2}$. Thus the probability that our estimator $\mu_D$ falls in $(\mu \pm \epsilon)$ is $1 - e^{-2} \approx 0.86$.
\end{proof}

\begin{claim}
For any $\epsilon, \delta > 0$, we have the estimator $\mu_A \in (\mu \pm \epsilon)$ with probability $1-\delta$.
\end{claim}

\begin{proof}
This follows directly from our discussion of expander error-reduction in class. Since $D$ is correct with constant (nonzero) probability $\delta'$ from Claim \ref{d}, we can boost the error to $\leq 2^{-k}$ using $O(k)$ random bits as discussed in class. This means we can make the error $\leq \delta$ using $O(\log \frac{1}{\delta})$ random bits, using expanders as we do is this algorithm. Since the probability of error is $\delta$, the probability of correctness is $1 - \delta$. Thus the probability that $A$ is correct is $1-\delta$ as desired.
\end{proof}


\end{document}