\documentclass[11pt]{article}

\include{macros}

\begin{document}

\scribe{Kernelization Lower Bounds}{10/3/2017}{Sam Lemley and Sam Vinitsky}

\draft 

\section{Introduction}

In the previous lecture, we discussed the notion of advice, and proved some results about how helpful it could be in solving problems. In particular, we proved the Karp-Lipton Theorem, which states that if \cc{NP} $\subseteq$ \cc{P/poly}, then the polynomial hierarchy collapses to the second level, which is unlikely. We stated the following theorem, which will be useful later in this lecture. The proof is left as an exercise.

\begin{theorem} \label{coNP}
\cc{coNP} $\subseteq$ \cc{NP/poly} $\implies$ \cc{PH} $= \Sigma_3^\cc{P}$.
\end{theorem}

We also introduced the notion of a selector for a language \cc{L}, which is a function $f(x_0,x_1)$ that, if either $x_0$ or $x_1$ is in $L$, returns that element. This means the only way $f$ returns an element not in \cc{L} is if both inputs are not in \cc{L}. We noted that if \cc{SAT} has a selector, then \cc{P} $=$ \cc{NP}. We also proved the following theorem:

\begin{theorem}\label{selector}
\cc{L} has a selector $\implies$ \cc{L} $\in$ \cc{P/poly}.
\end{theorem}

Recall that the proof idea was to find an element $y$ that was not in \cc{L} such that $f(x,y) = y$ for many values of $x \in \overline{\cc{L}}$ (we say that $y$ dominates $x$). We can then give the machine $y$ as advice. In fact, we can construct polynomially many $y_i$'s such that every $x \in \overline{\cc{L}}$ is dominated by some $y_i$, and then give the whole set $\{y_i\}$ as advice. In order to test whether a given $x$ was in \cc{L}, we merely need to compute $f(x,y_i)$ for each $y_i$. If any of these functions return $x$, then $x \in \cc{L}$, and otherwise $x \in \overline{\cc{L}}$. 

In this lecture, we will use the ideas from the previous lecture in a more general manner in order to prove lower bounds for the kernelization of vertex cover.

\section{Compression of a Language}

We begin our discussion of kernelization by formulating a generalization of selectors. We will first need two definitions:

\begin{definition}[compression to length $l$]
We say that a language \cc{L} can be \text{compressed} to length $l$ if there exists some polynomial time mapping reduction $f$ from \cc{L} to \cc{L'} such that $|f(x)| \leq l(x) \leq |x|$.
\end{definition}

\begin{definition}[\cc{OR(L)}]
Given some language \cc{L}, then $\cc{OR(L)} = \{ \langle x_1,...,x_t \rangle \ | \bigvee\limits_{i=1}^t (x_i \in \cc{L}) \}$. 
\end{definition}

In other words, \cc{OR(L)} consists of all tuples (of any size) such that \textit{some} member of the tuple is in \cc{L}. We will use $s$ to denote the length of each tuple element $x_i$, and we use $n$ to denote the length of the entire tuple. That is, $s = |x_i|$, and $n = | \langle x_1,...,x_t \rangle |$. Note that $n = st$.

By applying compression to this language \cc{OR(L)} obtained from \cc{L}, we get a generalization of a selector for \cc{L}. This is formalized in the following claim.

\begin{claim}
If \cc{L} has a selector, then \cc{OR(L)} can be compressed to length $s$. 
\end{claim}

\begin{proof}
Suppose \cc{L} has a selector $f$. We can use $f$ to compress some instance $X = \langle x_1, ..., x_n \rangle$ of \cc{OR(L)} to length $s$. First, note that there must be some element $x^*$ in $X$ that dominates all the others. \label{TODO:why is this true} We can easily compute $x^*$ by computing $f(x_i, x_j)$ for each pair of $x_i$ and $x_j$, and selecting the element that was never dominated. For any element $x_j$ in $X$, we know that if $x^* \notin \cc{L}$ then $x_j \notin \cc{L}$, since $f(x^*, x_j) = x^*$. This means that if $x^* \notin \cc{L}$, then \textit{none} of the elements of $X$ are in \cc{L}, and thus $\langle x_1, ..., x_n \rangle \notin \cc{OR(L)}$. Conversely, if $x^* \in \cc{L}$, then clearly $\langle x_1, ..., x_n \rangle \in \cc{OR(L)}$, because at least one element of this tuple (namely $x^*$) is in \cc{L}. This means that $x^* \in \cc{L}$ if and only if $X \in \cc{OR(L)}$. Thus $g(X) = x^*$ is indeed a mapping reduction from \cc{OR(L)} to \cc{L}.

Note that $x^*$ only takes polynomial time to compute, since we are doing a polynomial number of comparisons $f(x_i,x_j)$, and $f$ is polynomial time computable. Thus $g$ is a polynomial time mapping reduction from \cc{OR(L)} to \cc{L} such that $|g(X)| = |x^*| = s$. This means \cc{OR(L)} can be compressed to length $s$.
\end{proof}

We will now prove a theorem about compression that will be instrumental in our later proof of the kernelization lower bound for vertex cover.

\begin{theorem}\label{compression}
If \cc{OR(L)} can be compressed to length $l(s,t) = O(t)$ for $t \geq s^c$ (for some constant $c$), then $\cc{\overline{L}} \in \cc{NP/poly}$.
\end{theorem}

Note: Since we require that $l(s,t) = O(t)$, this means the amortized length per instance $x_i$ is \textit{constant}. 

% TODO: I don't get this next commented out line, but he said it...
% Without the restriction that $t \geq s^c$, we would be able to compress any number of instances into a single instance of constant length. However, we can only do this for trivial languages.

% **************************************************************************************************
% PROOF TODO's: 
%  	 - Somewhere we switched from \overline{L'} to \overline{L}, and I can't figure out why
%    - something feels wrong with f
% **************************************************************************************************


\begin{proof}[of Theorem \ref{compression}]
The idea of this proof is simple, and is similar to the proof of Theorem \ref{selector}. By assumption, we have a polynomial time mapping reduction $f$ from \cc{OR(L)} to some language \cc{L'}. Note that if a tuple $\langle x_1, ..., x_n \rangle$ gets mapped to $y \in \overline{\cc{L'}}$, then every element in the tuple must be in $\overline{\cc{L'}}$. \label{TODO:elaborate} We say that such a $y$ vouches for the non-membership of $x_i$ in \cc{L'}. We wish to find some $y$ that vouches for as many elements as possible, and give this as advice (as in the proof of Theorem \ref{selector}). In order to do so, let us define a set of all the elements for which $y$ can vouch. Let $\mathcal{V}(y)$ denote the set of all elements that are in some tuple that maps to $y$ under $f$. This is, 
$$\mathcal{V}(y) = \{x\ |\ (\exists j \in [t])(\exists x_1,...,x_{j-1},x_{j+1},...,x_t) \text{  s.t.  } f(\langle x_1,...,x_{j-1},x,x_{j+1},...,x_t\rangle) = y\}$$

We can determine membership in $\mathcal{V}(y)$ in nondeterministic polynomial time: guess some assignment to $x_1,...,x_{j-1},x_{j+1},...,x_t$, and verify that $f(\langle x_1,...,x_{j-1},x,x_{j+1},...,x_t\rangle) = y$ ($f$ is polynomial time computable by assumption).

Since we are passing $y$ as advice, it only needs to vouch for elements of some fixed length, $n$. We want to find some $y$ that vouches for a majority of the elements $x$ of length $n$. Let $X_0$ denote all $n$-bit strings that are not in $L$. Let $\mathcal{V}_0(y)$ denote the number of tuples of length $n$ that map to $y$. Formally, 
\begin{align*}
X_0 &= \{0,1\}^n \cap \overline{L} \\
\mathcal{V}_0(y) &= \{0,1\}^n \cap \mathcal{V}(y)
\end{align*}

We wish to find some $y_0^*$ such that the size of $\mathcal{V}_0(y_0^*)$ is some constant fraction of the size of $X_0$. Then repeated application of this method will produce a sequence of $y_0^*, y_1^*, ... , y_k^*$ such that every element in $X_0$ is vouched for by some $y_i^*$. The following claim proves that such a $y_0^*$ exists:
(Note that $X_0^t$ denotes the set of tuples of length $t$ whose elements are in $X_0$).


\begin{claim}
There exists some $y_0^*$ such that the number of tuples from $X_0^t$ that get mapped to $y_0^*$ under $f$ is $\geq \dfrac{|X_0^t|}{2^{at}}$ for some constant $a$.
\end{claim}

\begin{proof}
Let us compute the average number of tuples from $X_0^t$ that map to a given $y$ under $f$. This is equal to the number of tuples in $X_0^t$ divided by the number of possible $y$'s we can map to. The length of the output $y$ is bounded by $l(s,t)$ by definition of our mapping. \label{TODO:someting about s=n} But $l(s,t) = O(t) \leq at$ for some constant $a > 0$. Thus $|y| \leq at$, and so the number of possible $y$ is the number of binary strings of length $at$, which is $2^{at}$. This means that the average number of tuples that map to a given $y$ is $\geq \dfrac{|X_0^t|}{2^{at}}$. Clearly there must exist some $y_0^*$ such that the number of strings that map to $y_0^*$ is greater than or equal to the average. 
\end{proof}

We have shown that some constant fraction of the tuples $X_0^t$ get mapped to $y_0^*$, but we need to show that some constant fraction of $X_0$ is in some tuple that gets mapped to $y_0^*$. We furnish a new definition:
$$\tilde{\mathcal{V}}_0(y) = \{x \in X_0\ |\ (\exists j \in [t])(\exists x_1,...,x_{j-1},x_{j+1},...,x_t \in X_0) \text{  s.t.  } f(\langle x_1,...,x_{j-1},x,x_{j+1},...,x_t\rangle) = y\}$$
Note that $\tilde{\mathcal{V}}_0(y) \subseteq \mathcal{V}_0(y)$, since we are just restricting the elements $x$ and $x_j$ to be in $X_0$. \label{TODO: explain} Then $|\tilde{\mathcal{V}}_0(y)| \leq |\mathcal{V}_0(y)|$

If we have a tuple from $X_0^t$ with some component $x \in \tilde{\mathcal{V}}_0(y_0^*)$, then every component of the tuple must also be in $\tilde{\mathcal{V}}_0(y_0^*)$. \label{TODO: maybe explain} This means that:

$$ \frac{|X_0^t|}{2^{at}} \leq \text{the number of tuples from $X_0$ that map to $y_0^*$} \leq |\tilde{\mathcal{V}}_0(y_0^*)|^t$$  \label{TODO: really this makes no sense}

And thus by taking the $t^\text{th}$ root, we get $\dfrac{|X_0^t|}{2^{at}} \leq |\tilde{\mathcal{V}}_0(y_0^*)| \leq |\mathcal{V}_0(y_0^*)|$. This means that $y_0^*$ vouches for some constant fraction of $X_0$, as desired.

We can repeat this process with the remaining, unvouched for members of $X_0$. That is, let $X_1 = X_0 - \mathcal{V}_0(y_0^*)$. Then $|X_1| = |X_0| - |\mathcal{V}_0(y_0^*)| \leq (1-1\frac{1}{2^a}) \cdot |X_0|$. We can then produce an element $y_1^*$ that vouches for a constant fraction of $X_1$. Repeating this $k$ times gives us the inequality $X_k \leq (1-\frac{1}{2^a})^k \cdot |X_0|$, and a sequence of $y_1^*, ..., y_k^*$ that together vouch for everything not in $X_0 - X_k$. But as $k$ gets large (say, polynomially in $n$), then $(1-\frac{1}{2^a})^k$ approached $0$, and thus $X_k \leq 0 \cdot |X_0| = 0$. This means that if we take polynomially many repetitions, we can find some set of polynomially many $y_i^*$ that together vouch for all of $X_0$.

We use this sequence of polynomially many $y_i^*$'s as advice for our nondeterministic machine. In order to test whether $x \in \overline{L}$, just nondeterministically guess a $y_i^*$ and a tuple $\langle x_1, ..., x_{j-1}, x, x_{j+1},...x_t \rangle$ containing $x$, and then test whether $f(\langle x_1, ..., x_{j-1}, x, x_{j+1},...x_t \rangle) = y_i^*$. If they are equal, then we accept $x$. This is clearly a nondeterministic polynomial machine that decides $\overline{L}$ with polynomial advice. Thus $\overline{L} \in \cc{NP/poly}$.
\end{proof}

%TODO: maybe move this above the proof for the above thm
Note that Theorem \ref{compression} immediately implies the following corollary:

\begin{corollary}\label{satPH}
If \cc{OR(3SAT)} can be compressed to length $l(s,t) = O(t)$ for $t \geq s^c$, then $\cc{PH} = \Sigma_3^\cc{P}$.
\end{corollary}

\begin{proof}
Suppose \cc{OR(3SAT)} can be compressed to length $l(s,t) = O(t)$ for $t \geq s^c$. Then by Theorem \ref{compression}, $\cc{\overline{3SAT}} \in \cc{NP/poly}$, and thus $\cc{coNP} \subseteq \cc{NP/poly}$. But then Theorem \ref{coNP} tells us that $\cc{PH} = \Sigma_3^\cc{P}$.
\end{proof}

% TODO: Maybe somehting about compressor showing NP/poly and selector showing P/poly, showing that having a selector is stronger than having a compressor

\section{Kernelization Lower Bounds for Vertex Cover}

%TODO: parameterized problem definition??

In this section, we consider an approach to dealing with the difficulty of \cc{NP}-hard problems called \textit{kernelization}. The general idea is that we will take some instance $x$ of a particular \cc{NP}-hard language \cc{L}, and reduce it to another instance $f(x) \in \cc{L}$ of ``small enough'' size $k$. We can then solve the reduced instance $f(x)$ in time $h(k)$ (for example, by brute force). If $f$ was polynomial time computable, then this gives us an algorithm $\mathcal{A}$ for $x$ that runs in time $O(h(k) \cdot n^c)$ for some constant $c$. Such an algorithm is called a \textit{fixed-parameter tractable} algorithm for \cc{L}. We formalize this definition below:

\begin{definition}[fixed-parameter tractable]
An algorithm $\mathcal{A}$ is fixed-parameter tractable (FPT) if there exists some $k$ such that $\mathcal{A}$ runs in time $O(h(k) \cdot n^c)$ for some constant $c$ and some function $h:\NN \rightarrow \NN$.
\end{definition}

In practice these parameters only take on small values, and thus the function $h$ is allowed to be arbitrarily large. Even if $h(k)$ is a very large value, it only contributes a multiplicative factor to our running time. The important observation is that $k$ does not appear in the exponent of $n$.

\subsection{A Fixed-Parameter Tractable Algorithm for Vertex Cover}

One natural problem that admits a fixed-parameter tractable algorithm is Vertex Cover (\cc{VC}). Recall that \cc{VC} is the problem of determining, for a given graph $G = (V,E)$ whether there exists a set $S \subseteq E$ of edges such that each vertex in $V$ is adjacent to some member of $S$. A natural way to parametrize this problem is to let $k$ be the size of the vertex cover. We will consider the size of our input to be the number of vertices $n$. 

A trivial algorithm for determining if there exists a vertex cover of size $k$ is to examine every possible subset of size $k$. Unfortunately, there are $O(n^k)$ such subsets, and thus the running time of this algorithm is not $O(h(k) \cdot n^c)$, since $k$ is not a constant. Then this algorithm is not fixed-parameter.

However, there do exists fixed-parameter tractable algorithms for \cc{VC}. One method of constructing such an algorithm is through the use of the \textit{kernelization} method described in the introduction to this section. The idea is that we wish to map instances of some language \cc{L} to ``small enough'' instances of that same language. We formalize the notation here:

%TODO: maybe write f: L < L
\begin{definition}
[Kernelization]
We say that $f$ is a kernelization of a language \cc{L} if $f$ is a polynomial time mapping reduction from \cc{L} to itself such that $|f(x)| \leq g(k)$ for some $g:\NN \rightarrow \NN$.
\end{definition}

Note that the size of the reduced instance is bounded by some function of the parameter $k$. This formalizes our desired notion of a ``small enough'' instance. We will now prove the intuition notion that a kernelization admits an FPT algorithm.

\begin{claim}
If \cc{L} has a kernelization $f$, then \cc{L} also has a fixed-parameter tractable algorithm $\mathcal{A}$.
\end{claim}

\begin{proof}
We explicitly describe the algorithm $\mathcal{A}$: 

\begin{enumerate}

\item On input $x$, compute the kernelization $f(x)$.

\item Use any algorithm to determine whether $f(x) \in \cc{L}$. Output whatever this algorithm outputs.

\end{enumerate}

Our algorithm declares that $x$ is in $\cc{L}$ if and only if $f(x) \in \cc{L}$. But since $f$ is a mapping reduction, $x \in \cc{L} \iff f(x) \in \cc{L}$, and thus our algorithm always correctly decides $x$. Step 1 takes polynomial time, since $f$ was a polynomial mapping reduction. Step 2 takes time $h(|f(x)|)$, for some function $h: \NN \rightarrow \NN$. But $|f(x)| \leq g(k)$ for some function $g: \NN \rightarrow \NN$ since $f$ is a kernelization, and so $h(|f(x)|) \leq h(g(k)) = p(k)$, where $p = h \circ g$. So step 2 takes time less than $p(k)$ for some $p: \NN \rightarrow \NN$. Thus the entire algorithm takes time $O(p(k) + n^c)$, which is clearly $O(p(k) \cdot n^c)$. Therefore this algorithm is fixed-parameter tractable.
\end{proof}

%TODO: "there are various reasons why we might want to make $g(k)$ [the size of th kernel] as small as possible..."

We have shown above that if a problem has a kernelization of size $g(k)$, it also has a fixed-parameter tractable algorithm. In order to prove the \cc{VC} has a fixed-parameter algorithm, we will explicitly describe a kernelization.

\begin{theorem}\label{ker}
\cc{VC} has a kernelization with $O(k^2)$ edges.
\end{theorem}

\begin{proof}

We will explicitly describe the kernelization. If any vertex $v$ in $G$ has more than $k$ neighbors, it must be in our vertex cover. (Otherwise we would need to pick all of its neighbors, of which there are more than $k$, resulting in a vertex cover of size greater than $k$.) If there were more than $k$ such vertices, then there can be no vertex cover of size $k$. We can thus assume our graph had $l \leq k$ vertices of degree more than $k$. We have removed all $l$ of these vertices (and their incident edges), resulting in a graph $G'$ on $k-l$ vertices. What remains is the find a vertex cover of size $k-l$ in this graph $G'$. 

Every vertex in $G'$ has degree at most $k$, because otherwise it would have been removed from $G$ at the beginning. This means that each of the $k-l$ vertices in $G'$ can cover at most $k$ edges. This means that any vertex cover of $G'$ will cover at most $k (k-l)$ edges. If $G'$ has more than this number of edges, we know that no vertex cover exists. Otherwise, $G'$ has no more than $k(k-l) \leq k^2$ edges that we need to cover with $k-l$ vertices. This is a problem in \cc{VC} with $O(k^2)$ edges. Thus this entire algorithm is a kernelization that reduces a problem with $n$ vertices to a problem on $O(k^2)$ edges, and thus $2k^2 = O(k^2)$ vertices.
\end{proof}

The best known kernelization algorithm for \cc{VC} produces a kernel with $O(k)$ vertices and $O(k^2)$ edges. The natural question is whether this is the best we can do. That is, does there exists some kernelization with fewer vertices or edges? In the next section, we will use all the tools at our disposal to prove that no kernelization of \cc{VC} can have fewer than $O(k^2)$ edges unless the polynomial time hierarchy collapses to the third level.

\subsection{A Lower Bound for Kernelizations of \cc{VC}}

We will prove the following lower bound for the number of edges in the kernelization of \cc{VC}:

\begin{theorem}\label{eps}
If VC has a kernel with $k^{2-\epsilon}$ edges (for some $\epsilon > 0$), then $\cc{coNP} \subseteq \cc{NP/poly}$.
\end{theorem}

This theorem taken with Theorem \ref{coNP} immediately imply the following corollary:

\begin{corollary}
\cc{VC} has no kernels with $k^{2-\epsilon}$ edges for any $\epsilon>0$ unless $\cc{PH = \Sigma_3^\cc{P}}$.
\end{corollary}

This means that unless the polynomial hierarchy collapses (an unlikely assumption), \cc{VC} has no kernelization with fewer than $k^2$, and thus our kernelization in the proof of Theorem \ref{ker} is tight (in the number of edges). 

\begin{flushleft}\textit{Proof idea (for Theorem \ref{eps}).}
We will assume that \cc{VC} has a kernel with $k^{2-\epsilon}$ edges, and use this fact to prove that $\cc{OR(3SAT)}$ has a compression of short length. We can then use Theorem \ref{compression} to show that $\cc{OR(\overline{3SAT})} \in \cc{NP/poly}$, which in turn proves that $\cc{coNP} \subseteq \cc{NP/poly}$, and thus that the polynomial time hierarchy collapses to the third level (by Theorem \ref{coNP}).
\end{flushleft}

\begin{proof}[of Theorem \ref{eps}]

Suppose that \cc{VC} has a kernel with $k^{2-\epsilon}$ edges for some $\epsilon >0$. We will use this kernel to establish a compression algorithm for 3SAT. Rather than dealing with 3SAT directly, we will instead consider the conceptually simpler problem of compressing a instance of Clique. (Recall that Clique consists of pairs $(G,k)$ such that the graph $G$ has a clique of size $k$).

We make use of the following fact about the relationship between VC and Clique. Let us denote by $\overline{E}$ the set of edges that are not in $E$.

\begin{claim}
If $S$ is a set of edges in a graph $G = (V,E)$, then $S$ is a vertex cover for $G$ if and only if $\overline{S}$ is a clique in $G' = (V,\overline{E})$.
\end{claim}

% \begin{proof}
% NOWTODO: maybe we need to prove this?
% \end{proof}

We can use this claim to show that Clique has a compression into a graph with $n^2$ edges, where $n$ is the number of vertices. Suppose we have instance $(G',k')$ of Clique. We can transform this into an instance of $(G',n-k')$ of VC. \label{TODO: why is it n-k' and not m-k'} By assumption, this instance of VC has a kernelization of size $(n-k')^{2-\epsilon} \leq n^{2-\epsilon}$. This process exactly describes a compression of Clique into an instance of VC of length $n^{2-\epsilon}$. This means Clique can be compressed to length $n^{2-\epsilon}$.

We will now exploit this compression of Clique to develop a compression for 3SAT. Keep in mind that the compression for \cc{OR(3SAT)} that satisfies the length requirements from Theorem \ref{compression}. We can represent an instance of $\cc{OR(3SAT)}$, as a tuple $\langle \varphi_1,\varphi_2,...,\varphi_t \rangle$ of $t$ 3CNF formulas. Note that since $|\varphi_i| = s$, the number of variables in $\varphi_i$ and the number of clauses in $\varphi_i$ are both at most $s$. W.l.o.g. we assume that there are exactly $s$ clauses (if not, just add duplicates). 

Using the standard reduction from 3SAT to Clique (see Sipser p.284 [\ref{Sipser}]), we can transform each 3SAT instance $\varphi_i$ into an equivalent Clique instances $(G_i,s)$. By applying this reduction to each $\varphi_i$ in the tuple $\langle \varphi_1,\varphi_2,...,\varphi_t \rangle$, we can reduce this tuple to a new tuple $\langle (G_0,s), (G_1,s), ... (G_t,s) \rangle$ that is an instance of \cc{OR(Clique)}. This gives us a polynomial time reduction from $\cc{OR(3SAT)}$ to $\cc{OR(Clique)}$. 

Now we would like to reduce this tuple of Clique instances to a single  Clique instance $(G,s)$ with few vertices. If we can do so, then we know 3SAT also has a compression of a small length by our reduction above. This will allow us to finish our proof by applying Theorem \ref{compression}.

\begin{flushleft}\textbf{Method 1: Disjoint Union}\end{flushleft}
A first attempt to produce a single instance $(G,s)$ of Clique from the tuple of instances $(G_i,s)$ is by taking the disjoint union of the vertices of all of the graphs $G_i$. In establishing the disjoint union, we note that each $G_i$ has $3s$ vertices and we note that there are $t$ graphs. Thus, the disjoint union has $3st$ vertices. By applying our compression, we get a compression length of $n^{2-\epsilon} = O((st)^{2-\epsilon})$. 

This is not quite good enough. To apply Theorem \ref{compression}, the compression length must be $O(t)$ for a sufficiently large $t$ (polynomial in $s$). Even if we set $\epsilon= 1$ in $O((st)^{2-\epsilon})$ we will not be able to reduce this to $O(t)$ as $s$ will always be a polynomial fraction. Since we cannot use disjoint union to reduce the compression length to $O(t)$, we will need to identify a different way to pack these $t$ Clique instances $G_t$ into one Clique instance. \label{TODO: I never quite got this}

\begin{flushleft}\textbf{Method 2: Packing Construction}\end{flushleft}

% TODO: IDK i think this might be more confusing than helpful
% Each graph $G_i$ we can view as being embedded in an independent set of size 3 tensor with some clique of size s.
% \[G_i \subseteq I_3 \otimes K_s^{(i)}\]

We will introduce the following lemma which is key to our construction. We will prove this lemma in the next lecture

\begin{lemma}[Packing Lemma]
There exists a graph $P_{s,t}$ with $s\cdot\sqrt[]{t}^{1+o(1)}$ vertices such that:
\begin{enumerate}
\item The edges of $P_{s,t}$ partition into $t$ cliques $K_s^{(i)}$ of size $s$
\item $P_{s,t}$ does not contain any cliques of size $s$ other than the ones than the ones we already have (i.e. the $K_s^{(i)}$)
\end{enumerate}
\end{lemma}

The first condition of the packing lemma suggests that if we look at the edges of graph $P_{s,t}$, they partition into $t$ cliques, and there are no cliques other than those in the graph. Our disjoint union construction satisfies the first portion of the packing lemma. As noted above, the problem with the disjoint union construction is that it uses too many vertices. To avoid this issue in the packing construction, we will make our Clique graphs share vertices. Note that our graphs will not share edges - the edges must remain disjoint. Then we will embed the instance that we have into this graph. This ensures that if at least one of the graphs $G_i$ has a Clique of at least size $s$, then there will be a Clique of size at least $s$ in our packing construction.

The second condition ensures that the other direction holds. Namely, if our construction has a clique of size $s$, then that will mean that one of the given graphs has a clique of size $s$. The reason for this is that if we have a clique of size $s$, it has to come from one the established cliques in $K_s^{(i)}$. If we let these cliques share vertices outside of the cliques in $K_s^{(i)}$, then we would no longer be performing a mapping reduction from $\cc{OR(Clique)}$ to Clique.

Supposing we can compute $P_{s,t}$ in polynomial time, we can use $P_{s,t}$ as a compressed version OR(Clique) instances. This compressed instance $P_{s,t}$ has $s \cdot \sqrt{t}^{1+o(1)}$ vertices, resulting in a compression length $n^{2-\epsilon} = O(s^{2-\epsilon} \cdot t^{1-(\epsilon / 2) + o(1)})$. Note that this running time is dominated by $t$ if it is a large enough polynomial in $s$. That is, there exists some constant $c$ such that it $t>s^c$ then $s^{2-\epsilon} \cdot t^{1-(\epsilon / 2) + o(1)}=O(t)$. The proof of this is left as an exercise to the reader.

Since our compression of OR(Clique) has size is linear in $t$, we also have a 
compression of OR(SAT) whose size is linear in $t$. This means we can apply Theorem \ref{compression} to show that $\overline{3SAT} \in \cc{NP/poly}$, and thus $\cc{coNP}$ is in $\cc{NP/poly}$. This completes our proof.
\end{proof}

\section{Next Time}

In the next lecture, we will complete the proof of our kernelization lower bound for vertex cover by proving the packing lemma. In order to do so, we will use the well-known fact that there exist high density subsets of $\ZZ$ with no arithmetic progression of length 3. This is a generalization of Szemer\'edi's theorem, which states that for every positive integer $k$ and real $\delta \in [0,1)$, there exists a positive integer $n$ such that every subset of $\{1,2,...,n\}$ of size greater then $\delta n$ contains an arithmetic progression of length $k$. %TODO: Something about this is wrong, but I don't know what. I think this is good enough for the draft, but should be fixed after next lecture.

\bibliographystyle{plain}

\begin{thebibliography}{9}
\bibitem{fpt} 
J.~Flum and M.~Grohe.
\textit{Parameterized Complexity Theory}, 2006.
\bibitem{fpt2}\label{Sipser}
M. Sipser.
\textit{Introduction to the Theory of Computation} (2nd ed.) 2006.
\end{thebibliography}

\end{document}