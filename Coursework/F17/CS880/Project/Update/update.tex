\documentclass[11pt]{article}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Header borrowed from Shuchi Chawla %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{fullpage}
\usepackage{epsfig}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amstext}
\usepackage{amsmath}
\usepackage{xspace}
\usepackage{theorem}
\usepackage{hyperref}

% This is the stuff for normal spacing
\makeatletter
 \setlength{\textwidth}{6.5in}
 \setlength{\oddsidemargin}{0in}
 \setlength{\evensidemargin}{0in}
 \setlength{\topmargin}{0.25in}
 \setlength{\textheight}{8.25in}
 \setlength{\headheight}{0pt}
 \setlength{\headsep}{0pt}
 \setlength{\marginparwidth}{59pt}

 \setlength{\parindent}{0pt}
 \setlength{\parskip}{5pt plus 1pt}
 \setlength{\theorempreskipamount}{5pt plus 1pt}
 \setlength{\theorempostskipamount}{0pt}
 \setlength{\abovedisplayskip}{8pt plus 3pt minus 6pt}

 \renewcommand{\section}{\@startsection{section}{1}{0mm}%
                                   {2ex plus -1ex minus -.2ex}%
                                   {1.3ex plus .2ex}%
                                   {\normalfont\Large\bfseries}}%
 \renewcommand{\subsection}{\@startsection{subsection}{2}{0mm}%
                                     {1ex plus -1ex minus -.2ex}%
                                     {1ex plus .2ex}%
                                     {\normalfont\large\bfseries}}%
 \renewcommand{\subsubsection}{\@startsection{subsubsection}{3}{0mm}%
                                     {1ex plus -1ex minus -.2ex}%
                                     {1ex plus .2ex}%
                                     {\normalfont\normalsize\bfseries}}
 \renewcommand\paragraph{\@startsection{paragraph}{4}{0mm}%
                                    {1ex \@plus1ex \@minus.2ex}%
                                    {-1em}%
                                    {\normalfont\normalsize\bfseries}}
 \renewcommand\subparagraph{\@startsection{subparagraph}{5}{\parindent}%
                                       {2.0ex \@plus1ex \@minus .2ex}%
                                       {-1em}%
                                      {\normalfont\normalsize\bfseries}}
\makeatother

\newenvironment{proof}{{\bf Proof:  }}{\hfill\rule{2mm}{2mm}}
\newenvironment{proofof}[1]{{\bf Proof of #1:  }}{\hfill\rule{2mm}{2mm}}
\newenvironment{proofofnobox}[1]{{\bf#1:  }}{}
\newenvironment{example}{{\bf Example:  }}{\hfill\rule{2mm}{2mm}}

\renewcommand{\theequation}{\thesection.\arabic{equation}}
\renewcommand{\thefigure}{\thesection.\arabic{figure}}

\newtheorem{fact}{Fact}[section]
\newtheorem{lemma}[fact]{Lemma}
\newtheorem{theorem}[fact]{Theorem}
\newtheorem{definition}[fact]{Definition}
\newtheorem{corollary}[fact]{Corollary}
\newtheorem{proposition}[fact]{Proposition}
\newtheorem{claim}[fact]{Claim}
\newtheorem{exercise}[fact]{Exercise}

% math notation
\newcommand{\R}{\ensuremath{\mathbb R}}
\newcommand{\Z}{\ensuremath{\mathbb Z}}
\newcommand{\N}{\ensuremath{\mathbb N}}
\newcommand{\F}{\ensuremath{\mathcal F}}
\newcommand{\SymGrp}{\ensuremath{\mathfrak S}}

\newcommand{\CCP}{\ensuremath{\sf P}}
\newcommand{\CCNP}{\ensuremath{\sf NP}}
\newcommand{\C}{\ensuremath{\mathcal C}}

\newcommand{\size}[1]{\ensuremath{\left|#1\right|}}
\newcommand{\ceil}[1]{\ensuremath{\left\lceil#1\right\rceil}}
\newcommand{\floor}[1]{\ensuremath{\left\lfloor#1\right\rfloor}}
\newcommand{\poly}{\operatorname{poly}}
\newcommand{\polylog}{\operatorname{polylog}}

% some abbreviations
\newcommand{\e}{\epsilon}
\newcommand{\half}{\ensuremath{\frac{1}{2}}}
\newcommand{\junk}[1]{}
\newcommand{\sse}{\subseteq}
\newcommand{\union}{\cup}
\newcommand{\meet}{\wedge}

\newcommand{\prob}[1]{\ensuremath{\text{{\bf Pr}$\left[#1\right]$}}}
\newcommand{\expct}[1]{\ensuremath{\text{{\bf E}$\left[#1\right]$}}}
\newcommand{\var}[1]{\ensuremath{\text{{\bf Var}$\left[#1\right]$}}}
\newcommand{\Event}{{\mathcal E}}

\newcommand{\mnote}[1]{\normalmarginpar \marginpar{\tiny #1}}

\newcommand{\headings}[4]{
\noindent
\fbox{\parbox{\textwidth}{
{\bf CS880: Algorithms for Massive Datasets} \hfill {{\bf} #2}\\
\vspace{0.02in}\\
{{\bf Project Proposal}} \hfill {{\bf Date:} #3}
}}
\vspace{0.2in}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Document begins here %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\newcommand{\lecnum}{1}
\headings{\lecnum}{Sam Vinitsky}{11/3/2017}

\section*{Project Update: Lower Bounds for Graph Streaming}

For my project, I am looking at space lower bounds for graph streaming algorithms. In particular, I am looking at the proof techniques used to prove these lower bounds. One common (an unexpected) proof technique I am encountering has to do with communication cost (i.e. the number of bits required to solve some communication game). Consider a one-round communication game where Alice and Bob both have edge sets $E_A$ and $E_B$ on some common set of vertices $V$. Bob wants to compute some property $P$ of the graph $G' = \langle V, E_A \cup E_B \rangle$. Alice sends some message $m(E_A)$ to Bob. Notice that there is a streaming algorithm to compute the property $P$ that uses $f(n)$ bits of storage space, then Alice can simply run that algorithm to get some data structure that takes $f(n)$, and then send that to Bob. This means that if there is a streaming algorithm that uses $f(n)$ space, then the communication cost of this game is $f(n)$. To prove a lower bound for the streaming algorithm, we consider the contrapositive. Namely, if there is \textit{no} message of length $f(n)$ that Alice can send to Bob that allows him to compute $P$, then there is no streaming algorithm that uses only $f(n)$ space. So if we can compute a lower bound for the communication cost of computing the property $P$, then we have a lower bound for the streaming algorithm for $P$. 

In order to actually prove lower bounds for the communication cost of a graph problems, we use a reduction from a communication problem of known cost. Consider the problem ``Perm'', in which Alice has some permutation $\sigma$ on $n$ elements, and Bob wishes to compute the $i$th bit of $\sigma(1)\sigma(2)\sigma(3)...\sigma(n)$. This problem has known communication cost $\Omega (n \log n)$. In order to prove that a graph problem $P$ has communication cost $\Omega(n \log n$, we show that if we have a protocol with communication cost $f(n)$, then we also have a protocol for Perm of cost $f(n)$. The lower bound for Perm means $P$ also has a lower bound of $\Omega (n \log n)$.

The way I described this technique, we were only considering deterministic streaming algorithms. But we can also prove lower bounds for a \textit{randomized} communication game between Alice and Bob, and consider the randomized communication cost of a graph problem. This allows us to prove lower bounds for randomized streaming algorithms as well! As far as I can tell, there is no known way to extend this technique to prove lower bounds for approximate streaming algorithms (this is something I am actively trying to find a reference for). 

When I started this project, I had no experience with communication games or information theory. The proof that the cost of Perm has a lower bound of $\Omega (n \log n )$, and some of the reductions, require a good deal of information theory, and so most of my time has been spent learning that basics of that. Now that I have a good handle on these proof techniques, I will begin exploring to see if there are other proof techniques used for these lower bounds. In particular, I am very interested if there are lower bounds known for approximate streaming of graphs. Additionally, all of the work I have done so far has only considered insertion streams. I am interested to see what kind of proof techniques are used for other models.

An updated list of references is given below. References 5 through 7 are new, and relate to information theory, and the general communication complexity lower bounds.

\begin{thebibliography}{}

\bibitem{sun}
X.~Sun and D.~Woodruff.
\newblock {Tight Bounds for Graph Problems in Insertion Streams}, 2015.

\bibitem{peng}
Z.~Huang and P.~Peng.
\newblock {Dynamic Graph Stream Algorithms in $o(n)$ Space}, 2016.

\bibitem{feg}
J.~Feigenbaum, S.~Kannan, A.~McGregor, S.~Suri, and J.~Zhang.
\newblock {On Graph Problems in a Semi-Streaming Model}, 2005.

\bibitem{feg2}
J.~Feigenbaum, S.~Kannan, A.~McGregor, S.~Suri, and J.~Zhang.
\newblock{Graph Distances in the Data-Stream Model}, 2008.

\bibitem{amit}
Amit Chakrabarti, Yaoyun Shi, Anthony Wirth, and Andrew Chi-Chih Yao. 
\newblock{Informational complexity and the direct sum problem for simultaneous message complexity}, 2001.

\bibitem{ilan}
Ilan Kremer, Noam Nisan, and Dana Ron. 
\newblock{On randomized one-round communication complexity}, 1995.

\bibitem{ziv}
Ziv Bar-Yossef, T. S. Jayram, Ravi Kumar, and D. Sivakumar. 
\newblock{An information statistics approach to data stream and communication complexity}, 2004.

\end{thebibliography}

\end{document}
