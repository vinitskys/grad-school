\documentclass[11pt]{beamer}

\usefonttheme{serif}
\usetheme{Madrid}

% \include{macros880}
\include{enumerate}
\usepackage{graphicx}
\graphicspath{ {images/} }

\newcommand{\prob}[1]{\ensuremath{\text{{\bf Pr}$\left[#1\right]$}}}
\newcommand{\expct}[1]{\ensuremath{\text{{\bf E}$\left[#1\right]$}}}
\newcommand{\var}[1]{\ensuremath{\text{{\bf Var}$\left[#1\right]$}}}

\begin{document}

\title{Lower Bounds for Graph Streaming Algorithms}
\subtitle{A Survey}
\date{December 13, 2017}
\institute{UW-Madison}
\author{Sam Vinitsky}

\begin{frame}
\titlepage
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
\frametitle{Introduction: Connectivity}

\begin{itemize}
\item<1-> Connectivity problem:

\begin{itemize}
\item<1-> Given: Graph $G=(V,E)$
\item<1-> Goal: Determine whether $G$ is connected
\item<2-> Streaming setting: Edges arrive one at a time (insertion-only)
\end{itemize}

\item<3-> Streaming lower bounds from class: 

\begin{itemize}
\item<4-> Lower bound: $\Omega(n)$
\item<4-> Proof method: Direct proof
\end{itemize}

\item<5-> This presentation: 

\begin{itemize}
\item<6-> Lower bound: $\Omega(n \log n)$ [TIGHT!]
\item<6-> Proof method: Reduction from communication problems
\end{itemize}

\end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% \begin{frame}
% \frametitle{Introduction: Method Outline}

% \begin{itemize}
% \item<1-> Key idea: 
% \begin{itemize}
% \item<1-> Treat streaming problems as communication problems
% \item<1-> Use known results from communication complexity

% \end{itemize}
% \item<2-> To prove a lower bound for computing some graph property $\mathcal{P}$:
% \begin{itemize}
% \item<3-> Choose some communication problem $\mathcal{C}$ of known complexity
% \item<3-> Show a reduction from $\mathcal{C}$ to $\mathcal{P}$
% \begin{itemize}
% \item<4-> i.e. a solution for $\mathcal{P}$ can be used to solve $\mathcal{C}$
% \end{itemize}
% \item<5-> Conclude that computing $\mathcal{P}$ is at least as hard as computing $\mathcal{C}$
% \end{itemize}
% \end{itemize}


% \end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
\frametitle{Introduction: Overview of Results}

\begin{itemize}

\item<1-> Reductions from \textsc{Index}: (Feigenbaum et al., 2008)
\begin{itemize}
\item<2-> {\makebox[7cm][l]{Connectivity} $\Omega(n)$}
\item<2-> {\makebox[7cm][l]{Bipartite} $\Omega(n)$}
\item<2-> {\makebox[7cm][l]{Cycle-free} $\Omega(n)$}
\end{itemize}

\item<3-> Reductions from \textsc{Perm}: (Sun and Woodruff, 2015)
\begin{itemize}
\item<4-> {\makebox[7cm][l]{Connectivity} $\Omega(n \log n)$}
\item<4-> {\makebox[7cm][l]{Bipartite} $\Omega(n \log n )$}
\item<4-> {\makebox[7cm][l]{Cycle-free} $\Omega(n \log n)$}
\item<5-> {\makebox[7cm][l]{$O(1)$-approximating minimum cut} $\Omega(n \log^2 n)$}$^*$
\item<6-> Many others...
\item<7-> {\makebox[7cm][l]{MST in unweighted graphs} $\Omega(n \log n)$}
\end{itemize}

\item<8-> Reductions from $\textsc{BHH}_n^t$: (Huang and Peng, 2016)
\begin{itemize}
\item<9-> Approximating minimum spanning tree
\item<10-> Property testing (in the streaming model) for:
\begin{itemize}
\item<11-> Connectivity
\item<11-> Cycle-freeness
\item<11-> Bipartiteness in planar graphs
\end{itemize}
\end{itemize}

\end{itemize}

\end{frame}

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% \begin{frame}
% \frametitle{Introduction: Onwards!} % MAYBE JUST SAY ALL THIS

% \begin{itemize}
% \item<1-> Now that we know the power of this method, let's see it in action!
% \item<2-> For the sake of storytelling, we will focus on connectivity.
% \item<3-> But as seen in the last slide, this method is versatile and powerful.
% \item<4-> Like any good story, we start with Alice and Bob... 
% \end{itemize}

% \end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
\frametitle{Communication Complexity: Overview}

\begin{figure}
\includegraphics[scale=0.5]{protocol.png}
%\caption{lion!!}
\end{figure}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
\frametitle{Communication Complexity: Overview}

\begin{itemize}
\item<1-> Communication Game: (between Alice and Bob)

\begin{itemize}
\item<2-> Given: 

\begin{itemize}
\item<2-> Two arbitrary sets $X$ and $Y$
\item<2-> A boolean function $f: X \times Y \rightarrow \{0,1\}$
\end{itemize}

\item<3-> Input:
\begin{itemize}
\item<3-> Alice has $x \in X$
\item<3-> Bob has $y \in Y$
\end{itemize}
\item<4-> Goal: Bob computes $f(x,y)$
\end{itemize}

\begin{alertblock}<5->{Constraints:}
\begin{itemize}
\item<6-> Alice can only send a SINGLE message to Bob
\item<6-> Bob can't send any messages 
\item<7-> Alice and Bob can both be randomized
\end{itemize}
\end{alertblock}

\begin{itemize}
\item<6-> \textit{One-way} communication game
\end{itemize}

\end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
\frametitle{Communication Complexity: Definitions}

\begin{definition}<1->[Protocol]
A \textit{protocol} is a plan for Alice and Bob's communication.
\end{definition}

\begin{definition}<2->[Communication Cost]
The \textit{communication cost} of a protocol is the largest number of bits sent in the worst case (over all inputs $x$ and $y$).
\end{definition}

\begin{definition}<3->[Randomized Communication Complexity]
The \textit{randomized communication complexity} of a function $f$ is the minimum cost of any randomized protocol computing $f$ with constant probability $>\frac{1}{2}$. We denote this $R^{1-way}(f)$.
\end{definition}

\begin{itemize}
\item<4-> We can treat properties as function...
\end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
\frametitle{Communication Complexity: Connection to Streaming}

\begin{itemize}
\item<1-> Let $\mathcal{A}$ be a streaming algorithm computing $f$ in $M$ bits of space.
\end{itemize}

\begin{alertblock}<2->{Protocol for computing $f$: (with cost $M$)}
\begin{enumerate}
\item<3-> Alice treats her input $x$ as a stream $s_x$; she runs $\mathcal{A}$ on $s_x$
\item<4-> Alice sends the current state of $\mathcal{A}$ to Bob [sends $M$ bits]
\item<5-> Bob treats his input $y$ as a stream $s_y$
\item<6-> Bob continues the execution of $\mathcal{A}$ on $s_y$ from where Alice left off
\item<7-> Bob outputs the results of $\mathcal{A}$
\end{enumerate}
\end{alertblock}

\begin{itemize}
\item<8-> Analysis:
\begin{itemize}
\item<9-> Bob has the result of running $\mathcal{A}$ on $s_x \circ s_y$.
\item<10-> Since $\mathcal{A}$ computes $f$, this protocol also computes $f$.
\item<11-> Cost of this protocol computing $f$ is $M$, so $M \geq R^{1-way}(f)$.
\end{itemize}

\item<12-> Conclusion:
\begin{itemize}
\item<12-> \textit{Communication lower bounds are also streaming lower bounds.}
\end{itemize}
\end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
\frametitle{\textsc{Conn} Lower Bounds: \textsc{Perm} Definition}

\begin{itemize}
\item<1-> \textsc{Perm} communication problem:
\begin{itemize}
\item<2-> Alice is given a permutation $\sigma$ on $[n]$, encoded as
 $x = \sigma(1)\sigma(2)\sigma(3)...\sigma(n)$
\item<2-> Bob is given a bit $i$ in $[n \log n]$
\item<2-> Goal: Bob wishes to compute the $i$-th bit of $\sigma$
\end{itemize}

\item<3-> $R^{2-way}(\textsc{Perm}) = \Theta(\log n)$.

\end{itemize}

\begin{theorem}<4->
$R^{1-way}(\textsc{Perm}) = \Omega(n \log n)$
\end{theorem}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
\frametitle{\textsc{Conn} Lower Bounds: \textsc{Perm} Lower Bound}

\begin{theorem}<1->
$R^{1-way}(\textsc{Perm}) = \Omega(n \log n)$
\end{theorem}

\begin{proof}<1->
\begin{itemize}
\item<2-> Recall that Bob cannot send anything to Alice.
\item<3-> This means that Alice has to just ``guess'' which bit Bob wanted.
\item<4-> Any protocol is Alice sending $r$ random bits of $x = \sigma(1)...\sigma(n)$.
\item<5-> The probability of success is just the probability of Alice choosing bit $i$ in the set of $r$ random bits from $n \log n$ possible bits: $\frac{r}{n \log n}$.
\item<6-> By definition of $R^{1-way}$, the protocol must succeed with constant probability...
\item<7-> $r = \Omega(n \log n)$.
\end{itemize}
\end{proof}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
\frametitle{\textsc{Conn} Lower Bounds: \textsc{Conn} Definition}

\begin{itemize}
\item<1-> \textsc{Conn} communication problem:
\begin{itemize}
\item<2-> Alice and Bob have some shared vertex set $V$
\item<2-> Alice has an edge set $E_A$ 
\item<2-> Bob has an edge set $E_B$ disjoint from $E_A$
\item<3-> Goal: Bob learns if $G = (V, E_A \cup E_B)$ is connected.
\end{itemize}
\end{itemize}

\begin{theorem}<4->[Class]
$R^{1-way}(\textsc{Conn}) = \Omega(n)$
\end{theorem}

\begin{theorem}<5->[Sun and Woodruff, 2015]
$R^{1-way}(\textsc{Conn}) = \Omega(n \log n)$
\end{theorem}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
\frametitle{\textsc{Conn} Lower Bounds: Reduction from \textsc{Perm}}

\begin{theorem}<1->[Sun and Woodruff, 2015]
$R^{1-way}(\textsc{Conn}) = \Omega(n \log n)$
\end{theorem}

\begin{proof}<1->\renewcommand{\qedsymbol}{}
\begin{itemize}
\item<1-> Reduction from \textsc{Perm} instances of size $\frac{n}{2}$ to \textsc{Conn} instances of size $|V| = n$. 
\item<2-> i.e Assume we have a \textsc{Perm} instance $\langle \sigma,i \rangle$, \\ and create a \textsc{Conn} instance $G = (V, E_A \cup E_B)$
\item<3-> Let $L$ and $R$ denote two halves of $V$, each of size $\frac{n}{2}$. 
\item<4-> Elements of $L$ are labeled $\{1,2,3,...,\frac{n}{2}\}$ (same with $R$)
\item<5-> Alice has $\sigma$
\item<6-> Alice's edge set $E_A$:

\begin{itemize} 
\item<7-> Alice creates a perfect matching from $L$ to $R$ such that \\ the $i$-th vertex in $L$ is matched with the $\sigma(i)$-th vertex in $R$. 
\end{itemize}
\end{itemize}
\end{proof}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{\textsc{Conn} Lower Bounds: Reduction from \textsc{Perm}}

\begin{proof}[Proof. (continued)]\renewcommand{\qedsymbol}{}

\begin{itemize}

\item<1-> Bob has the the input $i$ to \textsc{Perm}. 
\item<2-> This means he wants the $i$-th bit of $\sigma(1)\sigma(2)...\sigma(\frac{n}{2})$. 
\item<3-> Note that $i$ is really referring to the $\ell$-th bit of $\sigma(j)$ \\ for some $j \in [\frac{n}{2}]$ and $\ell \in [\log n]$. 
\item<4-> Bob's edge set $E_B$: 
\begin{itemize}
\item<5-> Let $S \subset R$ denote the vertices in $R$ whose $\ell$-th bit is equal to 1.
\item<6-> Bob's edge set will be a spanning tree on the vertices $(L - \{j\}) \cup S$.
\end{itemize}
\item<7-> Wait! $E_A$ and $E_B$ might overlap, $\textsc{Conn}$ needs them disjoint! 
\end{itemize}

\end{proof}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
\frametitle{\textsc{Conn} Lower Bounds: Reduction from \textsc{Perm}}

\begin{proof}[Proof. (continued)]\renewcommand{\qedsymbol}{}

\begin{itemize}

\item Bob has the the input $i$ to \textsc{Perm}. 
\item This means he wants the $i$-th bit of $\sigma(1)\sigma(2)...\sigma(\frac{n}{2})$. 
\item Note that $i$ is really referring to the $\ell$-th bit of $\sigma(j)$ for some $j \in [\frac{n}{2}]$ and $\ell \in [\log n]$. 
\item Bob's edge set $E_B$: (Attempt \#2)
\begin{itemize}
\item<2-> Let $S \subset R$ denote the vertices in $R$ whose $\ell$-th bit is equal to 1.
\item<3-> Introduce a new vertex $w$ with edges to each vertex in $(L - \{j\}) \cup S$. 
\item<4-> Construct $E_B$ to be the spanning tree that uses all of the edges between $w$ and $(L - \{j\}) \cup S$. 
\end{itemize}
\item<5-> Now $E_A$ and $E_B$ are disjoint, because everything in $E_B$ is incident to $w$ (and nothing in $E_A$ is)
\end{itemize}

\end{proof}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
\frametitle{\textsc{Conn} Lower Bounds: Reduction from \textsc{Perm}}
\begin{proof}[Proof. (continued...)]\renewcommand{\qedsymbol}{}

\begin{itemize}
\item<1-> Let $G = (V, E_A\cup E_B)$. 
\item<2-> $L - \{j\}$ is connected in $G$ [every vertex is adjacent to $w$ via $E_B$]
\item<3-> Since $E_A$ is a perfect matching from $L$ to $R$, every non-$\sigma(j)$ vertex in $R$ must be connected to every vertex in $L - \{j\}$
\item<4-> This means that $C_1 = L \cup R - \{j,\sigma(j)\}$ and $C_2 = \{j,\sigma(j)\}$ are both connected components. Are they connected to one another?
\item<5-> There are now two possibilities:
\begin{enumerate}
\item<6-> If the $\sigma(j)$-th right vertex has its $\ell$-th bit equal to 1:
\begin{itemize}
\item<7-> $\sigma(j)\in S$. This means $\sigma(j)$ is adjacent to $w$, and thus to everything in $C_1$, and so the components are connected and $G$ is connected.
\end{itemize}
\item<8-> Otherwise, $\sigma(j)$-th right vertex has its $\ell$-th bit equal to 0:
\begin{itemize}
\item<9-> $\sigma(j) \notin S$. There are no edges incident to $j$ or $\sigma(j)$, and thus $C_2 = \{j,\sigma(j)\}$ is not reachable from $C_1$, so $G$ is disconnected.
\end{itemize}
\end{enumerate}
\item<10-> $G$ is connected $\iff$ the $i$-th bit of $\sigma(1)\sigma(2)...\sigma(\frac{n}{2})$ equals 1. 
\end{itemize}

\end{proof}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
\frametitle{\textsc{Conn} Lower Bounds: Reduction from \textsc{Perm}}
\begin{proof}[Proof. (continued...)]

\begin{itemize}
\item $G$ is connected $\iff$ the $i$-th bit of $\sigma(1)\sigma(2)...\sigma(\frac{n}{2})$ equals 1. 
\item<2-> This completes the reduction from \textsc{Perm} to \textsc{Conn}.
\item<3-> Let $M(E_A)$ be Alice's message to Bob in some protocol for \textsc{Conn}.
\item<4-> Suppose Bob can decide, from $M(E_A)$ and $E_B$, if the graph $G = (L \cup R, E_A \cup E_B)$ is connected (with constant probability).
\item<5-> It follows that from our reduction that Bob can also solve \textsc{Perm} with constant probability with a protocol of the same cost.
\item<6-> $R^{1-way}(\textsc{Conn}) \geq R^{1-way}(\textsc{Perm}) = \Omega(n \log n)$
\end{itemize}

\end{proof}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
\frametitle{\textsc{Conn} Lower Bounds: Tightness}

\begin{itemize}
\item<1-> We have shown a $\Omega(n \log n )$ lower bound for \textsc{Conn}.
\item<2-> There is a matching upper bound...
\end{itemize}

\begin{alertblock}<3->{Protocol for computing \textsc{Conn}:}
\begin{itemize}
\item<4-> Alice constructs a spanning forest $\mathcal{F}$ of her edges $E_A$
\item<5-> Alice sends $\mathcal{F}$ to Bob [sends $???$ bits]
\item<6-> Bob runs any connectivity test on $(V, E_B \cup \mathcal{F})$.
\end{itemize}
\end{alertblock}

\begin{itemize}
\item<7-> Analysis:
\begin{itemize}
\item<8-> This protocol correctly tests connectivity.
\item<9-> $\mathcal{F}$ contains at most $O(n)$ edges.
\item<10-> Thus sending $\mathcal{F}$ requires $O(n \log n)$ bits.
\end{itemize}

\item<11-> Therefore the cost of this protocol is $O(n \log n)$.
\item<12-> This means our lower bound is tight, i.e. $R^{1-way} = \Theta(n \log n)$.

\end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
\frametitle{Conclusions}

\begin{itemize}
\item<2-> Method for proving lower bounds for many streaming problems
\item<3-> Power of a reduction depends on the complexity of the problem
\item<4-> General method, applications beyond streaming algorithms
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{document}