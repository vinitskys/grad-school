\documentclass[11pt]{article}

\include{macros880}
\include{enumerate}

\newcommand{\prob}[1]{\ensuremath{\text{{\bf Pr}$\left[#1\right]$}}}
\newcommand{\expct}[1]{\ensuremath{\text{{\bf E}$\left[#1\right]$}}}
\newcommand{\var}[1]{\ensuremath{\text{{\bf Var}$\left[#1\right]$}}}

\begin{document}

\title{Lower Bounds for Graph Streaming Algorithms: A Survey}
\date{December 11, 2017}
\author{Sam Vinitsky}

\maketitle

\begin{abstract}
In this paper, we give a survey of various lower bounds for randomized exact graph streaming algorithms in the insertion-only model, with a special emphasis on proof techniques. The proofs we present will rely on reduction from various communication problems with known lower bounds. The general trend in these proofs is that more ``complicated'' base problems give us stronger lower bounds. We present several communication problems of known complexity, namely \textsc{Index}, \textsc{Perm} and $\textsc{BHH}_n^t$, and give reductions from them to various graph streaming problems. These reductions will give use lower bounds for several graph problems such as testing for connectivity, testing for bipartiteness, and finding a minimum spanning tree. We present matching upper bounds for several of these problems, which means these lower bounds are tight. 
\end{abstract}

\section{Introduction}

In the streaming setting, we see a sequence of elements $(u_1, u_2, u_3, ..., u_n)$ of some known universe $U$ one at a time, and we wish to compute some property $\mathcal{P}$ of the elements we have seen. The difficultly here is that we will be seeing far more elements than we have space to store. This means we must find a way to compute the property $\mathcal{P}$ as we go, and modify the results properly when we see new elements. Developing streaming algorithms that use space efficiently is a huge focus of recent research. Applications range across computer science, touching networking, parallel computing, database systems, social networks and much much more. For an excellent introduction to streaming algorithms, see Chapter 6 of \cite{blum}.

Social networks in particular present an interesting challenge: the input in this setting is a set of edges from some unknown graph $G$. We then wish to compute some property of the graph $G$ in a space-efficient manner. In reality, this graph may be gigantic. Consider a graph representing friendships on Facebook: every person is a vertex, and an edge exists between two people if they are friends. There would be billions of nodes and edges in this graph! There is no way we could store such a large graph in memory at one time; instead we can read it in one edge at a time, and treat it as a data stream! This motivates the idea behind graph streaming algorithms.

There are several models of graph streaming algorithms. In the \textit{insertion-only} model, we see some sequence of edges $(e_1,e_2,e_3,...,e_n)$ arriving, and wish to compute some property of the graph $G$ made up of those edges. In the \textit{dynamic streaming} model, edges are allowed to arrive or depart (meaning we delete them from $G$). There is also the notion of the \textit{semi-streaming} model, which relaxes the restriction placed on the number of edges we may store. For a thorough overview of graph streaming algorithms, we refer the reader to \cite{mcGreg}.

We will almost exclusively give lower bounds for randomized exact algorithms in the insertion-only model. This means they also apply in the dynamic streaming model.

\subsection{Lower Bounds and Proof Methods}

In this paper, we will present a survey of recent research in lower bounds for graph streaming algorithms. We will give special attention to the proof methods used in finding these lower bounds. 

The important of lower bounds is often understated. Beyond their inherent beauty, lower bounds are important for guiding research. Since most of computer science involves developing algorithms to solve specific problems, it is vitally important to have tight lower bounds for open problems in order to help researchers focus their efforts in potentially fruitful areas. 

Consider for example the problem of finding the minimum cut in an unweighted graph in the dynamic streaming model. The current best known algorithm solves in problem in $O(n \log ^4 n)$ space (see \cite{kap3}). However, the current best known lower bounds (presented in \cite{{sun}}) is $\Omega(n \log^2 n)$. This discrepancy implores future research in this area, because we know that one of these two results could be strengthened. In contrast, we will see matching upper and lower bounds of for deciding the connectivity of a graph in the insertion-only model, which implies future researchers should explore other areas since we have already found the most efficient possible algorithm for this problem.

Similarly, discussing proof methods is an undeniably important aspect of research in theoretical computer science. The way we prove our results not only serves to enlighten the reader and help convince them of the correctness of our assertion, but robust and powerful proof methods allow future researchers to follow in our footsteps, applying our methods to prove interesting results of their own. 

In this paper, we will see the same proof methods used over and over to prove lower bounds: to prove a lower bound for some problem \textsc{P}, we will take some problem \textsc{P}' with a known lower bound $B$, and show that if we can solve P in space $S$, then we can also solve P' in space $S$. This means that P must have the same lower bound as P', namely $B$. This idea of reducing one problem to another appears all over computer science, and is at the heart of complexity theory.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Preliminaries} \label{PRESENTATION}

The proofs we will present in this paper rely on reductions from communication games. In this section, we present the necessary background for the rest of the paper. The exposition in this section is due to \cite{sun}. 

\subsection{Communication Complexity}

There are many models of communication games. We will chose a model that will lend itself nicely to comparison with streaming models. We will consider the two-party, one-way communication game between two players, Alice and Bob. In this model, we have two arbitrary sets $X$ and $Y$ and a boolean function $f: X \times Y \rightarrow \{0,1\}$. Alice is given some input $x \in X$, and Bob is given some input $y \in Y$. Alice is only allowed to send \textit{one} message to Bob. Bob is not allowed to send anything to Alice. The goal is for Bob to compute $f(x,y)$. The \textit{communication cost} of a protocol is the number of bits sent in the protocol (in the worst case). Since Alice is the only party who can send messages, and she can only send one, this amounts to the number of bits sent in that single message (in the worst case). In our model, Alice and Bob are allowed to use randomness to help their computation. We say that a protocol $P$ has error probability at most $\delta$ if $\prob{P(x,y) = f(x,y)} \geq 1 - \delta$ for all inputs $x$ and $y$. The \textit{randomized communication complexity} of $f$, denoted $R^{1-way}(f)$, is the minimum communication cost of any randomized protocol for $f$ has constant error probability. For further background on communication complexity, we refer the reader to \cite{kush}.

\subsection{From Communication to Streaming} \label{stream}

In this paper, we will prove lower bounds for the randomized communication complexity of several problems. These lend themselves naturally to streaming lower bounds via a standard reduction: Alice treats her input $x$ as a stream $\sigma_x$, and runs a streaming algorithm on $\sigma_x$. Alice then sends the state of her algorithm to Bob. Bob treats his input $\sigma_y$ as a stream, and continues the execution of the streaming algorithm based on what Alice sent him. Bob then has the result of the streaming algorithm run on $\sigma_x \circ \sigma_y$. If this output can be used to solve $f$, then the space complexity of the algorithm must be at least $R^{1-way}(f)$. We will refer to this standard reduction outline throughout the paper, opting to only prove lower bounds on $R^{1-way}$ and then referring to this reduction.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Lower Bounds from Index}

In this section, we explore some preliminary lower bounds for several related graph problems. We start with a simple example that gives unimpressive lower bounds in order to illustrate the general proof ideas used for lower bound reductions. The problem considered in this section is very simple, and thus will lead to weak lower bounds. The lower bounds in this section are also the oldest of all the results we will see in this paper. All of the results in this section are due to \cite{feig}.

\subsection{The ``Index'' Problem} \label{index}

We now present the problem we will use for our first reduction. In the \textsc{Index} problem, Alice starts with some string $x \in \{0,1\}^n$, and Bob has $j \in [n]$. Bob wishes to compute $\textsc{Index}(x,j) = x_j$, the $j$-th bit of $x$. In a 2-way communication model, this is easy. Bob can just send $j$ to Alice, and then Alice can send $x_j$ back. This take $\log n$ bits (to send $j$). But in order to relate communication complexity to streaming algorithms (as discussed in section \ref{stream}), we need to consider the 1-way communication model where Alice sends a message to Bob, and Bob sends nothing. Unfortunately, this means Bob can't tell Alice what bit he wants. This makes the job of designing a protocol much harder, because Alice has no knowledge of which bit Bob needs. Intuitively, this means that all Alice can do is send $r$ random bits of $x$, and hope she correctly guessed Bob's index $j$. The probability of this happening is $\frac{r}{n}$. If we want our protocol to succeed with constant probability, then we need to make sure $\frac{r}{n}$ is a constant, meaning $r = \Omega(n)$. This leads to the following theorem:

\begin{theorem} \label{thm:index}
$R^{1-way}(\textsc{Index}) = \Omega(n)$
\end{theorem}

A formal proof of this theorem requires information theory, and is beyond the scope of this course. The intuition given above is sufficient for our purposes, since we will be proving our lower bounds via reductions from such problems rather than directly. For a more thorough proof of Theorem \ref{thm:index}, see \cite{kush}.

\subsection{Balanced Properties} \label{balanced}

In this section, we will use a reduction from the \textsc{Index} problem to prove lower bounds for a whole class of graph properties called \textit{balanced properties}. We start with an obtuse definition:

\begin{definition}
We say a graph property $\mathcal{P}$ is \textbf{balanced} if there exists a constant $c>0$ such that, for all sufficiently large $n$, there exists a graph $G=(V,E)$ with $|V| = n$ and some $u \in V$ such that

\begin{align}
\min \{|\{v:G_{(u,v)} \text{ has } \mathcal{P}\}|, |\{v:G_{(u,v)} \text{ does not have } \mathcal{P}\}|\} \geq cn \label{eq:balanced}
\end{align}

Where $G_{(u,v)} = (V, E \cup \{(u,v)\})$ denotes the graph $G$ augmented with the edge $(u,v)$.

\end{definition}

In other words, there are $\Omega(n)$ vertices $v$ in $G$ such that adding the edge $(u,v)$ results in a graph \text{with} property $\mathcal{P}$, and $\Omega(n)$ vertices $v$ in $G$ such that adding the edge $(u,v)$ results in a graph \textit{without} property $\mathcal{P}$. Intuitively, this means there is some graph that is both close to having $\mathcal{P}$ and not having $\mathcal{P}$.

Several of the traditional properties of graphs we discuss are balanced, including connectivity and bipartiteness. In order to understand why this is true, and in order to get some intuition about what being ``balanced'' means, we now present a proof of the fact that connectively is balanced:

Consider a graph that contains two connected components $C_1$ and $C_2$. Consider some $u \in G$. Wlog, assume it's in $C_1$. Then for any $v \in C_1$, adding the edge $(u,v)$ doesn't make $G_{(u,v)}$ connected, since $u$ and $v$ are in the same connected component. There are $n/2 = \Omega(n)$ such vertices. Similarly, If $v \in C_2$, then adding the edge $(u,v)$ makes $G_{(u,v)}$ connected, and there are $n/2 = \Omega(n)$ such vertices. A similar argument shows why bipartiteness is balanced.

We are now ready to prove our first lower bound. This lower bound will apply to \textit{any} balanced property. We will prove the lower bound by reduction from \textsc{Index}.

\begin{theorem}
Any single-pass streaming algorithm testing for a balanced graph property $\mathcal{P}$ with constant probability of success must use $\Omega(n)$ space.
\end{theorem}

\begin{proof}
Let $c$ be a constant, and let $G=(V,E)$ be a graph on $n$ vertices with $u \in V$ that satisfies the conditions from (\ref{eq:balanced}). Consider an instance of $\textsc{Index}$ of length $cn$. That is, Alice has $x \in \{0,1\}^{cn}$ and Bob has $j \in [cn]$. Let $G^x$ be a relabeling of the vertices of $G$ such that for every $i \in [cn]$, $G^x_{(u,v_i)}$ has property $\mathcal{P}$ if and only if $x_i = 1$. That is, adding the edge $(u,v_i)$ results in a graph with property $\mathcal{P}$ if and only if $x_i = 1$. This is possible since $\mathcal{P}$ is balanced. \label{QUESTION: why?} 

Suppose we have a single-pass streaming algorithm \textsc{A} for testing $\mathcal{P}$ that uses $M$ bits of work space. We can use this to get a 1-way communication protocol for \textsc{Index} that sends $M$ bits: Alice computes $G^x$ from $x$, and then runs the algorithm \textsc{A} on $G^x$. This uses $M$ bits of work space. Alice then sends the current state of the algorithm to Bob (which consists only of her $M$ bits of work space). Bob then continues the execution of the algorithm on the edge $(u,v_j)$ \label{QUESTION: how does he know v_j after the relabelling?} to determine if $G^x_{(u,v_j)}$ has $\mathcal{P}$. Since the algorithm will state that $G^x_{(u,v_j)}$ has $\mathcal{P}$ if and only if $x_j = 1$, Bob has determined the value of $x_j$. The cost of this protocol is $M$, which means that the 1-way communication complexity of \textsc{Index} is at most $M$. This means $M \geq R^{1-way}(\textsc{Index})$. But from Theorem \ref{thm:index}, we have that $R^{1-way}(\textsc{Index}) = \Omega(n)$, and therefore $M = \Omega(n)$.
\end{proof}

This general lower bound could be improve for many specific problems. For example, we know there is a $O(n \log n)$ bit upper bound for \textsc{Conn}, since Alice could just a spanning forest of her edges to Bob, who runs any connectivity algorithm on the resulting graph. Could we get a tighter, matching lower bound? In the next section, we will do just that.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Lower Bounds from Perm} \label{TODO: clean up} \label{PRESENTATION2}

In this section, we present a slight generalization of the \textsc{Index} problem from section \ref{index} that will allow us to prove tight lower bounds for streaming algorithms for several graph properties. By using a slightly more complicated problem as the basis for our reductions, we are able to get stronger lower bounds. All of the results in this section are due to \cite{sun}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{The ``Perm'' Problem}\label{perm}

We first describe the new target problem for our reductions. In the \textsc{Perm} problem, Alice has a permutation $\sigma$ of $[n]$, represented as an ordered list $\sigma(1),\sigma(2),...,\sigma(n)$. This is a list of length $n \log n$, since $\sigma(i)$ is a integer in $[n]$ and thus requires $\log(n)$ bits to write down. Bob wishes to know the $j$-th bit of $\sigma$ for some $j \in [n \log n]$.

If we want a 1-way protocol for \textsc{Perm} in which Bob succeeds with constant probability, we need Alice to send a constant fraction of $\sigma$ to Bob (for the same reasons explained in \ref{index} with regards to \textsc{Index}). Here $\sigma$ has length $n \log n$, and thus Alice needs to send $\Omega(n \log n)$ bits to Bob to succeed with constant probability. This gives the intuition for the following theorem:

\begin{theorem}\label{thm:perm}
$R^{1-way}(\textsc{Perm}) = \Omega(n \log n)$
\end{theorem}

Again, a formal proof requires information theory and is beyond the scope of this paper, but the intuition is sufficient for understanding.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Connectivity}

We will now give our first reduction from \textsc{Perm} to get a lower bound for the important problem of testing connectivity. In the \textsc{Conn} communication problem, Alice and Bob have disjoint edge sets $E_A$ and $E_B$ of an undirected graph $G$ on $n$ vertices. Alice will send some randomized message $M(E_A)$ to Bob, who should determine if $G = (V, E_A \cup E_B)$ is connected.

\begin{theorem} \label{thm:conn}
$R^{1-way}(\textsc{Conn}) = \Omega(n \log n)$
\end{theorem}

\begin{proof}
We will give a reduction from \textsc{Perm} instances of size $n/2$ to \textsc{Conn} instances of size $|V| = n$. Let $L$ and $R$ denote two halves of $V$, each of size $n/2$. Alice, her permutation $\sigma$ in hand, creates a perfect matching from $[n/2]$ to $[n/2]$ such that the $i$-th vertex in $L$ connects to the $\sigma(i)$-th vertex in $R$. Alice's edge set $E_A$ consists of the edges in this perfect matching. 

Suppose Bob has the the input $i$ to \textsc{Perm}. This means he wants the $i$-th bit of $\sigma(1),\sigma(2),...,\sigma(n)$. Note that $i$ is really referring to the $\ell$-th bit of $\sigma(j)$ for some $j \in [n/2]$ and $\ell \in [\log n]$. Bob will construct his edge set $E_B$ as follows: Let $S \subset R$ denote the vertices in $R$ whose $\ell$-th bit is equal to 1. Bob's edge set will be a spanning tree on the vertices $(L - \{j\}) \cup S$. However, this could very well overlap with some of Alice's edges. To prevent this, we introduce a new vertex $w$ with edges to every node in $(L - \{j\}) \cup S$. Then construct $E_B$ to be the spanning tree that uses only edges from $w$ to $(L - \{j\}) \cup S$. Now $E_B$ must be disjoint with $E_A$, since the new vertex $w$ is not incident to any of the edges in $E_A$, but $w$ is incident to every edge in $E_B$.

Let $G = (V, E_A\cup E_B)$. Note that $L - \{j\}$ is connected in $G$, since every vertex in $L - \{j\}$ is adjacent to $w$ by construction. This means that if we have a perfect matching from $L$ to $R$, any vertex $u$ in $R$ (other than possibly $\sigma(j)$) must be connected to every vertex in $L \cup R - \{j,\sigma(j)\} = V - \{j,\sigma(j)\}$. There are now two possibilities:

\begin{enumerate}
\item If the $\sigma(j)$-th right vertex has its $\ell$-th bit equal to 1:
\begin{itemize}
\item Then $\sigma(j)$ is in $S$ by Bob's construction. This means $\sigma(j)$ is adjacent to $w$, and is thus connected to every edge in $V - \{j\}$. Since $\sigma(j)$ is matched with $j$ by Alice's construction, $\sigma(j)$ is connected to every vertex. This means $G$ is connected.
\end{itemize}
\item Otherwise, $\sigma(j)$-th right vertex has its $\ell$-th bit equal to 0:
\begin{itemize}
\item Then $\sigma(j)$ is not in $S$ by Bob's construction. Thus there are no edges incident to either $j$ or $\sigma(j)$ (other than the one between them), and thus $\{j,\sigma(j)\}$ is not reachable from any other vertex in $V$, and thus $G$ is disconnected
\end{itemize}
\end{enumerate}

This means $G$ is connected if and only if the $i$-th bit of $\sigma(1),\sigma(2),...,\sigma(n)$ is equal to 1. This completes the reduction from \textsc{Perm} to \textsc{Conn}.

Suppose $M(E_A)$ is Alice's message to Bob in some protocol for \textsc{Conn}. Suppose also that Bob can decide, from $M(E_A)$ and $E_B$, whether the graph $G = (L \cup R, E_A \cup E_B)$ is connected with constant probability. It follows that from our reduction that Bob can also solve \textsc{Perm} with constant probability. This means $R^{1-way}(\textsc{Conn}) \geq R^{1-way}(\textsc{Perm})$. From Theorem \ref{thm:perm}, we know that $R^{1-way}(\textsc{Perm}) = \Omega(n \log n)$, and thus $R^{1-way}(\textsc{Conn}) = \Omega(n \log n)$.

\end{proof}

This result is much better than the $\Omega(n)$ bound given in section \ref{balanced}. In fact, it is tight! Recall that we have a matching $O(n \log n)$ bit upper bound for \textsc{Conn}, since Alice could just a spanning forest of her edges to Bob. This means that $R^{1-way}(\textsc{Conn}) = \Theta(n \log n)$. From the discussion in section \ref{stream}, this means the space complexity of testing connectivity with a randomized exact streaming algorithm is $\Theta(n \log n)$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Bipartiteness \& Cycle-Freeness}

We now consider another important graph problem: bipartite testing. In the \textsc{Bipartite} communication problem the setup is as before, but now Bob wishes to compute whether $G = (V, E_A \cup E_B)$ is bipartite.

\begin{theorem}
$R^{1-way}(\textsc{Bipartite}) = \Omega(n \log n)$
\end{theorem}

\begin{proof}
As before, we reduce from \textsc{Perm} instances of size $n/2$. Let $L$ and $R$ denote two halves of $V$, each of size $n/2$. Alice, her permutation $\sigma$ in hand, creates a perfect matching from $[n/2]$ to $[n/2]$ such that the $i$-th vertex in $L$ connects to the $\sigma(i)$-th vertex in $R$. Alice's edge set $E_A$ consists of the edges in this perfect matching. 

Suppose Bob has the input $i$ to \textsc{Perm}. As before, this corresponds to the $\ell$-th bit of $\sigma(j)$ for some $j\in [n/2]$ and $\ell \in [\log n]$. As in the proof of Theorem \ref{thm:conn}, Bob creates his edge set as follows: Let $S \subset R$ denote the edges in $R$ whose $\ell$-th bit is equal to 0. Then construct $E_B$ to contain edges from $w$ to every member of $S \cup \{v\}$ (where $v$ is the $j$-th vertex in $L$). 

$E_A$ is bipartite since it is a perfect matching. Every edge in $E_B$ is incident to $w$, which means that $G$ is bipartite if and only if there is no odd cycle containing $w$. But note that the edge $(v,w)$ is the only edge from any vertex in $L$ to $w$. Similarly, there are no edges between vertices in $R$. This means that if we remove $(v,w)$, the resulting graph is acyclic. This means $(v,w)$ must be in any cycle, and so must $(v,\sigma(v))$, and $(\sigma(v),w)$. But we have the edge $(\sigma(v),w) \in E_B$ if and only if $\sigma(v) \in S$, which means the $\ell$-th bit of $\sigma(j)$ is 0. Therefore $G$ has an odd cycle if and only if the $\ell$-th bit of $\sigma(j)$ is 0. This completes the reduction, since a graph is bipartite if and only if it has no odd cycles. 

Thus if some protocol allows Bob can solve \textsc{Bipartite} with constant probability, he can also solve \textsc{Perm} with constant probability. Therefore $R^{1-way}(\textsc{Bipartite}) \geq R^{1-way}(\textsc{Perm}) = \Omega(n \log n)$.
\end{proof}

There is a matching $O(n \log n)$ upper bound from a streaming algorithm discussed in \cite{mcGreg}. This means $R^{1-way}(\textsc{Bipartite}) = \Theta(n \log n)$.

As a corollary to \textsc{Bipartite}, we consider the \textsc{Cycle-Free} communication problem. Here, Bob wishes to compute whether $G = (V, E_A \cup E_B)$ is cycle-free. The reduction is the same as the reduction as for \textsc{Bipartite}, since that reduction amounted to cycle checking, where the only cycle happened to be the cycle of odd length containing $(v,w)$, $(v,\sigma(v))$, and $(w,\sigma(v))$. This lead to the following theorem:

\begin{theorem}
$R^{1-way}(\textsc{Cycle-Free}) = \Omega(n \log n)$
\end{theorem}

There is a matching upper bound for testing cycle-freeness. Simply store the first $n-1$ edges of $G$ (each one takes $\log n$ space). If $G$ has more than $n-1$ edges, it must have a cycle. If not, then we can just run any cycle detection algorithm on $G$, since we have all of it stored. Therefore $R^{1-way}(\textsc{Cycle-Free}) = \Theta(n \log n)$.

Again, the connection to streaming lower bounds is as outlined in section \ref{stream}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Minimum Spanning Tree} \label{sun:mst}

We now consider the last problem we will use \textsc{Perm} for: finding a minimum spanning tree in an unweighted graph (we explore this problem for weighed graphs in section \ref{imDumb}). In the \textsc{MST} communication problem, we are promised that $G$ is connected. Since $G$ is unweighted, all minimum spanning trees have the same weight. Thus Bob's goal is just to find some spanning tree in $G = (V, E_A \cup E_B)$. 

\begin{theorem}
$R^{1-way}(\textsc{MST}) = \Omega(n \log n)$
\end{theorem}

\begin{proof}
We will again reduce from \textsc{Perm} instances of size $n/2$. Let $L$ and $R$ denote two halves of $V$, each of size $n/2$. Alice, her permutation $\sigma$ in hand, creates a perfect matching from $[n/2]$ to $[n/2]$ such that the $i$-th vertex in $L$ connects to the $\sigma(i)$-th vertex in $R$. Alice's edge set $E_A$ consists of the edges in this perfect matching. 

Bob's edge set $E_B$ is just a line of edges connecting every vertex in $L$. The edge sets $E_A$ and $E_B$ are distinct, since no edge of $E_A$ is between two vertices in $L$ (and every edge of $L$ is as such). This means the total number of edges is $|E_A| + |E_B| = (n/2) + (n/2 -1) = n-1$. By our construction, $G=(V,E_A\cup E_B)$ is connected and contains $n-1$ edges, meaning $G$ itself is the only spanning tree for $G$. Therefore if Bob can solve \textsc{MST}, he can reconstruct $G$ entirely. From $G$, he can compute the $i$-th bit of Alice's permutation. This means that if Alice and Bob can solve \textsc{MST} with constant probability, they can solve \textsc{Perm} with constant probability. Thus $R^{1-way}(\textsc{MST}) \geq R^{1-way}(\textsc{Perm}) = \Omega(n \log n)$.
\end{proof}

As always, the connection to streaming lower bounds follow as in section \ref{stream}.

The result stated here is unfortunately not very powerful, since it only applies to finding exact MST's in unweighted graphs (a relatively trivial problem). To get lower bounds for MST in \textit{weighted} graphs, we will need to use more powerful machinery, as introduced in the next section.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Lower Bounds from BHH} 

In this section, we will present a much more complicated problem used for reductions. This problem allows us to prove far more interesting and specific lower bounds, but requires a much more complicated reduction that is beyond the scope of this paper. All of the results in this section are due to \cite{huang}.

\subsection{BHH Definition}

We start by defining a problem that is significantly more complicated than the \textsc{Index} and \textsc{Perm} problems we have seen used for reductions up to this point,

In the \textit{boolean hidden hypermatching} problem ($\textsc{BHH}_n^t$), Alice is given a vector $x \in \{0,1\}^n$, where $n = 2kt$ for some $k \geq 1$. Bob gets a partition of the set $[n]$ into subsets of size $t$, called $\{m_1,m_2,...,m_{n/t}\}$. Bob also gets a vector $w \in \{0,1\}^{n/t}$. Let the length $n$ vector $M_i$ be the indicator vector for $m_i$ (i.e., $M_i[j] = 1$ if $j \in m_i$, and 0 otherwise.). Let $M$ denote the $\frac{n}{t} \times n$ whose $i$-th row is $M_i$. In this problem Bob's goal is to compute $Mx+w$, which is promised to be in $\{0,1\}$.

We have the following lower bound for $\textsc{BHH}_n^t$ due to \cite{verbin}. The proof is far beyond our current scope, and so here we even omit any intuition behind it:

\begin{theorem}
If $n = 2kt$ for some integer $k \geq 1$, then $R^{1-way}(\textsc{BHH}_n^t) = \Omega(n^{1-1/t})$
\end{theorem}

\subsection{Reductions from $\textsc{BHH}_n^t$} \label{imDumb}

We can use reductions from this problem to obtain more interesting and specific lower bounds. Unfortunately, the reductions involving this problem are incredibly involved, and require far more space than we have here. As such, we will present all results in this section without proof (we refer the interested reader to section 5 of \cite{huang} for thorough proofs). We will start by giving a very vague overview of the reduction methods used for these problems:

All of the reductions from $\textsc{BHH}_n^t$ rely on Alice and Bob using $x$ and $M$ respectively to choose specific edges $E_A$ and $E_B$ of a bipartite graph $G(x,M)$. As usual, Alice sends a randomized message based on $M(E_A)$ to Bob. Bob then adds some edges specific to $G(x,M)$ specific to the problem at hand. The connection from communication complexity to streaming algorithm is as discussed in section \ref{stream}.

The lower bounds we get by reducing from $\textsc{BHH}_n^t$ are very powerful. Recall that in section \ref{sun:mst}, we only gave a lower bound for the \textit{unweighted} MST problem. Using a reduction to $\textsc{BHH}_n^t$, we can get a much stronger theorem about approximating MST in weighted graph:

\begin{theorem} \label{thm:mstW}
In the insertion-only model, if all edges of the graph have weights in $[W]$, any algorithm that $(1\pm \epsilon)$-approximates the weight of the MST must use $\Omega(n^{1-\frac{4\epsilon}{W-1}})$ bits of space.
\end{theorem}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Related Work} \label{TODO: write this}

The lower bounds we presented in this paper were merely a taste of the many problems that we can get lower bounds for via reduction from \textsc{Index}, \textsc{Perm}, and $\text{BHH}_n^t$. In this section, we give an overview of some of the more interesting results that have been obtained from these problems. 

In \cite{feig}, they use \textsc{Index} to find lower bounds for using a randomized algorithm to exactly find the girth of a graph. They also give an interesting result about approximating the diameter of a graph to within a factor of $o(\log n / \log \log n)$. This result is based on an observation of the fact that certain edges can be removed without affecting the diameter too much. 

In \cite{sun}, they use reduction from \textsc{Perm} to get a plethora of lower bounds for randomized streaming algorithms for various graph problems, such as Eulerian testing, calculating diameter, planarity testing, and many more. For most of these, they state matching upper bounds. They also give a deterministic lower bounds for $k$-edge and $k$-vertex connectivity that match known upper bounds. Interestingly, they prove these two directly rather than via reduction, making them the only graph lower bounds we've seen that were proven directly. They also introduce a slight variation on \text{Perm} in which Alice has $r$ different permutations, and Bob wishes to learn a specific bit of one of a specific one of them (this is called \textsc{$r$-AugementedPerm}). They use this problem to get a lower bound for $O(1)$-approximating minimum cut that does not match the current upper bound (they are off by a factor of $\log^2n$). Future research areas may attempt to close this gap.

In \cite{huang} they also use $\textsc{BHH}_n^t$ to get lower bounds for streaming algorithms for property testing of several interesting graph properties: bipartiteness in planar graphs, connectivity, and cycle-freeness. These proofs all rely on the same construction of $G(x,M)$ used in the proof of Theorem \ref{thm:mstW}. Interestingly, all of these properties are balanced (as defined in section \ref{balanced}). 

Beyond lower bounds for graph problems, there has been a plethora of work on lower bounds communication games. These lower bounds form the basis of everything we did in this paper, and are thus the bedrock of developing lower bounds for other streaming algorithms. For a thorough overview of communication complexity, see the book \cite{kush}. For seminal works in the area, we refer the reader to \cite{ilan}, \cite{ziv}, and \cite{amit}. For more recent work, see \cite{phil}.

There has also been an abundance of research in using communication complexity to prove lower bounds for other types of algorithms. In particular, these has been a push of recent research to use these ideas to prove lower bounds for property testing problems. For a good overview of this technique, see \cite{eric}. For additional research in non-graph related streaming lower bounds, see \cite{gal} and \cite{kap2}. For other recent research in graph lower bounds, \cite{kap} provides lower bounds for approximating \textsc{Max-Cut} and \cite{conrad} provides lower bounds for maximum matchings in the dynamic streaming model. 

In summary, lower bounds for streaming algorithms are a vibrant and relevant research area. Future research in lower bounds for streaming algorithms (and graph streaming in particular) will be pivotal in the development of an algorithmic theory surrounding streaming, as knowing we have tight bounds on the complexity for specific problems allow researchers to focus their energy elsewhere. It also provides us with fodder for future research, in cases where our lower and upper bounds do not match, such as approximating the minimum cut \cite{sun}. Finally, understanding proof techniques is invaluable in developing future proofs and advancing mathematical understanding of computational problems.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\nocite{*}
\bibliographystyle{alpha}
\bibliography{bibliography}

% latex writeup.tex
% bibtex writeup.aux
% latex writeup.tex
% t writeup.tex

\end{document}