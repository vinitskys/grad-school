\documentclass[11pt]{article}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Header borrowed from Shuchi Chawla %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{fullpage}
\usepackage{epsfig}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amstext}
\usepackage{amsmath}
\usepackage{xspace}
\usepackage{theorem}
\usepackage{hyperref}
\usepackage{enumerate}

% This is the stuff for normal spacing
\makeatletter
 \setlength{\textwidth}{6.5in}
 \setlength{\oddsidemargin}{0in}
 \setlength{\evensidemargin}{0in}
 \setlength{\topmargin}{0.25in}
 \setlength{\textheight}{8.25in}
 \setlength{\headheight}{0pt}
 \setlength{\headsep}{0pt}
 \setlength{\marginparwidth}{59pt}

 \setlength{\parindent}{0pt}
 \setlength{\parskip}{5pt plus 1pt}
 \setlength{\theorempreskipamount}{5pt plus 1pt}
 \setlength{\theorempostskipamount}{0pt}
 \setlength{\abovedisplayskip}{8pt plus 3pt minus 6pt}

 \renewcommand{\section}{\@startsection{section}{1}{0mm}%
                                   {2ex plus -1ex minus -.2ex}%
                                   {1.3ex plus .2ex}%
                                   {\normalfont\Large\bfseries}}%
 \renewcommand{\subsection}{\@startsection{subsection}{2}{0mm}%
                                     {1ex plus -1ex minus -.2ex}%
                                     {1ex plus .2ex}%
                                     {\normalfont\large\bfseries}}%
 \renewcommand{\subsubsection}{\@startsection{subsubsection}{3}{0mm}%
                                     {1ex plus -1ex minus -.2ex}%
                                     {1ex plus .2ex}%
                                     {\normalfont\normalsize\bfseries}}
 \renewcommand\paragraph{\@startsection{paragraph}{4}{0mm}%
                                    {1ex \@plus1ex \@minus.2ex}%
                                    {-1em}%
                                    {\normalfont\normalsize\bfseries}}
 \renewcommand\subparagraph{\@startsection{subparagraph}{5}{\parindent}%
                                       {2.0ex \@plus1ex \@minus .2ex}%
                                       {-1em}%
                                      {\normalfont\normalsize\bfseries}}
\makeatother

\newenvironment{proof}{{\bf Proof:  }}{\hfill\rule{2mm}{2mm}}
\newenvironment{proofof}[1]{{\bf Proof of #1:  }}{\hfill\rule{2mm}{2mm}}
\newenvironment{proofofnobox}[1]{{\bf#1:  }}{}
\newenvironment{example}{{\bf Example:  }}{\hfill\rule{2mm}{2mm}}

\renewcommand{\theequation}{\thesection.\arabic{equation}}
\renewcommand{\thefigure}{\thesection.\arabic{figure}}

\newtheorem{fact}{Fact}[section]
\newtheorem{lemma}[fact]{Lemma}
\newtheorem{theorem}[fact]{Theorem}
\newtheorem{definition}[fact]{Definition}
\newtheorem{corollary}[fact]{Corollary}
\newtheorem{proposition}[fact]{Proposition}
\newtheorem{claim}[fact]{Claim}
\newtheorem{exercise}[fact]{Exercise}

% math notation
\newcommand{\R}{\ensuremath{\mathbb R}}
\newcommand{\Z}{\ensuremath{\mathbb Z}}
\newcommand{\N}{\ensuremath{\mathbb N}}
\newcommand{\F}{\ensuremath{\mathcal F}}
\newcommand{\SymGrp}{\ensuremath{\mathfrak S}}

\newcommand{\CCP}{\ensuremath{\sf P}}
\newcommand{\CCNP}{\ensuremath{\sf NP}}
\newcommand{\C}{\ensuremath{\mathcal C}}

\newcommand{\size}[1]{\ensuremath{\left|#1\right|}}
\newcommand{\ceil}[1]{\ensuremath{\left\lceil#1\right\rceil}}
\newcommand{\floor}[1]{\ensuremath{\left\lfloor#1\right\rfloor}}
\newcommand{\poly}{\operatorname{poly}}
\newcommand{\polylog}{\operatorname{polylog}}

% some abbreviations
\newcommand{\e}{\epsilon}
\newcommand{\half}{\ensuremath{\frac{1}{2}}}
\newcommand{\junk}[1]{}
\newcommand{\sse}{\subseteq}
\newcommand{\union}{\cup}
\newcommand{\meet}{\wedge}

\newcommand{\prob}[1]{\ensuremath{\text{{\bf Pr}$\left[#1\right]$}}}
\newcommand{\expct}[1]{\ensuremath{\text{{\bf E}$\left[#1\right]$}}}
\newcommand{\var}[1]{\ensuremath{\text{{\bf Var}$\left[#1\right]$}}}
\newcommand{\Event}{{\mathcal E}}

\newcommand{\mnote}[1]{\normalmarginpar \marginpar{\tiny #1}}

\newcommand{\headings}[4]{
\noindent
\fbox{\parbox{\textwidth}{
{\bf CS880: Algorithms for Massive Datasets} \hfill {{\bf} #2}\\
\vspace{0.02in}\\
{{\bf HW#1 Solutions}} \hfill {{\bf Date:} #3}
}}
\vspace{0.2in}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Document begins here %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\newcommand{\lecnum}{2}
\headings{\lecnum}{Sam Vinitsky (with Zachary Charles)}{10/15/2017}

\section{} % PROBLEM 1


\subsection*{Algorithm 1: Preprocessing $G$}

Consider the following algorithm for maintaining $\log W$ different unweighed graphs $F_i$, whose vertices and edges come from the weighted, streamed graph $G$.

\begin{enumerate}

\item Construct unweighted graphs $F_i$ for $i \in [\log W]$ such that $F_i = \{\}$.

\item Upon seeing an edge $e = (u,v,w_G(e))$ from $G$:

\begin{enumerate}

\item Compute the unique integer $j$ such that $2^j \leq w_G(e) < 2^{j+1}$.

\item If $d_{F_j}(u,v) > t$, add an unweighted edge $(u,v)$ to $F_i$.

\end{enumerate}

\end{enumerate}

\begin{claim}
The above algorithm uses space $O(n^{1+2/t} \log W)$.
\end{claim}

\begin{proof}
We are constructing $\log W$ different graphs $F_i$. Each $F_i$ is a $t$-spanner for some subset $S_i$ of $G$, constructed following our algorithm from class. Let $|S_i| = s_i \leq n$. Then the number of edges in $F_i$ is $O(s_i^{1+2/t})$ from class. Thus the total number of edges in all of the $F_i$'s is $O(\sum_{i=1}^{\log W} s_i^{1+2/t}) \leq O(\sum_{i=1}^{\log W} n^{1+2/t})$ since $s_i \leq n$. But this is $O(n^{1+2/t} \log W)$, and thus we have $O(n^{1+2/t} \log W)$ edges in total. Note that we do not need to store $j$ for any $F_j$ as long as we store the graphs sorted by increasing $j$ value. Thus the total space required for the above streaming algorithm is $O(n^{1+2/t} \log W)$ as desired.
\end{proof}

Let $G_j$ denote an unweighted subgraph of $G$ containing only the edges from $G$ that have weight $2^j \leq w_G(e) <2^{j+1}$.

\begin{claim} \label{spanner}
$F_j$ is a $t$-spanner for $G_j$.
\end{claim}

\begin{proof}
This follows immediately from our construction according to the analysis of the original $t$-spanner from class.
\end{proof}

\subsection*{Algorithm 2: Computing Distance Queries in $G$}

Suppose we have already streamed some graph $G$, and used Algorithm 1 to preprocess it into $\log W$ graphs $F_i$. In order to compute the distance from $u$ to $v$ in $G$, do the following: (We use the convention that if there is no path from $u$ to $v$ in $H$, then $d_H(u,v) = \infty$.)

\begin{enumerate}
\item For $j = 1,2,...,\log W$:

\begin{enumerate}

\item If $d_{F_j}(u,v) \neq \infty$, return $d(u,v) = 2^{j+1} \cdot d_{F_j}(u,v)$.

\end{enumerate}

\item Return $d(u,v) = \infty$

\end{enumerate}

\begin{claim}
This algorithm answers distance queries in $G$ with an approximation factor of $2t$, using $O(n)$ additional space.
\end{claim}

\begin{proof}
We wish to show that $d(u,v) \leq 2t d_G(u,v)$. First, note that $d(u,v) = 2^{j+1} \cdot d_{F_j}(u,v)$. But from Claim \ref{spanner}, $d_{F_j}(u,v) \leq t \cdot d_{G_i}(u,v)$. Then $d(u,v) \leq 2^{j+1} \cdot t \cdot d_{G_i}(u,v)$. But note that $2^j \cdot d_{G_j}(u,v) \leq d_G(u,v)$ from the way we constructed $G_j$. Then $d(u,v) \leq 2t \cdot 2^j d_{G_i}(u,v) \leq 2t \cdot d_G(u,v)$ as desired.

Note that computing this query required computing the distance in an undirected graph on at most $m$ edges and $n$ vertices, which can be done in linear space using BFS.

\end{proof}

Taken together, these two claims imply that we can $2t$-approximate distance queries in $G$ using space $O(n^{1+2/t} \log W) + O(n) = O(n^{1+2/t} \log W)$ as desired.

\section{} % PROBLEM 2

Consider the following algorithm for generating a $(1+\epsilon)$-cut sparsifier $H$ for an unweighted graph $G$:

\begin{enumerate}
\item For each edge $e$ in $G$, add it to $H$ with probability $p$ and assign it weight $1/p$.
\end{enumerate}

We are guaranteed that the min-cut of $G$ has weight $\omega(\log n)$. We will prove that with this guarantee, choosing $p = 1 / (\log n \cdot [1 + \epsilon^2 \cdot \frac{\delta}{2^n}])$ ensures that with probability $1-\delta$, the graph $H$ produced by this algorithm is a $(1+\epsilon)$-cut sparsifier for $G$. For simplicity of what follows, let $d = \frac{\delta}{2^n}$, and so $p = 1 / (\log n \cdot [1 + \epsilon^2 \cdot d)$.

Let $\delta(U)$ denote the set of edges from the cut $U$ to $V \backslash U$ as in class. Then $w_H(\delta(U))$ denotes the total weight of the cut $U$ in $H$.

Consider some cut $U$ in $G$. We will to show that with probability $1-d$, we have $w_H(\delta(U)) \in (1 \pm \epsilon) |\delta(U)|$. We can do this by bounding the variance of $w_H(\delta(U))$ and using Chebyshev's inequality. Recall the following claim from class:

\begin{claim} \label{expct_wH}
$\expct{w_H(\delta(U))} = |\delta(U)|$
\end{claim}

\begin{proof}
We showed this in class. $\expct{w_H(\delta(U))} = \sum_{e\in\delta(U)} [\frac{1}{p} \cdot p] = |\delta(U)|$
\end{proof}

We will now prove a bound on the variance that will allow us to apply Chebyshev's inequality:

\begin{claim}
$\var{w_H(\delta(U))} \leq \epsilon^2 d \cdot \expct{w_H(\delta(U))}$
\end{claim}

\begin{proof}

$\var{w_H(\delta(U))} = \expct{w_H(\delta(U))^2} - \expct{w_H(\delta(U))}^2$ by the definition of variance. Then $\var{w_H(\delta(U))} = \expct{w_H(\delta(U))^2} - |\delta(U)|^2$ by Claim \ref{expct_wH}. We can write out $\expct{w_H(\delta(U))^2}$ as 

\begin{align}
\expct{w_H(\delta(U))^2} &= \sum_{e \in \delta(U)}  \bigg( \frac{1}{p} \bigg) ^ 2 \cdot p \\
&= \sum_{e \in \delta(U)}  \frac{1}{p} \\
&= \frac{1}{p} \cdot |\delta(U)| 
\end{align}

Substituting this in for $\expct{w_H(\delta(U))^2}$ in $\var{w_H(\delta(U))} = \expct{w_H(\delta(U))^2} - |\delta(U)|^2$, we get:

\begin{align}
\var{w_H(\delta(U))} &= \frac{1}{p} \cdot |\delta(U)|  - |\delta(U)|^2 \\
&= \frac{1}{1 / (\log n \cdot [1 + \epsilon^2 d)]} \cdot |\delta(U)|  - |\delta(U)|^2 \label{var:p} \\
&= (\log n \cdot [1+ \epsilon^2 d]) \cdot |\delta(U)|  - |\delta(U)|^2  \\
&= [1+ \epsilon^2 d] \cdot \log n \cdot |\delta(U)|  - |\delta(U)|^2 \\
&\leq  [1+ \epsilon^2 d] \cdot |\delta(U)| \cdot |\delta(U)|  - |\delta(U)|^2 \label{var:omega} \\
&=  [1+ \epsilon^2 d] \cdot |\delta(U)|^2  - |\delta(U)|^2 \\
&=  |\delta(U)|^2 + \epsilon^2 d \cdot |\delta(U)|^2 - |\delta(U)|^2 \\
&= \epsilon^2 d \cdot |\delta(U)|^2 \\
&= \epsilon^2 d \cdot \expct{e_H(\delta(U))}^2 \label{var:final}
\end{align}

Equation \ref{var:p} comes from our choice of $p = (\log n \cdot [1+ \epsilon^2 d])$. Equation \ref{var:omega} comes from the fact that the min-cut of $G$ has size $\omega(\log n)$, and thus every cut must have size greater than $\log n$, meaning $\log n \leq \delta(U)$ for every cut $U$. Equation \ref{var:final} comes from Claim \ref{expct_wH}.

This string of inequalities shows that $\var{w_H(\delta(U))} \leq \epsilon^2 d \cdot \expct{w_H(\delta(U))}^2$. By Chebyshev's inequality, we have that with probability $1-d$, we have $w_H(\delta(U)) \in (1\pm \epsilon) \cdot \expct{w_H(\delta(U))}$. But recall from Claim \ref{expct_wH} that $\expct{w_H(\delta(U))} = |\delta(U)|$, and thus with probability $1-d$, we have $w_H(\delta(U)) \in (1 \pm \epsilon) |\delta(U)|$. 

Since we chose the cut $U$ arbitrarily from $G$, this bound on $w_H(\delta(U))$ holds for each cut $U$ in $G$ with probability $1-d$. Then the probability of this bound failing for any given cut $U$ is $d$. Thus by the union bound, the probability of it failing for \textit{any} of the $2^n$ cuts of $G$ is at most $d\cdot 2^n$. Then the probability that this bound holds for every cut in $G$ is at least $1 - d \cdot 2^n$. But recall that we defined $d = \frac{\delta}{2^n}$. Thus the probability that this bound holds for every cut $U$ in $G$ is at least $1 - \delta$.

We have now proven that our choice of $p = 1 / (\log n \cdot [1 + \epsilon^2 \cdot \frac{\delta}{2^n}])$ ensures that with probability at least $1-\delta$ the algorithm returns a graph $H$ with the property that for every cut $U$ in $G$, we have $w_H(\delta(U)) \in (1\pm \epsilon) \cdot |\delta(U)|$. By definition, $H$ fulfills the requirements of being a $(1+\epsilon)$-cut sparsifier for $G$. 

We can finally conclude that for our choice of $p$, and the guarantee that the min-cut has size $\omega(\log n)$, the algorithm produces a $(1+\epsilon)$-cut sparsifier with arbitrarily high probability $1 - \delta$. 

\end{proof}

\section{}

\subsection*{a)}

Let $L$ denote the set of large coordinates. (i.e, $L = \{i:|y_i| > F / \log n\}$). Let $\ell = |L|$. 

\begin{claim}
$\expct{\ell} = O(\log^p n)$
\end{claim}

\begin{proof}
Note that $\expct{\ell} = \sum_{i=1}^{n} \prob{|y_i| > F / \log n}$. But 

\begin{align}
\prob{|y_i| > F / \log n} &= \prob{\frac{|f_i|}{|u_i|^{1/p}} > F / \log n} \\
&= \prob{u_i < \frac{|f_i|^p \cdot \log^p n}{F^p}} = \prob{u_i < \frac{|f_i|^p \cdot  \log^p n}{F_p}} \\
&= 1 - e^{-\frac{|f_i|^p \cdot  \log^p n}{F_p}} \label{exp} \\
&\approx 1 - \bigg(1 - \frac{|f_i|^p}{F_p \log^p n}\bigg) \label{taylor} \\
&=\frac{|f_i|^p \cdot  \log^p n}{F_p}
\end{align}

Where \ref{exp} comes from the fact that $u_i$ is chosen from the exponential distribution with CDF $1 - e^{-t}$. Additionally, $\ref{taylor}$ comes from the Taylor series approximation $e^{-x} \approx (1-x)$ for $x$ near 0.

We now have $\expct{\ell} = \sum_{i=1}^{n} \prob{|y_i| > F / \log n} \approx \sum_{i=1}^{n} \frac{|f_i|^p \cdot (\log^p n)}{F_p} = \frac{(\log^p n) \cdot (|f_1|^p + |f_2|^p + ... + |f_n|^p)}{F_p} = \frac{(\log^p n) \cdot F_p}{F_p} = \log^p n$. Thus $\expct{\ell} = \expct{|L|} \approx \log^p n$. Then $\expct{\ell} = \expct{|L|} = O(\log^p n)$, since big $O$ suppresses the approximation factor asymptotically.

\end{proof}

\subsection*{b)} 

We are interested in bounding the probability $p$ that there exists some $j\in[k]$ such that two or more members of $L$ hash to $j$. Let us consider the opposite case: every member of $L$ hashes to a unique value in $[k]$ (the probability of this occurring is $1-p$). Let $\ell =|L|$ as before.

If each $x_i \in L$ must hash to a unique $y_i \in [k]$, then there are $k$ choices for $h(x_1)$, since we can choose anything in $[k]$. There are $(k-1)$ choices for $h(x_2)$, since we can no longer choose $h(x_2) = h(x_1)$. There $(k-2)$ choices for $x_2$, since we can no longer choose $h(x_3) = h(x_2)$ or $h(x_1)$, and $h(x_2) \neq h(x_1)$. Generalizing this pattern, there are $(k- i +1)$ choices for $h(x_i)$, since there are now $i-1$ values in $[k]$ that we cannot choose (the $i-1$ unique value of $h(x_j)$ for $j<i$). 

So the total number of hash functions that map each member of $L$ to a unique value is $k \cdot (k-1) \cdot (k-2) \cdot ... \cdot (k-\ell+1) = \frac{k!}{\ell!}$. The total number of hash functions mapping $L$ to $k$ is $k^\ell$, so the probability of a randomly chosen has function being one of the $\frac{k!}{(k-\ell)!}$ that hashes every element of $L$ differently is $\frac{k!}{k^\ell \cdot(k-\ell)!}$. Then the probability of having some $j$ with two members of $L$ hashing onto it is the probability of this \textit{not} happening, which is $1 - \frac{k!}{k^\ell \cdot(k-\ell)!}$. 

We have now proven that $\prob{\exists j \text{ s.t. } |\{i \in L : h(i) =j \}| \geq 2} = 1 - \frac{k!}{k^\ell \cdot(k-\ell)!}$. Noting that the fractional term is equal to $\frac{k\cdot(k-1)(k-2)...(k-l+1)}{k^\ell} = \Pi_{i=1}^{\ell} \frac{k-i+1}{k}$ which is turn is $\geq \big(\frac{k-\ell+1}{k}\big)^\ell \geq \big(\frac{k-\ell}{k}\big)^\ell = (1-\frac{\ell}{k})^\ell$, which for $k \gg \ell$ can be approximated as $1 - \frac{\ell^2}{k}$. (Note that this analysis works because $k \gg |L|$ asymptotically in expectation.) 

Then we know that $\frac{k!}{k^\ell \cdot(k-\ell)!} \geq 1 - \frac{\ell^2}{k}$, and so $1 - \frac{k!}{k^\ell \cdot(k-\ell)!} \leq 1 - (1 - \frac{\ell^2}{k}) = \frac{\ell^2}{k}$. Therefore the probability of there being some $j \in [k]$ with two elements of $L$ hashing to it is at most $\frac{|L|^2}{k}$. But asymptotically $k \gg |L|$ in expectation, and so this probability will get very small as $n$ gets very large.

\subsection*{c)}

Recall that $S = \{i : |y_i| \leq F / \log n\}$, and $z_j' = \sum_{i\in S : h(i)=j} (\sigma_i y_i)$, where $\sigma_i$ are iid $\{-1,1\}$ variables.

\begin{claim}
$\var{z_j'} = O(F^2 / \log n)$
\end{claim}

\begin{proof}

First, note that $\expct{z_j'} = \sum_{i\in S : h(i)=j} (\expct{\sigma_i} \cdot \expct{y_i}) = \sum_{i\in S : h(i)=j} (0 \cdot \expct{y_i}) = 0$. Then we have  $\var{z_j'} = \expct{z_j'^2} - \expct{z_j'} = \expct{z_j'^2}$. We will now rewrite this as a sum over all $i$ in $n$, not just the small coordinates. Let $I_{i,j}$ denote the indicator variable that $h(i) = j$. Note that $\expct{I_{i,j}} = \prob{h(i) = j} = \frac{1}{k}$. Then $z_j' = \sum_{i=1}^{n} (\sigma_i \cdot y_i \cdot I_{i,j})$.

\begin{align}
\var{z_j'} = \expct{z_j'^2} &= \expct{\bigg( \sum_{i=1}^{n} (\sigma_i \cdot y_i \cdot I_{i,j}) \bigg)^2} \\
&= \expct{ \sum_{i=1}^{n} (\sigma_i ^2 \cdot y_i^2 \cdot I_{i,j}^2) + \sum_{i\neq h} (\sigma_i \sigma_h \cdot y_i y_h \cdot I_{i,j}I_{h,j})}\label{cross} \\
&= \expct{ \sum_{i=1}^{n} (\sigma_i ^2 \cdot y_i^2 \cdot I_{i,j}^2)} + \expct{\sum_{i\neq h} (\sigma_i \sigma_h \cdot y_i y_h \cdot I_{i,j} I_{h,j} ) } \\
&= \expct{ \sum_{i=1}^{n} (\sigma_i ^2 \cdot y_i^2 \cdot I_{i,j}^2)} + \sum_{i\neq h} (\expct{\sigma_i} \expct{\sigma_h} \cdot \expct{ y_i y_h \cdot I_{i,j}I_{h,j}) } \label{iid} \\
&= \expct{ \sum_{i=1}^{n} (\sigma_i ^2 \cdot y_i^2 \cdot I_{i,j}^2)} \label{byebye} \\
&= \expct{ \sum_{i=1}^{n} (y_i^2 \cdot I_{i,j}^2)} \label{sigmaboom} \\
&= \sum_{i=1}^{n} \expct{y_i^2} \cdot \expct{I_{i,j}^2} \\
&= \frac{1}{k} \cdot \sum_{i=1}^{n} \expct{y_i^2} \label{expI}
\end{align}

Where \ref{cross} comes from taking the square of the sum, and collecting all like and cross terms. \ref{iid} comes from the fact that $\sigma_i$ and $\sigma_k$ are i.i.d., andd thus independent from $y_i$, $y_h$, $I_{i,j}$, and $I_{h,j}$. \ref{byebye} comes from the fact that $\expct{\sigma_i} = 0$, and hence every term in the cross sum is 0. \ref{sigmaboom} comes from the fact that $\sigma_i \in \{-1, 1\}$, so $\sigma_i^2 = 1$. Finally, \ref{expI} comes from the fact (stated above) that $\expct{I_{i,j}} = \frac{1}{k}$. We then have 

\begin{align}
\var{z_j'} &= \frac{1}{k} \cdot \sum_{i=1}^{n} \expct{y_i^2} \\
&= \frac{1}{k} \cdot \sum_{i=1}^n \expct{\frac{|f_i|^2}{|u_i|^{2/p}}} \\
&= \frac{1}{k} \cdot \sum_{i=1}^n \expct{\frac{1}{|u_i|^{2/p}}} \expct{|f_i|^2} \label{ind_fu}\\
&= \frac{1}{k} \cdot \sum_{i=1}^n \expct{\frac{1}{u_i^{2/p}}} \expct{f_i^2} \label{signs} \\
&= \frac{g(p)}{k} \cdot \sum_{i=1}^n f_i^2 \label{gop} \\
&= O\bigg(\frac{1}{k} \cdot \sum_{i=1}^n f_i^2 \bigg) \label{bigO}
\end{align}

Where \ref{ind_fu} comes from the fact that $u_i$ and $f_i$ are independent. \ref{signs} comes from the fact that $(-x)^2 = (x)^2$, and so we can drop the signs of terms that get squared. \ref{gop} comes from that fact that the expectation of ${1}/{u_i^{2/p}}$ is just some function $g$ that depends only on $p$ (not on $k$, $i$ or $n$). \ref{bigO} comes from treating $p$ as a fixed constant. We now have 

\begin{align}
\var{z_j'} &= O\bigg(\frac{1}{k} \cdot \sum_{i=1}^n f_i^2 \bigg) \\
&= O\bigg(\frac{1}{n^{1-2/p} \log n} \cdot \sum_{i=1}^n f_i^2 \bigg) \label{kdef} \\
&= O\bigg(\frac{n^{2/p}}{\log n} \cdot \frac{\sum_{i=1}^n f_i^2}{n} \bigg) \label{brb}
\end{align}

Where \ref{kdef} comes from plugging in our choice of $k = n^{1-2/p} \log n$. We now take a brief detour to show the following claim, a direct result of Jensen's inequality:

\begin{claim}\label{jensen}
$n^{2/p} \cdot \frac{\sum_{i=1}^n f_i^2}{n} \leq F^2$
\end{claim}
\begin{proof}
By Jensen's inequality, we have 

\begin{align}
\bigg( \frac{\sum_{i=1}^n f_i^2}{n} \bigg)^{p/2} &\leq \frac{\sum_{i=1}^n (f_i^2)^{p/2}}{n} = \frac{\sum_{i=1}^n f_i^p}{n} \\
\bigg( \bigg( \frac{\sum_{i=1}^n f_i^2}{n} \bigg)^{p/2}\bigg)^{n/p} &\leq \bigg(\frac{\sum_{i=1}^n f_i^p}{n} \bigg)^{2/p} \\
\frac{\sum_{i=1}^n f_i^2}{n} &\leq \frac{((\sum_{i=1}^n f_i^p)^{1/p})^2}{n^{2/p}} \\
\frac{\sum_{i=1}^n f_i^2}{n} &\leq \frac{F^2}{n^{2/p}} \label{noF}\\
n^{2/p} \cdot \frac{\sum_{i=1}^n f_i^2}{n} &\leq F^2
\end{align}

Where \ref{noF} comes from our definition of $F = F_p^{1/p} = \big(\sum_{i=1}^n f_i^p\big)^{1/p}$.

\end{proof}

Returning to equation \ref{brb}, we have:

\begin{align}
\var{z_j'} &= O\bigg(\frac{n^{2/p}}{\log n} \cdot \frac{\sum_{i=1}^n f_i^2}{n} \bigg) \\
&= O\bigg(\frac{F^2}{\log n}\bigg) \label{claim}
\end{align}

Where \ref{claim} follows directly from the inequality proven in Claim \ref{jensen} (because $O(X) = O(Y)$ for and $X \leq Y$). Thus, we have finally proven that $\var{z_j'} = O(F^2 / \log n)$ as desired.

\end{proof}

\end{document}
