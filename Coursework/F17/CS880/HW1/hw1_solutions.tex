\documentclass[11pt]{article}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Header borrowed from Shuchi Chawla %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{fullpage}
\usepackage{epsfig}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amstext}
\usepackage{amsmath}
\usepackage{xspace}
\usepackage{theorem}
\usepackage{hyperref}

% This is the stuff for normal spacing
\makeatletter
 \setlength{\textwidth}{6.5in}
 \setlength{\oddsidemargin}{0in}
 \setlength{\evensidemargin}{0in}
 \setlength{\topmargin}{0.25in}
 \setlength{\textheight}{8.25in}
 \setlength{\headheight}{0pt}
 \setlength{\headsep}{0pt}
 \setlength{\marginparwidth}{59pt}

 \setlength{\parindent}{0pt}
 \setlength{\parskip}{5pt plus 1pt}
 \setlength{\theorempreskipamount}{5pt plus 1pt}
 \setlength{\theorempostskipamount}{0pt}
 \setlength{\abovedisplayskip}{8pt plus 3pt minus 6pt}

 \renewcommand{\section}{\@startsection{section}{1}{0mm}%
                                   {2ex plus -1ex minus -.2ex}%
                                   {1.3ex plus .2ex}%
                                   {\normalfont\Large\bfseries}}%
 \renewcommand{\subsection}{\@startsection{subsection}{2}{0mm}%
                                     {1ex plus -1ex minus -.2ex}%
                                     {1ex plus .2ex}%
                                     {\normalfont\large\bfseries}}%
 \renewcommand{\subsubsection}{\@startsection{subsubsection}{3}{0mm}%
                                     {1ex plus -1ex minus -.2ex}%
                                     {1ex plus .2ex}%
                                     {\normalfont\normalsize\bfseries}}
 \renewcommand\paragraph{\@startsection{paragraph}{4}{0mm}%
                                    {1ex \@plus1ex \@minus.2ex}%
                                    {-1em}%
                                    {\normalfont\normalsize\bfseries}}
 \renewcommand\subparagraph{\@startsection{subparagraph}{5}{\parindent}%
                                       {2.0ex \@plus1ex \@minus .2ex}%
                                       {-1em}%
                                      {\normalfont\normalsize\bfseries}}
\makeatother

\newenvironment{proof}{{\bf Proof:  }}{\hfill\rule{2mm}{2mm}}
\newenvironment{proofof}[1]{{\bf Proof of #1:  }}{\hfill\rule{2mm}{2mm}}
\newenvironment{proofofnobox}[1]{{\bf#1:  }}{}
\newenvironment{example}{{\bf Example:  }}{\hfill\rule{2mm}{2mm}}

\renewcommand{\theequation}{\thesection.\arabic{equation}}
\renewcommand{\thefigure}{\thesection.\arabic{figure}}

\newtheorem{fact}{Fact}[section]
\newtheorem{lemma}[fact]{Lemma}
\newtheorem{theorem}[fact]{Theorem}
\newtheorem{definition}[fact]{Definition}
\newtheorem{corollary}[fact]{Corollary}
\newtheorem{proposition}[fact]{Proposition}
\newtheorem{claim}[fact]{Claim}
\newtheorem{exercise}[fact]{Exercise}

% math notation
\newcommand{\R}{\ensuremath{\mathbb R}}
\newcommand{\Z}{\ensuremath{\mathbb Z}}
\newcommand{\N}{\ensuremath{\mathbb N}}
\newcommand{\F}{\ensuremath{\mathcal F}}
\newcommand{\SymGrp}{\ensuremath{\mathfrak S}}

\newcommand{\CCP}{\ensuremath{\sf P}}
\newcommand{\CCNP}{\ensuremath{\sf NP}}
\newcommand{\C}{\ensuremath{\mathcal C}}

\newcommand{\size}[1]{\ensuremath{\left|#1\right|}}
\newcommand{\ceil}[1]{\ensuremath{\left\lceil#1\right\rceil}}
\newcommand{\floor}[1]{\ensuremath{\left\lfloor#1\right\rfloor}}
\newcommand{\poly}{\operatorname{poly}}
\newcommand{\polylog}{\operatorname{polylog}}

% some abbreviations
\newcommand{\e}{\epsilon}
\newcommand{\half}{\ensuremath{\frac{1}{2}}}
\newcommand{\junk}[1]{}
\newcommand{\sse}{\subseteq}
\newcommand{\union}{\cup}
\newcommand{\meet}{\wedge}

\newcommand{\prob}[1]{\ensuremath{\text{{\bf Pr}$\left[#1\right]$}}}
\newcommand{\expct}[1]{\ensuremath{\text{{\bf E}$\left[#1\right]$}}}
\newcommand{\var}[1]{\ensuremath{\text{{\bf Var}$\left[#1\right]$}}}
\newcommand{\Event}{{\mathcal E}}

\newcommand{\mnote}[1]{\normalmarginpar \marginpar{\tiny #1}}

\newcommand{\headings}[4]{
\noindent
\fbox{\parbox{\textwidth}{
{\bf CS880: Algorithms for Massive Datasets} \hfill {{\bf} #2}\\
\vspace{0.02in}\\
{{\bf HW#1 Solutions}} \hfill {{\bf Date:} #3}
}}
\vspace{0.2in}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Document begins here %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\newcommand{\lecnum}{1}
\headings{\lecnum}{Sam Vinitsky (with Zachary Charles)}{9/25/2017}

\section{} % PROBLEM 1

As we frequently do in class, we will consider the squared $\ell_2$ norm as a suitable distance metric. We must then compute $\expct{\|y-x\|_2^2}$. We do so via some straightforward simplification.

\begin{align}
\expct{\|y-x\|_2^2} &= \expct{\sum_{i=1}^d (y_i-x_i)^2} \label{eq:one} \\
&= d \cdot \expct{(y_i-x_i)^2} \label{eq:two} \\
&= d \cdot [4 \cdot \prob{y_i \neq x_i} + 0 \cdot \prob{y_i=x_i} ] \label{eq:three}\\
&= d \cdot [4 \cdot \half] \label{eq:four} \\
&= 2d \label{eq:five}
\end{align}

Where equation \ref{eq:three} follows from the definition of expectation and the fact that $(y_i-x_i)^2 = 4$ when $y_i \neq x_i$, since $y_i, x_i \in \{-1,1\}$.

Given a pair of vectors $x$ and $y$ from $S$, we will now compute the probability that $\| y - x \|_2^2 \in (1 \pm \e) \cdot \expct{\|y-x\|_2^2} = (1 \pm \e) 2d$. We will first compute the probability that our distance is too large, meaning $\| y - x \|_2^2 > (1 + \e)2d$. Let $T_i$ denote a random variable that is takes on the value $T_i = 0$ when $x_i = y_i$, and $T_i 4$ when $x_i \neq y_i$. Let $T = \sum_i T_i$. Note that $T = \| y - x \|_2^2$, since $\| y - x \|_2^2 = \sum_i (y_i - x_i)^2$. But clearly $(y_i - x_i)^2 = T_i$, and thus $\|y_i - x_i \|_2^2 = T$. Thus the probability we are after is 

\begin{align} 
\prob{T > (1+\e)2d} &= \prob{T - 2d > 2\e d} \\
&\leq \prob{|T - 2d| > 2 \e d} \\
&= \prob{|T - \expct{T}| > 2 \e d}
\end{align}

Since $T$ is the sum of random ${0,4}$ variables, we can use a special form of the Chernoff bound that we discussed in class for the sum of bounded random variables. Then 

\begin{align}
\prob{\| y - x \|_2^2 > (1 + \e)2d} &\leq \prob{|T - \expct{T}| > 2 \e d} \\
&\leq 2\exp \bigg(\frac{-2(2\e d )^2}{\sum_{i=1}^{d} (4 - 0)}\bigg) \\
&=2e^{-2\e^2 d}
\end{align}

Therefore the probability that $\|y - x\|_2^2$ is not in the range $(1 \pm \e)2d$ is equal to $\prob{\|y-x\|_2^2 < (1- \e)(2d)} + \prob{\|y-x\|_2^2 > (1 + \e)(2d)}$, which by symmetry is equal to $2  \cdot \prob{\|y-x\|_2^2 > (1 + \e)(2d)} = 4e^{-2\e^2d}$. Thus the probability of having $\|y - x\|_2^2$ be within $(1 \pm \e)$ of its expectation is $1 - 4e^{-2\e^2d}$.

We will now derive the largest possible $|S| = n$ for which there is a non-zero probability of having the distance between every pair of vectors in $S$ be within $(1 \pm \e)$ of its expectation. Let $Z_S$ denote the number of pairs of vectors in $S$ which have distance between them within $(1 \pm \e)$ of its expectation. Then we are interested in the largest possible $S$ such that $\prob{Z_S = 0} > 0$, or conversely $\prob{Z_S \geq 1} < 1$. By using the union bound, we note that 

\begin{align}
\prob{Z_S \geq 1} &\leq n^2 \prob{\|y-x\|_2^2 \in (1 \pm e) \cdot \expct{\|y-x\|_2^2}} \\
&= n^2 \cdot 4e^{-2\e^2 d}
\end{align}

Note that if $n^2 \cdot 4e^{-2\e^2 d} < 1$, then $\prob{Z_S \geq 1} < 1$ by the transitive property. It is easy to see that $n < 2e^{\e^2d}$ suffices. Thus the largest approximately equidistant set $S$ (when considering the $\ell_2^2$ norm) must have size $O(e^{\e^2d})$.

\section{} % PROBLEM 2

Suppose we have run the Misra-Gries algorithm for heavy hitters on two streams $\sigma_1$ and $\sigma_2$ of length $m_1$ and $m_2$ respectively. From class, this algorithm (when run on stream $\sigma_1$) returns a list $L_1$ of indices and counts of elements in $\sigma_1$ with the guarantee that $\text{count}_i^1$ is in the range $[f_i^1 - m_1 / k, f_i^1]$, where $f_i^1$ denotes the frequency of $i$ in $\sigma_1$ (and $\text{count}_i^1$ is the reported count for element $i$ from the algorithm run on stream $\sigma_1$.

In order to produce such a list $L$ of the $\frac{1}{k}$-heavy hitters of $\sigma_1 \cup \sigma_2$, we do the following: 

\begin{enumerate}

\item For each $i$ in $L_1 \cap L_2$:

\begin{enumerate}

\item Put $i$ into $L$, and set $\text{count}_i = \text{count}_i^1 + \text{count}_i^2$

\end{enumerate}

\item While $|L| < k-1$:

\begin{enumerate}

\item Pick the $i$ in $L_1$ or $L_2$ with largest count that isn't already in $L$, and put it into $L$.

\item Set $\text{count}_i = \text{count}_i^1 + \text{count}_i^2$

\end{enumerate}

\end{enumerate}

Clearly at the end of our algorithm, $|L| = k-1$. For each $i$ in $L$, either $i \in L_1 \cap L_2$ or $i$ was only in one of them. We look at each case separately, and prove that the count falls into the required range.

For each $i$ in both $L_1$ and $L_2$ (of which there can be at most $|L_1| = k-1$), the count of $i$ in $L$ will be $\text{count}_i = \text{count}_i^1 + \text{count}_i^2$. But we noted above that we know $\text{count}_i^1$ is in the range $[f_i^1 - m_1 / k, f_i^1]$, and similarly for $\text{count}_i^2$. Therefore $\text{count}_i$ is in the range $[f_i^1 - \frac{m_1}{k} + f_i^2 - \frac{m_2}{k}, f_i^1 + f_i^2]$. Note that $f_i = f_i^1 + f_i^2$, which simplifies the lower bound. Thus $\text{count}_i \in [f_i - \frac{m_1+m_2}{k}, f_i]$, where $m_1+m_2$ is the length of $\sigma_1 \cup \sigma_2$. Thus count$_i$ is guaranteed to be in the desired range for any $i \in L_1 \cap L_2$.

Consider an element $i$ that is only in one of $L_1$ or $L_2$ (w.l.o.g. assume $i\in L_1$). Note that since $i \notin L_2$, then from our analysis in class, it must be the case that $f_i^2 \leq \frac{m_2}{k}$ and $\text{count}_i^2 = 0$. Then $\text{count}_i = \text{count}_i^1$, and thus $\text{count}_i$ is in the range $[f_i^1 - \frac{m_1}{k}, f_i^1]$. But $f_i = f_i^1 + f_i^2 \geq f_i^1$, and thus $[f_i^1 - \frac{m_1}{k}, f_i^1] \subseteq [f_i^1 - \frac{m_1}{k}, f_i]$. Recall that count$_i \in [f_i^1 - \frac{m_1}{k}, f_i^1]$, since it was produce by the Misra-Gries algorithm. Thus $\text{count}_i$ in the first range means it must also fall into the second, larger, range. But note that 

\begin{align}
f_i - \frac{m_1+m_2}{k} &= f_i^1 + f_i^2 - \frac{m_1+m_2}{k} \label{eq2:one}\\
&\leq f_i^1 + \frac{m_2}{k} - \frac{m_1+m_2}{k} \label{eq2:two} \\
&= f_i^1 - \frac{m_1}{k} \label{eq2:three}
\end{align}

Where equation \ref{eq2:two} follows from the fact that $f_i^2 \leq \frac{m_2}{k}$. Thus $f_i - \frac{m_1+m_2}{k} \leq f_i^1 - \frac{m_1}{k}$, meaning $[f_i^1 - \frac{m_1}{k}, f_i] \subseteq [f_i - \frac{m_1+m_2}{k}, f_i]$. As before, $\text{count}_i$ is in the first range, and thus it must fall into the second, larger range as well. This which proves our guarantee for any $i$ that is only in one of $L_1$ or $L_2$. As we have already proved the claim for any $i$ in their intersection, we are done.

\section{} % PROBLEM 3

\subsection*{Reservoir Sampling Algorithm}

We present an algorithm for maintaining a random sample of size $k > 1$ from a stream:

\begin{enumerate}

\item Initialize $c = 0$, initialize $x_j =$ null for all $j \in [k]$

\item When we see an element $y$ in the stream:

\begin{enumerate}

\item $c$++.

\item If there is some null $x_j$, set $x_j = y$.

\item Otherwise, with probability $\frac{k}{c}$, choose a random $j\in [k]$ and set $x_j = y$. (With probability $1 - \frac{k}{c}$ do nothing.)

\end{enumerate}

\end{enumerate}

Consider the probability that the $i$th element (call it $y$) in our stream ends up in the list $\{x_j\}$'s of length $k$ after some $m$ elements have been seen. This is only possible if $y$ was chosen when it was seen (which occurs with probability $\frac{k}{i}$), and it was never ``overwritten'' by another element. Note that the probability of never being overwritten is

\begin{align}
&\prod_{j=i+1}^{m} \big(1 - \prob{\text{$y$ is overwritten by element $j$}}\big) \label{eq:threeOne} \\
={}&\prod_{j=i+1}^{m} \big(1 - \frac{1}{k} \cdot \frac{k}{j}\big) \label{eq:threeTwo}\\
={}& \prod_{j=i+1}^{m} \big(1 - \frac{1}{j}\big) \label{eq:threeThree}
\end{align}

Where \ref{eq:threeTwo} comes from the fact that the probability of $y$ being overwritten by element $j$ is the probability that $j$ is picked ($\frac{k}{j}$) times the probability that $y$ is picked to get kicked out of the random sample ($\frac{1}{k})$. Thus the total probability of $y$ (which is $i$th element) ending up in our sample of size $k$ from $m$ elements is 

\begin{align}
& \frac{k}{i} \cdot \prod_{j=i+1}^{m} \big(1 - \frac{1}{j}\big) \label{eq:threeThree} \\
={}& \frac{k}{i} \cdot \prod_{j=i+1}^{m} \big(\frac{j-1}{j}\big) \label{eq:threeFour} \\
={}& \bigg( \frac{k}{i} \bigg) \bigg( \frac{i}{i+1} \bigg) \bigg( \frac{i+1}{i+2} \bigg) \cdots \bigg(\frac{m-1}{m}\bigg) \label{eq:threeFiveo} \\
={}& \frac{k}{m} \label{eq:threeSixxx}
\end{align}

Which is exactly as it should be. Since we are taking a sample of size $k$ from a set of size $m$, each element should be selected with an equal probability of $\frac{k}{m}$. Note that the total space required is $O(k \log m)$

\subsection*{Approximate Median Streaming Algorithm}

We present an algorithm to find the approximate median of a stream with accuracy $\delta$

\begin{enumerate}

\item Run the above reservoir sampling algorithm on the stream with $k= \frac{1}{\e^2\delta}$.

\item Return the mean $X$ of the $k$ samples returned by the reservoir sampling algorithm.

\end{enumerate}

Simple computations will show $\expct{X} = \frac{m}{2}$, and $\var{X} = \frac{1}{k} \cdot \frac{1}{12} (m^2 +6m + 12)$. Note that $\frac{1}{12} (m^2 +6m + 12) \leq \frac{m^2}{4} = \expct{X}^2$ for large enough $m$, and thus $\var{X} = \e^2 \delta \cdot \frac{1}{12} (m^2 +6m + 12) \leq \e^2 \delta \frac{m^2}{4} \leq \e^2 \delta \expct{X}^2$. Thus we conclude that $\var{X} \leq \e^2\delta \expct{X}^2$. Therefore by Chebyshev's inequality, we know that with probability $1 - \delta$, our estimator $X$ falls into the range $(1 \pm \e) \expct{X} = (1 \pm \e) \frac{m}{2}$ as desired. This means that with probability $1 - \delta$, we can compute the approximate median. In terms of space complexity, this algorithm requires we store $k$ pointers of size $\log m$, thus the total space required is $O(k\log m)$, plus the space required to run the sampling algorithm, which was $O(k \log m$). Thus the total space required for our chosen $k = \frac{1}{\e^2\delta}$ is $O(\frac{1}{\e^2\delta} \log m)$.

\section{} % PROBLEM 4

\subsection*{Updated Sampling Algorithm For Last $w$}

We present a new sampling algorithm to keep track of a random sample of size $k$ from the last $w$ elements seen:

\begin{enumerate}

\item Run the Reservoir Sampling Algorithm from problem 3 until we have seen $w$ elements to get a sample of size $k$.

\item When we see element number $y$, do the following:

\begin{enumerate}

\item If $y - w$ is in the current sample, replace it with $y$.

\item Otherwise, do nothing.

\end{enumerate}

\end{enumerate}

Let $X_n$ denote the sample after we've seen element number $n$. That is, $X_n$ denotes our sample of size $k$ from $\{n - w + 1,...,i-1,n\}$. We will prove this algorithm is correct by using induction on $n$ to show that $X_n$ is indeed a random sample (with $n=w$ as the base case.) Clearly $X_w$ as found in step 1 of the algorithm is a random sample, since it is the result of our Reservoir Sampling Algorithm, whose correctness we proved in problem 3. Assume for all $n' <i$ that $X_{n'}$ is a random sample. We now wish to prove that $X_n$ is a random sample. Consider some $i \in \{n-w,...,n-1\}$. Then by the inductive hypothesis, $\prob{i \in X_{n-1}} = \frac{k}{w}$. When we construct $X_n$ from $X_n-1$, we never alter any elements in the range $\{n-w+1,...,n-1\}$. Therefore each element in that range must be in $X_n$ with probability $\frac{k}{w}$. Consider the probability that $n \in X_n$. This can only happen if $n-w \in X_{n-1}$, which by the inductive hypothesis happens with probability $\frac{k}{w}$. So $\prob{n \in X_n} = \frac{k}{w}$ as desired. We have now shown that each element in $\{n - w + 1,...,i-1,n\}$ is in $X_n$ with equal probability. Clearly nothing else can be in $X_n$. The only candidate would be something that was in $X_{n-1}$, which by assumption is a subset of $\{n - w,...,i-1,n-1\}$, so the only possible offender is $n-w$. But if $n-w$ were in the current sample, we would replace it with $n$ in step 2a. Therefore $X_n$ contains only elements from $\{n - w + 1,...,i-1,n\}$, selected with equal probability. Thus $X_n$ is indeed a random sample of the last $w$ elements, for any $n \geq w$. This means our algorithm is indeed correct.

\subsection*{Updated Median Algorithm For Last $w$}

We present an updated algorithm to compute the approximate median of the last $w$ elements. 

\begin{enumerate}

\item Run the updated sampling algorithm for $w$ on the stream with $k= \frac{1}{\e^2\delta}$.

\item Return the mean $X$ of the $k$ samples returned by the reservoir sampling algorithm.

\end{enumerate}

Analysis of this algorithm is identical to what we did in problem 3, with $m$ replaced by $w$. That is, we now have $\expct{X} = \frac{w}{2}$, and $\var{X} = \frac{1}{k} \cdot \frac{1}{12} (w^2 +6w + 12)$. The rest of analysis follows easily in the same manner (we omit it here for brevity). We can conclude that, as before, with probability $1 - \delta$, our estimator $X$ falls into the range $(1 \pm \e) \expct{X} = (1 \pm \e) \frac{w}{2}$ as desired. This means that with probability $1 - \delta$, we computed the approximate median. The space requirement for this algorithm is now $O(\frac{1}{\e^2\delta} \log w)$ as before, since we can now store pointers of size $\log w$. In order to achieve this, we just store the $i$th element in the stream as $(i \bmod w)$, since we never care about more than $w$ elements at a time.

\subsection*{Independent Queries}

Let us consider the case where we wish to compute the median of the last $w$ elements every time we see a new element. That is, we will run the above median-finding algorithm $m$ times per stream. We would like it to be the case that we can bound that, with high probability, every approximate median computation was correct (i.e. we never returned a median with rank outside of $(1 \pm \e)\frac{w}{2}$). In order to achieve this goal, we need to modify our sampling algorithm so that different queries are independent of one another. If we can do this, then the probability of success becomes $(1-\delta)^m$, which can be made arbitrarily close to 1 by selecting an appropriate $\delta$. The current sampling algorithm clearly does not suffice for this purpose, since each $y$ is in sample $X_n$ if and only if $y-w$ was in $X_{n-w}$.

We now present a median algorithm that guarantees pairwise independence for any two medians with probability $p$. 

\begin{enumerate}

\item Let $t = \frac{1}{1-\log_{w^2} p}$. Run $t$ separate instances of our last-$w$ sampling algorithm (of size $k = \frac{1}{\e^2\delta}$).

\item To compute the median for the last $w$, choose some $i \in [t]$ at random, and use the $i$th instance of the sampling algorithm as our sample.

\item Once we have our sample of size $k$, return the mean $X$ of the $k$ samples.

\end{enumerate}

Clearly this algorithm still correctly computes the approximate median of the last $w$ values. With probability $\frac{t-1}{t}$, each pair of calls to this algorithm are independent. The probability that all $w^2$ pairs of calls are independent is $\frac{t-1}{t}^{w^2}$. So with probability $p$, we have pairwise independence of our median estimates, and we can apply the analysis above to show that with probability $p(1-\delta)^m$, our algorithm gives us an approximate median in the range $(1 \pm \e)\frac{w}{2}$) every time we call it for the whole stream of length $m$. 

\end{document}
