\documentclass[11pt]{article}

\usepackage{enumerate}
\usepackage{color}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Header: DO NOT MODIFY %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{fullpage}
\usepackage{epsfig}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amstext}
\usepackage{amsmath}
\usepackage{xspace}
\usepackage{theorem}

% This is the stuff for normal spacing
\makeatletter
 \setlength{\textwidth}{6.5in}
 \setlength{\oddsidemargin}{0in}
 \setlength{\evensidemargin}{0in}
 \setlength{\topmargin}{0.25in}
 \setlength{\textheight}{8.25in}
 \setlength{\headheight}{0pt}
 \setlength{\headsep}{0pt}
 \setlength{\marginparwidth}{59pt}

 \setlength{\parindent}{0pt}
 \setlength{\parskip}{5pt plus 1pt}
 \setlength{\theorempreskipamount}{5pt plus 1pt}
 \setlength{\theorempostskipamount}{0pt}
 \setlength{\abovedisplayskip}{8pt plus 3pt minus 6pt}

 \renewcommand{\section}{\@startsection{section}{1}{0mm}%
                                   {2ex plus -1ex minus -.2ex}%
                                   {1.3ex plus .2ex}%
                                   {\normalfont\Large\bfseries}}%
 \renewcommand{\subsection}{\@startsection{subsection}{2}{0mm}%
                                     {1ex plus -1ex minus -.2ex}%
                                     {1ex plus .2ex}%
                                     {\normalfont\large\bfseries}}%
 \renewcommand{\subsubsection}{\@startsection{subsubsection}{3}{0mm}%
                                     {1ex plus -1ex minus -.2ex}%
                                     {1ex plus .2ex}%
                                     {\normalfont\normalsize\bfseries}}
 \renewcommand\paragraph{\@startsection{paragraph}{4}{0mm}%
                                    {1ex \@plus1ex \@minus.2ex}%
                                    {-1em}%
                                    {\normalfont\normalsize\bfseries}}
 \renewcommand\subparagraph{\@startsection{subparagraph}{5}{\parindent}%
                                       {2.0ex \@plus1ex \@minus .2ex}%
                                       {-1em}%
                                      {\normalfont\normalsize\bfseries}}
\makeatother

\newenvironment{proof}{{\bf Proof:  }}{\hfill\rule{2mm}{2mm}}
\newenvironment{proofof}[1]{{\bf Proof of #1:  }}{\hfill\rule{2mm}{2mm}}
\newenvironment{proofofnobox}[1]{{\bf#1:  }}{}
\newenvironment{example}{{\bf Example:  }}{\hfill\rule{2mm}{2mm}}
\renewcommand{\thesection}{\lecnum.\arabic{section}}

\renewcommand{\theequation}{\thesection.\arabic{equation}}
\renewcommand{\thefigure}{\thesection.\arabic{figure}}

\newtheorem{fact}{Fact}[section]
\newtheorem{lemma}[fact]{Lemma}
\newtheorem{theorem}[fact]{Theorem}
\newtheorem{definition}[fact]{Definition}
\newtheorem{corollary}[fact]{Corollary}
\newtheorem{proposition}[fact]{Proposition}
\newtheorem{claim}[fact]{Claim}
\newtheorem{exercise}[fact]{Exercise}

% math notation
\newcommand{\R}{\ensuremath{\mathbb R}}
\newcommand{\Z}{\ensuremath{\mathbb Z}}
\newcommand{\N}{\ensuremath{\mathbb N}}
\newcommand{\F}{\ensuremath{\mathcal F}}
\newcommand{\SymGrp}{\ensuremath{\mathfrak S}}

\newcommand{\CCP}{\ensuremath{\sf P}}
\newcommand{\CCNP}{\ensuremath{\sf NP}}
\newcommand{\C}{\ensuremath{\mathcal C}}

\newcommand{\size}[1]{\ensuremath{\left|#1\right|}}
\newcommand{\ceil}[1]{\ensuremath{\left\lceil#1\right\rceil}}
\newcommand{\floor}[1]{\ensuremath{\left\lfloor#1\right\rfloor}}
\newcommand{\poly}{\operatorname{poly}}
\newcommand{\polylog}{\operatorname{polylog}}

% some abbreviations
\newcommand{\e}{\epsilon}
\newcommand{\half}{\ensuremath{\frac{1}{2}}}
\newcommand{\junk}[1]{}
\newcommand{\sse}{\subseteq}
\newcommand{\union}{\cup}
\newcommand{\meet}{\wedge}

\newcommand{\prob}[1]{\ensuremath{\text{{\bf Pr}$\left[#1\right]$}}}
\newcommand{\expct}[1]{\ensuremath{\text{{\bf E}$\left[#1\right]$}}}
\newcommand{\Event}{{\mathcal E}}

\newcommand{\mnote}[1]{\normalmarginpar \marginpar{\tiny #1}}

\newcommand{\headings}[4]{
\noindent
\fbox{\parbox{\textwidth}{
{\bf CS880: Algorithms for Massive Datasets} \hfill {{\bf Scribe:} #3}\\
\vspace{0.02in}\\
{{\bf Lecture #1:} #2} \hfill {{\bf Date:} #4}
}}
\vspace{0.2in}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Document begins here %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% whoops I modified
\newcommand*{\mathcolor}{}
\def\mathcolor#1#{\mathcoloraux{#1}}
\newcommand*{\mathcoloraux}[3]{%
  \protect\leavevmode
  \begingroup
    \color#1{#2}#3%
  \endgroup
}

\begin{document}


%% Fill in lecture number, topic, your name, and date of lecture below. %%

\newcommand{\lecnum}{2}
\headings{\lecnum}{Heavy Hitters}{Sam Vinitsky}{9/8/2017}

\section{Introduction}

In the previous lecture, we discussed a streaming algorithm to solve the toy problem of counting the number of elements seen in a stream. We used randomness and approximation to save exponential space. We also introduced the ``median of means'' trick for boosting the confidence and accuracy of an existing unbiased estimator. In this lecture, we will begin our discussion of algorithms for finding more interesting statistics of a stream. We will begin by attempting to find the most prominent elements in a stream.

Recall from the previous lecture that a stream is a multiset of elements form ${1,...,n}$, where we think of seeing the elements ``fly by'' one at a time. Let $f_i$ denote the frequency of element $i$ in the stream. Suppose we want to estimate these frequencies. This is very expensive in terms of the space needed, requiring $O(n \log \log m)$ space for a stream of length $m$. In real life application where we would use streaming algorithms (router traffic, analyzing Google search queries), $n$ might be in the billions. Thus a space requirement that is linearly dependent on $n$ is far too large. 

\section{Heavy Hitters}

Since estimating the frequencies of all elements in the stream in infeasible due to space concerns, we will instead focus on estimating only the most frequent elements. A \textbf{$\theta$-heavy hitter} is an element $i$ with $f_i > \theta m$ for some constant $\theta$. For example, we may wish to find every element that occurs more than $\frac{1}{10}m$ times in the stream. Note that there can be at most 9 such elements (since each one occurs \textit{more} than $\frac{1}{10}$ of the time). This suggests that we should be able to determine the frequencies of all $\theta$-heavy hitters in $O(\frac{1}{\theta}\log m)$ space, since there can be at most $\frac{1}{\theta}-1$ elements that are $\theta$-heavy hitters, and the count for these elements are at most $\log m$ bits each. % Indeed, if we know in advance which elements are $\theta$-heavy hitters, we can easily maintain their counts in $O(\frac{1}{\theta}\log m)$ space. The difficulty here is that to start, we do not know which elements are $\theta$-heavy hitters. Instead, we 

\subsection{Finding the Majority Element}

We now present a deterministic algorithm for solving a special case of $\theta$-heavy hitters problem where $\theta = \frac{1}{2}$, i.e. we are just trying to find the majority element in a stream. Note that there is at most \textit{one} such element. The algorithm is as follows:

\begin{enumerate}[]

\item \textbf{Algorithm for finding the majority element in a stream:} ($\theta = \frac{1}{2}$)

\begin{enumerate}[1.]

\item Initialize index $= \bot$, count $= 0$

\item Upon receiving element $i$:

  \begin{enumerate}[(a)]

  \item if index $==i$, count$++$ % i swapped the order of this and the next one to make the other algorithm read better

  \item if index $==\bot$, set index $=i$, count $=1$

  \item else:

    \begin{enumerate}[i.]

    \item count$--$
    \item if count$==0$,  index$= \bot$ 

    \end{enumerate}

  \end{enumerate}

\item Return index

\end{enumerate}

\end{enumerate}

\begin{theorem}
After this algorithm terminates, ``index'' will always be the majority element of the stream if one exists.
\end{theorem}

\begin{proof}

Suppose that $k$ is the most frequent element in the stream. Then there must be at least $\floor{\frac{m}{2}} + 1$ occurrences of $k$ in the stream, and only $\floor{\frac{m}{2}}$ occurrences of any other element. Every element we see causes the count to increment or decrement by 1. Of all occurrences of $k$, at most $\floor{\frac{m}{2}}$ can decrement the count, since they can only decrement the count from a non-$k$ element, of which there are most $\floor{\frac{m}{2}}$. Since we have $\floor{\frac{m}{2}} + 1$ copies of $k$, and at most $\floor{\frac{m}{2}}$ can decrement the count, at least 1 must cause it to increment, for index$=k$. Therefore the algorithm must end with $k$ in index, and a positive count. 

\end{proof}

Therefore, if a majority element exists in the stream, this algorithm will return it. If no such element exists, then index could be anything, and thus the output is not meaningful in this case. Additionally, count gives us no indication of how many copies of index actually exist in the stream (even if it is a majority element, we just know it is in the range $(\frac{m}{2},m]$). 

This deterministic algorithm can be extended to find all $\theta$-heavy hitters. We simply keep track of $k-1$ different counts (where $k = \frac{1}{\theta}$) since, as before, there can be at most $k-1$ elements that are $\theta$-heavy hitters. The modified algorithm is given below, with additions denoted in \textcolor{red}{red}:

\begin{enumerate}[]

\item \textbf{Algorithm for finding $\theta$-heavy hitters in a stream:} ($\theta \mathcolor{red}{<} \frac{1}{2}$) (Misra \& Gries, 1982)

\begin{enumerate}[1.]

\item Initialize index$_{\mathcolor{red}{j}}$ $= \bot$, count$_{\mathcolor{red}{j}}$ $= 0$,\ \ \ $\mathcolor{red}{(\forall j \in \{1,...,k-1\})}$

\item Upon receiving element $i$:

  \begin{enumerate}[(a)]

  \item if $\mathcolor{red}{\exists}$index$_{\mathcolor{red}{j}}$ $==i$, count$_{\mathcolor{red}{j}}$$++$

  \item if $\mathcolor{red}{\exists}$index$_{\mathcolor{red}{j}}$ $==\bot$, set index$_{\mathcolor{red}{j}}$ $=i$, count$_{\mathcolor{red}{j}}=1$

  \item else:

    \begin{enumerate}[i.]

    \item count$_{\mathcolor{red}{j}}$$--$,\ \ \ $\mathcolor{red}{(\forall j \in \{1,...,k-1\})}$
    \item if count$_{\mathcolor{red}{j}}$$==0$,  index$_{\mathcolor{red}{j}}$$= \bot$,\ \ \ $\mathcolor{red}{(\forall j \in \{1,...,k-1\})}$

    \end{enumerate}

  \end{enumerate}

\item Return \textcolor{red}{the list \{}index$_{\mathcolor{red}{j}}$\textcolor{red}{\}}

\end{enumerate}

\end{enumerate}

We will now argue the correctness of this algorithm.

\begin{theorem}
Every $\frac{1}{k}$-heavy hitter belongs to the list $\{index_j\}$.
\end{theorem}

\begin{proof}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The only time that we decrement a count is if every index is assigned, and we see an element that is not currently in $\{index_j\}$. If this happens, we decrement the count of $k$ distinct elements (one for each index location). Clearly we can do this at most $\frac{m}{k}$ times in total (If we decremented $k$ items more than $\frac{m}{k}$ times, then we would have decremented more then $m$ times in total, which is impossible). Note that we increment the count for $i$ exactly $f_i$ times. Therefore our final estimate for the count of element $i$ will be in the interval $[f_i - \frac{m}{k}, f_i]$. Thus is $f_i > \frac{m}{k}$, which is this case if $i$ is a $\frac{1}{k}$-heavy hitter, we are guaranteed a non-zero count for element $i$, and thus it will be in some index$_j$. (Some proof details due to \cite{fds}.) 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{proof}

The space required for this algorithm is exactly the space required to keep track of $k = \frac{1}{\theta}$ pointers of size $\log m$, so the total space is $O(k \log m) = O(\frac{1}{\theta} \log m)$. Although this algorithm is deterministic (a very good thing), it still does not actually return the frequency counts of the $\theta$-heavy hitters. In order to do so, we would need to do another pass of the algorithm, with knowledge of which elements are $\theta$-heavy hitters. However, if we do two passes over our data, then this is no longer a streaming algorithm! 

We will now discuss a probabilistic algorithm that will allow us to determine the frequency of $\theta$-heavy hitters in a single pass over the stream. This algorithm is based on the the idea of bloom filters and hash functions. Before we lay out the algorithm, we will review the concept of bloom filters.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Bloom Filters}

In networking problems, we often have a stream in which want to keep track of which elements we have seen so far. There is an incredibly large universe of possible elements (think the set of all IP addresses), so we cannot possibly store whether or not we've seen each element directly. Instead, we will use hashing cleverly to keep track of elements. 

As usual when discussing hash tables, we want to avoid collisions. In a typical situation, we would need a hash table of size greater than $|S|^2$ in order to avoid collisions (where $S$ is the set of unique elements that show up). We will use a bloom filter, which uses a table of size much smaller than the number of unique elements, but hashes each element into the table multiple times. The goal of bloom filters is simply to answer membership questions (i.e., have we seen element $i$ in the stream yet).

A bloom filter works as follows: when we see element $i$ in the stream, we place it into the table by putting a 1 at location $h_b(i)$ for each of our $l$ hash functions $h_b$ (where $b \in [l]$). If some location already has a 1, we just leave it as a 1 (we do not increment it). In order to check membership for some element $j$, we check $h_b(j)$ for each $b \in [l]$. If there is a 0 in any location $h_b(j)$, then we know we have never seen $j$ before. If we find all 1's, then there we probably have seen $j$ before, although we could be mistaken. 

Note that although bloom filter can never have false negative (i.e. if an element has been seen, we will always report it as such), it may give us false positives (an element that we haven't seen may be reported as seen). Furthermore, we can bound the probability of such a false positive based off of the number of elements $n$, the size of the table $k$, and the number of hash functions $l$. 

We will now use the extend the concept of a bloom filters to construct a simple algorithm for solving the $\theta$-heavy hitters problem.

\subsection{The Count-Min Algorithm}

For this algorithm, we will use a data structure similar to a bloom filter. Rather than hashing each element $l$ times into a single table of size $k$ (as in the bloom filter), we will hash it once into $l$ different tables of size $k$ (using a different hash function for each table). Additionally, we will actually increment the count if we see a ``1'' in a location we are hashing to. The algorithm is detailed below:

\begin{enumerate}[]

\item \textbf{Count-Min} (Cormode \& Muthukrishnan, 2005)

\begin{enumerate}

\item Initialize count$_{a,b} = 0$, \ \ \  $\forall a \in [k],\ b \in [l]$. (these correspond to entries in our tables)

\item Upon receiving element $i$:

  \begin{enumerate}

  \item $\forall b \in [l]$, increment count$_{h_{b}(i),b}$ (where $h_b$ denotes the $b^{th}$ hash function)

  \end{enumerate}

\item Return $\min_b ($count$_{h_{b}(i),b})$ as an estimate for $f_i$

\end{enumerate}

\end{enumerate}

We assume the the $h_b$ are drawn from some 2-universal family of hash functions. % TODO: WHAT IS THIS??????

Each count$_{h_{b}(i),b}$ keeps track of the count of $i$, but also of any other elements that happened to has there via $h_b$. However, we have multiple counts, since we have one for each value of $b$ (this corresponds to having multiple tables). Each count overestimates $f_i$. The hope is that there is some count the doesn't overestimate it by too much. Thus, we use the minimum of all these counts as our estimate for $f_i$.

What is the expected value of count$_{h_{b}(i),b}$ for some fixed $b$? Note that count$_{h_{b}(i),b} = f_i + \sum_j f_j$ where the sum is over all $j$ such that $h_b(i) = h_b(j)$ but $i\neq j$. $f_i$ represents the frequency of $i$, and the sum represents the frequencies of all elements that hash to the same thing as $i$ does under $h_b$. By linearity of expectation, $\expct{\text{count}_{h_{b}(i),b}} = f_i + \sum_{j\neq i} (f_j \cdot \prob{h_b(i)=h_b(j)})$, where the probability is just the probability of a collision under $h_b$, which is $\frac{1}{k}$ for a table of size $k$. Thus $\expct{\text{count}_{h_{b}(i),b}} \leq f_i + \frac{1}{k}\sum_{j\neq i} f_j$. Note that $\sum_{j\neq i} f_j = m -f_i \leq m$, and thus $\expct{\text{count}_{h_{b}(i),b}} \leq f_i + \frac{m}{k} = f_i + \theta m$. If $f_i$ is very large and $\theta$ is very small, then our approximation will be very accurate. However, we can get better concentration results by constructing an $(\epsilon,\delta)$-approximation.

Suppose we set $k = \frac{1}{\epsilon}$. We can write count$_{h_{b}(i),b} = f_i + \sum_{j \neq i} (f_j \cdot Y_j)$, where $Y_j$ is a random variable that indicates whether $i$ and $j$ collide under $h_b$. Let $Y = \sum_{j \neq i} (f_j \cdot Y_j)$. Let us calculate the expected value of $Y$. Recall from before that $\expct{Y} \leq \frac{m}{k} = \frac{\epsilon m}{2}$. Then by Markov's inequality, we know:

\begin{equation} \label{eq:markov}
\prob{Y > \epsilon m} \leq \frac{\expct{Y}}{\epsilon m} \leq \frac{1}{2}
\end{equation}

This tells us that with probability $\geq \frac{1}{2}$, count$_{h_{b}(i),b} \leq f_i + \epsilon m$.

We not have an guarantee of our estimator $f_i$'s accuracy, but we would also like a confidence guarantee. That is, probability $\frac{1}{2}$ is not good enough. Recall that everything we have done so far is for a single $b$ (or a single table). In order to improve our confidence, we will take the minimum over all $b$. If each individual count is a good estimate with probability $\geq \frac{1}{2}$, what is the probability that the minimum is a good estimate?

\begin{align}
\prob{\min_b (\text{count}_{h_{b}(i),b}) > f_i + \epsilon m} &= \prob{\forall b,\  \text{count}_{h_{b}(i),b} > f_i + \epsilon m} \label{eq:one} \\
&= \prod_{b\in [l]} \prob{\text{count}_{h_{b}(i),b} > f_i + \epsilon m} \label{eq:two} \\
&\leq \prod_{b\in [l]} \frac{1}{2} \label{eq:three} \\
&= \bigg(\frac{1}{2}\bigg)^l = \delta \label{eq:four}
\end{align}

Where equation \ref{eq:one} follows from the fact that in order for the minimum count to be greater than $f_i + \epsilon m$, every count must be greater than $f_i + \epsilon m$. Equation \ref{eq:two} follows from the product rule for independent events, since our $h_b$ are independent. Equation \ref{eq:three} follows directly from equation \ref{eq:markov} above. 

The probably on the left hand side of equation \ref{eq:one} is exactly the probability that we make an estimate that is outside the acceptable bound. Thus is probability, which is equal to $(\frac{1}{2})^l$, is equal to $\delta$ as in \ref{eq:four}. We can then choose $l = \log (\frac{1}{\delta})$. We know have that with probability $\geq 1-\delta$, $f_i \leq \text{count}_{h_{b}(i),b} \leq f_i + \epsilon m$. Note that this is a good estimate only if $f_i \gg \epsilon m$, otherwise this will be far too big of a range to provide a useful estimate.

The space complexity of our algorithm is acceptably small. We are storing $k \cdot l$ counters of size $\log m$. Note that $ k = \frac{2}{\epsilon}$ and $l = \log (\frac{1}{\delta})$. Therefore the total space required by this algorithm is $O(k \cdot l \cdot \log m) = O(\frac{1}{\epsilon} \cdot \log (\frac{1}{\delta}) \cdot \log m)$. This is very good, since there is no dependence on $n$ (which might be gigantic). % Additionally, it takes a form similar to the space complexity for the median of means algorithm from last lecture. In fact, we will often see space complexities in the form $O(\frac{1}{\epsilon^k} \cdot \log (\frac{1}{\delta}) \cdot \log m)$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Consider a situation in which you work at Google, and you run the Count-Min algorithm every day for a month to determine the most frequently searched phrases each day. At the end of the month, your boss asks you for the most frequently searched phrases for the entire month. This algorithm has the very desirable property that we can simply add up the frequencies that we discovered in each algorithm in order to produce a new data structure that contains the frequencies for the entire month. We call such an algorithm a \textit{linear sketch}. It is linear because we are able to calculate the sum by simply adding the results, and it is a sketch because it produced a short summary of our data. 

However, in some cases we may not want to just add to stream, but also subtract them. For example, if we wanted to determine what were the most frequently searched phrases today that were not \textit{not} frequently searched yesterday. In order to answer a question like this, we will need to introduce a new model of streaming.

\section{Turnstile Model}

We now consider a modified streaming model where each element in our stream is of the form $(i, \delta_i)$ with $\delta_i \in \Z$. This models a situation where each element appears with a count that may be positive or negative. We think of a positive $\delta_i$ as having $\delta_i$ copies of $i$ ``arriving'', and negative counts as $\delta_i$ copies of $i$ ``leaving''. %(hence the name, since our stream acts as a turnstile that can turn in two directions).

In order to estimate the $\theta$-heavy hitters in this new context, the Count-Min algorithm is no longer good enough, since our counts are no longer guaranteed to overestimate the actual frequency. Recall that at upon termination of the Count-Min algorithm, we have count$_{h_{b}(i),b} = f_i + \sum_{j\neq i } f_j$. The term in the sum can now have positive or negative error. We sketch a slight modification of the algorithm (called a count sketch) to account for this difference.  We can no longer take the minimum of the counts to get the best estimate, so instead we will take the median of the counts. 

If we can get $\expct{\sum_{j \neq i } f_j } = 0$, then we will have an unbiased estimator for the frequency. If we can bound the variance of this estimator as well, then we can apply the median of means trick. In order to bound the variance, we will alter the Count-Min algorithm slightly. Suppose we have a random hash function $g(i) : [n] \rightarrow \{-1,1\}$, i.e. a function that assigns a random sign to each $i \in [n]$. Then we will estimate count$_{h_{b}(i),b} = g(i) \cdot f_i + \sum_{j\neq i } (g(j) \cdot f_j)$. The term inside the sum is negative with probability $\frac{1}{2}$, and positive with the same probability. Therefore in expectation, the term inside the sum will be 0. The variance of this sum is small as well. % what is it?

In the next lecture, we will further explore the setting of the turnstile model.

\begin{thebibliography}{}

%\bibitem{KLR}
%D.~Knuth, T.~Larrabee, and P.~Roberts.
%\newblock {Mathematical Writing}.
%\newblock In {\em Journal/Conf Name}, 1987.

\bibitem{tim}
T.~Roughgarden, G.~Valiant.
\newblock {CS168: The Modern Algorithmic Toolbox Lecture \#2: Approximate Heavy Hitters and the Count-Min Sketch}

\bibitem{fds}
A.~Blum, J.~Hopcroft, and R.~Kannan.
\newblock {Foundations of Data Science}

\end{thebibliography}

\end{document}
