from DecisionTree import *
import sys
from operator import itemgetter

# code is copy-pasted from dt-learn
def main():

	# get the command line arguments
	trainingSetFileName = sys.argv[1]
	testSetFileName = sys.argv[2]
	m = sys.argv[3]

	# train the tree
	dt = DecisionTree(trainingSetFileName, m, -1, True)

	# get the test instances
	testFile = open(testSetFileName, "r")
	
	testInstances = []
	for line in testFile:
		if line[0] != "@":
			line = re.sub(",",' ',line)
			line = line.lower().split()
			testInstances.append(line)
	testFile.close()

	#### TODO: GET THE THRESHOLDS
	trainingFile = open(trainingSetFileName, "r")
	
	trainingInstances = []
	for line in trainingFile:
		if line[0] != "@":
			line = re.sub(",",' ',line)
			line = line.lower().split()
			trainingInstances.append(line)
	trainingFile.close()

	classifications = []
	for instance in trainingInstances:
		confidence = dt.getConfidence(instance)
		actualClass = instance[-1]

		info = [confidence, actualClass]
		classifications.append(info)

	classifications = sorted(classifications, key=itemgetter(0), reverse=True)

	thresholds = []
	# for i in range(0, len(classifications)-1):
		
	# 	if classifications[i][1] == "+" and classifications[i+1][1] == "-":
			
	# 		confidence1 = classifications[i-1][0]
	# 		confidence2 = classifications[i][0]
			
	# 		t = (confidence2 + confidence1) / 2
	# 		thresholds.append(t)

	# 		print(str(classifications[i]) + " " + str(classifications[i+1]))

	for i in range(100):
		thresholds.append(i / 100)

	TPrates = []
	FPrates = []
	for i in range(len(thresholds)):
		threshold = thresholds[i]

		# do testing!
		TP = 0
		FP = 0
		pos = 0
		neg = 0
		total = 0

		for i in range(len(testInstances)):
			
			instance = testInstances[i]

			predictedClass = dt.classifyWithConfidence(instance, threshold)
			actualClass = instance[-1]
			
			if actualClass == "+":
				pos += 1
				if predictedClass == "+":
					TP += 1
			elif actualClass == "-":
				neg +=1
				if predictedClass == "+":
					FP += 1

		TPrate = TP / pos
		FPrate = FP / neg

		TPrates.append(TPrate)
		FPrates.append(FPrate)

	for i in range(len(thresholds)):
		print(str(FPrates[i]) + "," + str(TPrates[i]))

if __name__ == "__main__":
	main()