import re
from operator import itemgetter
import random
from math import log

class DecisionTree:
	
	# attributeNames[j] has the type listed at attributeTypes[j]
	attributeNames = []
	attributeTypes = []
	root = None

	m = 0

	def __init__(self, trainingSetFileName, m, prop=-1, confidence=False):

		self.getAttributesFromARFF(trainingSetFileName)
		self.m = m

		if prop < 0 and not confidence:
			self.train(trainingSetFileName)
		elif confidence:
			self.trainWithConfidence(trainingSetFileName)
		elif prop >= 0:
			self.trainOnPartial(trainingSetFileName, prop)
		else:
			print("Why did you come here?")

			

	def getCounts(self, trainingInstances):
	
		if len(trainingInstances) == 0:
			return (0,0)

		# get counts
		class1 = self.attributeTypes[-1][0]

		class1Count = 0
		totalCount = 0
		for i in range(len(trainingInstances)):
			if trainingInstances[i][-1] == class1:
				class1Count += 1
			totalCount += 1
		class2Count = totalCount - class1Count

		return (class1Count, class2Count)

	def getClassification(self, node):
		if len(node.trainingInstances) == 0:
			if node.parentNode == None:
				return None
			return self.getClassification(node.parentNode)

		# get the most common class
		(class1Count, class2Count) = self.getCounts(node.trainingInstances)

		if class1Count > class2Count:
			return self.attributeTypes[-1][0]
		elif class1Count < class2Count:
			return self.attributeTypes[-1][1]
		else:
			if node.parentNode == None:
				return None # what
			return self.getClassification(node.parentNode)

	def isNominalFeature(self, attributeIndex):
		if not isinstance(self.attributeTypes[attributeIndex], str):
			return True
		return False

	def getAttributesFromARFF(self,trainingSetFileName):
		
		trainingSetFile = open(trainingSetFileName, 'r')

		for line in trainingSetFile:
			
			line = re.sub(",",' ',line) # assume there are no commas in the attribute names
			line = line.lower().split()
			
			if line[0] == "@attribute":
				
				# remove the pesky quotes around the attribute
				line[1] = re.sub("'", '', line[1])

				# if this is a nominal attribute
				if line[2] == '{':

					aList = []

					for i in range(3, len(line)-1): # -1 cuz we ignore the last brace	
						aList.append(line[i])

					self.attributeNames.append(line[1])
					self.attributeTypes.append(aList)

				elif (line[2] == 'real') or (line[2] == 'integer') or (line[2] == 'numeric'):
					self.attributeNames.append(line[1])
					self.attributeTypes.append(line[2])

			elif line[0] == "@data":
				break

		trainingSetFile.close()

	# trains this tree on the given ARFF file
	def train(self, trainingSetFileName):
		
		# get the set of training instances
		trainingFile = open(trainingSetFileName, "r")

		trainingInstances = []
		for line in trainingFile:
			if line[0] != "@":

				line = re.sub(",",' ',line)
				line = line.lower().split()
				convertedLine = []
				for item in line:
					try:
						newItem = float(item)
					except:
						newItem = item
					convertedLine.append(newItem)

				trainingInstances.append(convertedLine)

		trainingFile.close()
		
		self.root = RootNode()
		onlyChild = self.makeSubtree(trainingInstances, self.root)

		self.root.addChild(onlyChild)

	def trainOnPartial(self, trainingSetFileName, prop):
		# get the set of training instances
		trainingFile = open(trainingSetFileName, "r")

		trainingInstances = []
		for line in trainingFile:
			if line[0] != "@":

				line = re.sub(",",' ',line)
				line = line.lower().split()
				convertedLine = []
				for item in line:
					try:
						newItem = float(item)
					except:
						newItem = item
					convertedLine.append(newItem)

				trainingInstances.append(convertedLine)

		trainingFile.close()
		
		numInstancesToTrainOn = int(prop * len(trainingInstances))
		numInstances = len(trainingInstances)
		trainingIndices = random.sample(range(numInstances), numInstancesToTrainOn)

		partialTrainingInstances = []
		for i in trainingIndices:
			partialTrainingInstances.append(trainingInstances[i])

		self.root = RootNode()
		onlyChild = self.makeSubtree(partialTrainingInstances, self.root)

		self.root.addChild(onlyChild)

	def trainWithConfidence(self, trainingSetFileName):
		# get the set of training instances
		trainingFile = open(trainingSetFileName, "r")

		trainingInstances = []
		for line in trainingFile:
			if line[0] != "@":

				line = re.sub(",",' ',line)
				line = line.lower().split()
				convertedLine = []
				for item in line:
					try:
						newItem = float(item)
					except:
						newItem = item
					convertedLine.append(newItem)

				trainingInstances.append(convertedLine)

		trainingFile.close()
		
		self.root = RootNode()
		onlyChild = self.makeSubtree(trainingInstances, self.root, True)

		self.root.addChild(onlyChild)

	def makeSubtree(self, trainingInstances, parentNode, confidence=False):

		candidateSplits = self.determineCandidateSplits(trainingInstances)

		if self.stoppingTime(trainingInstances, candidateSplits):

			node = LeafNode(trainingInstances, parentNode)

			if not confidence:
				classification = self.getClassification(node)
				node.classification = classification
			else:
				# imaginary counts
				pos = 1
				total = 2

				for instance in trainingInstances:
					if instance[-1] == self.attributeTypes[-1][0]:
						pos += 1
					total += 1

				node.confidence = pos / total

		else:

			(splitAttributeIndex, splitValue) = self.findBestSplit(trainingInstances, candidateSplits)

			# is this a nominal feature
			if self.isNominalFeature(splitAttributeIndex):

				node = NominalInternalNode(trainingInstances, splitAttributeIndex, parentNode)

				for val in self.attributeTypes[splitAttributeIndex]:
					
					# get only the instances where splitAttribute is val
					newInstances = []
					for instance in trainingInstances:
						if instance[splitAttributeIndex] == val:
							newInstances.append(instance)

					child = self.makeSubtree(newInstances, node, confidence)
						
					node.addChild(child)

			else:
				# it's a numeric feature
				leftChildInstances = []
				rightChildInstances = []
				for instance in trainingInstances:
					if float(instance[splitAttributeIndex]) <= float(splitValue):
						leftChildInstances.append(instance)
					else:
						rightChildInstances.append(instance)

				node = NumericInternalNode(trainingInstances, splitAttributeIndex, parentNode, splitValue)

				leftChild = self.makeSubtree(leftChildInstances,node, confidence)
				rightChild = self.makeSubtree(rightChildInstances,node, confidence)

				node.addChild(leftChild)
				node.addChild(rightChild)

		return node

	def determineCandidateSplits(self, trainingInstances):
		
		candidateSplits = []
		for attributeIndex in range(len(self.attributeNames) - 1): # -1 cuz otherwise it's can do the class...
			# nominal
			if self.isNominalFeature(attributeIndex):
				split = (attributeIndex, None)
				candidateSplits.append(split)
			# numeric...
			else:
				trainingInstances = sorted(trainingInstances, key=itemgetter(attributeIndex))

				for i in range(len(trainingInstances)-1):
					instance1Value = float(trainingInstances[i][attributeIndex])
					instance2Value = float(trainingInstances[i+1][attributeIndex])
					midpoint = 0.5 * (instance2Value + instance1Value)

					split = (attributeIndex, midpoint)
					candidateSplits.append(split)

		return candidateSplits

	def findBestSplit(self, trainingInstances, candidateSplits):

		bestIndex = None
		bestInfoGain = -1
		for i in range(len(candidateSplits)):
			thisInfoGain = self.infoGain(trainingInstances, candidateSplits[i])
			if thisInfoGain > bestInfoGain:
				bestIndex = i
				bestInfoGain = thisInfoGain

		return candidateSplits[bestIndex]

	def stoppingTime(self, trainingInstances, candidateSplits):

		# i. all the training instances are of the same class
		(class1Count, class2Count) = self.getCounts(trainingInstances)

		if class1Count == 0 or class2Count == 0:
			return True

		# ii. fewer than m instances
		if len(trainingInstances) < int(self.m):
			return True

		# iii. no positive infoGain
		anyPositive = False
		for split in candidateSplits:
			if self.infoGain(trainingInstances, split) >= 0: # TODO: maybe just >
				anyPositive = True
		if anyPositive == False:
			return True

		# iv: no remaining splits
		if len(candidateSplits) == 0:
			return True

		return False

	# computes information gain for a specific feature
	# split is a tuple (index, value) where value is None if feature is nominal
	def infoGain(self, trainingInstances, split):
		
		# compute P("+")
		(count1, count2) = self.getCounts(trainingInstances)
		p = count1 / (count1 + count2)
		initialEntropy = -1 * p * log(p) - (1-p) * log(1-p)

		if split[1] == None:
			
			values = self.attributeTypes[split[0]]
			
			prob = 0
			conditionalProb = 0
			conditionalEntropy = 0
			totalConditionalEntropy = 0
			
			for val in values:

				good = 0.0
				positive = 0.0
				total = 0.0
				for instance in trainingInstances:
					if instance[split[0]] == val:
						if instance[-1] == self.attributeTypes[-1][0]:
							positive += 1
						good += 1
					total += 1

				prob = good / total

				if good != 0 and positive != 0:
					conditionalProb = positive / good
				else:
					# TODO: wtf
					conditionalProb = 0.000001

				if conditionalProb == 1:
					conditionalProb -= 0.00000001

				conditionalEntropy = -1 * (conditionalProb * log(conditionalProb)+ \
					(1-conditionalProb) * log(1-conditionalProb))

				totalConditionalEntropy += prob * conditionalEntropy
			
			infoGain = initialEntropy - totalConditionalEntropy
		
		else:

			splitValue = float(split[1])

			left = 0.0
			right = 0.0
			leftPositive = 0.0
			rightPositive = 0.0
			total = 0.0

			for instance in trainingInstances:
				if float(instance[split[0]]) <= splitValue:
					if instance[-1] == self.attributeTypes[-1][0]:
						leftPositive += 1
					left += 1
				elif float(instance[split[0]]) > splitValue:
					if instance[-1] == self.attributeTypes[-1][0]:
						rightPositive += 1
					right += 1

				total += 1

			leftProb = left / total
			rightProb = right / total

			if left != 0 and leftPositive != 0:
				leftConditionalProb = leftPositive / left
			else:
				# TODO: wtf
				leftConditionalProb = 0.000001

			if right != 0 and rightPositive != 0:
				rightConditionalProb = rightPositive / right
			else:
				# TODO: wtf
				rightConditionalProb = 0.000001

			if leftConditionalProb == 1:
				leftConditionalProb -= 0.00000001

			if rightConditionalProb == 1:
				rightConditionalProb -= 0.00000001

			leftConditionalEntropy = -1 * (leftConditionalProb * log(leftConditionalProb)+ \
				(1-leftConditionalProb) * log(1-leftConditionalProb))

			rightConditionalEntropy = -1 * (rightConditionalProb * log(rightConditionalProb)+ \
				(1-rightConditionalProb) * log(1-rightConditionalProb))
			
			totalConditionalEntropy = leftProb * leftConditionalEntropy + rightProb * rightConditionalEntropy

			infoGain = initialEntropy - totalConditionalEntropy

		return infoGain

	def classifyAtNode(self, node, instance):
		
		if node.isRoot:
			onlyChild = node.children[0]
			return self.classifyAtNode(onlyChild, instance)

		elif node.isLeaf:
			return node.classification
		
		elif node.isNumeric:

			splitAttributeIndex = node.splitAttributeIndex
			splitValue = float(node.splitValue)
			instanceValue = float(instance[node.splitAttributeIndex])

			leftChild = node.children[0]
			rightChild = node.children[1]

			if instanceValue <= splitValue:
				return self.classifyAtNode(leftChild, instance)
			else:
				return self.classifyAtNode(rightChild, instance)

		elif node.isNominal:
			
			splitAttributeIndex = node.splitAttributeIndex
			splitAttributeTypes = self.attributeTypes[splitAttributeIndex]
			instanceType = instance[splitAttributeIndex]

			for i in range(len(splitAttributeTypes)):
				if instanceType == splitAttributeTypes[i]:
					return self.classifyAtNode(node.children[i], instance)

	# input: an ARFF file line (as a list)
	# output: class label
	def classify(self, instance):
		
		return self.classifyAtNode(self.root, instance)

	def classifyWithConfidenceAtNode(self, node, instance, threshold):

		if node.isRoot:
			onlyChild = node.children[0]
			return self.classifyWithConfidenceAtNode(onlyChild, instance, threshold)

		elif node.isLeaf:
			if node.confidence > threshold:
				return self.attributeTypes[-1][0]
			else:
				return self.attributeTypes[-1][1]
		
		elif node.isNumeric:

			splitAttributeIndex = node.splitAttributeIndex
			splitValue = float(node.splitValue)
			instanceValue = float(instance[node.splitAttributeIndex])

			leftChild = node.children[0]
			rightChild = node.children[1]

			if instanceValue <= splitValue:
				return self.classifyWithConfidenceAtNode(leftChild, instance, threshold)
			else:
				return self.classifyWithConfidenceAtNode(rightChild, instance, threshold)

		elif node.isNominal:
			
			splitAttributeIndex = node.splitAttributeIndex
			splitAttributeTypes = self.attributeTypes[splitAttributeIndex]
			instanceType = instance[splitAttributeIndex]

			for i in range(len(splitAttributeTypes)):
				if instanceType == splitAttributeTypes[i]:
					return self.classifyWithConfidenceAtNode(node.children[i], instance, threshold)


	def classifyWithConfidence(self, instance, threshold):

		return self.classifyWithConfidenceAtNode(self.root, instance, threshold)

	def makeCountsString(self, node):
		(count1, count2) = self.getCounts(node.trainingInstances)
		counts = "[" + str(count1) + " " + str(count2) + "]"

		return counts

	def makeLeafSuffix(self, node, confidence=False):
		if not node.isLeaf:
			return ""
		elif confidence:
			return ": " + str(node.confidence)
		else:
			return ": " + node.classification

	def printNode(self, node, depth, confidence=False):

		if node.isRoot:
			onlyChild = node.children[0]
			self.printNode(onlyChild, depth, confidence)

		elif node.isLeaf:
			print("WE SHOULD NOT BE HERE")

		elif node.isNumeric:
			spacing = "|\t" * depth
			attributeName = self.attributeNames[node.splitAttributeIndex]
			splitValueString = "{0:0.6f}".format(node.splitValue) # is this the right formatting?
			
			#### print the left child ####

			leftChild = node.children[0]
			leftCountString = self.makeCountsString(leftChild)
			leftLeafSuffix = self.makeLeafSuffix(leftChild, confidence)

			leftString = spacing + attributeName + " <= " + splitValueString + " " + leftCountString + leftLeafSuffix
			print(leftString)

			# recurse
			if not leftChild.isLeaf:
				self.printNode(leftChild, depth + 1, confidence)
			
			#### print the right child ####

			rightChild = node.children[1]
			rightCountString = self.makeCountsString(rightChild)
			rightLeafSuffix = self.makeLeafSuffix(rightChild, confidence)

			rightString = spacing + attributeName + " > " + splitValueString + " " + rightCountString + rightLeafSuffix
			print(rightString)

			# recurse
			if not rightChild.isLeaf:
				self.printNode(rightChild, depth + 1, confidence)

		elif node.isNominal:
			spacing = "|\t" * depth
			attributeName = self.attributeNames[node.splitAttributeIndex]
			attributeValues = self.attributeTypes[node.splitAttributeIndex]

			for i in range(len(node.children)):

				child = node.children[i]
				childCountString = self.makeCountsString(child)
				childLeafSuffix = self.makeLeafSuffix(child, confidence)

				childString = spacing + attributeName + " = " + attributeValues[i] + " " + childCountString + childLeafSuffix
				print(childString)

				if not child.isLeaf:
					self.printNode(child, depth + 1, confidence)

	def printTree(self, confidence=False):
		self.printNode(self.root, 0, confidence)

	def getConfidenceFromNode(self, node, instance):

		if node.isRoot:
			onlyChild = node.children[0]
			return self.getConfidenceFromNode(onlyChild, instance)

		elif node.isLeaf:
			return node.confidence
		
		elif node.isNumeric:

			splitAttributeIndex = node.splitAttributeIndex
			splitValue = float(node.splitValue)
			instanceValue = float(instance[node.splitAttributeIndex])

			leftChild = node.children[0]
			rightChild = node.children[1]

			if instanceValue <= splitValue:
				return self.getConfidenceFromNode(leftChild, instance)
			else:
				return self.getConfidenceFromNode(rightChild, instance)

		elif node.isNominal:
			
			splitAttributeIndex = node.splitAttributeIndex
			splitAttributeTypes = self.attributeTypes[splitAttributeIndex]
			instanceType = instance[splitAttributeIndex]

			for i in range(len(splitAttributeTypes)):
				if instanceType == splitAttributeTypes[i]:
					return self.getConfidenceFromNode(node.children[i], instance)

	def getConfidence(self, instance):
		return self.getConfidenceFromNode(self.root, instance)

class Node:

	trainingInstances = None
	parentNode = None
	isLeaf = None
	printed = False
	isRoot = False

	def __init__(self):
		print("?YOU SHOULD NOT BE HERE?")

class LeafNode(Node):

	classification = None # this is set externally
	confidence = -1 # this is set externally too

	def __init__(self, trainingInstances, parentNode):
		self.trainingInstances = trainingInstances
		self.parentNode = parentNode

		self.isLeaf = True

class InternalNode(Node):

	splitAttributeIndex = None
	children = []
	isNumeric = False
	isNominal = False

	def __init__(self, trainingInstances, splitAttributeIndex, parentNode):
		self.trainingInstances = trainingInstances
		self.splitAttributeIndex = splitAttributeIndex
		self.parentNode = parentNode

		self.isLeaf = False
		self.children = []

	# give birth
	def addChild(self, childNode):
		self.children.append(childNode)

class RootNode(InternalNode):

	def __init__(self):
		self.isRoot = True

class NumericInternalNode(InternalNode):

	splitValue = None

	def __init__(self, trainingInstances, splitAttributeIndex, parentNode, splitValue):
		super().__init__(trainingInstances, splitAttributeIndex, parentNode)
		self.splitValue = splitValue
		self.isNumeric = True

class NominalInternalNode(InternalNode):

	def __init__(self, trainingInstances, splitAttributeIndex, parentNode):
		super().__init__(trainingInstances, splitAttributeIndex, parentNode)
		self.isNominal = True
