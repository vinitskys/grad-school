# 				predicted:
# 			[3017, 217, 114]
# actual: 	[469, 4034, 281]
# 			[268,  613, 987]


# cm[actual][predicted]
cm = [[0,0,0],[0,0,0],[0,0,0]]

f = open("predictions.txt", 'r')

for line in f:
	line=line[:-1].split(',')

	# each line is "predicted,actual"
	predicted = int(line[0])-1
	actual = int(line[1])-1

	cm[actual][predicted] = cm[actual][predicted] + 1

print(cm)

f.close()
