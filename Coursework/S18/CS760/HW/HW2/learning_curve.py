from DecisionTree import *
import sys

# code is copy-pasted from dt-learn
def main():

	# get the command line arguments
	trainingSetFileName = sys.argv[1]
	testSetFileName = sys.argv[2]
	m = 10

	try:
		prop = float(sys.argv[3])
	except:
		print("3rd arg should be a float")
		quit()

	# train the tree
	dt = DecisionTree(trainingSetFileName, m, prop, False)

	# print the tree
	# dt.printTree()

	# # do testing!
	# print("<Predictions for the Test Set Instances>")

	# get the test instances
	testFile = open(testSetFileName, "r")
	
	testInstances = []
	for line in testFile:
		if line[0] != "@":
			line = re.sub(",",' ',line)
			line = line.lower().split()
			testInstances.append(line)
	testFile.close()

	# do testing!
	correct = 0
	total = 0
	for i in range(len(testInstances)):
		
		instance = testInstances[i]

		predictedClass = dt.classify(instance)
		actualClass = instance[-1]

		# print(str(i+1) + ": Actual: " + actualClass + " Predicted: " + predictedClass)
		
		# increment the counters
		if predictedClass == actualClass:
			correct += 1
		total += 1

	# print the summary
	# print("Number of correctly classified: " + str(correct) + " Total number of test instances: " + str(total), end="") # so no newline
	print(str(float(correct / total)))

if __name__ == "__main__":
	main()