#python stuff
import sys

#stuff I wrote
from parseArff import * 
from BayesNet import *

def main():

		# get the command line arguments
	trainingSetFileName = sys.argv[1]
	testSetFileName = sys.argv[2]
	bayesType = sys.argv[3]

	trainingInstances = getInstances(trainingSetFileName)
	if bayesType == 'n':
		bayesNet = NaiveBayes(trainingSetFileName, trainingInstances)
	elif bayesType == 't':
		bayesNet = TAN(trainingSetFileName, trainingInstances)
	else:
		print("INPUT ERROR: third argument is invalid. Should be 'n' or 't'.")
		quit()

	bayesNet.printNet()

	testInstances = getInstances(testSetFileName)	
	# get the list of confidences
	confidenceList = []
	for i in range(len(testInstances)):
		
		instance = testInstances[i]

		confidence = bayesNet.getConfidence(instance)
		actualClass = instance[-1]

		confidenceList.append(confidence)

	# get the PR for each possible confidence
	prList = []
	for confidence in confidenceList:
		pr = getPRWithThreshold(bayesNet, testInstances, confidence)
		prList.append(pr)

	for pr in prList:
		p = pr[0]
		r = pr[1]

		print(str(p) + "," + str(r))

def getPRWithThreshold(bayesNet, testInstances, threshold):

	FP = 0
	TP = 0 
	TN = 0 
	FN = 0

	positiveClass = bayesNet.attributeTypes[-1][0]

	for i in range(len(testInstances)):

		instance = testInstances[i]

		(predictedClass, confidence) = bayesNet.classify(instance, threshold)
		actualClass = instance[-1]
		
		# increment the counters
		if predictedClass == actualClass and actualClass == positiveClass:
			TP += 1
		elif predictedClass == actualClass and actualClass != positiveClass:
			TN += 1
		elif predictedClass != actualClass and actualClass == positiveClass:
			FN += 1
		elif predictedClass != actualClass and actualClass != positiveClass:
			FP += 1

	if TP + FP != 0:
		precision = TP / (TP + FP)
	else:
		precision = 0
	
	if TP + FN != 0:
		recall = TP / (TP + FN)
	else:
		recall = 0

	return (precision, recall)

if __name__=='__main__':
	main()