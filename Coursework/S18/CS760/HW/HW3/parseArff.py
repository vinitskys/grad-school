import re

def getAttributes(fileName):

	attributeNames = []
	attributeTypes = []
	
	f = open(fileName, 'r')

	for line in f:
		
		if line[0] == '%':
			continue

		line = re.sub(",",' ',line) # assume there are no commas in the attribute names
		line = re.sub("}", '',line) # remove the meaningless last bracket

		line = line.lower().split()

		if line[0] == "@attribute":
			
			# remove the pesky quotes around the attribute
			line[1] = re.sub("'", '', line[1])

			# if this is a nominal attribute
			if line[2] == '{':

				aList = []

				for i in range(3, len(line)): 
					aList.append(line[i])

				attributeNames.append(line[1])
				attributeTypes.append(aList)

			elif (line[2] == 'real') or (line[2] == 'integer') or (line[2] == 'numeric'):
				attributeNames.append(line[1])
				attributeTypes.append(line[2])

		elif line[0] == "@data":
			break

	f.close()

	return (attributeNames, attributeTypes)

def getInstances(fileName):
	# get the test instances
	f = open(fileName, "r")
	
	instances = []
	for line in f:
		if line[0] != "@" and line[0] != '%':
			line = re.sub(",",' ',line)
			line = line.lower().split()
			instances.append(line)
	f.close()

	return instances