# python's stuff

# my stuff
from parseArff import *
import math

# abstract superclass
class BayesNet:

	attributeNames = []
	attributeTypes = []

	# key is 'y' value is P(class = y)
	classProbabilities = {}
	# key is '(i,x,y)', value is P(X_i = x | class = y)
	conditionalProbabilities = {}

	def __init__(self, trainingSetFileName, trainingInstances):
		(self.attributeNames, self.attributeTypes) = getAttributes(trainingSetFileName)
		self.trainingInstances = trainingInstances

	# compute classProbabilities and conditionalProbabilities
	def learnNaiveProbabilities(self):
		
		#### GET THE COUNTS ####
		classCounts = {}
		jointCounts = {}

		# initialize counting dictionaries!
		# everyone gets a pseudocount of 1 for Laplace estimates!
		for className in self.attributeTypes[-1]:
			classCounts[className] = 1
			# loop through all the non-class attributes
			for i in range(len(self.attributeTypes)-1):
				for attrValue in self.attributeTypes[i]:
					key = (i, attrValue, className)
					jointCounts[key] = 1

		instances = self.trainingInstances

		for instance in instances:

			className = instance[-1]
			classCounts[className] = classCounts[className] + 1

			# loop through all the non-class attributes
			for i in range(len(instance)-1):
				key = (i, instance[i], className)
				jointCounts[key] = jointCounts[key] + 1

		#### TURN THE COUNTS INTO PROBS ####

		### get P(class = className) ###
		# get the total counts
		totalCount = 0
		for className in self.attributeTypes[-1]:
			totalCount += classCounts[className]

		# get the specific counts
		for className in self.attributeTypes[-1]:
			self.classProbabilities[className] = classCounts[className] / totalCount 

		### conditional probabilities ###
		# P(X_i = attr | Class = className)
		for className in self.attributeTypes[-1]:

			# loop through all the non-class attributes
			for i in range(len(self.attributeTypes)-1):
				
				# get the total counts
				totalAttributeCount = 0
				for attrValue in self.attributeTypes[i]:
					key = (i, attrValue, className)
					totalAttributeCount += jointCounts[key]

				# compute the probabilities
				for attrValue in self.attributeTypes[i]:
					key = (i, attrValue, className)
					self.conditionalProbabilities[key] = jointCounts[key] / totalAttributeCount 

	def printNet(self):
		print("I am an abstract class so please don't call this")

	# general classification
	def classify(self, instance, threshold=0.5):

		p = self.getConfidence(instance)

		className = self.attributeTypes[-1][0]
		otherClassName = self.attributeTypes[-1][1]	

		if p > threshold:
			outputClass = className
		else:
			outputClass = otherClassName
			p = 1 - p

		return (outputClass, p)


##### NAIVE BAYES #####
class NaiveBayes(BayesNet):

	def __init__(self, trainingSetFileName, trainingInstances):
		super().__init__(trainingSetFileName, trainingInstances)
		self.learnNaiveProbabilities()

	def printNet(self):
		
		className = self.attributeNames[-1] # assume class label is last

		for attr in self.attributeNames[0:-1]:
			print(attr + " " + className)

	# gives confidence of a positive instance
	def getConfidence(self, instance):

		className = self.attributeTypes[-1][0] # try to classify it as the first one

		# compute the numerator
		num = self.classProbabilities[className]
		for i in range(len(self.attributeTypes)-1):
			key = (i, instance[i], className)
			num *= self.conditionalProbabilities[key]

		denom = num # because math

		otherClassName = self.attributeTypes[-1][1]	
		val = self.classProbabilities[otherClassName]
		for i in range(len(self.attributeTypes)-1):
			key = (i, instance[i], otherClassName)
			val *= self.conditionalProbabilities[key]

		denom += val

		p =  num / denom

		return p

##### TAN #####
class TAN(BayesNet):

	# key is '(i,j,x,z,y)' value is P(X_i = x && X_j = z | class = y)
	doubleConditionalProbabilities = {}
	# key is '(i,j,x,z,y)' value is P(X_i = x && X_j = z && class = y)
	doubleJointProbabilities = {}

	jointCounts = {}

	# key is 'i', value is '(parent[i], isClassParent)'
	families = {}

	# key is '(y)' or '(node,x_n,y)' or ('node,parent,x_n,x_p,y')
	cpts = {}
	net = None

	trainingInstances = []

	def __init__(self, trainingSetFileName, trainingInstances):
		super().__init__(trainingSetFileName, trainingInstances)

		self.learnNaiveProbabilities()
		self.learnTANProbabilities()
		self.learnTAN()

	def learnTANProbabilities(self):
		
		#### GET THE COUNTS ####
		jointCounts = {}

		# initialize counting dictionaries!
		for className in self.attributeTypes[-1]:
			# loop through all the non-class attributes
			for i in range(len(self.attributeTypes)-1):
				for j in range(len(self.attributeTypes)-1):
					for x_i in self.attributeTypes[i]:
						for x_j in self.attributeTypes[j]:

							key = (i, j, x_i, x_j, className)
							jointCounts[key] = 0 # do laplace later

		instances = self.trainingInstances

		totalCount = 0
		for instance in instances:

			className = instance[-1]
			totalCount += 1

			# loop through all the non-class attributes
			for i in range(len(instance)-1):
				for j in range(len(instance)-1):
					key = (i, j, instance[i], instance[j], className)
					jointCounts[key] = jointCounts[key] + 1

		#### TURN THE COUNTS INTO PROBS ####
		for i in range(len(self.attributeTypes)-1):
			for j in range(len(self.attributeTypes)-1):

				totalLaplaceCount = 0
				for className in self.attributeTypes[-1]:				
					for x_i in self.attributeTypes[i]:
						for x_j in self.attributeTypes[j]:
							totalLaplaceCount += 1

				for className in self.attributeTypes[-1]:
				
					# get the total counts
					attributeLaplaceCount = 0
					totalAttributeCount = 0
					for x_i in self.attributeTypes[i]:
						for x_j in self.attributeTypes[j]:
							key = (i, j, x_i, x_j, className)
							attributeLaplaceCount += 1
							totalAttributeCount += jointCounts[key]

					# compute the probabilities
					for x_i in self.attributeTypes[i]:
						for x_j in self.attributeTypes[j]:
							key = (i, j, x_i, x_j, className)
							self.doubleConditionalProbabilities[key] = (jointCounts[key] + 1) / (totalAttributeCount + attributeLaplaceCount)
							self.doubleJointProbabilities[key] = (jointCounts[key] + 1) / (totalCount + totalLaplaceCount)

		self.jointCounts = jointCounts # we'll need these again later

	# computes mutual information between attribute i and j given class the variable
	def computeConditionalMI(self, i, j):
		total = 0
		for y in self.attributeTypes[-1]:
			for x_i in self.attributeTypes[i]:
				for x_j in self.attributeTypes[j]:
					
					key = (i, j, x_i, x_j, y)
					p = self.doubleJointProbabilities[key]
					
					top = self.doubleConditionalProbabilities[key]
					bot = self.conditionalProbabilities[(i, x_i, y)] * self.conditionalProbabilities[(j, x_j, y)]

					q = top / bot
					
					total += p * math.log(q, 2)

		return total

	# adjacency matrix
	def computeAdjacencyMatrix(self):

		matrix = []
		for i in range(len(self.attributeNames)-1):
			innerMatrix = []
			for j in range(len(self.attributeNames)-1):
				innerMatrix.append(0)
			matrix.append(innerMatrix)

		for i in range(len(self.attributeNames)-1):
			for j in range(len(self.attributeNames)-1):
				if i == j:
					matrix[i][j] = -1.0
				else:
					matrix[i][j] = self.computeConditionalMI(i,j)

		return matrix

	# mst tuple of edges
	def buildMST(self, matrix):

		vertices = []
		for i in range(len(self.attributeTypes)-1): # ingore the last one cuz it's class
			vertices += [i]

		n = len(vertices)
		
		mstVertices = [0] # just add the first one
		outVertices = vertices[1:] # remove the first one
		mstEdges = []
		# until we have every vertices
		while len(mstVertices) < n:

			best = -1
			bestI = -1
			bestJ = -1

			for i in mstVertices:			
				for j in outVertices:
					if matrix[i][j] > best:

						best = matrix[i][j]
						bestI = i
						bestJ = j

			mstEdges.append((bestI, bestJ))
			mstVertices.append(bestJ)
			mstVertices = sorted(mstVertices)
			outVertices.remove(bestJ)

		mst = (mstVertices, mstEdges)
		return mst

	def buildNet(self, matrix):
		mst = self.buildMST(matrix)
		mstVertices = mst[0]
		mstEdges = mst[1]
		
		n = len(self.attributeTypes) - 1
		netEdges = mstEdges
		for i in mstVertices:
			netEdges.append((n, i))
	
		netVertices = mstVertices + [n]

		self.net = (netVertices, netEdges)

	def learnCPTs(self):

		netVertices = self.net[0]
		netEdges = self.net[1]

		families = {}
		for node in netVertices:
			
			parent = None
			isClassParent = False
			
			if node != netVertices[-1]:
				for p in netVertices:
					if p == netVertices[-1]:
						isClassParent = True
					elif (p, node) in netEdges:
						parent = p

			families[node] = (parent, isClassParent)

		# key is '(y)' or '(node,x_n,y)' or ('node,parent,x_n,x_p,y')
		cpts = {}
		# TURN COUNTS INTO PROBABILITIES
		for node in families.keys():
			
			parentInfo = families[node]
			parent = parentInfo[0]
			isClassParent = parentInfo[1]

			# the class node
			if parent == None and isClassParent == False:
				for y in self.classProbabilities.keys():
					cpts[(y)] = self.classProbabilities[(y)]
			# the root node
			elif parent == None:
				for y in self.classProbabilities.keys():
					for x in self.attributeTypes[node]:
						cpts[(node, x,y)] = self.conditionalProbabilities[(node,x,y)]
				
			# everyone else
			else:
				
				laplaceCount = 0
				# for className in self.attributeTypes[-1]:
					# for x_j in self.attributeTypes[parent]:
				for x_i in self.attributeTypes[node]:
					laplaceCount += 1

				# get the cpts for every combination of x, z and z
				for y in self.classProbabilities.keys():
					for z in self.attributeTypes[parent]:

						# count the number of (z and y) instances
						jointTotal = 0
						for x in self.attributeTypes[node]:
							key = (node, parent, x, z, y)
							jointTotal += self.jointCounts[key]

						for x in self.attributeTypes[node]:

							key = (node, parent, x, z, y)
							cpts[key] = (self.jointCounts[key] + 1) / (jointTotal + laplaceCount)
		
		self.families = families
		self.cpts = cpts

	def learnTAN(self):
		matrix = self.computeAdjacencyMatrix()
		self.buildNet(matrix)
		self.learnCPTs()

	def getConfidence(self, instance):
		className = self.attributeTypes[-1][0] # try to classify it as the first one

		# compute the numerator
		num = self.classProbabilities[className]
		for i in range(len(self.attributeTypes)-1):

			family = self.families[i]
			parent = family[0]
			isClassParent = family[1]

			# the class node
			if parent == None and isClassParent == False:
				key = (className)
			# the root node
			elif parent == None:
				key = (i, instance[i], className)
				
			# everyone else
			else:
				key = (i, parent, instance[i], instance[parent], className)

			num *= self.cpts[key]

		denom = num # because math

		otherClassName = self.attributeTypes[-1][1]
		val = self.classProbabilities[otherClassName]
		for i in range(len(self.attributeTypes)-1):

			family = self.families[i]
			parent = family[0]
			isClassParent = family[1]

			# the class node
			if parent == None and isClassParent == False:
				key = (otherClassName)
			# the root node
			elif parent == None:
				key = (i, instance[i], otherClassName)
				
			# everyone else
			else:
				key = (i, parent, instance[i], instance[parent], otherClassName)

			val *= self.cpts[key]

		denom += val

		p =  num / denom

		return p

	def printNet(self):
		netVertices = self.net[0]
		netEdges = self.net[1]

		for node in netVertices:
			
			outString = self.attributeNames[node]

			if outString == 'class':
				continue
			
			for parent in netVertices:
				if (parent, node) in netEdges:
					outString += " " + self.attributeNames[parent]

			print(outString)

