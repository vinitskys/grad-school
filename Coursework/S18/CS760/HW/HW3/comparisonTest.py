#python stuff
import sys
import random
import math

#stuff I wrote
from parseArff import * 
from BayesNet import *

def main():

	random.seed(100)

	# get the command line arguments
	fileName = sys.argv[1]

	instances = getInstances(fileName)
	random.shuffle(instances)

	# split up the instances into 10 folds:
	n = len(instances)
	foldSize = int(n / 10)

	accDeltas = []
	for i in range(10):
		testFirstIndex = foldSize * i
		testLastIndex = testFirstIndex + foldSize

		if i < 9:
			testSet = instances[testFirstIndex: testLastIndex]
			trainingSet = instances[:testFirstIndex] + instances[testLastIndex:]
		else:
			testSet = instances[testFirstIndex:]
			trainingSet = instances[:testFirstIndex]

		naive = NaiveBayes(fileName, trainingSet)
		tan = TAN(fileName, trainingSet)

		# do testing!
		correctNaive = 0
		correctTAN = 0
		total = 0
		for i in range(len(testSet)):
			
			instance = testSet[i]

			(predictedClassNaive, probability) = naive.classify(instance)
			(predictedClassTAN, probability) = tan.classify(instance)
			actualClass = instance[-1]
			
			# increment the counters
			if predictedClassNaive == actualClass:
				correctNaive += 1
			if predictedClassTAN == actualClass:
				correctTAN += 1
			total += 1

		naiveAcc = correctNaive / total
		tanAcc = correctTAN / total

		deltaAcc = naiveAcc - tanAcc

		accDeltas.append(deltaAcc)

	# print(accDeltas)

	n = len(accDeltas)
	mean = sum(accDeltas) / n

	# print(mean)

	sumThing = 0
	for d in accDeltas:
		sumThing += (d - mean) ** 2
	print(sumThing)
	frac = 1 / (n*(n-1))
	print(frac)
	denom = math.sqrt(frac * sumThing)

	t = mean / denom

	# print(t)

if __name__=='__main__':
	main()