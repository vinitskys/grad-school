#python stuff
import sys

#stuff I wrote
from parseArff import * 
from BayesNet import *

def main():

	# get the command line arguments
	trainingSetFileName = sys.argv[1]
	testSetFileName = sys.argv[2]
	bayesType = sys.argv[3]

	trainingInstances = getInstances(trainingSetFileName)
	if bayesType == 'n':
		bayesNet = NaiveBayes(trainingSetFileName, trainingInstances)
	elif bayesType == 't':
		bayesNet = TAN(trainingSetFileName, trainingInstances)
	else:
		print("INPUT ERROR: third argument is invalid. Should be 'n' or 't'.")
		quit()

	bayesNet.printNet()

	testInstances = getInstances(testSetFileName)
	
	print() # blank line

	# do testing!
	correct = 0
	total = 0
	for i in range(len(testInstances)):
		
		instance = testInstances[i]

		(predictedClass, probability) = bayesNet.classify(instance)
		actualClass = instance[-1]

		probabilityString = "{0:0.12f}".format(probability)
		print(predictedClass + " " + actualClass + " " + probabilityString)
		
		# increment the counters
		if predictedClass == actualClass:
			correct += 1
		total += 1


	print() # blank line
	print(correct)

if __name__=='__main__':
	main()