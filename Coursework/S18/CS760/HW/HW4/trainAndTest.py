#python stuff
import sys

#stuff I wrote
from utils import * 
from NeuralNet import *
from Perceptron import *
from NNsingle import *

compact = False
def trainAndTest(learningRate, epochs, trainingSetFileName, testSetFileName, numHiddenNodes=0):

	trainingInstances = getInstances(trainingSetFileName)

	if numHiddenNodes > 0:
		nn = NNsingle(learningRate, numHiddenNodes, epochs, trainingSetFileName)
	else:
		nn = Perceptron(learningRate, epochs, trainingSetFileName)

	testInstances = getInstances(testSetFileName)
	(testVectors, labels) = nn.convertInstanceSetToVectors(testInstances)

	# do testing!
	TP = 0
	FP = 0
	TN = 0
	FN = 0
	for i in range(len(testInstances)):
		
		instance = testVectors[i]
		(predictedClass, activation) = nn.classify(instance)
		actualClass = labels[i]

		activationString = "{0:0.9f}".format(activation)
		if not compact:
			print(activationString + "\t" + str(predictedClass) + "\t" + str(actualClass))

		# increment the counters
		if predictedClass == actualClass and actualClass == 1:
			TP += 1
		elif predictedClass == actualClass and actualClass != 1:
			TN += 1
		elif predictedClass != actualClass and actualClass == 1:
			FN += 1
		elif predictedClass != actualClass and actualClass != 1:
			FP += 1

	######## TOTALS ########
	correct = TP + TN
	incorrect = FP + FN
	total = correct + incorrect
	if not compact:
		print(str(correct) + "\t" + str(incorrect))

	######## F1 SCORE ########
	if TP + FP != 0:
		precision = TP / (TP + FP)
	else:
		precision = 0

	if TP + FN != 0:
		recall = TP / (TP + FN)
	else:
		recall = 0

	if precision + recall != 0:
		F1 = (2 * (precision * recall)) / (precision + recall)
	else:
		F1 = 0
	
	F1_out = "{0:0.12f}".format(F1)
	print(F1)
