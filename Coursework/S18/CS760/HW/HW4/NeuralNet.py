#python stuff
import random
import numpy as np

#stuff I wrote
from utils import * 

compact = False

class NeuralNet:

	attributeTypes = []
	attributeNames = []

	trainingInstances = []
	trainingVectors = [] # trainingVectors[i] is the vector for trainingInstances[i]
	labels = [] # label[i] is the numeric label of trainingInstances[i]

	hasHiddenLayer = False

	hiddenWeights = None # weights going into hidden layer
	outputWeights = None # weights going into output layer (could be from the input layer)

	# mu[i] and sigma[i] are the standardization factors for feature i
	mu = []
	sigma = []

	def __init__(self, learningRate, numHiddenNodes, epochs, trainingSetFileName):
		
		# read the arff file
		(self.attributeNames, self.attributeTypes) = getAttributes(trainingSetFileName)
		self.trainingInstances = getInstances(trainingSetFileName)
		
		self.numHiddenNodes = numHiddenNodes

		# train! 
		self.computeStandardizationFactors()
		(self.trainingVectors, self.labels) = self.convertInstanceSetToVectors(self.trainingInstances)

		self.initializeWeights()
		self.train(learningRate, epochs)

	def computeStandardizationFactors(self):
		
		D = len(self.trainingInstances)
		
		mu_sums = []
		sigma_sums = []

		for i in range(len(self.attributeTypes) - 1):
			mu_sums.append(0)
			sigma_sums.append(0)
			self.mu.append(0)
			self.sigma.append(0)

		for i in range(len(self.attributeTypes) - 1):
			try:
				for x in self.trainingInstances:
					mu_sums[i] += float(x[i])

				self.mu[i] = mu_sums[i] / D

				for x in self.trainingInstances:
					sigma_sums[i] += (float(x[i]) - self.mu[i]) ** 2

				self.sigma[i] = math.sqrt(sigma_sums[i] / D)
			except:
				pass

	def convertInstanceSetToVectors(self, instanceSet):
		
		vectors = []
		labels = []
		positiveClassString = self.attributeTypes[-1][1]
		for instance in instanceSet:
			instanceVector = self.convertInstanceToVector(instance)
			vectors.append(instanceVector)

			actualClassString = instance[-1]
			if actualClassString == positiveClassString:
				actualClass = 1
			else:
				actualClass = 0

			labels.append(actualClass)

		return (vectors, labels)

	def convertInstanceToVector(self, instance):
		vectorString = ""
		for i in range(len(instance) - 1): # last one is class
			try:
				x = float(instance[i])
				standardizedValue = (x - self.mu[i]) / self.sigma[i]
				vectorString += str(standardizedValue) + ";"
			except:
				# 1 of k encoding
				for a in self.attributeTypes[i]:
					if a == instance[i]:
						vectorString += "1;"
					else:
						vectorString += "0;"

		# add the bias term
		vectorString += "1.0"
		
		vector = np.matrix(vectorString)

		return vector

	def train(self, learningRate, epochs):
		numInstances = len(self.trainingInstances)
		
		order = []
		for i in range(numInstances):
			order.append(i)

		for e in range(epochs):

			random.shuffle(order)

			for i in order:
				self.sgd(i, learningRate)

			#### do classification ####
			correct = 0
			incorrect = 0
			crossEntropy = 0
			for i in order:

				instance = self.trainingVectors[i]
				(predictedClass, activation) = self.classify(instance)
				actualClass = self.labels[i]

				if predictedClass == actualClass:
					correct += 1 
				else:
					incorrect += 1

				crossEntropy += calcCrossEntropy(actualClass, activation)

			if not compact:
				crossEntropyString = "{0:0.9f}".format(crossEntropy)
				print(str(e + 1) + "\t" + crossEntropyString + "\t" + str(correct) + "\t" + str(incorrect))


