#python stuff
import random
import numpy as np

#stuff I wrote
from utils import * 
from NeuralNet import *

compact = False

class NNsingle (NeuralNet):

	def __init__(self, learningRate, numHiddenNodes, epochs, trainingSetFileName):
		super().__init__(learningRate, numHiddenNodes, epochs, trainingSetFileName)

	def initializeWeights(self):

		# make sure the weight vector has the right length
		example = self.trainingVectors[0]
		numInputNodes = np.size(example)
		numHiddenNodes = self.numHiddenNodes
		
		weightString = ""
		outputWeightString = ""
		# wants a numInput * numHiddenNodes matrix
		for i in range(numHiddenNodes):
			
			for j in range(numInputNodes):
				w = random.uniform(-0.01, 0.01)
				weightString += str(w) + " "

			if i < numHiddenNodes - 1: # only don't do this on the very last row
				weightString += ";" 

			ow = random.uniform(-0.01, 0.01)
			outputWeightString += str(ow) + " "

		# one more for the bias!
		ow = random.uniform(-0.01, 0.01)
		outputWeightString += str(ow)

		self.hiddenWeights = np.matrix(weightString)
		self.outputWeights = np.matrix(outputWeightString)

	# perform sgd with instance i
	def sgd(self, i, learningRate):
		instance = self.trainingVectors[i]
		actualClass = self.labels[i]

		(predictedClass, activation, hiddenOutputs) = self.classifyVerbose(instance)

		#### compute the deltas ####
		output_delta = actualClass - activation
		
		firstTerm = np.multiply(hiddenOutputs, 1 - hiddenOutputs)
		secondTerm = output_delta * self.outputWeights.T

		hidden_delta = np.multiply(firstTerm, secondTerm)[:-1] # the last one is bias

		#### compute the edge updates ####
		output_weight_updates = learningRate * output_delta * hiddenOutputs.T
		self.outputWeights = self.outputWeights + output_weight_updates

		hiddenWeightUpdates = learningRate * np.multiply(hidden_delta, instance.T)
		self.hiddenWeights = self.hiddenWeights + hiddenWeightUpdates

	# don't return the hidden outputs
	def classify(self, instance):
		(classification, activation, hiddenOutputs) = self.classifyVerbose(instance)
		return (classification, activation)

	def classifyVerbose(self, instance):

		hiddenInputs = (self.hiddenWeights * instance)

		mString = ""
		for i in range(np.size(hiddenInputs)):
			mString += str(sigmoid(hiddenInputs[i,0])) + ";"
		mString += "1.0" # add the bias term

		hiddenOutputs = np.matrix(mString)

		outputInput = (self.outputWeights * hiddenOutputs)[0,0]
		activation = sigmoid(outputInput)

		if activation >= 0.5:
			classification = 1
		else: 
			classification = 0

		return (classification, activation, hiddenOutputs)
