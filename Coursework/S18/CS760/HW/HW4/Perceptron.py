#python stuff
import random
import numpy as np

#stuff I wrote
from utils import * 
from NeuralNet import *

compact = False

class Perceptron (NeuralNet):

	def __init__(self, learningRate, epochs, trainingSetFileName):
		super().__init__(learningRate, 0, epochs, trainingSetFileName)

	def initializeWeights(self):

		# make sure the weight vector has the right length
		example = self.trainingVectors[0]
		numInputNodes = np.size(example)

		weightString = ""
		for i in range(numInputNodes):
			w = random.uniform(-0.01, 0.01)
			weightString += str(w) + " "
		
		self.outputWeights = np.matrix(weightString) # bias is built in here

	# perform sgd with instance i
	def sgd(self, i, learningRate):
		instance = self.trainingVectors[i]
		(predictedClass, activation) = self.classify(instance)
		actualClass = self.labels[i]

		dEdw = (activation - actualClass) * instance
		delta_weights = -1 * learningRate * dEdw

		# update weights
		self.outputWeights = self.outputWeights + delta_weights.T

	def classify(self, instance):

		sigmoidInput = (self.outputWeights * instance)[0,0] # bias in built in

		activation = sigmoid(sigmoidInput)

		if activation >= 0.5:
			classification = 1
		else: 
			classification = 0

		return (classification, activation)