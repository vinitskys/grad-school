#python stuff
import sys

#stuff I wrote
from trainAndTest import *

def main():

	# get the command line arguments (as the right types)
	learningRate = float(sys.argv[1])
	numHiddenNodes = int(sys.argv[2])
	epochs = int(sys.argv[3])
	trainingSetFileName = sys.argv[4]
	testSetFileName = sys.argv[5]

	# code reuse is fun
	trainAndTest(learningRate, epochs, trainingSetFileName, testSetFileName, numHiddenNodes)

if __name__=='__main__':
	main()