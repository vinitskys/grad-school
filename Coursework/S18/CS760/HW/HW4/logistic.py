#python stuff
import sys

#stuff I wrote
from trainAndTest import *

def main():

	# get the command line arguments (as the right types)
	learningRate = float(sys.argv[1])
	epochs = int(sys.argv[2])
	trainingSetFileName = sys.argv[3]
	testSetFileName = sys.argv[4]

	# code reuse is fun
	trainAndTest(learningRate, epochs, trainingSetFileName, testSetFileName)

if __name__=='__main__':
	main()