% outputs needle map overlaid on img1
function result = computeFlow(img1, img2, search_win_radius, template_radius, grid_MN)

width = size(img1, 2);
height = size(img1, 1);

% starting/ending coordinates to avoid edge cases
first_x = 1 + max(template_radius, search_win_radius);
first_y = 1 + max(template_radius, search_win_radius);
last_x = width - max(template_radius, search_win_radius);
last_y = height - max(template_radius, search_win_radius);

% partition the region we're actually looking at
x_step = (last_x - first_x) / (grid_MN(2)+1);
y_step = (last_y - first_y) / (grid_MN(1)+1);

% points to sample
start_index_x = first_x + x_step;
end_index_x = last_x - x_step;
start_index_y = first_y + y_step;
end_index_y = last_y - y_step;

x_list = [];
y_list = [];
u_list = [];
v_list = [];

for x = start_index_x : x_step : end_index_x
    for y = start_index_y : y_step : end_index_y
        
        % round here to avoid star navigation errors
        i = round(x);
        j = round(y);
        
        template = img1(j - template_radius : j + template_radius, ...
                        i - template_radius : i + template_radius);
                    
        search_window = img2(j - search_win_radius : j + search_win_radius, ...
                             i - search_win_radius : i + search_win_radius);
        try
            C = normxcorr2(template, search_window);
        catch
            C = zeros(2*search_win_radius+1 + 2*template_radius);
        end
        
        % remove the padding
        search_C = C(template_radius + 1 : size(C,1) - template_radius, ...
                     template_radius + 1 : size(C,2) - template_radius);
                 
        % this line taken from matlab documentation for normxcorr2
        [y_peak, x_peak] = find(search_C == max(search_C(:))); 
        
        % multiple peaks, just pick one...
        if size(x_peak,1) > 1
            x_peak = x_peak(1);
            y_peak = y_peak(1);
        end
        
        % compute the velocities
        center = (search_win_radius + 1);
        u = x_peak - center;
        v = y_peak - center;
        
        x_list = [x_list i];
        y_list = [y_list j];
        u_list = [u_list u];
        v_list = [v_list v];
        
    end
end

fh1 = figure(); 
imshow(img1);
hold on;
quiver(x_list, y_list, u_list, v_list, 'Color', 'm');

result = saveAnnotatedImg(fh1); % from pervious assignment's demoMATLAB...
% delete(fh1);
    