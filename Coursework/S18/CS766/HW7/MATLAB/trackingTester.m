function trackingTester(data_params, tracking_params)
    
    % [xmin ymin width height]
    x_min = tracking_params.rect(1);
    y_min = tracking_params.rect(2);
    object_width = tracking_params.rect(3);
    object_height = tracking_params.rect(4);
    
    first_id = data_params.frame_ids(1);
    % load the image
    img1 = imread([data_params.data_dir '/' ...
                   data_params.genFname(first_id)]);

    if ~exist(data_params.out_dir, 'file')
        mkdir(data_params.out_dir);
    end

    rect = [x_min y_min object_width object_height];
                
    box_img = drawBox(img1, rect, [255 0 255], 1);
    imwrite(box_img, [data_params.out_dir '/' data_params.genFname(first_id)]);
               
    % create the histogram count for the object
    object = img1(y_min: y_min + object_height, x_min: x_min + object_width, :);
    [object_ind, map] = rgb2ind(object, tracking_params.bin_n);
    object_col = im2col(object_ind, [1 1]);
    object_hist = histcounts(object_col, tracking_params.bin_n);
    
    for index = 2:size(data_params.frame_ids,2)
        % id of the frame
        id = data_params.frame_ids(index);
        
        img = imread([data_params.data_dir '/' ...
               data_params.genFname(id)]);
        
        best_i = 0;
        best_j = 0;
        best_corr = -11111111111111;
        for i = -tracking_params.search_half_window_size:1:tracking_params.search_half_window_size
            for j = -tracking_params.search_half_window_size:1:tracking_params.search_half_window_size
                try
                    x = x_min + i;
                    y = y_min + j;
                    window = img(y: y + object_height, x: x + object_width, :);

                    win_ind = rgb2ind(window, map);
                    win_col = im2col(win_ind, [1 1]);
                    win_hist = histcounts(win_col, tracking_params.bin_n);

                    % see below for this function
                    corr = d(win_hist, object_hist);
                catch 
                    corr = -1111;
                end
                if corr >= best_corr
                    best_i = i;
                    best_j = j;
                    best_corr = corr;
                end
           end
        end
        
        x_min = x_min + best_i;
        y_min = y_min + best_j;

        rect = [x_min y_min object_width object_height];

        box_img = drawBox(img, rect, [255 0 255], 1);

        imwrite(box_img, [data_params.out_dir '/' data_params.genFname(id)]);

        
    end
    
    disp("D");
end

% compute the correlation between two histograms    
function corr = d(G, H)
    N = size(H,2);
    
    mean_G = mean(G);
    mean_H = mean(H);
    
    num = 0;
    for k =1:N
        num = num + ((G(k) - mean_G)*(H(k) - mean_H));
    end
    
    denom_G = 0;
    for k =1:N
        denom_G = denom_G + ((G(k) - mean_G)^2);
    end
    denom_G = sqrt(denom_G);
    
    denom_H = 0;
    for k =1:N
        denom_H = denom_H + ((H(k) - mean_H)^2);
    end
    denom_H = sqrt(denom_H);
    
    corr = num / (denom_G * denom_H);
    
end