function out_img = blendImagePair(img_s, mask_s, img_d, mask_d, mode)
    
    % make the masks 0/1 (they were up to 255 before)
    mask_s = uint8(mask_s > 0);
    mask_d = uint8(mask_d > 0);
    
    % if blending
    if mode == "blend"
        
        % because bwdist_s shades according to distance to white
        % need to make the masked region black and the rest white
        inverse_mask_s = 1 - mask_s;
        inverse_mask_d = 1 - mask_d;
    
        % make blending mask using bwdist
        bwdist_s = bwdist(inverse_mask_s);
        bwdist_d = bwdist(inverse_mask_d);
        
        max_s = max(max(bwdist_s));
        max_d = max(max(bwdist_d));
        
        if max_s > 0
            weights_s = double(bwdist_s) / double(max_s);
        else
            weights_s = 0 * double(bwdist_s);
        end
        
        if max_d > 0
            weights_d = double(bwdist_d) / double(max_d);
        else
            weights_d = 0 * double(bwdist_d);
        end
        
        % scale so all values are in (0,1)
        if max(max(max(img_s))) <= 1
            scaleFactor = 1;
        else
            scaleFactor = 255;
        end
        % convert to double for multiplication purposes
        scaled_img_s = double(img_s) / scaleFactor;
        scaled_img_d = double(img_d) / scaleFactor;
        
        % weighted blending as discussed in lecture
        weighted_img_s = weights_s .* scaled_img_s;
        weighted_img_d = weights_d .* scaled_img_d;
        sum_of_weighted_images = weighted_img_s + weighted_img_d;
        sum_of_weights = weights_s + weights_d;
        
        blended_img = sum_of_weighted_images ./ sum_of_weights;
        
        zero_indices = isnan(blended_img);
        blended_img(zero_indices) = 0;
        
        out_img = blended_img;

    else
        if mode == "overlay"
            % mask_d gets preference when they overlap 
            % s = (s & not d)
            mask_s = mask_s .* (1 - mask_d);
            
            % output
            out_s = mask_s .* img_s;
            out_d = mask_d .* img_d;
            
            out_img = out_s + out_d;
        else
            disp("5th argument must be 'blend' or 'overlay'")
            quit();
        end
    end
    
    