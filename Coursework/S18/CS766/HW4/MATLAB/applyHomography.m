function dest_pts_nx2 = applyHomography(H_3x3, src_pts_nx2)
    
    n = size(src_pts_nx2, 1);
    
    % convert into homogenous coordinates
    homo_src_pts_nx3 = [src_pts_nx2 ones(n,1)]; 
    
    % calculate the new coordinates
    homo_dest_pts_nx3 =(H_3x3 * homo_src_pts_nx3');
    
    % convert back to 2d
    homo_x = homo_dest_pts_nx3(1,:);
    homo_y = homo_dest_pts_nx3(2,:);
    homo_z = homo_dest_pts_nx3(3,:);
    
    x = (homo_x ./ homo_z)';
    y = (homo_y ./ homo_z)';
    
    dest_pts_nx2 = [x y];