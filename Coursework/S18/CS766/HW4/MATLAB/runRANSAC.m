function [inliers_id, H] = runRANSAC(src_points, dest_points, ransac_n, eps)
    
    num_points = size(src_points,1);

    % initial values
    inliers_id = [];
    H = eye(3);
    for i = 1:ransac_n
        % get 4 random points
        indices = randi(num_points, 1, 4);
        
        src_points_sample = src_points(indices, :);
        dest_points_sample = dest_points(indices, :);
        % compute the homography for our points
        H_sample = computeHomography(src_points_sample, dest_points_sample);
        
        % compute the proposed homography
        proposed_dest_points = applyHomography(H_sample, src_points);
        
        % compute the distance from the real point to the proposed point
        difference = proposed_dest_points - dest_points;
        squared_diff = difference.^2;
        squared_distance = squared_diff(:,1) + squared_diff(:,2);
        distance = squared_distance .^ (1/2);
        
        % compute the number of inliers
        is_inlier = (distance <= eps);
        inliers_id_sample = find(is_inlier);
        
        % if this is the most inliers so far
        if size(inliers_id_sample, 1) >= size(inliers_id, 1)
            H = H_sample;
            inliers_id = inliers_id_sample;
        end
    end