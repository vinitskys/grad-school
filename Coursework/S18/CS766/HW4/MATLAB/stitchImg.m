function stitched_img = stitchImg(varargin)
    
    %%%%%% PARAMS %%%%%%
    %%% for 1e) %%%
    ransac_n = 300;
    ransac_eps = 1;
    %%%%%%%%%%%%%%%%%%%%
    
    n = size(varargin,2);
    base_index = ceil(n/2); % index of the base image
    base_img = varargin{base_index};
    
    % H_array(:,:,i) is the homography from img i to img base_img
    H_array = zeros(3,3,n);
    
    H_array(:,:,1) = eye(3);
    for i=1:n
        if i==base_index
            H_array(:,:,i) = eye(3);
            continue
        end
        [x_s, x_d] = genSIFTMatches(varargin{i}, base_img);
        [inliers_id, H] = runRANSAC(x_s, x_d, ransac_n, ransac_eps);
        
        H_array(:,:,i) = H;
    end

    img_width = size(varargin{1},2);
    img_height = size(varargin{1},1);
    
    % compute the locations of the corners
    corners = [0 0; img_width 0; 0 img_height; img_width img_height];
    mapped_corners = zeros(4,2,n);
    for i=1:n
        H = H_array(:,:,i);
        mapped_corners(:,:,i) = applyHomography(H, corners);
    end
    
    max_x = max(max(mapped_corners(:,1,:)));
    max_y = max(max(mapped_corners(:,2,:)));
    min_x = min(min(mapped_corners(:,1,:)));
    min_y = min(min(mapped_corners(:,2,:)));

     % compute the dimensions of the output canvas
    delta_x = ceil(max_x - min_x);
    delta_y = ceil(max_y - min_y);
    dest_canvas_width_height = [delta_x delta_y];

    % figure out homography to the canvas
    t_x = abs(min_x);
    t_y = abs(min_y);
    H_base_to_canvas = [1 0 t_x; 0 1 t_y; 0 0 1];
    
    % H is now img_i to canvas
    for i = 1:n
        H_array(:,:,i) =  H_base_to_canvas * H_array(:,:,i);
    end
    
    canvas = zeros(delta_y, delta_x, 3);
    canvas_mask = zeros(delta_y, delta_x);

    for i=1:n
        canvasToSrc_H = inv(H_array(:,:,i));
        src_img = varargin{i};
        [mask, warped_img] = backwardWarpImg(src_img, canvasToSrc_H,...
    dest_canvas_width_height);

        out_img = blendImagePair(warped_img, mask, canvas, canvas_mask, 'blend');
        canvas = out_img;
        canvas_mask = (canvas_mask | mask); % new mask
        
    end
    
    stitched_img = canvas;