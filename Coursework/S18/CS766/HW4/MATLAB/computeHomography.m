function H_3x3 = computeHomography(src_pts_nx2, dest_pts_nx2)
    
    n = size(src_pts_nx2,1);
    A = zeros(2*n, 9);
    for i = 1:n
        x_s = src_pts_nx2(i,1);
        y_s = src_pts_nx2(i,2);
        x_d = dest_pts_nx2(i,1);
        y_d = dest_pts_nx2(i,2);
        
        first_row = [x_s y_s 1 0 0 0 -x_d*x_s -x_d*y_s -x_d];
        second_row = [0 0 0 x_s y_s 1 -y_d*x_s -y_d*y_s -y_d];
        
        rowNum = 2*i - 1;
        A(rowNum, :) = first_row;
        A(rowNum + 1, :) = second_row;
    end
    
    % get the eigenvalues
    [eigenvectors, D] = eig(A'*A);   
    eigenvalues = sum(D);
    
    % get the eigenvector with minimum eigenvalue
    [min_eigenvalue, index] = min(eigenvalues);
    min_eigenvector = eigenvectors(:, index);
    
    % convert 1x9 to 3x3
    % transpose to get the variables the line up right
    H_3x3 = reshape(min_eigenvector,3,3)'; 