function result_img = ...
    showCorrespondence(orig_img, warped_img, src_pts_nx2, dest_pts_nx2)

    fh1 = figure();
    
    combined_img = [orig_img warped_img];
    
    imshow(combined_img);
    hold on;
    
    % 1 is x, 2 is y
    x_src = src_pts_nx2(:,1);
    y_src = src_pts_nx2(:,2);
    
    src_width = size(orig_img,2); % 2 is the x size
    
    x_dest = dest_pts_nx2(:,1) + src_width;
    y_dest = dest_pts_nx2(:,2);
    
    for i=1:size(x_src)
        x_points = [x_src(i) x_dest(i)];
        y_points = [y_src(i) y_dest(i)];
        line(x_points, y_points, 'LineWidth',2, 'Color', [1, 0, 1]);
    end
    
    result_img = saveAnnotatedImg(fh1);