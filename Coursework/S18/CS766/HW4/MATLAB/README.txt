1a) 
    - didn't vectorize "computeHomography.m" because it would be WAY messier and not much faster
      (only loops over 4 items ever)
%%%% INTERESTING IMAGE POINTS (for manual testing / verification)
% identifier = orig_img_coord, warped_img_coord
% 
% top_left_corner = [159 100], [138 142]
% top_right_corner = [642 100], [621 28]
% bottom_right_corner = [642 700], [663 776]
% bottom_left_corner = [159 700], [114 596]
%     
% right_eye = [413 312], [336 282]
% left_eye = [347 314], [273 286]
% jacket_button = [386 573], [305 527]
% shirt_crinkle = [408 480], [329 440]
% shoulder_touch = [622 509], [614 506]
% background_swirl = [233 543], [172 476]
% jacket_middle_meets_edge = [384 701], [302 658]

1b)
    - backward warping using linear interpolation from intperp2
        - extrapolated values were 0

1c) - MEX ISSUE ON MAC: run this when you open matlab (on linux it's fine)
      run('/Users/tarquinius/Desktop/Grad_School/Coursework/CS766/HW4/MATLAB/sift_lib/vlfeat-0.9.21/toolbox/vl_setup')
    - yes, I used a loop here, but it's still fast

1d) 
    - distance map using bwdist

1e) 
    - % assume all pictures have the same dimensions
    - for the mountain images: chose eps = 1, n = 300 (but this will depend a LOT on the pictures themselves)...
    - takes about 5s on my laptop

1f) 
    - output to "custom_panorama.png"
    - I took these pictures on 3/16 at my friend's cabin.
    - using 4 images
    - it's a little blurry on the far-right image blend
    - chose eps = 3, n = 300 