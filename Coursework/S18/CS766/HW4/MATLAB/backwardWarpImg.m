function [mask, result_img] = backwardWarpImg(src_img, resultToSrc_H,...
    dest_canvas_width_height)

% variables for the sizes
dest_width = dest_canvas_width_height(1);
dest_height = dest_canvas_width_height(2); 
src_width = size(src_img,2);
src_height = size(src_img,1);

%%%%%% MAKE AN ARRAY FOR ALL THE DESTINATION POINTS %%%%%%
% dimensions: 3 x numpoints
% each column is [i; j; 1]

% initialize
dest_points= zeros(3, dest_width*dest_height);

% x: 1 1 1 ... 1 2 2 2 ... 2 ... w w w ... w (each repeats heighttimes)
step = 1 / (dest_height);
max_x = dest_width + 1 - step; % this works!!!
x_array = (1:step:max_x);
dest_points(1,:) = floor(x_array);

% y: 1 2 3... height 1 2 3... height 1 2 3... height (repeated width times)
dest_points(2,:) = repmat(1:1:dest_height, 1, dest_width);

% z: all ones
dest_points(3,:) = ones(1, dest_width * dest_height);

%%%%%% FIGURE OUT WHERE EACH DEST POINT CAME FROM %%%%%%

% compute the inverse homography
homo_src_points = resultToSrc_H * dest_points;
% normalize (dimenstions are 3x(#pix))
normalized_homo_src_points = homo_src_points ./ homo_src_points(3,:);
% remove the 3rd coordinate
src_points_vector = normalized_homo_src_points(1:2, :);

%%%%%% COMPUTE THE MASK %%%%%%

% compute which points got mapped from the source image
x_small = (src_points_vector(1,:) >= 1);
y_small = (src_points_vector(2,:) >= 1);
x_big = (src_points_vector(1,:)<= src_width);
y_big = (src_points_vector(2,:) <= src_height);

mask_vector = (x_small & y_small & x_big & y_big);

%%%%%% RESHAPE %%%%%%

% turn (3 x numPoints) arrays into a (dest_height x dest_width) arrays
mask = reshape(mask_vector, dest_height, dest_width);

% the (i,j) in dest_canvas came from (Y(j,i), X(j,i)) in src_img
X_src = reshape(src_points_vector(1,:), dest_height, dest_width);
Y_src = reshape(src_points_vector(2,:), dest_height, dest_width);

%%%%%% DO THE BACKWARDS WARPING %%%%%%

% need to do it for each RGB layer
homod_src_img(:,:,1) = interp2(src_img(:,:,1), X_src, Y_src, 'linear', 0);
homod_src_img(:,:,2) = interp2(src_img(:,:,2), X_src, Y_src, 'linear', 0);
homod_src_img(:,:,3) = interp2(src_img(:,:,3), X_src, Y_src, 'linear', 0);

result_img = homod_src_img;