function light_dirs_5x3 = computeLightDirections(center, radius, img_cell)
    
    % some constants
    num_imgs = size(img_cell,1);
    x_center = center(2);
    y_center = center(1);

    light_dirs_5x3 = zeros(num_imgs, 3);

    for i=1:num_imgs
        % find the brightest point
        img = img_cell{i};
        [intensity, index] = max(img(:));
        [row_index, col_index] = ind2sub(size(img),index);
        
        % get the direction
        % from the formula in the readme
        x_dir = col_index - x_center;
        y_dir = row_index - y_center;
        z_dir = sqrt(radius^2 - x_dir^2 - y_dir^2);
        
        % scale to have the length equal to the intensity
        v = [x_dir y_dir z_dir];
        v = v / norm(v); % scale to unit length
        v = double(intensity) * v; 
        
        light_dirs_5x3(i,:) = v;
    end
    