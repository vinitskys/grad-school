function [normals, albedo_img] = ...
    computeNormals(light_dirs, img_cell, mask)
 
    num_imgs = size(img_cell,1);
    x_dim = size(img_cell{1},2);
    y_dim = size(img_cell{1},1);
    
    normals = zeros(y_dim, x_dim, 3);
    albedos = zeros(y_dim, x_dim);
    
    for y=1:y_dim
        for x=1:x_dim
            
            if mask(y,x) ~= 0
            
                S = [];
                I = [];
                for i = 1:num_imgs
                    % use every cell, because even the 0 intensities give
                    % information
                    intensity = double(img_cell{i}(y,x));
                    v = light_dirs(i,:);
                    S = [S; v]; % don't normalize here!!! (see README).
                    I = [I; intensity];
                end

                % use least-squares regression
                R = (S' * I);
                L = inv(S' * S);
                N = L * R; 

                n = N / norm(N);
                albedo = pi * norm(N);
            else
                % background
                n = [0 0 1]; % pointing towards the camera
                albedo = 0;
            end
            
            normals(y,x,:) = n;
            albedos(y,x) = albedo;
        end
        
        % scale the albedo img
        albedo_img = rescale(albedos);
    end