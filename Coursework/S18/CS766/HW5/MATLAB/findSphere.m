function [center, radius] = findSphere(img)
    threshold = 0.001;
    bw_img = im2bw(img, threshold);
    stats = regionprops(bw_img, 'centroid', 'Area');
    
    center = stats.Centroid;
    area = stats.Area;
    
    % area = pi * r^2
    radius = sqrt(area / pi);