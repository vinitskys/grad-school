TODO:
    - everything seems right, except for the "reconstructNormals" method

1a) 
    - assume background is all 0's, so use a threshold of 0.001
    - area = pi * r^2
    - currently not rounding!!!

1b)
    - 3D formula: given a point (x,y,z) on a sphere with center (h,k,0), 
      the surface normal at the point (x,y,z) will be the same direction 
      as the vector from the center of the sphere to the point (x,y,z). 
      Thus the surface normal at (x,y,z) will be [x-h, y-k, z]. 
    - we have x and y from the image, now we just need to solve for z
    - (x-h)^2 + (y-k)^2 + z^2 = r^2 for a sphere centerd at (h,k,0), so
      z = sqrt(r^2 - (x-h)^2 - (y-k)^2)
    - this the surface normal at the point (x,y) is given by 
      [x-h, y-k, sqrt((x-h)^2 + (y-k)^2 + r^2)]
    - choose the z direction to be coming towards the viewer of the image
      [i.e. the light sources have positive z] (by right-hand rule, if z = y cross x)
    - Q: why can we assume that the brightest surface normal is the 
      direction of the light? 
      - From class, we know that (for a Lambertain surface) how bright the 
        scene appears is given by L = (albedo / pi) * (J / r^2) * (n dot s)
        --> L = k (n dot s)
        where n is the surface normal and s is the direction of the light source
      - The part of the sphere that appears brightest is the one that 
        maximizes L = k (n dot s)
      - Since the coefficient k is constant for every part of the sphere, 
        (if we fix the light intensity and direction) the only thing changing 
        as we move along the sphere is the direction of the surface normal n.
      - Then L = k (n dot s) is maximized when (n dot s) is maximized.
      - Recall that (n dot s) is maximized when n and s are parallel
        ( n dot s = |n| * |s| cos (theta) is maximized at theta=0)
      - Therefore the point with maximum intensity L is the point whose n
        is parallel to s (i.e. in the sam direction). But does such a point
        exist? Yes!
        - Since the surface is a sphere, there are normals in every direction
        (at least in one hemisphere)
        - Therefore there is also a normal in the direction of the light source,
        and so there WILL be an n parallel to s. 

1d) 
    - use least-squares solution (using 3+ points)
    - take into account the different intensities by not normalizing s
      (i.e. we are solving I = albedo / pi * k * (n dot s)) where k is the 
      intensity and s is a unit vector. k * s is what we found in part b