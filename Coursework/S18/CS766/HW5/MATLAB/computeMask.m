function mask = computeMask(img_cell)
    
    pseudomask = double(img_cell{1});
    
    % there have to be at least 3 images
    for i = 2:size(img_cell,1)
        pseudomask = pseudomask + double(img_cell{i});
    end
    
    mask = (pseudomask > 0);