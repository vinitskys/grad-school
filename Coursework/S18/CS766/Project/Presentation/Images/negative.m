function negative(name)
    
    orig_img = ~imread([name '.png']);
    
    imwrite(orig_img, [name '-negative' '.png']);
end
