function overlay_stafflines(name)

    orig_img = ~imread(['Images/' name '.png'])/255;
    no_staff_img = ~imread(['Images/' name '-nostaff.png'])/255;
    
    w = size(orig_img,2);
    h = size(orig_img,1);
    
    output_img = zeros(h,w);
    
    staff_lines = (orig_img & ~no_staff_img);
    
    %%% notes on top of staff %%%
    output_img(orig_img>0) = 1;
    output_img(staff_lines>0) = 2;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    x = label2rgb(output_img, [0 0 0; 1 0 0]);
    imwrite(x, 'overlay_staff_on_top.png');

end