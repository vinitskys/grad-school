function hough_naive(name)
    img = imread(['Images/' name '.png']);
    
    BW = edge(img,'canny');
    
    imwrite(BW, [name '-hough_out.png']);
end