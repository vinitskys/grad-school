\documentclass[11pt]{article}

\include{macros766}
\include{enumerate}

\usepackage{graphicx}
\graphicspath{ {images/} }

\begin{document}
\project{Project Proposal: Optical Music Recognition}{2/14/2018}

\section{Introduction}

Despite the existence of digital composing tools, most composers still prefer to write sheet music by hand. This is perhaps due to how cumbersome these tools are to use, or the fact that most composers grew up writing sheet music by hand. Manually converting this hand-written music into a digital format is cumbersome and time-consuming, and automating this process would greatly ease the process of writing music. Additionally, most of the sheet music that already exists is hand-written, and it would be very valuable to automate the process of archive this music into a digital format. 

Optical music recognition (OMR) is the task of converting an image of hand-written or printed sheet music into a digital sheet music storage format (such as MusicXML or MIDI). OMR is useful for a variety of tasks beyond those described above, such as: automating the turning of pages while playing music, correcting mistakes in printed scores, standardizing digital sheet music notation across printed scores, changing the key of printed sheet music, and removing parts from a printed score. 

OMR is a very complicated process that involves many steps and interconnected tasks. Most research in this area will focus on one specific subtask. A very thorough breakdown of the OMR pipeline is given in \cite{overviewMusc} and reproduced below:

\begin{center}
 \begin{tabular}{l | l | l} 
 \textbf{Sub-task} & \textbf{Input} & \textbf{Output} \\ [0.5ex] 
 \hline\hline
 Image Processing & Score image & ``Cleaned'' image \\ 
 Binarization & ``Cleaned'' image & Binary image \\
 \hline
 Staff ID \& Removal & Binary image & Stafflines list \\
 Symbol detection & (Staff-less) image & Symbol regions \\
 Symbol classification & Symbol regions & Symbol labels \\
 \hline
 Notation Assembly & Symbol regions \& labels & Notation graph \\
 Infer pitch/duration & Notation Graph & Pitch/duration attributes \\
 \hline
 Output conversion & Notation graph \& attributes & MusicXML, MIDI, ... \\
\end{tabular}
\end{center}

% \subsection{Challenges in Performing OMR}

% OMR is a very difficult task. Although it bears some semblance to optical character recognition, it is orders of magnitude more difficult for a variety of reasons. 

% One difficulty is that sheet music is inherently hierarchical. For example, a simple dot could mean a variety of things depending on what other objects appear near it in the image (i.e. ``this note lasts 1.5 times as long'', ``this note should be played very short'', ``repeat the last X measures'', etc). In light of this, even once the symbols in the sheet music have been recognized (a difficult task since disjoint symbols often overlap) and classified (difficult because of variation in writing style), it is a very difficult to reconstruct the semantics of the score. This is an active area of research, and is beyond the scope of this project. \cite{overviewMusc} provides a good overview of this issue, and a dataset with hierarchical dependencies manually labeled.

% Another big issue in OMR is a lack of ground truth datasets and labelled data. Many researchers are actively attempting to produce such datasets \cite{staffDalitz, overviewMusc, classifyRebelo} or unify existing datasets \cite{dataPach}.

% Some recent have attempted to use deep learning for an end-to-end approach. These have been met with difficulties even for standardized computer-generated scores that consist only of a single voice \cite{endZar, endWel}.

\subsection{Staff Line Removal}

For this project, I will focus on one small (but substantial) part of the OMR pipeline: staff detection and removal. This is a very important task, as the staff lines serve as a 2D axis that all other symbols appear with respect to. For example, the placement of a note vertically on the staff lines determine its pitch, and the placement horizontally determine when it is played.

Most approaches choose to remove the staff lines after detecting them, as this helps with classifying the remaining symbols. This is not trivial, as since the staff lines overlap with almost all of the other symbols in the score. Removing the staff lines without severely altering the rest of the image is not a trivial task. Additionally, staff lines may be curved or broken due to imperfections in the physical score, which makes this process even harder. 

There has been a great deal of work in this area, and a good overview is given in \cite{staffDalitz}. There are four main categories that algorithms for staff removal fall into: line tracking, vector field, vertical run-length analysis, and skeletonization. The current state of the art of staff line removal involves using generative adversarial networks (GAN's) \cite{staffGAN}, which requires large amounts of ground truth data.

\section{Project Goals}

I intend to perform a comparative study of existing staff removing algorithms. I intend to implement one of each of the ``basic'' types of algorithms listed above (i.e. line tracking, vector field, vertical run-length analysis, and skeletonization). I also plan to implement to GAN algorithm, but this may require more computing resources than I have access to.

For evaluation and testing, I will use the publicly available dataset from the 2013 staff removal competition ``ICDAR'' (http://www.cvc.uab.es/cvcmuscima/competition2013/). This consists of 6000 binarized hand-written score images, and the corresponding ground-truth data (i.e. with staff lines removed).

I will also test performance on an existing dataset of 32 printed score images (with ground truth images) provided by \cite{staffDalitz}. This is a much smaller dataset, but is the largest I could find for printed music that included the ground truth images (this is necessary so we can actually measure the accuracy of the algorithm).

For calculating accuracy, I will follow the methods set out in \cite{staffDalitz}, which proposes benchmarks for how to compare an image generated by a staff-removal algorithm with the ground truth image (of the sheet music without staff lines). Their definition of accuracy incorporates both non-staff pixels that were incorrectly classified as ``not a staff line'' and kept in the image (false negatives) and staff pixels that were incorrectly identified as ``staff line'' and removed (false positives).

\section{Timetable}

Below is a rough time-table of the project. I have included the course deadlines in bold for reference.

\begin{center}
 \begin{tabular}{l | l | l} 
 \textbf{Task} & \textbf{Time alloted} & \textbf{Target date for completion} \\
 \hline
 \hline
 Create testing infrastructure & 2 weeks & Feb. 26 \\
 \hline
 Implement ``basic'' algorithms & 3 weeks & Mar. 12 \\
 \hline
 Implement GAN algorithm & 4 weeks & April 9 \\
 \hline
 \textbf{Mid-term report} & & \textbf{April 2} \\
 \hline
 Perform comparative tests & 1 week & April 16 \\
 \hline
 Create project presentation & 1 week & April 23 \\
 \hline
 \textbf{Presentations Begin} & & \textbf{April 23} \\
 \hline
 Create project website & 1 week & April 30 \\
 \hline
 Final tweaks & 1 week & May 7\\
 \hline
 \textbf{Project Website Due} & & \textbf{May 7}

 \end{tabular}
\end{center}

\pagebreak
\nocite{*}
\bibliographystyle{plain}
\bibliography{bibliography} 

% latex proposal.tex
% bibtex proposal.aux
% t proposal

\end{document}