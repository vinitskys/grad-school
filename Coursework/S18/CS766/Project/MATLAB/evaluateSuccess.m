function score = evaluateSuccess(orig_img, real_nostaff_img, real_staff_img, predicted_staff_img, evalMetric)
    
    switch evalMetric
        
        case 'acc'
            TP_img = predicted_staff_img & real_staff_img ; % correctly took
            TN_img = (~predicted_staff_img) & real_nostaff_img; % correctly didn't take

            total = nnz(orig_img); % count the number of foreground pixels
            TP = nnz(TP_img);
            TN = nnz(TN_img);

            score = (TP + TN) / total;
    
        case 'precision'
            
            TP_img = predicted_staff_img & real_staff_img ; 
            FP_img = predicted_staff_img & real_nostaff_img;

            TP = nnz(TP_img);
            FP = nnz(FP_img);

            precision = TP / (TP + FP);
            
            score = precision;
            
        case 'recall'
            TP_img = predicted_staff_img & real_staff_img ; 
            FN_img = (~predicted_staff_img) & real_staff_img;

            TP = nnz(TP_img);
            FN = nnz(FN_img);

            recall = TP / (TP + FN);
         
            score = recall;   
            
        case 'F1'
            
            TP_img = predicted_staff_img & real_staff_img ; 
            FP_img = predicted_staff_img & real_nostaff_img;
            FN_img = (~predicted_staff_img) & real_staff_img;

            TP = nnz(TP_img);
            FP = nnz(FP_img);
            FN = nnz(FN_img);

            precision = TP / (TP + FP);
            recall = TP / (TP + FN);
            
            F1 = (2 * precision * recall) / (precision + recall);
            
            score = F1;
            
        case 'cc_count'
            predicted_nostaff_img = (orig_img & (~predicted_staff_img));
            
            CC_real = bwconncomp(real_nostaff_img);
            CC_predicted = bwconncomp(predicted_nostaff_img);
            
            score = CC_real.NumObjects / CC_predicted.NumObjects;
            
        case 'cc_preserved'
            predicted_nostaff_img = (orig_img & (~predicted_staff_img));

            CC_real = bwconncomp(real_nostaff_img);
            CC_predicted = bwconncomp(predicted_nostaff_img);
            
            predicted_labels = labelmatrix(CC_predicted);
            
            % for each connected component, count the number of CC's
            % it was broken up into
            num_preserved_ccs = 0;
            for i = 1:CC_real.NumObjects
                % find the labels (in the predicted img) of each element of
                % this cc (from the real img)
                label_list = predicted_labels(CC_real.PixelIdxList{i});
                
                % count the number of unique nonzero elements
                unique_labels = unique(label_list);
                num_nonzero_unique_labels = nnz(unique_labels);
                
                % we didn't break up this cc!
                if num_nonzero_unique_labels == 1
                    num_preserved_ccs = num_preserved_ccs + 1;
                end
            end
            
            score = num_preserved_ccs / CC_real.NumObjects;

        otherwise
            disp('Not a valid evaluation metric, using constant 1 as score');
            score = 1;
    end