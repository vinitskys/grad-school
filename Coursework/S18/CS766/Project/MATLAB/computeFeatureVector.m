function feature_vector_array = computeFeatureVector(bw_img, method, param)
    
    switch method
        case '4-dist'
            feature_vector_array = computeFV_4dist(bw_img);
        case 'window'
            feature_vector_array = computeFV_window(bw_img, param);
    end

end