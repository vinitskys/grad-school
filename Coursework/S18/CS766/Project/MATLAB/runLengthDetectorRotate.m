function staff_lines_img = runLengthDetectorRotate(bw_img)

    % unrotate before doing anything
    rotation_angle = computeRotation(bw_img);
    bw_img = imrotate(bw_img, -rotation_angle, 'bicubic', 'crop');
    
    staff_lines_img = runLengthDetector(bw_img);
    
    % re-rotate
    staff_lines_img = imrotate(staff_lines_img, rotation_angle, 'bicubic', 'crop');