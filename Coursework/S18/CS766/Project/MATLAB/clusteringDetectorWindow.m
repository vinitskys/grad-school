function staff_lines_img = clusteringDetectorWindow(bw_img, half_window_size, metric)

    fv_array = computeFeatureVector(bw_img, 'window', half_window_size);
    
    num_clusters = 2;
    
    width = size(bw_img,2);
    height = size(bw_img,1);
    
    num_features = size(fv_array,3);
    num_foreground= nnz(bw_img);
    
    % stores the indices on nonzero pixels in the form [y,x]
    [y_indices, x_indices] = find(bw_img);
        
    % fv_list(i,:) contains the FV for the index nonzero_indices(i,:)
    fv_list = zeros(num_foreground, num_features);
    
    % only use foreground pixels in the fv_list
    for k = 1:size(x_indices,1)
        x = x_indices(k);
        y = y_indices(k);
        
        fv_list(k,:) = fv_array(y, x, :);
    end
    
    %%% CLUSTER %%%
    cluster_labels = kmeans(fv_list, num_clusters, 'Distance', metric);

    % need to do smart stuff here
   
    cluster_labels_img = zeros(height, width);
    
    % convert the 1-D vector labels into an img
    for k = 1:size(x_indices,1)
        x = x_indices(k);
        y = y_indices(k);
        
        cluster_labels_img(y, x, :) = cluster_labels(k);
    end
    
    %% TESTING %%%
    x = label2rgb(cluster_labels_img, [1 0 0; 0 0 1; 0 1 0]);
    imwrite(x, 'rgb.png');
    %%%%%%%%%%%%%%
    
    staff_label = computeStaffLabel(cluster_labels_img, num_clusters, 'freq');
    
    staff_lines_img = (cluster_labels_img == staff_label);
%     figure; imshow(staff_lines_img);
%     disp(staff_label);
%     
%     disp("GUCCI");
    
end