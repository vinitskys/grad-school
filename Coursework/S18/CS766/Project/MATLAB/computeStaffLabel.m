function staff_label = computeStaffLabel(cluster_labels_img, num_clusters, method)

    switch method
        case 'freq'
            % choose the least frequent label (this is a hack)    
            count_array = [];
            for i=1:num_clusters
                x = nnz(cluster_labels_img == i);
                count_array = [count_array x];
            end

            [M, staff_label] = min(count_array);
            
        otherwise
            staff_label = -1;
    end
    
end