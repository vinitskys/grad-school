function mainDalitz()

path = 'Datasets/Staff_Lines/Dalitz/modern/';

names = {'bach' 'bellinzani' 'brahms02' 'bruckner01' 'buxtehude' ...
         'dalitz03'  'pmw01' 'pmw03' 'rameau' 'schumann' ...
         'wagner' 'williams'}; 

detectionMethod = 'clustering_hamming_window_3';
deformType = 'none';
deformParam = 0;
evalMetric = 'cc_preserved'; % precision, recall, F1, cc_preserved, cc_count

scoreArray = removeStaffLines(detectionMethod, deformType, deformParam, ...
                              evalMetric, path, names, 'Dalitz');

disp(scoreArray);
disp(mean(scoreArray,'omitnan'));