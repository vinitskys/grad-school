function staff_lines_img = runLengthDetector(bw_img)

    width = size(bw_img,2);
    height = size(bw_img,1);
    
    % list of all the white runs for this image
    runs = []; 
    % index (j,i) is the length of the longest run that pixel j,i is in
    longest_run_array = zeros(height, width);
    
    % do each column one by one
    for i = 1:width
        
        current_run = 1;
        for j = 1:height-1
            
            % pixel values for this and the pixel under it
            current_val = bw_img(j,i);
            next_val = bw_img(j+1,i);
            
            if current_val ~= 1
                continue
            end
            
            if current_val == next_val
                current_run = current_run + 1;
            else
                % store this as the longest run for all the pixels in it
                for prev = 0:current_run-1
                    longest_run_array(j-prev, i) = current_run;
                end
                runs = [runs current_run];
                current_run = 1;
            end
        end
    end
    
    longest_run = mode(runs);
    
    in_longest_run = (longest_run_array == longest_run) & (bw_img);
        
    staff_lines_img = in_longest_run;