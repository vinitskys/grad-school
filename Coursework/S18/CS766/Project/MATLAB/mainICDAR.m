function mainICDAR()

path = 'Datasets/Staff_Lines/ICDAR/Combined/';

staff_names = {'81'};%{'23' '28' '44' '72' '64' '43' '17' '1' '30' '12' '69' '111'};

detectionMethod = 'clustering_hamming_window_3';
deformType = 'none';  
deformParam = 0;
evalMetric = 'F1'; % precision, recall, F1, cc_preserved, cc_count

scoreArray = removeStaffLines(detectionMethod, deformType, deformParam, ...
                              evalMetric, path, staff_names, 'ICDAR');

disp(scoreArray);                       
disp(mean(scoreArray,'omitnan'));