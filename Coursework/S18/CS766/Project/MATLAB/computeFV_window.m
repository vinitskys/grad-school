function feature_vector_array = computeFV_window(bw_img, half_width)

    width = size(bw_img,2);
    height = size(bw_img,1);
    window_width = 2*half_width+1;

    fv_array = zeros(height, width, window_width^2);
    
    for i= 1+half_width : width-half_width
        for j= 1+half_width : height-half_width
        
            window = bw_img(j-half_width: j+half_width, ...
                            i-half_width: i+half_width);
            
            fv = reshape(window, [1, window_width^2]);
            fv_array(j,i,:) = fv;
        end
    end
    
    feature_vector_array = fv_array;
end