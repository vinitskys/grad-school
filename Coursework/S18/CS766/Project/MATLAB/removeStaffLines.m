% main function to remove staff lines
% see README for a description of this function and it parameters
function scoreArray = removeStaffLines(detectionMethod, deformationType, ...
                                       deformParam, evalMetric, path, names, dataset)
                                   
    %%%%%%%%%% PARAMS %%%%%%%%%%
    threshold = 0.5;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%

    num_imgs = size(names,2);
    
    scoreArray = zeros(1, num_imgs);
    for i = 1 : num_imgs
        % get the images
        orig_img = imread([path names{i} '.png']);
        nostaff_img = imread([path names{i} '-nostaff.png']);

        % convert to black and white
        bw_orig = im2bw(orig_img, threshold);
        bw_nostaff = im2bw(nostaff_img, threshold);

        % flip so important pixels are white
        if ~strcmp(dataset, 'ICDAR')
            bw_orig = ~bw_orig;
            bw_nostaff = ~bw_nostaff;
        end
        
        [deformed_orig_img, deformed_nostaff_img] = ...
            deform(bw_orig, bw_nostaff, deformationType, deformParam);

        final_orig_img = deformed_orig_img;
        final_nostaff_img = deformed_nostaff_img;
        
        % find the correct staff lines
        correct_staff_img = final_orig_img & ~final_nostaff_img;

        % try to detect the staff lines
        predicted_staff_img = detectStaffLines(final_orig_img, detectionMethod);
        
        % see how good we did
        score = evaluateSuccess(final_orig_img, final_nostaff_img, correct_staff_img, predicted_staff_img, evalMetric);
        scoreArray(1,i) = score;
                
        % generate the staff-less image based on what we removed
        predicted_nostaff_img = ~(final_orig_img & (~predicted_staff_img));
        
        % save the staff-less image to a file
        if strcmp(deformationType, 'none')
            deformString = 'no_deform';
            deformParamString = '';
        else
            deformString = deformationType;
            deformParamString = num2str(deformParam);
        end
        
        output_path = ['Output/' dataset '/' detectionMethod '/' deformString '/'...
            names{i} '-' detectionMethod '-' deformString '-' deformParamString '.png'];
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%
%         labelled = bwlabel(~predicted_nostaff_img);
%         x = label2rgb(labelled, 'hsv', 'w', 'shuffle');
%         imwrite(x, 'rgb_bad.png');
%         
%         correct_nostaff_img = (final_orig_img & (~correct_staff_img));
%         labelled_good = bwlabel(correct_nostaff_img);
%         y = label2rgb(labelled_good, 'hsv', 'w', 'shuffle');
%         imwrite(y, 'rgb_good.png');
        %%%%%%%%%%%%%%%%%%%%%%%%%%
        
        imwrite(predicted_nostaff_img, output_path);
    end