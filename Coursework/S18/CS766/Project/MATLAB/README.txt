REQUIREMENTS:
    - matlab 2017b
    - statistics and machine learning toolbox

*** BIG IDEAS ***
IDEA:
    - machine learning at the pixel level
    - make some feature based on each pixel, use this to classify it
Dataset:
    DALITZ
        - Using the Dalitz dataset because printed music is easier
            - 18 total "modern" scores
            - use 12 for training, 6 held out for testing (chosen randomly)
            - don't use the testing names for ANYTHING until the very last evaluation
              testing_names = {'carcassi01' 'mahler' 'pmw04' 'tye' 'victoria09' 'diabelli'};
        - Apply deformations (as described in his paper) to produce more images
    ICDAR
        - handwritten. pick some random # and test on them
General Ideas:
    - convert into bw image using thresholding
    - flip bits so background (old white) is 0 and foreground (old black) is 1
        - this helps with matlab function (like connectivity testing, etc)
    - want to classify each foreground pixel as staff or nonstaff
    - yes, some background pixels are on the "staff line" but we don't need to worry about 
      that (for now, just focus on REMOVING staff lines so we can segment. 
        - Eventually we'll need to deal with this when we're trying to localize symbols to the staff, but that's far away)


Evaluation:
    - eval performance by combining an accuracy metric with a 
      segmentation metric (i.e. we want to classify most things correctly
      while not breaking up too many symbols) [combine PR or F1 and CC_*]
        *** we want to get all of the staff pixels (recall) 
        *** but not segment any symbols (cc_preserved)
        * (and not too many of the nonstaff pixels (precision))
        * (and not create too many new symbols (cc_count)) [DO WE THIS??]
___________________________________________________________________________

* Call program as:
*   scoreArray removeStaffLines(detectionMethod, deformationType, ...
*                                 deformParam, evalMetric, path, names, dataset)
*   where the files to be tested on are located at path/names{i} (names is a cell)
*
*   Ouput is to a file called "Output/<detectionMethod>/<deformType>/...
*                              name{i}-<detectionMethod>-<deformType>-<deformParam>/png"
*
*   The possibilities for each of the other args are described below.

=== NAIVE ALGORITHMS: ===
    - 'runlength' 
        - Idea:
            - use vertical runlength
        - Notes:
            - works really well on accuracy, but splits up symbols pretty badly
    - 'runlength_gaps'
        - Idea:
            - fix the gaps in the vertical runlenght one
        - Notes:
            - pretty much only works on pefect scores (w/ horizontal staff lines)
                - even 1 degree of rotation gives us 0 recall (i.e. miss 
                  EVERY staff pixel).
            - seems pretty freaking good.
                - doesn't seems to split up much (except the cut time symbol)
                - recall is still >0.95
                - cc_preserved is >0.9, which is AMAZING (some are 1!!!)
            - can't extend this to rotation because of artefacting 
              (i.e. a straight line might be weird after rotating...)
              (--> see the staff lines for 1 degree rotation...)
            - can't combine this with runlength_rotate either because
              of the weird prickly artefacting
    - 'runlength_rotate' (this is mine)
        - Idea:
            - use vertical runlength, but SMARTER
            - first: correct for rotation
                - use a horizontal histogram, look for the ones with
                  the highest peaks (least spread out) --> L2 norm
        - Notes:
            - actually this just doesn't work
            - too much artefacting in what's left over (rotate and rotate 
              over and over makes weird stuff happen)...
            - we'd like something that works in ROTATED or otherwise deformed 
              images
            - could improive runlength more (i.e. for the nonroated case)
              but better to just do way better all at once by using a more 
              robust system)...
    - 'shortest_path' 
        - Idea:
            - find a shortest path from left margin to right margin
            - see Cardoso for explanation
            - rough outline: staff lines don't zigzag left to right
              so just pick out one pixel from each column 
        - Notes:
            - this is hot trash
            - WHY IS THIS SO BAD: 
                - Slow
                - segments symbols
                - can get derailed from the "straight" path for an optimal path if rotated
            - speed doesn't matter THAT much here, but it still matters...
        - Refrences: 
            - Cardoso (A CONNECTED PATH APPROACH FOR STAFF DETECTION ON A MUSIC SCORE)

=== MACHINE LEARNING ALGORITHMS === 
    - Feature vector ideas: 
        - 'window' (3 cluster)
            - use a w by w window with the pixel in the center
            - fv is this window converted into a w^2 length array
        - '4-dist' (2 clusters, including the background pixels)
            - like k-dist, but only 4 dimensions because I'm lazy
            - Notes:
                - recall of 1, but removed EVERY staff line (even in the middle of 
                  notes --> bad cc_preserved score)
                - doesn't work for rotated images (either takes nothing or everything)
                - F1 is good, but cc_preserved is bad.....
        - 'k-dist' (param: k) (UNIMPLEMENTED)
            - k-dim vector, where each dimension is "distance to nearest 
              background pixel along a line with angle 360/k" 
        - 'weird-k-dist' (UNIMPLEMENTED)
            - list k-dist, but along each line only pick the BIGGER ONE
            - i.e. if up is 10 away and down is 100 away, pick down
    - Algorithms:
        - clustering overview:
            - convert each pixels into a vector
                - use clustering to group into staff / nonstaff
                - how to choose which cluster is 'staff'?
                    - min count (doesn't always work - 4301, 722 hamming_3) 
                    - see "computeStaffLabel"
        - 'clustering_4dist'
            - Idea:
            - Notes:
                - works too well in the perfect case (removes parts of symbols!)
                  (recall is 1 for ALL OF THEM, but cc_preserved is really low)
                - doesn't work at all in the rotated case
        - 'clustering_trim_4dist'
            - Idea:
                - trim the lines so we don't break up symbols
                - combine with runlength detector
                - in particular, use 4-dist
            - Notes:    
                - VERY SIMILAR TO clusteringTrimming (and just as successful)
        - 'classifier_log' 'classifier_nn' 'classifier_svm'
            - learn a linear classifier using some FV and learning algo.
            - how to train? is there too much data?

=== COMPUTE STAFF LABEL ===
    - 'freq'
        - pick the less frequent label
    

=== DEFORMATIONS ===
    - 'none'
        - leave the image as it is
    - 'rotate' 
        - deformParam: 
            - theta: degrees counterclockwise to rotate (MUST BE A NUMBER, NOT A STRING)
              (Dalitz param range = -18:1:18)
        - Description:
            - rotate about the center of the image by angle theta
            - uses imrotate
        - Notes:
            - EVERYTHING IS REALLY BAD AT THIS, EVEN DALITZ
    - 'resize'
        - deformParam:
            - scale: factor to shrink down by, in (0,1)
        - Description:    
            - shrink entire image by a factor of scale 
            - produces an image with new dimension
            - uses linear interpolation 
            - uses imresize

=== EVALUATION METRICS ===
    - ACCURACY METRICS: (accuracy, precision, recall, F1)
        - 'acc'
            - Description:
                - accuracy
                - # correctly classified pixels / # black pixels 
                - range of [0, 1], 1 is best
                - i.e. (1 - first Dalitz metric)
            - Results: 
                - not a good metric. Look at 'runlength' -- it works really well 
                  in terms of accuracy, but splits up a lot of symbols...
                - this is an issue, because for classification we first need to 
                  segment the image, but this would split up each object so 
                  classification would be impossible)
        - 'precision'
            - Desciption
                - TP / (TP + FP)
                - Proportion of what we removed that was staff line
        - 'recall'
            - Description
                - TP / (TP + FN)
                - # staff pixels we removed / # of staff pixels
                - proportion of the staff line pixels we correctly removed  
        - 'F1'
            - Description
                - F1 score

    - SEGMENTATION METRICS: (cc_preserved, cc_count)
        - 'cc_preserved'
            - Description:
                - proprtion of connected components that we preserved
                - # of CC's intact (in predicted staffless) / # of CC's in real staffless
            - Notes:
                - assumption: each CC in the staffless image is a symbol
                - this measures (roughly) how many symbols are still usable for classification 
                - this by itself is an awful metric (because it incentivizes removing
                  nothing. Or we could remove ALMOST ALL of each symbol) 
                --> combine with accuracy
            - Restults:         
                - Runlength is much worse now (i.e. not >95% on everything!!)
        - 'cc_count'
            - Description:
                - how badly we messed up connected components, on average
                - # of CC's in the ideal image / # of cc's we had 
                - range is [0,1], 1 is ideal (we can't ever have fewer cc's, 
                  since we only REMOVE black pixels)
                - variation on # 2 from Dalitz
            - Notes:
                - assumption: each CC in the staffless image is a symbol
                - so this measures how badly we broke up symbols (on average)
                - this is a weird metric, I should proably get rid of it
            - Results:
                - when you rotate and use runlength, you get a BUNCH of 
                  broken up staff pieces left, but the accuracy is still great
                  and cc_preserved is unchanged. cc_count is SO LOW THOUGH