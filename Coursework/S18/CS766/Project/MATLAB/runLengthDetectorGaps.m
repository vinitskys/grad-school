function staff_lines_img = runLengthDetectorGaps(bw_img)

    width = size(bw_img,2);
    height = size(bw_img,1);

    % list of all the white runs for this image
    white_runs = []; 
    black_runs = [];

    % index (j,i) is the length of the longest run that pixel j,i is in
    longest_white_run_array = zeros(height, width);
      
    % do each column one by one
    for i = 1:width
        
        current_run = 1;
        for j = 1:height-1 % avoid out of bound errors
            
            % pixel values for this and the pixel under it
            current_val = bw_img(j,i);
            next_val = bw_img(j+1,i);

            if current_val == next_val
                current_run = current_run + 1;
            else
                if current_val == 1
                     % store this as the longest run for all the pixels in it
                    for prev = 0:current_run-1
                        longest_white_run_array(j-prev, i) = current_run;
                    end
                    white_runs = [white_runs current_run];
                    current_run = 1;
                else
                    black_runs = [black_runs current_run];
                    current_run = 1;
                end
            end
        end
    end
    
    longest_white_run = mode(white_runs);
    longest_black_run = mode(black_runs);

    in_longest_run = (longest_white_run_array == longest_white_run) & (bw_img);
    
    %%% I REALLY THINK IVE LOST IT %%%
    
    numshifts = 2*longest_black_run;
    
    seems_staffy_r = bw_img;
    for i=1:numshifts
        seems_staffy_shifted = circshift(seems_staffy_r,i,2);
        seems_staffy_r = seems_staffy_shifted & seems_staffy_r;
    end
    
    seems_staffy_l = bw_img;
    for i=1:numshifts
        seems_staffy_shifted = circshift(seems_staffy_l,-i,2);
        seems_staffy_l = seems_staffy_shifted & seems_staffy_l;
    end
    
    seems_staffy = seems_staffy_r | seems_staffy_l;
        
    staff_lines_img = in_longest_run & seems_staffy;
    
end