% this is VERY similar to runLengthDetector.m
function [longest_white_run, longest_black_run] = computeLongestRuns(bw_img)

    width = size(bw_img,2);
    height = size(bw_img,1);
    
    % list of all the white runs for this image
    white_runs = []; 
    black_runs = [];
    
    % do each column one by one
    for i = 1:width
        
        current_run = 1;
        for j = 1:height-1 % avoid out of bound errors
            
            % pixel values for this and the pixel under it
            current_val = bw_img(j,i);
            next_val = bw_img(j+1,i);

            if current_val == next_val
                current_run = current_run + 1;
            else
                if current_val == 1
                    white_runs = [white_runs current_run];
                    current_run = 1;
                else
                    black_runs = [black_runs current_run];
                    current_run = 1;
                end
            end
        end
    end
    
    longest_white_run = mode(white_runs);
    longest_black_run = mode(black_runs);

