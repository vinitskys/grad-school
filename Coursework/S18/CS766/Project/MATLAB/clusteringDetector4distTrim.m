function staff_lines_img = clusteringTrimmingDetector4dist(bw_img)

    untrimmed_staff_lines = clusteringDetector4dist(bw_img);
    
    runlength_staff_lines = runLengthDetector(bw_img);
    
    trimmed = untrimmed_staff_lines & runlength_staff_lines;
    
    staff_lines_img = trimmed;
end
