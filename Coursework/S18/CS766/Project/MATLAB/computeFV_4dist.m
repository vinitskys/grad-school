% will be [0,0,0,0] for background pixels
function feature_vector_array = computeFV_4dist(bw_img)

    width = size(bw_img,2);
    height = size(bw_img,1);

    fv_array = zeros(height, width, 4);
    
    %%%% 1 is up %%%%
    
    %base case
    for i=1:width
        fv_array(1,i,1) = bw_img(1,i); % assume offscreen is 0s
    end

    for j = 2:height
        for i = 1:width    
            if bw_img(j,i) == 0
                fv_array(j,i,1) = 0;
            else
                fv_array(j,i,1) = fv_array(j-1,i,1) + 1;
            end

        end
    end
   
    %%%% 2 is down %%%%
    
    %base case
    for i=1:width
        fv_array(height,i,2) = bw_img(height,i); % assume offscreen is 0s
    end

    for j = height-1:-1:1
        for i = 1:width    
            if bw_img(j,i) == 0
                fv_array(j,i,2) = 0;
            else
                fv_array(j,i,2) = fv_array(j+1,i,2) + 1;
            end

        end
    end

    %%%% 3 is left %%%%
    
    %base case
    for j=1:height
        fv_array(j,1,3) = bw_img(j,1); % assume offscreen is 0s
    end

    for i = 2:width  
        for j = 1:height  
            if bw_img(j,i) == 0
                fv_array(j,i,3) = 0;
            else
                fv_array(j,i,3) = fv_array(j,i-1,3) + 1;
            end

        end
    end
   
    %%%% 4 is right %%%%
    
    %base case
    for j=1:height
        fv_array(j,width,3) = bw_img(j,width); % assume offscreen is 0s
    end

    for i = width-1:-1:1    
        for j = 1:height
            if bw_img(j,i) == 0
                fv_array(j,i,4) = 0;
            else
                fv_array(j,i,4) = fv_array(j,i+1,4) + 1;
            end

        end
    end
    
    feature_vector_array = fv_array;
    
%     u = fv_array(:,:,1);
%     d = fv_array(:,:,2);
%     l = fv_array(:,:,3);
%     r = fv_array(:,:,4);
%     
%     plot(l.*d);
end