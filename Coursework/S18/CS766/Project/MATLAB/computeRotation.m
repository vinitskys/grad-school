function rotation_angle = computeRotation(bw_img)

    range = 18;
    step = 1;

    norms = [];
    for angle = -range:step:range
        proposed_img = imrotate(bw_img, angle);
        histogram = sum(proposed_img,2);
        
        this_norm = norm(histogram, 2); % the 2-norm
        norms = [norms this_norm];
    end
    
    [m,i] = max(norms);
    correction_angle = (i-1) * step - range;
    rotation_angle = -correction_angle;
        
end

