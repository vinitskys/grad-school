function staff_lines_img = shortestPathDetector(bw_img)
    
    % staff_width: width of each staff line (longest white runlength)
    % staff_space: space between two staff lines (longest black runlength)
    [staff_width, staff_space] = computeLongestRuns(bw_img);

    img_width = size(bw_img,2);
    img_height = size(bw_img,1);
    
    % TABLE: opt(j,i) = length of the shortest path from img(j,i) to
    %                    to the first column
    % RESULT: length of the shortest path is min_j(opt(j,img_width)) 
    opt = zeros(img_height, img_width); 
    prev = zeros(img_height, img_width); % pixel from previous column we chose
    
    % base case
    for j = 1:img_height
        opt(j,1) = 0;
    end
    
    % build up the table
    for i = 2:img_width
        for j = 1:img_height
            
            if j == 1
                choices = [Inf ...
                           opt(j,   i-1) + w(bw_img(j  ,i-1), bw_img(j,i), '4') ...
                           opt(j+1, i-1) + w(bw_img(j+1,i-1), bw_img(j,i), '8')];
            else
                if j == img_height
                    choices = [opt(j-1, i-1) + w(bw_img(j-1,i-1), bw_img(j,i), '8') ...
                               opt(j,   i-1) + w(bw_img(j  ,i-1), bw_img(j,i), '4') ...
                               Inf ];
                else
                    % normal case
                    choices = [opt(j-1, i-1) + w(bw_img(j-1,i-1), bw_img(j,i), '8') ...
                           opt(j,   i-1) + w(bw_img(j  ,i-1), bw_img(j,i), '4') ...
                           opt(j+1, i-1) + w(bw_img(j+1,i-1), bw_img(j,i), '8')];
                end
            end
             
            [min_val, prev_index] = min(choices);
            opt(j,i) = min_val;
            prev(j,i) = j + (prev_index - 2); % (prev, offset) \in (1,-1), (2,0), (3,1)
        end
    end
    
    % find the return value
    [min_length, index] = min(opt(:,img_width));
    
    
    shortest_path_img = zeros(img_height, img_width);
    new_index = index;

    for i = img_width:-1:2
        last_index = new_index;
        new_index = prev(last_index,i);       
        
        height = ceil((staff_width-1) / 2);
        for j = -height:1:height
            try
                shortest_path_img(new_index + j, i) = 1;
            catch
                % pass
            end
        end
    end

    
    staff_lines_img = shortest_path_img;
    
end
    
% according to Cardonos
function weight = w(I0, I1, distance)
    
    multiplier = 1;
    if distance == '8'
        multiplier = sqrt(2);
    end
 
    weight = multiplier * (2 + (1-I0) + (1-I1));
end