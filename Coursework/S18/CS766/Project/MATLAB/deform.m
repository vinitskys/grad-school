function [d_orig_img, d_nostaff_img] = deform(orig_img, nostaff_img, deformation, deformParam);

    switch deformation
        case 'none'
            d_orig_img = orig_img;
            d_nostaff_img = nostaff_img;
        case 'rotate'
            theta = deformParam;
            d_orig_img = imrotate(orig_img, theta, 'bicubic');
            d_nostaff_img = imrotate(nostaff_img, theta, 'bicubic');
        case 'resize'
            scale = deformParam;
            d_orig_img = imresize(orig_img, scale);   
            d_nostaff_img = imresize(nostaff_img, scale);
        otherwise
            d_orig_img = orig_img;
            d_nostaff_img = nostaff_img;
            disp("Not valid deformation method.");
    end