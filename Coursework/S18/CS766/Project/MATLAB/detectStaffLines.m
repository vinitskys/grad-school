% returns a logical img: 1 where there are staff lines, 0 elsewhere
function staff_lines_img = detectStaffLines(bw_img, method)

    switch method
        case 'runlength'
            staff_lines_img = runLengthDetector(bw_img); 
        case 'runlength_gaps'
            staff_lines_img = runLengthDetectorGaps(bw_img); 
        case 'runlength_rotate'
            staff_lines_img = runLengthDetectorRotate(bw_img); 
            
        case 'shortest_path'
            staff_lines_img = shortestPathDetector(bw_img);
        
        case 'clustering_hamming_window_1'
            staff_lines_img = clusteringDetectorWindow(bw_img, 1, 'hamming');
        case 'clustering_hamming_window_2'
            staff_lines_img = clusteringDetectorWindow(bw_img, 2, 'hamming');
        case 'clustering_hamming_window_3'
            staff_lines_img = clusteringDetectorWindow(bw_img, 3, 'hamming');
        case 'clustering_hamming_window_4'
            staff_lines_img = clusteringDetectorWindow(bw_img, 4, 'hamming');
            
        case 'clustering_default_window_1'
            staff_lines_img = clusteringDetectorWindow(bw_img, 1, 'sqeuclidean');
        case 'clustering_default_window_2'
            staff_lines_img = clusteringDetectorWindow(bw_img, 2, 'sqeuclidean');
        case 'clustering_default_window_3'
            staff_lines_img = clusteringDetectorWindow(bw_img, 3, 'sqeuclidean');
        case 'clustering_default_window_4'
            staff_lines_img = clusteringDetectorWindow(bw_img, 4, 'sqeuclidean');    

        case 'clustering_cosine_window_1'
            staff_lines_img = clusteringDetectorWindow(bw_img, 1, 'cosine');
        case 'clustering_cosine_window_2'
            staff_lines_img = clusteringDetectorWindow(bw_img, 2, 'cosine');
        case 'clustering_cosine_window_3'
            staff_lines_img = clusteringDetectorWindow(bw_img, 3, 'cosine');
        case 'clustering_cosine_window_4'
            staff_lines_img = clusteringDetectorWindow(bw_img, 4, 'cosine');    
            
        case 'clustering_cityblock_window_1'
            staff_lines_img = clusteringDetectorWindow(bw_img, 1, 'cityblock');
        case 'clustering_cityblock_window_2'
            staff_lines_img = clusteringDetectorWindow(bw_img, 2, 'cityblock');
        case 'clustering_cityblock_window_3'
            staff_lines_img = clusteringDetectorWindow(bw_img, 3, 'cityblock');
        case 'clustering_cityblock_window_4'
            staff_lines_img = clusteringDetectorWindow(bw_img, 4, 'cityblock');   
            
        case 'clustering_4dist'
            staff_lines_img = clusteringDetector4dist(bw_img);
        case 'clustering_trim_4dist'
            staff_lines_img = clusteringDetector4distTrim(bw_img);
            
        otherwise
            disp("Staff removal method not recognized. Everyting is a staff!!!!")
            staff_lines_img = bw_img;
    end
    