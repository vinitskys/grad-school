function [rgb_stack, gray_stack] = loadFocalStack(focal_stack_dir)
    
    % get the number of images and their size
    trying = 1;
    index = 0;
    while trying
        index = index+1;
        path = [focal_stack_dir '/frame' int2str(index) '.jpg'];

        try    
            img = imread(path);
        catch
            trying = 0;
            index = index - 1; % the last one didn't work
        end
    end
    
    % set the variables
    width = size(img,2);
    height = size(img,1);
    num_imgs = index;

    % need to convert to uint8
    rgb_stack = zeros(height, width, 3*num_imgs);
    gray_stack = zeros(height, width, num_imgs);
    
    % read each image
    for i = 1:num_imgs
        path = [focal_stack_dir '/frame' int2str(i) '.jpg'];   
        
        rgb_img = imread(path);
        k = 3*(i-1) + 1; % 1, 4, 7, ...
        rgb_stack(:,:, k:k+2) = uint8(rgb_img);
        
        gray_img = rgb2gray(rgb_img);
        gray_stack(:,:,i) = gray_img;
    end
    
    