function refocusApp(rgb_stack, index_map)
    
    width = size(rgb_stack,2);
    height = size(rgb_stack,1);
    
    % ummm
    rgb_stack = uint8(rgb_stack);
    
    % initialize
    current_img = rgb_stack(:,:,1:3);
    current_index = 1;
    x = 1;
    y = 1;
    
    fh = figure('Name', 'Choose point to focus on:');
    imshow(current_img);
    
    while 1 % loop forever
        % get the indices
        [x,y] = ginput(1);
        x = uint64(x);
        y = uint64(y);
        
        % break condition
        if ~(0<x && x <= width && 0 < y && y<=height)
            break
        end
        
        next_index = index_map(y,x);
        % going forward
        if current_index < next_index
            for int = current_index:next_index
                k = 3 * (int-1) + 1;
                int_img = rgb_stack(:,:, k:k+2);
                imshow(int_img);
            end
            current_index = next_index;
            
        % going backwards
        else 
            for int = current_index:-1:next_index
                k = 3 * (int-1) + 1;
                int_img = rgb_stack(:,:, k:k+2);
                imshow(int_img);
            end
            current_index = next_index;
        end
        
    end
    
    delete(fh);
    