function index_map = generateIndexMap(gray_stack, w_size) 
    
    %%% DO THE MODIFIED LAPLACIAN TO GET FOCUS MEASURE %%%
    
    % d^2f/dx^2 (partial) from edge detection lecture 
    x_filter = [0 0 0; 1 -2 1; 0 0 0];
    y_filter = [0 1 0; 0 -2 0; 0 1 0];
    
    x_stack = abs(imfilter(gray_stack, x_filter, 'replicate'));
    y_stack = abs(imfilter(gray_stack, y_filter, 'replicate'));
    
    %lap_filter = |d^2f/dx^2| + |d^2f/dy^2|
    lap_stack = x_stack + y_stack;
    
    % sum over the window
    sum_filter = ones(2*w_size+1, 2*w_size+1);
    summed_lap_stack = imfilter(lap_stack, sum_filter, 0);
    
    %%% COMPUTE THE LAYER WITH THE BEST FOCUS %%%
    % DO SMOOTHING
    avg_size = 25;
    avg_filter = fspecial('average', avg_size);
    smoothed_lap_stack = imfilter(summed_lap_stack, avg_filter, 'replicate');
    
    % GET THE MAX FOR EACH PIXEL
    [M, I] = max(smoothed_lap_stack, [], 3);
    index_map = I;