% test code

img0 = 'two_objects.png';
img1 = 'many_objects_1.png';
img2 = 'many_objects_2.png';

threshold = 0.5;

target_img_gray = img0; % image w/ target objects
search_img_gray = img2; % image to find objects in

target_img_labeled = generateLabeledImage(target_img_gray, threshold);
search_img_labeled = generateLabeledImage(search_img_gray, threshold);

[target_obj_db, target_img_marked] = compute2DProperties(target_img_gray, target_img_labeled);

search_img_marked = recognizeObjects(search_img_gray, search_img_labeled, target_obj_db);
imshow(search_img_marked);

imwrite(search_img_marked, 'many_1_found_in_many_2.png');

% for part b's output
% imwrite(target_img_marked,'two_objects_annotated.png');