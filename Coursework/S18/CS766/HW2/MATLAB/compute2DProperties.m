function [obj_db, out_img] = compute2DProperties(gray_img, labeled_img)

    % get the labels
    maxLabel = max(labeled_img(:));
   
    obj_db = [];
   
    %%% GET LABELS %%%
    labels = [];
    for label = 1:maxLabel
       labels = [labels label];
    end
   
   
    %%%% COMPUTE AREA %%%%
    A = zeros(1,maxLabel);
    
    numRows = size(labeled_img,1);
    numCols = size(labeled_img,2);
   
    for i = 1:numRows
        for j = 1:numCols
            pixelLabel = labeled_img(i,j);
            
            if pixelLabel ~= 0
                A(pixelLabel) = A(pixelLabel) + 1;
            end
        end
    end
            
    %%% COMPUTE ROW CENTERS %%%
    rowCenters = zeros(1,maxLabel);
    
    for i = 1:numRows
        for j = 1:numCols
            pixelLabel = labeled_img(i,j);
            
            if pixelLabel ~= 0
                rowCenters(pixelLabel) = rowCenters(pixelLabel) + i;
            end
        end
    end
   
    % divide out the area
    for label = 1:maxLabel
        rowCenters(label) = rowCenters(label) / A(label);
    end
    
    %%% COMPUTE COL CENTERS %%%
    colCenters = zeros(1,maxLabel);
    
    for i = 1:numRows
        for j = 1:numCols
            pixelLabel = labeled_img(i,j);
            
            if pixelLabel ~= 0
                colCenters(pixelLabel) = colCenters(pixelLabel) + j;
            end
        end
    end
   
    % divide out the area
    for label = 1:maxLabel
        colCenters(label) = colCenters(label) / A(label);
    end

    %%% COMPUTE INERTIA %%% 
   
    %%% compute a, b and c
    a = zeros(1,maxLabel);
    b = zeros(1,maxLabel);
    c = zeros(1,maxLabel);
    
    for i = 1:numRows
        for j = 1:numCols
            pixelLabel = labeled_img(i,j);
            
            if pixelLabel ~= 0
                % adjust for the center of the object
                iPrime = i-rowCenters(pixelLabel);
                jPrime = j-colCenters(pixelLabel);
                
                % a is for x (row), c is for col (y)
                a(pixelLabel) = a(pixelLabel) + iPrime^2;
                b(pixelLabel) = b(pixelLabel) + 2*iPrime*jPrime;
                c(pixelLabel) = c(pixelLabel) + jPrime^2;
            end
        end
    end
    
    %%% compute thetas
    theta_1 = 0.5 * atan2(b,a-c);
    theta_2 = theta_1 + pi/2;
    
%     % do the second derivtive test to determine which theta is min/max
%      secondDerivative = (a-c) .* cos(2*theta_1) + b .* sin(2 * theta_1)

    %%% compute E_min and E_max using element-wise operations
    E_min = a .* sin(theta_1).^2 - b .* sin(theta_1) .* cos(theta_1) + c .* cos(theta_1).^2;
    E_max = a .* sin(theta_2).^2 - b .* sin(theta_2) .* cos(theta_2) + c .* cos(theta_2).^2;
    
    % compute orientation (theta is counterclockwise, so need to negate it) 
    
    % ********** %
    orientation = -1 * radtodeg(theta_2); 
    % ********** %
    
    % compute roundness
    roundness = E_min ./ E_max;
    
    labels = double(labels);
    
    obj_db = [labels; rowCenters; colCenters; E_min; orientation; roundness];
    
    out_img = markImage(gray_img,obj_db);
        
end