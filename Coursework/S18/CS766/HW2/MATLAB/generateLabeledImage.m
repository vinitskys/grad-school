function labeled_img = generateLabeledImage(gray_img, threshold)

%      gray_img_open = imread(gray_img);
     binary_img = im2bw(gray_img, threshold);  
     labeled_img = bwlabel(binary_img);
end