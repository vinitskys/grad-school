function out_img = markImage(gray_img,obj_db)

    maxLabel = size(obj_db,2);
    rowCenters = obj_db(2,:);
    colCenters = obj_db(3,:);
    orientation = obj_db(5,:);

    fh1 = figure();
    imshow(gray_img);
    hold on;
    
    for label = 1:maxLabel 

        theta = degtorad(orientation(label));

        % plot the orientation
        hyp = 50; % length of the line
        
        deltaX = hyp * cos(theta);
        deltaY = hyp * sin(theta);
        
        x = [colCenters(label), colCenters(label) + deltaX];
        y = [rowCenters(label), rowCenters(label) + deltaY];
        
        line(x, y, 'LineWidth',2, 'Color', [0, 1, 1]);
        
        % plot the center
        plot(colCenters(label), rowCenters(label), '*', 'color', [1,0,0]); % swap x and y cuz matlab is weird
        
    end
    
    % from demoMATLABTricksFun.m
    out_img= saveAnnotatedImg(fh1);
    
    % close the figure
    delete(fh1);
end

% from demoMATLABTricksFun.m
function annotated_img = saveAnnotatedImg(fh)
figure(fh); % Shift the focus back to the figure fh

% The figure needs to be undocked
set(fh, 'WindowStyle', 'normal');

% The following two lines just to make the figure true size to the
% displayed image. The reason will become clear later.
img = getimage(fh);
truesize(fh, [size(img, 1), size(img, 2)]);

% getframe does a screen capture of the figure window, as a result, the
% displayed figure has to be in true size. 
frame = getframe(fh);
pause(0.5); 

% Because getframe tries to perform a screen capture. it somehow 
% has some platform depend issues. we should calling
% getframe twice in a row and adding a pause afterwards make getframe work
% as expected. This is just a walkaround. 
annotated_img = frame.cdata;
end