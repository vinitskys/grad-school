function output_img = recognizeObjects(gray_img, labeled_img, target_obj_db)

    [search_obj_db, out_img] = compute2DProperties(gray_img, labeled_img);
    
    % want to identify every object that is in both databases, 
    % then plot it in gray_img based off its data from out_db_2
    
    found_obj_db = [];
    for target_label = 1:size(target_obj_db,2)
        for search_label = 1:size(search_obj_db,2)
            
            % objects are columns
            if areSimilar(target_obj_db(:,target_label), search_obj_db(:,search_label))
                % append the info from the searchdatabase
                found_obj_db = [found_obj_db search_obj_db(:,search_label)];
            end
            
        end
    end
    
    output_img = markImage(gray_img, found_obj_db);
end

% a function to compute whether two objects are similar
function similar = areSimilar(obj1, obj2)
    
    similar = 0;
    
    % TEST ROUNDNESS
    roundnessIndex = 6;
    if abs(obj1(roundnessIndex) - obj2(roundnessIndex)) < 0.1 * min(obj1(roundnessIndex), obj2(roundnessIndex))
        similar = 1;
    end
end
