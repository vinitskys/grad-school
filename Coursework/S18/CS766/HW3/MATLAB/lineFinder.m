function line_detected_img = lineFinder(orig_img, hough_img, hough_threshold)

%%%%%%% PARAM %%%%%%%
neighborhoodSize = 3;
%%%%%%%%%%%%%%%%%%%%%

% find all the lines
lines = [];
for i=1:size(hough_img,1)
    for j=1:size(hough_img,2)
        
        cellValue = hough_img(i,j);
        % only consider those above the threshold
        if cellValue > hough_threshold
            
            % find the biggest neighbor
            maxNeighborValue = 0;
            for n = -1 * neighborhoodSize : neighborhoodSize
                for m = -1 * neighborhoodSize : neighborhoodSize
                    
                    % check for out of bounds
                    if i+n <= 0 || j+m <= 0 || i+n > size(hough_img,1) || j + m > size(hough_img,2)
                        thisValue = 0;
                    else
                        thisValue = hough_img(i+n,j+m);    
                    end
                    
                    % ingore the center cell
                    if m ~= 0 || n ~= 0
                        if thisValue > maxNeighborValue
                            maxNeighborValue = thisValue;
                        end

                    end
                end
            end
            
            if cellValue > maxNeighborValue
                lines = [lines; [i j]];                
            end
        end
    end
end

rho_num_bins = size(hough_img,1);
theta_num_bins = size(hough_img,2);
    
rho_values = computeRhoValues(rho_num_bins, orig_img);
theta_values = computeThetaValues(theta_num_bins, orig_img);

fh1 = figure();
imshow(orig_img);
hold on;

for i=1:size(lines,1)
    % matlab is fun
    rho_index = lines(i,1);
    theta_index = lines(i,2);
    
    rho = rho_values(rho_index);
    theta = theta_values(theta_index);

    orig_img = drawLine(orig_img, rho, theta); 
end

% from demoMATLABTricksFun.m
line_detected_img= saveAnnotatedImg(fh1);

% close the figure
% delete(fh1);
   

function new_img = drawLine(orig_img, rho, theta)

if cos(theta) ~= 0
    m = tan(theta);
    b = rho / cos(theta);
else
    disp('VERTICAL LINE DETECTED');
end

x = [0 size(orig_img,2)];
y = m * x + b;

line(x, y, 'LineWidth',2, 'Color', [0, 1, 1]);
new_img = orig_img;

