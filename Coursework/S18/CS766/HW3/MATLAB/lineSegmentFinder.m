function cropped_line_img = lineSegmentFinder(orig_img, hough_img, hough_threshold)

%%%%%%% PARAM %%%%%%%
neighborhoodSize = 3;
%%%%%%%%%%%%%%%%%%%%%

% find all the lines
lines = [];
for i=1:size(hough_img,1)
    for j=1:size(hough_img,2)
        
        cellValue = hough_img(i,j);
        % only consider those above the threshold
        if cellValue > hough_threshold
            
            % find the biggest neighbor
            maxNeighborValue = 0;
            for n = -1 * neighborhoodSize : neighborhoodSize
                for m = -1 * neighborhoodSize : neighborhoodSize
                    
                    % check for out of bounds
                    if i+n <= 0 || j+m <= 0 || i+n > size(hough_img,1) || j + m > size(hough_img,2)
                        thisValue = 0;
                    else
                        thisValue = hough_img(i+n,j+m);    
                    end
                    
                    % ingore the center cell
                    if m ~= 0 || n ~= 0
                        if thisValue > maxNeighborValue
                            maxNeighborValue = thisValue;
                        end

                    end
                end
            end
            
            if cellValue > maxNeighborValue
                lines = [lines; [i j]];                
            end
        end
    end
end

rho_num_bins = size(hough_img,1);
theta_num_bins = size(hough_img,2);
    
rho_values = computeRhoValues(rho_num_bins, orig_img);
theta_values = computeThetaValues(theta_num_bins, orig_img);

fh1 = figure();
imshow(orig_img);
hold on;

thresh = [0.0125, .0312];
edge_img = edge(orig_img, 'canny', thresh); 

for i=1:size(lines,1)
    % matlab is fun
    rho_index = lines(i,1);
    theta_index = lines(i,2);
    
    theta = theta_values(theta_index);
    rho = rho_values(rho_index);
    
    % find all the points IN THE EDGE IMAGE that map to this line
    points = [];
    for y = 1:size(edge_img,1)
        for x = 1:size(edge_img,2)
            
            if edge_img(y,x) == 0
                continue
            end
            
            % the rho for this x,y,theta pair
            thisRho = y * cos(theta) - x * sin(theta);
            
            % thisRho mapped into this bin (and not an earlier one)
            if thisRho < rho
                if rho_index ~= 1
                    prevRho = rho_values(rho_index - 1);
                else
                    prevRho = -1 * Inf;
                end
                
                if prevRho < thisRho
                    points = [points; [x y]];
                end
                
            end
        end
    end
    
%     orig_img = drawPoints(orig_img, points);
    pruned_points = removeOutliersStdDevAlongLine(points);
    extrema = getExtremaFromPoints(pruned_points);  
    orig_img = drawLineFromExtrema(orig_img, extrema);
   
end

cropped_line_img = saveAnnotatedImg(fh1);

% close the figure
% delete(fh1);
% 
% % percentage of the line that is covered
% function metric = computeGoodness(points)
% numPoints = size(points,1);
% extrema = getExtremaFromPointsLazy(points);
% 
% p_1 = [extrema(1,1) extrema(1,2)];
% p_2 = [extrema(2,1) extrema(2,2)];
% 
% d = dist(p_1, p_2);
% 
% metric = numPoints / d^2;
% 
% 
% function pruned_points = removeOutliersFancy(points)
% 
% numPoints = size(points,1);
% 
% prop = 0.1; % only consider removing the top/ bottom prop
% 
% % find the contiguous subarry with maximum goodness
% bestIndices = [0 0];
% bestGoodness = 0;
% 
% goodness = zeros(numPoints, numPoints);
% lastI = int32(prop*numPoints);
% firstJ = max(1, int32(numPoints*(1-prop)));
% 
% for i = 1 : lastI
%     for j = firstJ : numPoints
%         if j < i 
%             continue
%         end
%         proposed_points = points(i:j, 1:2);
%         goodness(i,j) = computeGoodness(proposed_points);
%         if goodness(i,j) > bestGoodness
%             bestGoodness = goodness(i,j);
%             bestIndices = [i j];
%         end
%     end
% end
% 
% bestI = bestIndices(1);
% bestJ = bestIndices(2);
% pruned_points = points(bestI:bestJ, 1:2);


function pruned_points = removeOutliersQuartile(points)
% points is sorted by x coordinate
numPoints = size(points, 1);
outlierThreshold = 0.03;

firstIndex = max(1, int16(numPoints * outlierThreshold));
lastIndex = int16(numPoints * (1-outlierThreshold));

pruned_points = points(firstIndex:lastIndex, 1:2);


% using std deviations along x and y axis
function pruned_points = removeOutliersStdDevXandY(points)
    xList = points(1:size(points,1),1);
    yList = points(1:size(points,1),2);
    
    x_mean = mean(xList);
    x_std_dev = std(xList);
    
    y_mean = mean(yList);
    y_std_dev = std(yList);
    
    pruned_points = [];
    for i = 1:size(points,1)
        x = points(i,1);
        y = points(i,2);

        if (abs(x - x_mean) < 4 * x_std_dev) && (abs(y - y_mean) < 3 * y_std_dev)
            pruned_points = [pruned_points; [x y]];
        end
    end

% using the # of std deviations from the mean point
function pruned_points = removeOutliersStdDevAlongLine(points)
    xList = points(1:size(points,1),1);
    yList = points(1:size(points,1),2);
    
    x_mean = mean(xList);
    y_mean = mean(yList);
    
    center_point = [x_mean y_mean];
    
    distances = [];
    for i = 1:size(points,1)
        x = points(i,1);
        y = points(i,2);
        p = [x y];
        d = dist(p, center_point);
        
        distances = [distances d];
    end
        
    d_mean = mean(distances);
    d_std_dev = std(distances);
    
    pruned_points = [];
    for i = 1:size(points,1)
        x = points(i,1);
        y = points(i,2);
        p = [x y];
        
        d = distances(i);
        if abs(d - d_mean) <= 4 * d_std_dev
            pruned_points = [pruned_points; p];
        end
    end
    
function extrema = getExtremaFromPointsLazy(points)
smallX = points(1,1);
smallY = points(1,2);
bigX = points(size(points(1),1),1);
bigY = points(size(points(1),1),2);

extrema = [[smallX smallY]; [bigX bigY]];
        
    
function extrema = getExtremaFromPoints(points)
pointWithSmallestX = [Inf Inf];
pointWithLargestX = [-Inf -Inf];

for i = 1:size(points,1)
    x = points(i,1);
    y = points(i,2);
        
    if x < pointWithSmallestX(1)
        pointWithSmallestX = [x y];
    end
    
    if x > pointWithLargestX(1)
    pointWithLargestX = [x y];
    end
end

extrema = [pointWithSmallestX; pointWithLargestX];

function orig_img = drawPoints(orig_img, points)

for i = 1 : size(points,1)
    x = points(i, 1);
    y = points(i, 2);
    plot(x,y, '.', 'color', [0, 0, 1]);
end

function orig_img = drawLineFromExtrema(orig_img, extrema)

% extrema is of the form [[x_1 y_1]; [x_2 y_2] ... ]
for i = 1 : size(extrema,1)-1
    line(extrema(i:i+1, 1), extrema(i:i+1, 2),...
        'LineWidth',2, 'Color', [0, 1, 1]);
end