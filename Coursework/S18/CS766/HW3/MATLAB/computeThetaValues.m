function theta_values = computeThetaValues(theta_num_bins, img)

theta_values = zeros(theta_num_bins,1);
for i = 1:theta_num_bins
    % angle of a line must be between 0 and pi
    % i-1 so first is 0, last is 3.11
    maxPi = 0.99 * pi; % so that we don't get repeats at each edge
    theta_values(i) = (maxPi) * (i-1) / theta_num_bins; 
end