function rho_values = computeRhoValues(rho_num_bins, img)

rho_values = zeros(rho_num_bins,1);
maxRho = (size(img,1)^2 + size(img,2)^2)^(1/2); % sqrt(x^2+y^2)
for i =1:rho_num_bins
    % how far into the array
    % want 0 at the middle
    % i-1 as above
    pos = 2 * ((i-1) - rho_num_bins/2) / rho_num_bins; 
    rho_values(i) = maxRho * pos;
end