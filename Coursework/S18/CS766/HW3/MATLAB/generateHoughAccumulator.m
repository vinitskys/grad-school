function hough_img = generateHoughAccumulator(img, theta_num_bins, rho_num_bins)

% initialize the accumulator
acc = zeros(rho_num_bins, theta_num_bins);

% values associated with each cell
theta_values = computeThetaValues(theta_num_bins, img);
rho_values = computeRhoValues(rho_num_bins, img);

% create the accumaltor array
for i=1:size(img,1)
    for j=1:size(img,2)
        if img(i,j) ~= 0
            x = j;
            y = i;
            
             for theta_index = 1:size(theta_values,1)
                 theta = theta_values(theta_index);
                 
                 rho = y * cos(theta) - x * sin(theta);
                 
                 rho_index = -1;
                 for r = 1:size(rho_values,1)
                     if rho < rho_values(r)
                         rho_index = r;
                         break
                     end
                     
                 end
                
                acc = vote(acc, rho_index, theta_index);
             end
        end
    end
end

% scale the accumulator
maxAccValue = 0;
for i = 1:size(acc,1)
    for j=1:size(acc,2)
        maxAccValue = max(acc(i,j), maxAccValue);
    end
end

for i = 1:size(acc,1)
    for j=1:size(acc,2)
        acc(i,j) = uint8(255 * acc(i,j) / maxAccValue);
    end
end

figure; 
imshow(uint8(acc));

hough_img = acc;

% voting function
function acc = vote(acc, rho_index, theta_index)
    acc = simpleVote(acc, rho_index, theta_index);

function acc = simpleVote(acc, rho_index, theta_index)
    acc(rho_index, theta_index) = acc(rho_index, theta_index) + 1;

function acc = lessSimpleVote(acc, rho_index, theta_index)
    acc(rho_index, theta_index) = acc(rho_index, theta_index) + 10;
    
    for i = -2: 2
         for j = -2:2
             new_rho_index = rho_index + i;
             new_theta_index = theta_index + j;
             
             % make sure we're in bounds
             if new_rho_index > 0 && new_theta_index > 0 && new_rho_index  <= size(acc,1) && new_theta_index <= size(acc,2)
                 acc(new_rho_index, new_theta_index) = acc(new_rho_index, new_theta_index) + 1;
             end
         end
      end


% this is bad and doesn't work well (plus is slow)
% function acc = neighborhoodVote(acc, rho_index, theta_index)
% 
%     %%%%%%%% PARAM %%%%%%%%
%     votingNeighborhood = 2;
%     maxVote = 5;
%     %%%%%%%%%%%%%%%%%%%%%%%
%     
%      for i = -1 * votingNeighborhood : votingNeighborhood
%         for j = -1 * votingNeighborhood : votingNeighborhood
%             new_rho_index = rho_index + i;
%             new_theta_index = theta_index + j;
%             
%             % make sure we're in bounds
%             if new_rho_index > 0 && new_theta_index > 0 && new_rho_index  <= size(acc,1) && new_theta_index <= size(acc,2)
%                 vote = max(0, maxVote - (i^2+j^2)^(-1/2));
%                 acc(new_rho_index, new_theta_index) = acc(new_rho_index, new_theta_index) + vote;
%             end
%         end
%      end
%     
   