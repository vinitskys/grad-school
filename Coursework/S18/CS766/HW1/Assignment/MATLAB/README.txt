Parameters:
     - threshold on line 18 of hw1_walkthrough3:
     	- 0.3 made the heart disappear
     	- 1.0 made everything white
     	- everything in (0.4, 0.5, ... 0.9) were pretty similar to one another, and all worked.
     	- settled on 0.9 because it looked the least pixelated
  
Implementation notes:
	- pad top and bottom: line 59. Need to fix dimensions of the array: (line 58)
		mask_width = size(iresized_mask, 2); % need to reassign this

	- making 1 <3 NY opaque in the image:
		- had to set the blue and green channels (in the ??? section) following this paradigm:
		
		red_channel = love_small_nyc(:, :, 1);
		red_channel(iresized_mask) = red(1);
		love_small_nyc(:, :, 1) = red_channel;

Known Bugs:
	- when I run hw1_walkthrough2.m, I get "this image is too big for the screen, scaling by 50%" warnings when trying to display the original van Gogh images. this doesn't seem to be an issue though!
