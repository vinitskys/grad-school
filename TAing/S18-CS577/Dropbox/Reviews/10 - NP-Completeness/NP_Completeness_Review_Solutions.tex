%
% Written by Aravind Soundararajan and Zach Zhou
%

\documentclass{article}
\usepackage{fullpage}

\usepackage{epsfig}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amstext}
\usepackage{amscd}
\usepackage{amsmath}
\usepackage{theorem}
\usepackage{times}
\usepackage{graphicx}
\usepackage{alltt}
\usepackage{algpseudocode}
\usepackage{enumitem}
\usepackage{verbatim}

\begin{document}

%\thispagestyle{empty}

\noindent
\fbox{
\parbox{\textwidth}{
\begin{Large}
{\bf CS 577: Introduction to Algorithms\hfill NP-Completeness Review}
\end{Large}

\vspace{0.02in}
\hfill Aravind Soundararajan, Zach Zhou
}}

\vspace{0.25in}
\noindent\textit{Note to those leading study groups:} Talk mainly about NP-hardness. There's no need to mention NP-completeness unless directly asked. NP-completeness is mentioned throughout the solutions so that they are thorough.

\subsection*{Solutions}

\begin{enumerate}
% Problem 1
\item First, we prove that NAE SAT is in NP. Given an assignment of variables, we can check whether $\psi$ is satisfied such that each clause contains one literal that is 1 and one literal that is 0; this check is of course performed in time polynomial with respect to the number of variables and clauses. Thus, NAE SAT is in NP.\\\\
Next, we prove that NAE SAT is NP-hard by showing that an instance of CNF SAT can be reduced to an instance of NAE SAT. Let $\varphi$ be formula for an instance of CNF SAT and $\psi$ be the formula formed by the reduction to an instance of NAE SAT. These steps outline the reduction from CNF SAT to NAE SAT:
\begin{itemize}
\item For $\psi$, define variables $y_i,\ i = 1, \ldots, n$ where $n$ is the number of variables in $\varphi$, and $z$.
\item For each clause $C_j$ in $\varphi$, create a clause $C_j'$ for $\psi$ by converting all variables $x_i$ in $C_j$ to $y_i$, then adding $z$ to the clause. For example, the clause $(x_1 \lor x_2 \lor x_3)$ in $\varphi$ becomes $(y_1, y_2, y_3, z)$ in $\psi$.
\end{itemize}
$\psi$ can be created in polynomial time. Now we prove that $\psi$ has a satisfying assignment if and only if $\varphi$ has a satisfying assignment.\\\\
Suppose $y^*$ and $z$ form a satisfying assignment for $\psi$. For $i = 1, \ldots, n$, let 
\begin{center}
$x_i^* = 
\begin{cases}
1, & y_i^* \neq z \\
0, & y_i^* = z.
\end{cases}$
\end{center}
Equivalently, we have 
\begin{center}
$\neg x_i^* = 
\begin{cases}
1, & \neg y_i^* \neq z \\
0, & \neg y_i^* = z.
\end{cases}$
\end{center}
Since $y^*$ and $z$ form a satisfying assignment for $\psi$, each clause $C_j'$ in $\psi$ contains a literal that is 1 and a literal that is 0. Furthermore, some $y_i^*$ must differ from $z$, thus some $x_i^*$ in the corresponding clause $C_j$ in $\varphi$ must be 1. Therefore, every clause $C_j$ in $\varphi$ is satisfied, so $\varphi$ is satisfied.\\\\
Suppose $x^*$ is a satisfying assignment for $\varphi$. Consider any clause $C_j$ in $\varphi$; it must be the case that some literal is 1. Thus, we can form a satisfying assignment $y^*$ for $\psi$ by setting $y_i^* \gets x_i^*,\ i = 1, \ldots, n$, and $z \gets 0$. Every clause $C_j^*$ in $\psi$ will be satisfied because some literal will still be 1, and $z$ is 0, so not all literals will be equal. Therefore, $\psi$ is satisfied.\\\\
Since CNF SAT is NP-hard, NAE SAT must also be NP-hard. We conclude that NAE SAT is NP-complete.

% Problem 2
\item First, we prove that Monotone SAT is in NP. Given an assignment of variables, we can check whether $k$ or fewer variables are set to 1 and whether the function is satisfied in polynomial time. Thus, Monotone SAT is in NP.\\\\
Next, we prove that Monotone SAT is NP-hard by showing a reduction from Vertex Cover to Monotone SAT. Let $\mathcal{V}$ denote an instance of Vertex Cover, and $\phi$ denote the Monotone SAT function created by the reduction. In addition, let $G$ denote the graph corresponding to $\mathcal{V}$. These steps outline the reduction:
\begin{itemize}
\item For each vertex $v$ in $G$, create a variable $x_v$ in $\phi$.
\item For each edge $(u, v)$ in $G$, create a clause $(x_u \lor x_v)$ in $\phi$.
\end{itemize}
$\phi$ can be created in polynomial time with respect to the size of $G$. Now we prove that $\phi$ has a satisfying assignment if and only if $\mathcal{V}$ is a "Yes" instance.\\\\
Given a satisfying assignment for $\phi$, we can construct a vertex cover $S$ that satisfies $\mathcal{V}$ by including vertex $u$ in $S$ if and only if $x_u$ is set to 1 in the satisfying assignment for $\phi$. $S$ is a valid vertex cover because for each edge $(u, v)$, because of how $\phi$ was constructed in the reduction, either $x_u$, $x_v$, or both will be set to 1, thus at least one of $u$ or $v$ will be included in $S$. Furthermore, there is a one-to-one correspondence between the variables in $\phi$ and the vertices in $G$, so because $k$ or fewer variables are set to 1 in the satisfying assignment for $\phi$, $k$ or fewer vertices will be included in $S$. Therefore, we have a "Yes" instance for $\mathcal{V}$.\\\\
Given the vertex cover $S$ corresponding to a "Yes" instance for $\mathcal{V}$, we can construct a satisfying assignment for $\phi$ by setting variable $X_u$ to 1 if and only if vertex $u$ is included in $S$. $\phi$ will be satisfied because each clause $(x_u \lor x_v)$ directly corresponds to an edge $(u, v)$ in $G$, and since $S$ is a vertex cover of $G$, either $u$, $v$, or both are included in $S$, thus either $x_u,$, $x_v$, or both are set to 1. Again, since there is a one-to-one correspondence between the variables in $\phi$ and the vertices in $G$, and $k$ are fewer vertices are included in $S$, $k$ or fewer variables will be set to 1 in the satisfying assignment to $\phi$. Therefore, we have a satisfying assignment to $\phi$.\\\\
Since Vertex Cover is NP-hard, Monotone SAT must also be NP-hard. We conclude that Monotone SAT is NP-complete.

% Problem 3
\item First, we prove that Max Cut is in NP. Given a proposed cut $(V_1, V_2)$, we verify that the total cost over all edges $(v_1, v_2)$ such that $v_1 \in V_1$ and $v_2 \in V_2$ is at least $k$.\\\\
Next, we prove that Max Cut is NP-hard. We will introduce a new problem called Unweighted Max Cut: Given a graph $G$ and a number $k$, determine whether $G$ has a cut $(V_1, V_2)$ with at least $k$ edges crossing the cut. Clearly, Unweighted Max Cut reduces to Max Cut (set all edge weights to 1; rigorous proof omitted). Thus, if we show that NAE 3-SAT reduces to Unweighted Max Cut, we will have shown that NAE 3-SAT reduces to Max Cut, thus proving that Max Cut is NP-hard (an underlying assumption is that NAE 3-SAT is NP-hard; the proof for this is omitted).\\\\
Roughly speaking, we will reduce NAE 3-SAT to Unweighted Max Cut by creating a graph such that the max cut partitions the graph into vertices on one side that correspond to literals that have value 1, and vertices on the other side that correspond to literals that have value 0. Let $x_1, \ldots, x_n$ be the $n$ variables in the NAE 3-SAT function $\psi$ with $m$ clauses, and $G = (V, E)$ be the graph associated with the Max Cut instance. These steps outline the reduction:
\begin{itemize}
\item Add $2n$ vertices to $G$, namely $x_1, \ldots, x_n, \neg x_1, \ldots, \neg x_n$.
\item For each clause $(l_1, l_2, l_3)$ in $\psi$, we add edges $(l_1, l_2)$, $(l_2, l_3)$, and $(l_3, l_1)$ to $G$, thus adding $3m$ edges to $G$.
\item For each variable $x_i$ in $\psi$, add an edge from $x_i$ to $\neg x_i$ in $G$, thus adding $n$ edges to $G$.
\item Now, we have $3m + n$ edges in $G$. Let $k = 2m + n$.
\end{itemize}
Clearly, $G$ can be constructed in polynomial time. Now we prove that this reduction is correct.\\\\
Suppose $\psi$ has a satisfying assignment. Then the max cut partition is as follows: $V_1$ contains all vertices corresponding to variables in $\psi$ assigned 1, and $V_2$ contains all the other vertices. The cut will have one edge for each variable in $\psi$, as $x_i$ and $\neg x_i$ cannot both be in $v_1$ or $V_2$. In addition, for each clause, two literals will lie on one side of the cut, and one on the other. Thus, two edges will cross the cut for each clause. Therefore, the total number of edges crossing the cut is $2m + n$, which is the same as $k$.\\\\
Conversely, suppose $G$ has a cut of size $k = 2m + n$. For each variable $x_i$, either both $x_i$ and $\neg x_i$ lie on the same side of the cut, or they lie on opposite sides. Thus, a variable contributes at most one edge to the cut. For a clause, either all literals lie on one side of the cut, or two lie on one and one on the other. Thus, a clause contributes at most two edges to the cut. Therefore, if there is a solution to Max Cut, then every variable contributes an edge and every clause contributes two edges. Furthermore, we can use this solution to construct a satisfying assignment for $\psi$.\\\\
% TODO: Not sure if I want to talk about the "multigraph" thing from the solutions.
Since NAE 3-SAT is NP-complete, Max Cut must also be NP-complete.

% Problem 4
\item We can reduce the Hamiltonian path problem to the Longest Simple Path problem by setting all edge costs to 1 and $k$ to $|V| - 1$; if there is indeed a longest simple path of length $k$, then we conclude all vertices were reached. Since the Hamiltonian path problem is NP-complete, Longest Simple Path must also be NP-complete.

% Problem 5
\item First, we prove that Shortest Simple Path is in NP. Given a proposed simple path, we can verify whether the path costs at most $k$. Thus, Shortest Simple Path is in NP.\\\\
Next, we prove that Shortest Simple Path is NP-hard by showing a reduction from Longest Simple Path to Shortest Simple Path. We reduce an instance of Longest Simple Path $\mathcal{L}$ to an instance of Shortest Simple Path $\mathcal{S}$ by negating all edge weights in $G$, the graph corresponding to $\mathcal{L}$, to obtain $G'$, a graph we will associate to $\mathcal{S}$. We then determine whether $G'$ has a simple path of cost at least $-k$.\\\\
Since Longest Simple Path is NP-hard, Shortest Simple Path must also be NP-hard. We conclude that Shortest Simple Path is NP-complete.\\\\
\textit{Note:} If the edge costs are nonnegative, Dijkstra's algorithm will solve Shortest Simple Path in polynomial time. Equivalently, we can solve Longest Simple Path in polynomial time if the edge costs are nonpositive. Recall the Bellman-Ford algorithm, which appears to be a valid solution to Shortest Simple Path. The reason we cannot use Bellman-Ford here is in the case of negative-cost cycles; Bellman-Ford will simply report the existence of such a cycle and terminate, thus it is an unacceptable algorithm for Longest Simple Path.


% Problem 6
\item\begin{enumerate}[label = (\alph*)]
\item Another term for a graph being 2-colorable is that it is bipartite.\\
A \textit{Bipartite Graph} is a graph whose vertices can be divided into two independent sets, $U$ and $V$ such that every edge ($u$, $v$) either connects a vertex from $U$ to $V$ or a vertex from $V$ to $U$. In other words, for every edge ($u$, $v$), either $u$ belongs to $U$ and $v$ to $V$, or $u$ belongs to $V$ and $v$ to $U$. We can also say that there is no edge that connects vertices of same set.\\ \\ 

Following is a simple algorithm to find out whether a given graph is Bipartite or not using Breadth First Search (BFS).
\begin{enumerate}
\item Assign RED color to the source vertex - randomly chosen source vertex (putting into set $U$).
\item Color all the neighbors with BLUE color (putting into set $V$).
\item Color all neighbor's neighbor with RED color (putting into set $U$).
\item This way, assign color to all vertices such that it satisfies all the constraints of $m$-way coloring problem where $m = 2$.
\item While assigning colors, if we find a neighbor which is colored with same color as current vertex, then the graph cannot be colored with 2 vertices (or graph is not Bipartite)
\end{enumerate}

\item
2-SAT is a special case of Boolean Satisfiability Problem and can be solved in polynomial time.

Now, 2-SAT limits the problem of SAT to only those Boolean formula which are expressed as a CNF with every clause having only 2 terms (also called 2-CNF).
For the CNF value to come TRUE, value of every clause should be TRUE.
Let one of the clause be (A $\vee$ B).\\
(A $\vee$ B) = TRUE

\begin{itemize}
\item If A = 0, B must be 1 i.e. ($\bar{A} \Rightarrow B$)
\item If B = 0, A must be 1 i.e. ($\bar{B} \Rightarrow A$)
\end{itemize}

Thus, (A $\vee$ B) = TRUE is equivalent to ($\bar{A} \Rightarrow B$) $\wedge$ ($\bar{B} \Rightarrow A$)
Now, we can express the CNF as an Implication. So, we create an Implication Graph which has 2 edges for every clause of the CNF.
(A $\vee$ B) is expressed in Implication Graph as edge($\bar{A} \rightarrow B$) and edge($\bar{B} \rightarrow A$).\\
Thus, for a Boolean formula with 'm' clauses, we make an Implication Graph with:

\begin{itemize}
\item 2 edges for every clause i.e. '2m' edges.
\item 1 node for every Boolean variable involved in the Boolean formula.
\end{itemize}

Now, consider the following cases:
\begin{itemize}
\item \underline{CASE 1:} If edge($X \rightarrow \bar{X}$) exists in the graph
This means ($X \Rightarrow \bar{X}$).
If X = TRUE, $\bar{X}$ = TRUE, which is a contradiction.
But if X = FALSE, there are no implication constraints.
Thus, X = FALSE
\item \underline{CASE 2:} If edge($\bar{X} \rightarrow X$) exists in the graph
This means ($\bar{X} \Rightarrow X$).
If $\bar{X}$ = TRUE, X = TRUE, which is a contradiction.
But if $\bar{X}$ = FALSE, there are no implication constraints.
Thus, $\bar{X}$ = FALSE i.e. X = TRUE
\item \underline{CASE 3:} If edge($X \rightarrow \bar{X}$) and edge($\bar{X} \rightarrow X$) both exist in the graph.
One edge requires X to be TRUE and the other one requires X to be FALSE.
Thus, there is no possible assignment in such a case.
\end{itemize}
\end{enumerate}
If any two variables X and $\bar{X}$ are on a cycle i.e. path($\bar{A} \rightarrow B$) and path($\bar{B} \rightarrow A$) both exists, then the CNF is unsatisfiable. Otherwise, there is a possible assignment and the CNF is satisfiable.
Note here that, we use path due to the following property of implication:
If we have ($A \Rightarrow B$) and ($B \Rightarrow C$),  then  $A \Rightarrow C$
Thus, if we have a path in the Implication Graph, that is pretty much same as having a direct edge.

% Problem 7
\item We will reduce from CNF SAT. Given a CNF SAT formula $\varphi = C_1 \land \ldots \land C_m$ with clauses $C_1, \ldots, C_m$ and variables $x_1, \ldots, x_n$, we can construct an instance of the solitaire problem $\mathcal{S}$ so that $\mathcal{S}$ has a solution if and only if $\varphi$ has a satisfying assignment. These steps outline the reduction:
\begin{itemize}
\item Create an $m \times n$ solitaire grid $A$. Each row $i$ will correspond to clause $C_i$, and each column $j$ will correspond to variable $x_j$.
\item For each clause $C_i$,
\begin{itemize}
\item If $x_j$ is present in nonnegated form (as $x_j$), put a blue stone in $A[i][j]$.
\item If $x_j$ is present in negated form (as $\neg x_j$), put a red stone in $A[i][j]$.
\end{itemize}
\end{itemize}
\textit{Note:} The reduction assumes that $x_j$ and $\neg x_j$ cannot both appear within the same clause. If they are, then because $x_j \lor \neg x_j = 1$, discarding $x_j \lor \neg x_j$ will have no impact on $\varphi$.\\\\
We prove that $\mathcal{S}$ has a solution if and only if $\varphi$ has a satisfying assignment.\\\\
Suppose that $x_j^*,\ j = 1, \ldots, n$ is a satisfying assignment to $\varphi$. Upon constructing $\mathcal{S}$, we remove red stones from column $j$ if $x_j^* = 1$, or blue stones otherwise ($x_j^* = 0$). This leaves every column with one type of stone in it. We now verify that every row has at least one stone in it. Consider a row $i$. Since $x_j^*,\ j = 1, \ldots, n$ is a satisfying assignment to $\varphi$, some literal in $C_i$ is set to 1, thus there must exist some $j$ such that the stone in the $i$th row and $j$th column is left alone. Therefore, every row of $\mathcal{S}$ has a stone, hence $\mathcal{S}$ is solvable.\\\\
Now suppose that $\mathcal{S}$ is solvable. This means that every column contains only one type of stone (or has no stones in it, in which case we associate some arbitrary stone type to it). Moreover, for each row, there is a stone remaining in that row. This solution to $\mathcal{S}$ can be used to construct a satisfying assignment $x_j^*,\ j = 1, \ldots, n$ to $\varphi$. For each column $j$, if the column contains only blue stones, set $x_j^* := 1$, else (column contains only red stones) set $x_j^* := 0$ (or set $x_j^*$ arbitrarily if the column contains no stones). For any clause $C_i$, we know that in the solution to $\mathcal{S}$, there is some stone in some column $j$ in the $i$th row. This corresponds to the literal set to 1. Thus, every clause in $\varphi$ is satisfied, and so $\varphi$ is satisfied. This concludes the proof.

% Problem 8
\item For each vertex $v$ in $G = (V, E)$, associate to $v$ a decision variable $x_v$. The following ILP is a reduction of vertex cover:
\begin{equation*}
\begin{aligned}
& \text{min}
& & \sum\limits_{v \in V} x_v \\
& \text{s.t.}
& & x \geq 0 \\
&&& x \leq 1 \\
&&& x_u + x_v \geq 1,\ \forall (u, v) \in E.
\end{aligned}
\end{equation*}
For the decision variant of vertex cover with target $k$, an instance of vertex is a "Yes" instance if there exists some feasible $x$ such that $\sum\limits_{v \in V} x_v \leq k$.

% Problem 9
\item\begin{enumerate}[label = (\alph*)]
\item An equivalent way of solving this problem is determining whether it is possible to drop some edges from $G$ such that each vertex in $G$ only has one outgoing edge and one incoming edge; if this is true, then every vertex belongs to a node-disjoint cycle. Thus, the problem can be reformulated as a matching problem, where each vertex is matched to the "next" vertex in its cycle.
\begin{comment}
Thus, the problem can be reformulated as a matching problem as follows:
\begin{itemize}
\item Create two copies of the vertex set $V_1$ and $V_2$, a source $s$, and sink $t$; these define the vertex set of the flow network.
\item For every vertex $v$ in $V_1$, add an edge $(s, v)$ with capacity 1 to the flow network. For every vertex $v$ in $V_2$, add an edge $(v, t)$ with capacity 1 to the flow network.
\item For every edge $(u, v)$ in $G$, add an edge $(u_{V_1}, v_{V_2})$ with capacity 1 to the flow network, where $u_{V_1}$ is the vertex in $V_1$ corresponding to $u$ and $v_{V_2}$ is vertex in $V_2$ corresponding to $v$.
\item Find the max $s$-$t$ flow. If the value of the flow is equal to $|V|$, then $G$ has a cycle cover, otherwise it does not.
\end{itemize}
\end{comment}

\item 
\end{enumerate}

% Problem 10
\item 

% Problem 11
\item 

\end{enumerate}

\end{document}
