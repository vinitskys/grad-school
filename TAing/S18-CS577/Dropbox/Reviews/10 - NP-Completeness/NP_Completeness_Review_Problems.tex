%
% Written by Aravind Soundararajan and Zach Zhou
%

\documentclass{article}
\usepackage{fullpage}

\usepackage{epsfig}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amstext}
\usepackage{amscd}
\usepackage{amsmath}
\usepackage{theorem}
\usepackage{times}
\usepackage{graphicx}
\usepackage{alltt}
\usepackage{algpseudocode}
\usepackage{enumitem}

\begin{document}

%\thispagestyle{empty}

\noindent
\fbox{
\parbox{\textwidth}{
\begin{Large}
{\bf CS 577: Introduction to Algorithms\hfill NP-Completeness Review}
\end{Large}

\vspace{0.02in}
\hfill Aravind Soundararajan, Zach Zhou
}}

\subsection*{Definitions}

\begin{itemize}
\item A \textit{Boolean function} is a formula in propositional logic built using variables, the operators AND ($\land$), OR ($\lor$), and NOT ($\neg$), and parentheses. A \textit{literal} is a Boolean variable or the negation of a Boolean variable. Any Boolean function can be expressed in \textit{conjunctive normal form (CNF)}, which those of you who have taken a class in digital logic would call product-of-sums. For example, 
\begin{equation*}
(x_1 \lor x_2 \lor x_3) \land (\neg x_1 \lor x_3) \land (\neg x_1 \lor x_5)
\end{equation*}
is a Boolean function expressed in CNF. Each parenthetical term is a \textit{clause}.

\item The Boolean satisfiability problem (SAT) is the problem of deciding whether there is an assignment of all variables such that a given Boolean function evalutes to 1 (True). SAT is NP-complete. A specific variant of SAT is CNF-SAT, where the Boolean function is in CNF. SAT reduces to CNF-SAT, thus CNF-SAT is NP-complete.

\item A \textit{monotone Boolean function} is a Boolean function where all variables are non-negated. For example, 
\begin{equation*}
(x_1 \lor x_2) \land (x_1 \lor x_3) \land (x_3 \lor x_4 \lor x_5)
\end{equation*}
is a monotone Boolean function, whereas 
\begin{equation*}
(x_1 \lor x_2 \lor x_3) \land (\neg x_1 \lor x_3) \land (\neg x_1 \lor x_5)
\end{equation*}
is not. Note that a monotone Boolean function always has a satisfying assignment -- the one where all variables are set to 1.

\item A \textit{simple} path in a graph is one that does not visit the same vertex twice.

\item The Hamiltonian Path problem is the problem of determining whether a Hamiltonian path, a path in a graph that visits each vertex exactly once, exists. The Hamiltonian Path problem is NP-complete.
\end{itemize}

\subsection*{Problems}

For problems 1-5, prove the NP-hardness of the given problems.

\begin{enumerate}
% Problem 1
\item\textit{Not-All-Equal SAT (NAE SAT):} Determine whether a Boolean function $\psi$ is satisfiable such that the literals in each clause are not all equal to each other.

% Problem 2
\item\textit{Monotone SAT:} Determine whether a monotone Boolean function $\phi$ is satisfiable when $k$ or fewer variables are set to 1.

% Problem 3
\item\textit{Max Cut:} Given a graph $G$ with nonnegative edge weights and a number $k$, determine whether $G$ has a cut $(V_1, V_2)$ with cost at least $k$. (\textit{Hint:} Reduce from NAE SAT.)

% Problem 4
\item\textit{Longest Simple Path:} Given a graph $G$ and a number $k$, determine whether $G$ (which \textit{can} contain negative cost edges) contains a simple path of cost \textit{at least} $k$. (\textit{Hint:} Reduce from the Hamiltonian Path problem.)

% Problem 5
\item\textit{Shortest Simple Path:} Given a graph $G$ and a number $k$, determine whether $G$ (which \textit{can} contain negative cost edges) contains a simple path from a vertex $u$ to a vertex $v$ of cost \textit{at most} $k$.

% Problem 6
\item\begin{enumerate}[label = (\alph*)]
\item Describe and analyze a polynomial-time algorithm for 2-Color. Given an undirected graph $G$, your algorithm will determine in polynomial time whether $G$ has a proper coloring that uses only two colors.
\item Describe and analyze a polynomial-time algorithm for 2-SAT. Given a boolean formula $\phi$ in CNF, with exactly two literals per clause, your algorithm will determine in polynomial time whether $\phi$ has a satisfying assignment.
\item The problem 12-Color is defined as follows: Given an undirected graph $G$, determine whether we can color each vertex with one of twelve colors, so that every edge touches two different colors. Prove that 12-Color is NP-hard. (Hint: Reduce from 3-Color.)
\item The problem 12-SAT is defined as follows: Given a boolean formula $\phi$ in CNF, with exactly twelve literals per clause, determine whether $\phi$ has a satisfying assignment. Prove that 12-Sat is NP-hard. (Hint: Reduce from 3SAT.)
\end{enumerate}

% Problem 7
\item Consider the following solitaire game. The puzzle consists of an $m \times n$ grid of squares, where each square may be empty, occupied by a red stone, or occupied by a blue stone. The goal of the puzzle is to remove some of the given stones so that the remaining stones satisfy two conditions: (1) every row contains at least one stone, and (2) no column contains stones of both colors. For some initial configurations of stones, reaching this goal is impossible. Prove that it is NP-hard to determine, given an initial configuration of red and blue stones, whether the puzzle can be solved.
\begin{center}
\includegraphics[width=0.7\textwidth]{solitaire_problem.JPG}
\end{center}

% Problem 8
\item Integer linear programming (ILP) is an optimization problem that can be formulated as follows:
\begin{equation*}
\begin{aligned}
& \text{min}
& & c'x \\
& \text{s.t.}
& & Ax \geq b,
\end{aligned}
\end{equation*}
where the decision variables $x_i$ are additionally constrained to be integral. The decision variant of ILP also includes a target value $k$; an ILP is considered a "Yes" instance if there exists some $x$ such that $x$ is feasible and $c'x \leq k$. Show that ILP is NP-Hard by reducing vertex cover to it.

% Problem 9
\item Given a directed graph $G$, a cycle cover is a set of node-disjoint cycles so that each node of $G$ belongs to a cycle. The Cycle Cover problem asks whether a given directed graph has a cycle cover.
\begin{enumerate}[label = (\alph*)]
\item Show that the Cycle Cover problem can be solved in polynomial time (Hint: Use bipartite matching).
\item Suppose we require each cycle to have at most three edges. Show that determining whether a graph $G$ has such a cycle cover is NP-hard.
\end{enumerate}

% Problem 10
\item In 1-in-3 SAT, we want to find whether there is an assignment that satisfies exactly one literal in every clause. Show a reduction from  3-coloring to 1-in-3 SAT.

% Problem 11
\item The RectangleTiling problem is defined as follows: Given one large rectangle and several smaller rectangles, determine whether the smaller rectangles can be placed inside the large rectangle with no gaps or overlaps. Prove that RectangleTiling is NP-hard.
\begin{center}
\includegraphics[width=0.7\textwidth]{tiling_problem.JPG}
\end{center}

\end{enumerate}

\end{document}
