%
\documentclass{article}
\usepackage{fullpage}

\usepackage{epsfig}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amstext}
\usepackage{amscd}
\usepackage{amsmath}
\usepackage{theorem}
\usepackage{times}
\usepackage{graphicx}
\usepackage{alltt}
\usepackage{algpseudocode}
\usepackage{enumerate}

\begin{document}

%\thispagestyle{empty}

\noindent
\fbox{
\parbox{\textwidth}{
{\bf CS 577: Introduction to Algorithms \hfill Dynamic Programming Review: Solutions}

}}

\begin{enumerate}
\item A natural idea for a dynamic program for this question is to set up a table $SUM[i,j]$ which stands for the sum of numbers in the contiguous subarray $A[i \dots j]$. The recurrence relation is simply $SUM[i,j] = SUM[i, j-1] + A[j]$. If we have the table, we can just return $SUM[1,n]$ as the result. However, this table is of size $\Theta(n^2)$ and computing each cell takes $O(1)$ time, so the running time of this simple algorithm does not satisfy the requirement. To design an algorithm that runs in only $O(n)$ time for this question, our algorithm can only visit each cell a constant number of times.

As a common idea in dynamic programming, we would like to set up a recursive equation by trying to decide if or not to include element $A[n]$ in the sum. If we don't include $A[n]$ in the sum, then we are left with the same subproblem over $A[1 \dots n-1]$; if we include $A[n]$ in the sum, then this sum is over a contiguous subarray of $A[1 \dots n]$ that ends at $A[n]$, which means we need to compute the largest sum over a suffix of $A[1 \dots n-1]$.

Now we have two things to compute over the array $A[1 \dots n-1]$: the largest sum in a subarray of $A[1 \dots n-1]$ and the largest sum in a suffix subarray of $A[1 \dots n-1]$. To generalize this idea, we define the following two functions on $1, 2, \dots, n$:
\begin{align*}
OPT[i] & = \text{ the largest sum in the contiguous subarray of } A[1..i],\\
OPT_{suffix}[i] & = \text{ the largest sum in a suffix of } A[1..i].
\end{align*}
Note that the arguments to these functions will always be prefixes of $A$, which explains why we can just pass the last index of the subarray we are considering as the argument.

According to the observation we make, we have the following recurrence relation:
\begin{align*}
OPT[i] & = \operatorname{max}(OPT[i-1], \ OPT_{suffix}[i]),\\
OPT_{suffix} & = \operatorname{max}(OPT_{suffix}[i-1] + A[i], \ A[i]).
\end{align*}
Now we make this to be a dynamic program:
\begin{algorithmic}
\State $OPT[1] = OPT_{suffix}[1] = A[1]$
\For{$i = 2 \ldots n$}
\State Determine $OPT_{suffix}[i]$ using the recurrence relation described above.
\EndFor
\For{$i = 2 \ldots n$}
\State Determine $OPT[i]$ as above.
\EndFor
\State \Return $OPT[n]$
\end{algorithmic}

This algorithm is correct due to the analysis above. Computing each cell of $OPT$ and $OPT_{suffix}$ takes $O(1)$ time, and there are $O(n)$ cells in each of them. Therefore, the running time of this dynamic program is $O(n)$.

Now we give a modification of the above algorithm. Instead of computing $OPT[i]$ for each $1 \le i \le n$ to get $OPT[n]$ as our final result, we only need to know the information of $OPT_{suffix}$ and go over all the values in it. The largest $OPT_{suffix}[i]$ among all possible $1 \le i \le n$ is the desired result, since the contiguous subarray with the largest sum has to end in cell $i$ for some $i$. This modification doesn't change the running time, because now we don't need to compute $OPT$ in $O(n)$ time but have to go over $OPT_{suffix}$ in $O(n)$ time. However, we now save the space that $OPT$ used to occupy with this modified algorithm.


\item Let OPT($A$) denote the fewest number of coins required to make change for $A$. Then we have the following recurrence for OPT($A$):

$$
\text{OPT}(A) = 1 + \min_{1\leq i \leq n} (\text{OPT}(A-a_i))
$$

If we choose one coin of value $a_i$, we then need to make change for $A - a_i$. The fewest number of coins required for this is OPT$(A - a_i)$ by definition of OPT. 

An algorithm for this is as follows:

\begin{enumerate}[1.]
\item OPT$(0) = 0$
\item For each $k \in \{1, ... A\}$:
\item $\textsc{lowest} = \min_i(a_i)$
\item $\text{OPT}(A) = 1 + \min_{1\leq i \leq n} (\text{OPT}(A-a_i))$ (if $A-a_i < 0$, then OPT($A-a_i) = \infty$
\end{enumerate}

As discussed above, OPT($A$) is indeed the output we want. OPT$(k)$ will be infinity whenever there is no way to make change for $k$.

If there are $m$ denominations, then our algorithm runs in time $O(mA)$. 

\item Let $b_i$ be the $i$'th operation. Note that a multiplication operation could be maximized by minimizing the two arguments if both arguments are negative. Thus, we will find both the maximum and minimum. Let $L_i$ be the expression to the left of $b_i$ and $R_i$ be the expression to the right of $b_i$. Let $E$ be the entire expression.

$$
\text{MAX}(E) = \max_{1\leq i \leq n-1}
\begin{cases}
\text{MAX}(L_i) \cdot b_i \cdot \text{MAX}(R_i) \\
\text{MAX}(L_i) \cdot b_i \cdot \text{MIN}(R_i) \\
\text{MIN}(L_i) \cdot b_i \cdot \text{MAX}(R_i) \\
\text{MAX}(L_i) \cdot b_i \cdot \text{MIN}(R_i)
\end{cases}
$$

$$
\text{MIN}(E) = \min_{1\leq i \leq n-1}
\begin{cases}
\text{MAX}(L_i) \cdot b_i \cdot \text{MAX}(R_i) \\
\text{MAX}(L_i) \cdot b_i \cdot \text{MIN}(R_i) \\
\text{MIN}(L_i) \cdot b_i \cdot \text{MAX}(R_i) \\
\text{MAX}(L_i) \cdot b_i \cdot \text{MIN}(R_i)
\end{cases}
$$

\item To construct our algorithm, we begin by making a few observations about palindromes.
\begin{itemize}
\item[$\cdot$] Any palindrome is its own shortest palindromic supersequence.
\item[$\cdot$] Any sequence of length 1 is a palindrome. The empty sequence will, by convention, be considered a palindrome. Sequences of length 0 or 1 will be used as the base cases for our algorithm.
\item[$\cdot$] A sequence $A[1, \dots, n]$ with $n \geq 2$ is a palindrome if and only if its first and last elements are equal (i.e., $A[1] = A[n]$) and the rest of the sequence, $A[2, \dots, (n-1)]$, is a palindrome.
\item[$\cdot$] If a sequence $A[1, \dots, n]$ has $A[1] = A[n]$, then the shortest palindromic supersequence of $A$ can be obtained by expanding the rest of the sequence, $A[2, \dots, (n-1)]$, into its shortest palindromic supersequence.
\item[$\cdot$] If $A[1] \ne A[n]$, determining the shortest palindromic supersequence is a little trickier. The shortest palindromic supersequence must have $A[1]$ as its first element or else have $A[n]$ as its last element. (Otherwise, we've added unnecessary elements which can be removed.) With this in mind, we try expanding $A$ in two ways, at least one of which must yield a shortest palindromic supersequence. First, we determine the shortest palindromic supersequence of $A[1, \dots, (n-1)]$ and add $A[n]$ to the beginning and end of this sequence. Second, we determine the shortest palindromic supersequence of $A[2, \dots, n]$ and add $A[1]$ to the beginning and end of the new sequence.
\end{itemize}
With these observations in mind, we are now ready to construct our algorithm. The algorithm takes as input a sequence $A$ and indices $i$ and $j$ and returns the shortest palindromic supersequence of $A[i, \dots, j]$. The following are the steps of the algorithm. The $+$ operator is used to denote sequence concatenation.
\\\\
\noindent $SPS(A, i, j)$:
\begin{itemize}
\item[(a)] If $A[i, \dots, j]$ has length 0 or 1 (i.e., $i \geq j$), simply return $A[i, \dots, j]$.
\item[(b)] Otherwise, if the first and last elements are the same ($A[i] = A[j]$), perform a recursive call to expand the rest of the sequence into its shortest palindromic supersequence. Return $A[i] + SPS(A, i+1, j-1) + A[j]$.
\item[(c)] Else, we're in the tricky case and must perform two recursive calls to determine which palindromic supersequence is shortest.
\begin{itemize}
\item[(i)] Perform a recursive call $SPS(A, i, j-1)$ on the sequence with the last element excluded. The first candidate answer is $A[n] + SPS(A, i, j-1) + A[n]$.
\item[(ii)] Perform another recursive call $SPS(A, i+1, j)$ on the sequence with the first element excluded. The second candidate answer is $A[1] + SPS(A, i+1, j) + A[1]$.
\item[(iii)] Of the candidate answers found in (i) and (ii), return whichever one is shorter. (If they are of the same length, arbitrarily choose one to return.)
\end{itemize}
\end{itemize}
This algorithm can be memoized so as to obtain a dynamic program by first creating an $n \times n$ table, in which the $(i, j)$ table entry will store $SPS(A, i, j)$. Our final answer will be the value in the $(1, n)$ cell. We may immediately fill in the entries with $i \geq j$, as these correspond to the base cases of our algorithm. (Note that when $i > j$, we understand $A[i, \dots, j]$ to denote an empty sequence.) There are $\frac{n(n+1)}{2} = O(n^2)$ such cells in the table, each taking $O(1)$ work to fill in. \\

\noindent We may now fill in the rest of the table entries using the recursive structure of our algorithm. The order in which we fill in these cells is important; proceeding carelessly may result in us making a recursive call to a cell that hasn't been filled in yet. One such order is to first fill in the $j = i+1$ diagonal, then the $j = i+2$ diagonal, etc., until the table is complete. (There are several valid orders which proceed by row or column instead.) There are $\frac{n(n-1)}{2} = O(n^2)$ such cells, and constructing and writing the appropriate sequence in each cell takes $O(n)$ work. \\

\begin{algorithmic}
\For{$j = 1 \ldots n$}
\For{$i = n \ldots 1$}
\If{$ i \geq j$}
\State $M[i, j] = A[i, \ldots j]$
\ElsIf{$A[i] = A[j]$}
\State $M[i, j] = A[i] + M[i+1, j-1] + A[j]$
\Else $M[i, j]$ = shorter of $(A[1] + M[i+1, j] + A[1]), (A[n] + M[i, j-1] + A[n])$
\EndIf
\EndFor
\EndFor
\Return $M[1, n]$
\end{algorithmic}

The total work needed to fill in the table in this way is $O(n^2)[O(1) + O(n)] = O(n^3)$. This can be improved to $O(n^2)$ by filling in the $i < j$ cells cleverly: instead of writing the entire correct sequence within each cell, we can just include a pointer to the appropriate recursive call cell, reducing the work for these cells to $O(1)$ each. We can then construct the correct final answer by starting at the $(1, n)$ cell and following the pointers through the table.

\end{enumerate}

\end{document}
