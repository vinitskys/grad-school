\documentclass{article}
\usepackage{fullpage}
\usepackage{subfiles}

\usepackage{epsfig}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amstext}
\usepackage{amscd}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{times}
\usepackage{graphicx}
\usepackage{alltt}

\usepackage{tikz}
\usetikzlibrary{matrix}
\usetikzlibrary{calc}
\usetikzlibrary{positioning}


\newtheorem{claim}{Claim}

\usepackage[linesnumbered,lined,boxed,commentsnumbered,ruled,vlined]{algorithm2e}

\begin{document}

\noindent
\fbox{
\parbox{\textwidth}{
\begin{Large}
{\bf CS 577: Introduction to Algorithms\hfill Homework 6 Solutions}
\end{Large}
}}
\newcommand{\OPT}{\mathrm{OPT}}
\section*{Problem 1}
We're given a time limit $x$ and a graph (V, E) with special vertices $s$ and $t$ corresponding to Madison and Seattle, respectively. Let $d(u, v)$ be the time required to go from vertex $u$ to vertex $v$. Let $position(u)$ be the number assigned to the vertex $u$, that defines the order in which Alice wants to visit the cities. We want to find the longest $s-t$ path $(v_1, v_2, ..., v_k)$, where $v_1 = s$ and $v_k = t$, such that $\sum_{i=1}^{k-1}d(v_i,v_{i+1}) \leq x$ and each vertex in the path must be numbered higher than the previous vertex in the path. 

Let $s = v_1$, $t = v_n$, and assume position($v_i$) $<$ position($v_{i+1}$) for $1 \leq i < n$. We know $position(s) = 1$ and $position(t) = n$

Intuitively, suppose we arrive at $v_i$ and we'd like to visit $j$ vertices on the remainder of our trip (including $v_i$ and $t$). Then, wherever we go next, we will still need to visit $j - 1$ additional vertices. This motivates the following definition. Let $\OPT[i,j]$ be the smallest amount of time required to go from $v_i$ to $t$ along a valid path while visiting $j$ vertices (including $v_i$ and $t$). In the base cases, $\OPT[n, 1] = 0$ (it takes no time to visit just Seattle when already in Seattle) and $\OPT[i, j] = +\infty$ for all $n - i + 1 < j$ (Since we are at $v_i$, we have $n - i$ more vertices that are numbered higher than $v_i$. It's impossible to visit more than this $n - i + 1$ number of vertices, including $v_i$ along the path to $t$). Let $k^*[j]$ be the list of vertices along the optimal valid path from $v_1$ to $v_n$ visiting exactly $j$ vertices. Then the following Bellman equation characterizes $\OPT[i, j]$.

\begin{align}
	\OPT[i,j] = \left\{
    	\begin{array}{lcl}
			0\ \ & : &\text{if } i=n \text{ and } j=1\\
            +\infty\ & : &\text{if}\ n-i+1<j\\
            \min_{k>i}(\OPT[k, j - 1] + d(v_i, v_k))\ & : &\text{otherwise}\\
		\end{array}
	\right.
\end{align}



\paragraph{Algorithm:}
\begin{enumerate}
\item Sort the vertices in order of increasing positions, breaking ties so that $t$ appears last.
\item Set $k^* \gets [\ ]$
\item For $i$ in $n$ to $1$, $j$ in $1$ to $n$
	\begin{enumerate}
      \item Set $k^*[j] \gets \{1\}$
      \item If $i = n$ and $j = 1$, Set $\OPT[i, j] \gets 0$
      \item Else If $n - i + 1 < j$, Set $\OPT[i, j] \gets +\infty$
      \item Else
      	\begin{enumerate}
       	  \item Set $minDuration \gets +\infty$ and $minK \gets i + 1$
          \item For $k$ in $i + 1$ to $n$
            \begin{enumerate}
              \item $duration \gets \OPT[k, j - 1] + d(v_i, v_k)$
              \item If $duration < minDuration$, set $minDuration \gets duration$ and $minK \gets k$
            \end{enumerate}
            \item Set $\OPT[i,j] \gets minDuration$ and add the vertex $minK$ to $k^*[j]$
        \end{enumerate}
	\end{enumerate}
    \item For $j$ in $n$ to $1$, find the largest $j$ such that $\OPT[1, j] \leq x$ and return $k^*[j]$
\end{enumerate}

The optimal path from $s$ to $t$ can be reconstructed by following the vertices in $k^*$ in order.\\
The correctness of this algorithm follows by induction on $n - i$. Clearly the algorithm is correct when $i = n$ or $n - i + 1 < j$. We previously argued that the Bellman equation is correct for $\OPT[i,j]$ assuming correct values for $\OPT[k,j]$ where $i < k \leq n$.
It takes $O(n \log n)$ time to sort the vertices; evaluating (1) takes $O(n)$ time, and there are $O(n^2)$ entries in the table, so filling it requires $O(n^3)$ time; and reconstructing the path takes $O(n)$ time. The total run time is therefore $O(n^3)$.

\section*{Problem 2} 

We will give a dynamic program which constructs the optimal stack using larger and larger subsets of the turtles, in a manner similar to the knapsack algorithm. That is, we build up solutions to subproblems restricted to the first $i$ items, then increment $i$. For the knapsack problem, the order in which we considered items didn't matter. Here, however, the order in which turtles are added to the stack determines how many more turtles can be added, so we must carefully consider the order in which we choose turtles.

Consider any stack of $k$ turtles $\sigma$ = ($t_1$,$t_2$,...,$t_k$), where $t_1$ is on the bottom and $t_k$ is on top. Let $s(t)$ be the strength of turtle $t$ and $w(t)$ the weight. We use an exchange argument to show that the turtles in $\sigma$ can be stacked in decreasing order by strength: suppose, for some index $i$, that s($t_i$) $<$ s($t_{i+1}$). Then we can swap the order of $t_i$ and $t_{i+1}$, because the weight carried by $t_i$ in the new order does not increase, and the weight carried by $t_{i+1}$ is at most the  weight previously carried by $t_i$ which, by feasibility of the stack $\sigma$, is at most s($t_i$) $<$ s($t_{i+1}$). In particular, an optimal stack can be so arranged. In the rest of the solution, we assume the turtles are numbered in order of decreasing strength (as given on the input - i.e. before considering any particular stack). That is, s($t_1$) $\geq$ s($t_2$) $\geq$ ... $\geq$ s($t_n$).

Define the strength of a stack of turtles to be the maximum weight that can be added to the stack. Given a stack $\sigma$ = ($t_1$,....,$t_k$) and a particular turtle $t_i$ in $\sigma$, let r($\sigma$, $t_i$) = s($t_i$) - $\sum_{j=i}^{k}w(t_j)$. So r($\sigma$, $t_i$) is the difference between $t_i$'s strength and the weight it is actually carrying in $\sigma$. The strength of the stack is the minimum of this quantity over all turtles: $\min_{t_i \in \sigma}(r(\sigma, t_i))$

Consider the weakest turtle; does it belong in an optimal stack? Suppose we decide we want to include this turtle; then we need to find the largest subset of the first $n - 1$ turtles which can form a stack strong enough to support the final turtle. Otherwise, we should simply find the tallest possible stack drawn from the first $n - 1$ turtles. In general, when considering the $i$th turtle, we want to make the highest possible stack which has strength at least s $\geq$ 0. Formally, let $\OPT[i, s]$ be the maximum number of turtles in a stack with strength at least $s$ using at most the first $i$ turtles; we have just sketched an argument for the following Bellman equation:

\begin{align}
	\OPT[i,s] &= \max \left\{
    	\begin{array}{lcl}
			\OPT[i - 1, s]\\
            \OPT[i - 1, s + w(t_i)] + 1, \text{only if } s(t_i) - w(t_i) \geq s\\
		\end{array}
	\right.
\end{align}

This equation leads to an algorithm which solves the problem. But the run time would depend on the strength of the strongest turtle, making the algorithm pseudo-polynomial. We can do better by swapping the roles of the (potentially exponential) strength and the (polynomial) count: that is, by putting the strength in the table and making the count an index.

Let $\OPT'[i, h]$ to be the maximal strength of any stack of h turtles drawn from the first $i$ turtles. To determine $\OPT'[i, h]$, we must decide whether or not to include $t_i$. If not, then clearly $\OPT'[i, h]$ is the strength of a stack of $h$ turtles drawn from the first $i - 1$. If we do include $t_i$, then $\OPT'[i, h]$ is the strength of a stack formed by placing $t_i$ on top of an optimal stack of $h - 1$ turtles drawn from the first $i - 1$. The strength of this stack is at most $\OPT'[i - 1, h - 1] - w(t_i)$ (the strength of the base stack after adding $t_i$), or else $s(t_i) - w(t_i)$ (the capacity of $t_i$ to carry weight beyond its own), whichever is smaller. We have derived the following Bellman equation for $\OPT'[i, h]$.

\begin{align}
	\OPT'[i,h] = \max \left\{
    	\begin{array}{lcl}
			\OPT'[i - 1, h]\\
            \min(\OPT'[i - 1, h - 1], s_i) - w_i\\
		\end{array}
	\right.
\end{align}

We take as base cases when $i = 0$ (representing that no turtles are available) or $h = 0$.
For these, we can set $\OPT'[i,0] = +\infty$ for all $i = 0,1,\ldots,n$,
since any weight of turtles can be stacked on an empty tower,
and $\OPT'[0,h] = -\infty$ for $h > 0$ since it is impossible to make a tower
of positive height with no turtles.

\paragraph{Algorithm:}
We initialize the base cases. Then, for $i = 1,...,n$ and $h = 1,...,n$ we set $\OPT'[i,h]$ according to (3). Finally, to recover the solution, we want the largest value of $h$ for which the strongest stack of $h$ turtles has non-negative strength; in other words, we return $max \{ h : \OPT'[n, h] \geq 0 \}$.
\pagebreak[4]
\begin{enumerate}
\item Sort turtles by decreasing order of strength, break ties by sorting on decreasing order of weights.
\item For $i$ in $0$ to $n$, set $\OPT'[i,0] \gets +\infty$
\item For $h$ in $1$ to $n$, set $\OPT'[0,h] \gets -\infty$

\item For $i$ in $1$ to $n$, For $h$ in $1$ to $i$
  \begin{enumerate}
		\item $weightIncludingithTurtle = \min (\OPT'[i-1, h-1], s_i) - w_i$
		\item $weightExcludingithTurtle = \OPT'[i-1, h]$
		\item Set $\OPT'[i, h] \gets \max(weightIncludingithTurtle, weightExcludingithTurtle)$
  \end{enumerate}
\item For $h$ in $n$ to $0$, find the largest $h$ such that $\OPT'[n, h] \neq + \infty$ and return it.
\end{enumerate}

\paragraph{Correctness follows by induction:}
Our algorithm clearly computes the correct value for $\OPT'[i, h]$ in the base cases (when at least one of $i$ or $h$ is 0). We argued in the paragraph preceding our Bellman equation (3) that it is correct for a given value of $i$, assuming we know the correct values of $\OPT'[i - 1, h]$ for all $h$. Thus our algorithm finds the highest stack possible among all stacks in which the turtles are sorted by strength. Since we have previously argued that the highest such stack is optimal, our algorithm is correct.

\paragraph{The run time analysis is straightforward:}
Sorting the turtles takes $O(n \log n)$ time; initialization requires $O(n)$ time; evaluating the Bellman equation takes $O(1)$ time, and the table has $O(n^2)$ entries, so filling it out takes $O(n^2)$ time; and finding the final answer takes $O(n)$ time. The total run time is therefore $O(n^2)$.

\end{document}
