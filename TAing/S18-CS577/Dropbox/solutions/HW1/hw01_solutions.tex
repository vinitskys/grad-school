%
\documentclass[11pt]{article}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Header: DO NOT MODIFY %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{fullpage}
\usepackage{epsfig}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amstext}
\usepackage{amsmath}
\usepackage{xspace}
\usepackage{theorem}
\usepackage{enumerate}
\usepackage{graphicx}

% This is the stuff for normal spacing
\makeatletter
 \setlength{\textwidth}{6.5in}
 \setlength{\oddsidemargin}{0in}
 \setlength{\evensidemargin}{0in}
 \setlength{\topmargin}{0.25in}
 \setlength{\textheight}{8.25in}
 \setlength{\headheight}{0pt}
 \setlength{\headsep}{0pt}
 \setlength{\marginparwidth}{59pt}

 \setlength{\parindent}{0pt}
 \setlength{\parskip}{5pt plus 1pt}
 \setlength{\theorempreskipamount}{5pt plus 1pt}
 \setlength{\theorempostskipamount}{0pt}
 % \setlength{\abovedisplayskip}{8pt plus 3pt minus 6pt}

 % \renewcommand{\section}{\@startsection{section}{1}{0mm}%
 %                                   {2ex plus -1ex minus -.2ex}%
 %                                   {1.3ex plus .2ex}%
 %                                   {\normalfont\Large\bfseries}}%
 % \renewcommand{\subsubsection}{\@startsection{subsubsection}{2}{0mm}%
 %                                     {1ex plus -1ex minus -.2ex}%
 %                                     {1ex plus .2ex}%
 %                                     {\normalfont\large\bfseries}}%
 % \renewcommand{\subsubsection}{\@startsection{subsubsection}{3}{0mm}%
 %                                     {1ex plus -1ex minus -.2ex}%
 %                                     {1ex plus .2ex}%
 %                                     {\normalfont\normalsize\bfseries}}
 % \renewcommand\paragraph{\@startsection{paragraph}{4}{0mm}%
 %                                    {1ex \@plus1ex \@minus.2ex}%
 %                                    {-1em}%
 %                                    {\normalfont\normalsize\bfseries}}
 % \renewcommand\subparagraph{\@startsection{subparagraph}{5}{\parindent}%
 %                                       {2.0ex \@plus1ex \@minus .2ex}%
 %                                       {-1em}%
 %                                      {\normalfont\normalsize\bfseries}}
\makeatother

\newenvironment{proof}{{\bf Proof:  }}{\hfill\rule{2mm}{2mm}}
\newenvironment{proofof}[1]{{\bf Proof of #1:  }}{\hfill\rule{2mm}{2mm}}
\newenvironment{proofofnobox}[1]{{\bf#1:  }}{}
\newenvironment{example}{{\bf Example:  }}{\hfill\rule{2mm}{2mm}}

\renewcommand{\theequation}{\thesection.\arabic{equation}}
\renewcommand{\thefigure}{\thesection.\arabic{figure}}

\newtheorem{fact}{Fact}[section]
\newtheorem{lemma}[fact]{Lemma}
\newtheorem{theorem}[fact]{Theorem}
\newtheorem{definition}[fact]{Definition}
\newtheorem{corollary}[fact]{Corollary}
\newtheorem{proposition}[fact]{Proposition}
% \newtheorem{claim}[fact]{Claim}
\newtheorem{claim}{Claim}
\newtheorem{exercise}[fact]{Exercise}

% math notation
\newcommand{\R}{\ensuremath{\mathbb R}}
\newcommand{\Z}{\ensuremath{\mathbb Z}}
\newcommand{\N}{\ensuremath{\mathbb N}}
\newcommand{\F}{\ensuremath{\mathcal F}}
\newcommand{\SymGrp}{\ensuremath{\mathfrak S}}

\newcommand{\CCP}{\ensuremath{\sf P}}
\newcommand{\CCNP}{\ensuremath{\sf NP}}
\newcommand{\C}{\ensuremath{\mathcal C}}

\newcommand{\size}[1]{\ensuremath{\left|#1\right|}}
\newcommand{\ceil}[1]{\ensuremath{\left\lceil#1\right\rceil}}
\newcommand{\floor}[1]{\ensuremath{\left\lfloor#1\right\rfloor}}
\newcommand{\poly}{\operatorname{poly}}
\newcommand{\polylog}{\operatorname{polylog}}

% some abbreviations
\newcommand{\e}{\epsilon}
\newcommand{\half}{\ensuremath{\frac{1}{2}}}
\newcommand{\junk}[1]{}
\newcommand{\sse}{\subseteq}
\newcommand{\union}{\cup}
\newcommand{\meet}{\wedge}

\newcommand{\prob}[1]{\ensuremath{\text{{\bf Pr}$\left[#1\right]$}}}
\newcommand{\expct}[1]{\ensuremath{\text{{\bf E}$\left[#1\right]$}}}
\newcommand{\Event}{{\mathcal E}}

\newcommand{\mnote}[1]{\normalmarginpar \marginpar{\tiny #1}}

\newcommand{\headings}[4]{
\noindent
\fbox{\parbox{\textwidth}{
{\bf CS577: Introduction to Algorithms (Spring 2018)} \hfill {{\bf Scribe:} #2}\\
\vspace{0.02in}\\
{{\bf HW#1 Solutions}} \hfill {{\bf Date:} #3}
}}
\vspace{0.2in}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Document begins here %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}


%% Fill in lecture number, topic, your name, and date of lecture below. %%

\newcommand{\lecnum}{1}
\headings{\lecnum}{Sam Vinitsky}{2/8/2018}


$\mathbf{1a)}$ We find the defective coin via binary search.
The main idea is that the testing procedure allows
us to check whether an even-size set $S$ of coins contains
the defective coin.
This is done by partitioning $S$ into two sets, $S_1$ and $S_2$, of equal size,
and checking whether $S_1$ and $S_2$ have the same weight.
Their weights are different if and only if $S$ contains the bad coin. 
We can write this algorithm out explicitly:

\begin{enumerate}[ ]
\item $\textsc{containsDefectiveCoin(S)}$
\begin{enumerate}[1.]
\item Split $S$ into two halves $S_1$ and $S_2$ (each with $n/2$ coins).
\item Weigh $S_1$ against $S_2$.
\item If they weigh the same, return $\textsc{false}$.
\item Otherwise return $\textsc{true}$.
\end{enumerate}
\end{enumerate}

From here,
the rest of the algorithm is a standard binary search:
given a collection $C$ of $2^k$, $k>2$, coins, partition $C$
into two sets $A$ and $B$ of equal size.
Then test whether the bad coin is in $A$ (using the above procedure);
if it is, recurse on $A$; otherwise, recurse on $B$.

In the base case of a collection $C$ of $2^2$ ($k=2$) coins,
say $\{c_1,c_2,c_3,c_4\}$,
we can just check whether $\{c_1,c_2\}$ and whether
$\{c_1,c_3\}$ contain the bad coin.
Depending on the results of these two tests,
we can determine whether the coin is $c_1,c_2,c_3,$ or $c_4$. 
We can write this algorithm out explicitly:

% We now explicitly describe this algorithm, starting with the subroutine to test if a set contains the defective coin:

% Now we describe the algorithm for \textit{finding} the defective coin:

\begin{enumerate}[ ]
\item $\textsc{findDefectiveCoin}(C)$:
\begin{enumerate}[1.]
\item Let $n$ denote the number of coins in $C$.
\item If $n = 4$: // base case
\begin{enumerate}[a)]
\item Suppose $C = \{c_1,c_2,c_3,c_4\}$
\item Let $r = \textsc{containsDefectiveCoin}(\{c_1,c_2\})$ 
\item Let $r' = \textsc{containsDefectiveCoin}(\{c_1,c_3\})$ 
\begin{enumerate}[-]
\item If $r = r' = \textsc{true}$, return $c_1$.
\item If $r = r' = \textsc{false}$, return $c_4$.
\item If $r = \textsc{false}$ and $r' = \textsc{true}$, return $c_3$.
\item If $r = \textsc{true}$ and $r' = \textsc{false}$, return $c_2$.
\end{enumerate}
\end{enumerate}

\item Split $C$ into two halves, $A$ and $B$ (each with $n/2$ coins).
\item If $\textsc{containsDefectiveCoin}(A)$, then return $\textsc{findDefectiveCoin}(A)$.
\item Else, return $\textsc{findDefectiveCoin}(B)$.
\end{enumerate}
\end{enumerate}

$\mathbf{1b)}$ We can obtain the number of tests by solving the recurrence $T(k) = T(k-1) +1 $ and $T(2) = 2$. We also provide an inductive proof.

\begin{claim} \label{claim:5b-explicit}
  For all $k = 2,3,\ldots$,
  and for all coin-sets $C$ of size $2^k$,
  $\textsc{Find-Coin}(C)$ makes $k$ tests.
  % on input $C$.
\end{claim}

\begin{proof}
By induction on $k$:

\begin{itemize}
\item Base case:
  When $k = 2$, we are in the base case,
  where two tests are made no matter which coin is bad.
  Since the number of tests equals $k$, the claim holds.

\item Recursive case:
  When $k > 2$,
  we fall into the recursive case.
  In this case, there is one explicit test,
  in addition to tests made by the recursive call.
  In the recursive call,
  the set passed to the recursive call has size $|C|/2 = 2^{k-1}$,
  and so the inductive hypothesis applies.
  From this, we conclude that the recursive call makes $k-1$
  tests.
  Consequently, our algorithm performs $k$ tests on input $C$.
\end{itemize}
\end{proof}

$\mathbf{1c)}$

\begin{claim} \label{claim:5c-explicit}
  For all $k = 2,3,\ldots$
  and for all coin-sets $C$ of size $2^k$
  containing exactly one bad coin,
  our algorithm
  returns the defective coin in $C$.
\end{claim}

\begin{proof}
By induction on $k$:
\begin{itemize}
\item Base case ($k = 2$):
  When $k = 2$, we are in the base case,
  in which there are four coins,
  $c_1,c_2,c_3,c_4$. 
  Suppose both $\{c_1,c_2\}$ and $\{c_1,c_3\}$
  contain the bad coin.
  Then it must be $c_1$.
  Hence our algorithm will correctly return $c_1$.
  The remaining cases similarly follow by direct inspection.

\item Recursive case ($k > 2$):
  When $k > 2$,
  we fall into the recursive case.
  Let $A, B$ be the algorithm's partition of $C$ into
  two halves of equal size.
  There is exactly one defective coin, $c$,
  so it is either in $A$ or in $B$:

  \begin{itemize}
  \item Case 1 ($c \in A$):
  If $c \in A$, $\textsc{containsDefectiveCoin}(A)$ returns \textsc{true} and our algorithm recurses on $S$.
  Since $A$ has size $|C|/2 = 2^{k-1}$
  and since $A$ contains exactly one defective coin,
  the assumptions in the inductive hypothesis hold.
  Therefore the conclusion of the inductive hypothesis holds,
  and so the recursive call correctly returns $c$.

  \item Case 2 ($c \in B$):
  If $c \in B$,
  then $\textsc{containsDefectiveCoin}(A)$ returns \textsc{false} and our algorithm recurses on $B$.
  As before, the inductive hypothesis applies,
  and so the recursive call returns $c$.
  \end{itemize}

  In both cases, our algorithm returns $c$,
  so Claim~\ref{claim:5c-explicit} is proven.

\end{itemize}
\end{proof}

\pagebreak

$\mathbf{2a)}$ Consider calling $\textsc{SlowSort}$ on $A = [4,3,2,1]$: (without lines 6-8 of $\textsc{Gather}$)

\begin{itemize}
\item Step 2 of $\textsc{SlowSort}$ will sort $[4,3]$ to be $[3,4]$. Now $A = [3,4,2,1]$
\item Step 3 of $\textsc{SlowSort}$ will sort $[2,1]$ to be $[1,2]$. Now $A = [3,4,1,2]$
\item Step 4 of $\textsc{SlowSort}$ will call $\textsc{Gather}$ on $A = [3,4,1,2]$.
\begin{itemize}
\item Step 9 of $\textsc{Gather}$ will recurse on $A' = [3,4]$. The recursive call will find $A'[1] < A'[2]$, and thus will leave $A'$ in place. So after step 9, $A= [3,4,1,2]$ still.
\item Step 10 of $\textsc{Gather}$ will recurse on $A' = [1,2]$. The recursive call will find $A'[1] < A'[2]$, and thus will leave $A'$ in place. So after step 10, $A= [3,4,1,2]$.
\item Step 11 of $\textsc{Gather}$ will recurse on $A' = [4,1]$. The recursive call will find $A'[1] > A'[2]$. It will then swap $4$ and $1$. So after step 11, $A = [3,1,4,2]$ (since we swapped 1 and 4).
\item Therefore after $\textsc{Gather}$ runs, we have $A = [3,1,4,2]$
\end{itemize}
\item After $\textsc{SlowSort}$ calls $\textsc{Gather}$ it does nothing to $A$, which was $A = [3,1,4,2]$. 
\item Therefore at the end of $\textsc{SlowSort}$, we have $A = [3,1,4,2]$ which is not sorted!
\end{itemize}

$\mathbf{2b)}$ Consider calling $\textsc{SlowSort}$ on $A = [3,4,1,2]$: (with lines 10 and 11 of $\textsc{Gather}$ swapped)

\begin{itemize}
\item Step 2 of $\textsc{SlowSort}$ will sort $[3,4]$ to be $[3,4]$. Now $A = [3,4,1,2]$ still.
\item Step 3 of $\textsc{SlowSort}$ will sort $[1,2]$ to be $[1,2]$. Now $A = [3,4,1,2]$ still.
\item Step 4 of $\textsc{SlowSort}$ will call $\textsc{Gather}$ on $A = [3,4,1,2]$.
\begin{itemize}
\item Steps 6-8 will swap 4 and 1, resulting in $A = [3,1,4,2]$.



\item Step 9 of $\textsc{Gather}$ will recurse on $A' = [3,1]$. The recursive call will find $A'[1] > A'[2]$, and thus it will swap $3$ and $1$. So after step 9, $A = [1,3,4,2]$.
\item Step 11 of $\textsc{Gather}$ will recurse on $A' = [3,4]$. The recursive call will find $A'[1] < A'[2]$, and thus it will do nothing. This means that after step 11, $A = [1,3,4,2]$ still. (Step 11 is called before step 10 by assumption)
\item Step 10 of $\textsc{Gather}$ will recurse on $A' = [4,2]$. The recursive call will find $A'[1] > A'[2]$, and thus it will swap $4$ and $2$. So after step 10, $A=[1,3,2,4]$.
\item Therefore after $\textsc{Gather}$ runs, we have $A = [1,3,2,4]$
\end{itemize}
\item After $\textsc{SlowSort}$ calls $\textsc{Gather}$ it does nothing to $A$, which was $A = [1,3,2,4]$. 
\item Therefore at the end of $\textsc{SlowSort}$, we have $A = [1,3,2,4]$ which is not sorted!
\end{itemize}

$\mathbf{2c)}$ Looking the structure of $\textsc{SlowSort}$, we see $\textsc{Gather}$ essentially performs the same role as the $\textsc{Merge}$ subroutine of $\textsc{MergeSort}$. We will prove the following claim:

\begin{claim}\label{gatherGood}
If $n$ is a power of $2$, and $A$ is an list such that $A[1 \ldots \frac{n}{2}]$ and $A[\frac{n}{2}+1 \ldots n]$ are both sorted, then $\textsc{Gather}(A)$ will return a sorted list.
\end{claim}

% Note that a valid input to $\textsc{Gather}$ could be $[1,5,2,5]$, since each half is sorted (even though the halves are not sorted with respect to each other). We will prove this claim using strong induction:

\begin{proof} We proceed by induction of $k= \log_2 n$.

\begin{itemize}
\item Base Case: If $n=2$, we have two cases:
\begin{itemize}
\item $A[1] < A[2]$: we do nothing, and output $A$. This is correct, since $A$ was already sorted.
\item $A[1] > A[2]$: we swap $A[1]$ and $A[2]$ in line 3. Now $A$ is sorted. 
\end{itemize}
\item Inductive Hypothesis: Given an list $A$ length $n = 2^{k-1}$, with $A[1 \ldots \frac{n}{4}]$ and $A[\frac{n}{4}+1 \ldots \frac{n}{2}]$ already sorted, $\textsc{Gather}(A)$ outputs $A$ in sorted order.
\item Inductive Step: Consider running $\textsc{Gather}(A)$ for some list $A$ of length $n = 2^k$. We will consider the ``quarters'' of $A$:

\begin{align*}
Q_1 &= A\big[1 \ldots \frac{n}{4}\big] \\
Q_2 &= A\big[\frac{n}{4}+1 \ldots \frac{n}{2}\big] \\
Q_3 &= A\big[\frac{n}{2}+1 \ldots \frac{3n}{4}\big] \\
Q_4 &= A\big[\frac{3n}{4}+1 \ldots n\big]
\end{align*}

Let $L_1  \circ L_2$ denote the concatenation of the lists $L_1$ and $L_2$. Using our new notation, we can write $A  = Q_1  \circ Q_2  \circ Q_3  \circ Q_4$. Note that each of the quarters is sorted. 

% maybe this should be itemize?
Consider the execution of $\textsc{Gather}$ on a list $A  = Q_1  \circ Q_2  \circ Q_3  \circ Q_4$ of length $n$.
After steps 6-8 , the list will be $Q_1 \circ Q_3 \circ Q_2 \circ Q_4$.
Step 9 recurses on $Q_1 \circ Q_3$, which it sorts by the inductive hypothesis (since this is a list with $\frac{n}{2} = 2^{k-1}$ elements, and $Q_1$ and $Q_3$ are sorted).
Step 10 recurses on $Q_2 \circ Q_4$, which it sorts by the inductive hypothesis (since this is a list with $\frac{n}{2} = 2^{k-1}$ elements, and $Q_2$ and $Q_4$ are sorted).
Therefore after steps 9 and 10, the list will be $Q_1' \circ Q_2' \circ Q_3' \circ Q_4'$. 

Observe that $Q_1' \circ Q_2'$ is a sorting of $Q_1 \circ Q_3$. 
Then $Q_1'$ contains the smallest $\frac{n}{4}$ elements of $A$ in sorted order, since all of these elements had to be in $Q_1$ or $Q_3$. 
Likewise, $Q_4'$ contains the largest $\frac{n}{4}$ elements of $A$ in sorted order. 
This means that that the middle $\frac{n}{2}$ elements of $A$ (by size) are in $Q_2' \circ Q_3'$.

Step 11 recurses on $Q_2' \circ Q_3'$, which it sorts by the inductive hypothesis (since $Q_2'$ and $Q_3'$ are each sorted).
After step 11, the list will be $Q_1' \circ Q_2'' \circ Q_3'' \circ Q_4$. 
Note that $Q_2'' \circ Q_3''$ is a sorting of the middle $\frac{n}{2}$ elements of $A$.
Now $Q_1'$ contains the smallest $\frac{n}{4}$ elements in sorted order, $Q_2'' \circ Q_4''$ contains the middle $\frac{n}{4}$ elements in sorted order, and $Q_4'$ contains the largest $\frac{n}{4}$ elements in sorted order.
Therefore $Q_1' \circ Q_2'' \circ Q_3'' \circ Q_4$ is sorted.
Thus we have a sorted list at the end of $\textsc{Gather}$.

\end{itemize}

\end{proof}

We can now use this claim to prove (by induction) the correctness of $\textsc{SlowSort}$ as follows:

\begin{claim}
If $n$ is a power of $2$, then $\textsc{SlowSort}(A[1 \ldots n])$ returns $A$ in sorted order.
\end{claim}

\begin{proof} We proceed by induction of $k= \log_2 n$.
\begin{itemize}
\item Base Case: If $n=1$, then $\textsc{SlowSort}$ just returns $A$, which is by definition in sorted order. 

\item Inductive Hypothesis: Given an list $A$ of length $n = 2^{k-1}$, $\textsc{SlowSort}(A)$ outputs $A$ in sorted order.

\item Inductive Step: Consider the execution of $\textsc{SlowSort}$ on some list $A$ of length $n$. By the inductive hypothesis, the recursive calls in lines 2 and 3 return $A[1 \ldots \frac{n}{2}]$ and $A[\frac{n}{2}+1 \ldots n]$ in sorted order. This means that the input to $\textsc{Gather}$ is a list of length $n$ with both the first and second halves sorted internally. By Claim \ref{gatherGood}, we know that in this case $\textsc{Gather}(A)$ will return $A$ in sorted order. But $\textsc{SlowSort}(A)$ just outputs the result of $\textsc{Gather}(A)$, and thus also outputs $A$ in sorted order for all lists of length $n$. Thus by induction, $\textsc{SlowSort}(A)$ sorts $A$ for lists of any length.

\end{itemize}
\end{proof}

$\mathbf{2d)}$

Let $T(n)$ denote the number of comparisons made by $\textsc{Gather}$ on an list of size $n$. In the base case of $n=2$, we do one comparison. Therefore $T(2) = 1$. If $n>2$, then we recursively call $\textsc{Gather}$ three times on inputs of size $n/2$. Therefore $T(n) = 3 \cdot T(n/2)$. Summarizing, we have:

\begin{align*}
T(n) &= 3 \cdot T(n/2)  \quad \text{if $n > 2$} \\
T(2) &= 1
\end{align*}

Let $S(n)$ denote the number of comparisons made by $\textsc{SlowSort}$ on list of size $n$. In the base case of $n=1$, $\textsc{SlowSort}$ does no comparisons. Therefore $S(1) = 0$. If $n>1$, then $\textsc{SlowSort}$ recursively calls itself twice on inputs of size $n/2$ (this is $2 \cdot S(n/2))$ comparisons), and then calls $\textsc{Gather}$ on an list of size $n$ (this is $T(n)$ comparisons). Therefore $S(n) = 2 \cdot S(n/2) + T(n)$. Summarizing this, we have:
\begin{align*}
S(n) &= 2 \cdot S(n/2) + T(n) \\
S(1) &= 0
\end{align*}
We now solve both recurrences, starting with $T(n)$. By unrolling the recurrence, we get:
\begin{align*}
T(n) &= 3 \cdot T(n/2) \\
T(n) &= 3 \cdot (3 \cdot T(n/4)) &= 3^2 \cdot T(n/4) \\
T(n) &= 3^2 \cdot (3 \cdot T(n/8)) &= 3^3 \cdot T(n/8) \\
& \ldots  \\
T(n) &= 3^i \cdot T(n/2^i)
\end{align*}
where the last line comes from generalizing the pattern (each time we unroll, we multiply the right-hand-side by 3 and divide $n$ by 2). We stop when we reach the base case of $2$, meaning we need to find a $k$ such that $n/2^k = 2$. This occurs when $n = 2^k \cdot 2 = 2^{k+1}$, so $k = \log n - 1$. Then 

\begin{align*}
T(n) &= 3^{\log n - 1} \cdot T(2) \\
&= \frac{1}{3} \cdot 3^{\log n} \\
&= \frac{1}{3} \cdot n^{\log 3} 
\end{align*}

Thus $T(n)  \in O(n^{\log 3})$. Note that $\log 3 \approx 1.58$.

We can now solve the recurrence for $S(n)$. Recall that\begin{align*}
S(n) &= 2 \cdot S(n/2) + T(n) \\
S(1) &= 0
\end{align*}
By unrolling the recursion we get, for every $i \in \{0,1,2,\dots, \log n\}$
\begin{align*}
S(n) &= 2^i \cdot S(n/2^i) + \left(\; T(n) + 2T(n/2) + \dots + 2^{i-1} T(n/2^{i-1}) \;\right) 
\end{align*}
in particular, setting $i = \log n$ gives
\begin{align*}
S(n) &= 2^{\log n } S(n/2^{\log n}) + \left(\; T(n) + 2T(n/2) + \dots + 2^{(\log n)-1} T(n/2^{(\log n)-1}) \;\right) \\
&= n S(1) + \sum_{j=0}^{(\log n) -1}\frac{1}{3} \cdot \bigg({\frac n {2^j}}\bigg)^{\log 3} \cdot 2^j\\ 
&= 0 + \frac 1 3  \cdot 
n^{\log 3} 
\cdot
\sum_{j=0}^{(\log n) -1}
{\bigg(\frac 2 {2^{\log 3}}\bigg)^j} 
\end{align*}
where the last sum is just a geometric series with ratio $r =\frac{2}{2^{\log 3}}$. This means the sum is bounded above by some constant (in particular, $\frac{1}{1-(2/2^{\log 3})} = 3$). Therefore, the total number of comparisons made by $\textsc{SlowSort}$ is in $O(n^{\log 3})$, just like what we found for $\textsc{Gather}$.
\end{document}
