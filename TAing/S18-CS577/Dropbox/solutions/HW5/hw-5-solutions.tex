\documentclass{article}
\usepackage{fullpage}
\usepackage{subfiles}

\usepackage{epsfig}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amstext}
\usepackage{amscd}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{times}
\usepackage{graphicx}
\usepackage{alltt}

\usepackage{tikz}
\usetikzlibrary{matrix}
\usetikzlibrary{calc}
\usetikzlibrary{positioning}


\newtheorem{claim}{Claim}

\usepackage[linesnumbered,lined,boxed,commentsnumbered,ruled,vlined]{algorithm2e}

\begin{document}

\noindent
\fbox{
\parbox{\textwidth}{
\begin{Large}
{\bf CS 577: Introduction to Algorithms\hfill Homework 5 Solutions}
\end{Large}
}}

\section*{Problem 1}
We design an algorithm that minimizes the number of employees required
to attend the ticketing desk using the following greedy strategy.
Starting with an empty schedule,
we consider the earliest flight whose boarding period is not yet
completely covered.
If there is no such flight, then we are done; otherwise, we schedule a
new shift as late as possible subject to covering the flight's boarding
period.
The new schedule covers the flight, after which we repeat until all
flights are covered.

To implement this efficiently, we sort the flights in order of
increasing time, and keep track of the latest end-time of a shift in the
schedule.
In more detail:
\begin{enumerate}
\item Sort the boarding times in increasing order.
\item Set $last\_shift\_end = -\infty$
\item For each boarding time $t$ (in order),
	\begin{enumerate}
    \item If $t + 15\text{ mins} > last\_shift\_end$
    	\begin{enumerate}
        	\item Let $s \gets max(t - 15\text{ mins},\ last\_shift\_end)$
            \item Staff a new employee from time $s$ until $s + 2\text{ hrs}$.
            \item Update $last\_shift\_end \gets s + 2\text{ hrs}$
        \end{enumerate}
	\end{enumerate}
\end{enumerate}

\subsection*{Proof of Correctness} 
Let $S$ be the schedule returned by our algorithm. It is clear that $S$
is a schedule covering all the required boarding periods. It remains to
argue that $S$ is as small as possible.

To this end, let $O$ be an arbitrary scheduling covering all flights.
We order the schedules in $S$ and $O$ by their ending time.
Let $s_j$ be the the end-time of the $j^{th}$ shift in $S$, and $o_j$ be
the end-time of the $j^{th}$ shift in $O$.
We also define $s_0 = o_0 = -\infty$.
Correctness of the algorithm follows from the following
greedy-stays-ahead argument.
\begin{claim}
For every $j = 0, 1, 2, \ldots, |S|$, it is the case that $|O| \ge j$,
and $o_j \le s_j$.
\end{claim}

\begin{proof} By induction on $j$.
\begin{description}
\item[Base case:]
	When $j = 0$, it trivially holds that $|O| \ge j$,
	and we have also defined $o_0 \le s_0$.
\item[Inductive Step:]
	We assume that the claim holds for a particular value of $j < |S|$,
	and show that $|O| \ge j+1$ and $o_{j+1} \le s_{j+1}$.

	Consider the first boarding time $t$ which is not covered by the first
	$j$ shifts in $S$; \textit{i.e.}, for which
	$t + 15\text{ mins} > s_j$.
	(By how $S$ is constructed and since $j < |S|$, such a shift exists.)
	By the inductive hypothesis, we know $o_j \le s_j$, so this flight is
	also not covered by the first $j$ flights in $O$.
	Thus there must be another scheduled shift in $O$; this shows
	$|O| \ge j+1$.

	Moreover, there must be a shift in $O$ that covers this flight, and
	thus must start either at $o_j$ or $t-15\text{ mins}$, whichever is
	later, in order to ensure the entire boarding period is covered.
	On the other hand, the $j+1^{th}$ schedule in $S$ starts either at
	$s_j$ or $t-15\text{ mins}$, whichever is later; in any case, this is
	no earlier than the $j+1^{th}$ shift in $O$.
	Moreover, the length of the $j+1^{th}$ shift in $O$ is at most the
	length of the $j+1^{th}$ shift in $S$, as the latter is the
	maximum-possible two hours.
	It follows that $o_{j+1} \le s_{j+1}$, completing the proof.\qedhere
\end{description}
\end{proof}

\subsection*{Running time analysis:}
Initial sorting of the boarding times takes $O(n\log n)$ time and the
remainder of the algorithm runs in time $O(n)$. Hence, the overall
runtime complexity of the algorithm is $O(n\log n)$.

\pagebreak[4]
\section*{Problem 2} 
\newcommand{\OPT}{\mathrm{OPT}}

We first explain how to compute the maximum possible score for the first
player, and then we describe the procedure to recover the first move of
the first player which gives this maximum possible score.
The trick is to use two simultaneous recurrences, one for computing the
maximum attainable value when it is the first player's turn, and one for
computing the maximum attainable value when it is the other player's
turn.

For $i,j$ with $1 \le i \le j \le n$,
define $\OPT_1(i,j)$ to be the largest value $v$
so that the person playing when the cards $i$ through $j$ are on the table
can always attain value at least $v$,
no matter how the other player plays.
Similarly, $\OPT_2(i,j)$ is the largest value $v$
so that the person \emph{not} playing when the cards $i$ through $j$ are on the
table is nevertheless guaranteed to get value at least $v$.
The value we are interested in is $\OPT_1(1,n)$.

We can find $\OPT_1(i,j)$ and $\OPT_2(i,j)$ co-recursively as follows:
\begin{align*}
	\OPT_1(i,j) &= \left\{\begin{array}{lcl}
			\textrm{Card}[i] &:& i = j \\
			\max \left\{
				\begin{array}{ccc}
					\textrm{Card}[i] &+& \OPT_2(i+1,j) \\
					\OPT_2(i,j-1) &+& \textrm{Card}[j]
				\end{array}
			\right\}  &:& i < j
		\end{array}
	\right.
	\\
	\OPT_2(i,j) &= \left\{\begin{array}{lcl}
			0 &:& i = j \\
			\min \left\{
				\begin{array}{cc}
					\OPT_1(i+1,j) \\
					\OPT_1(i,j-1)
				\end{array}
			\right\}  &:& i < j
		\end{array}
	\right.
\end{align*}

\subsection*{Proof of Correctness} 
The cases when $i=j$ follow straightforwardly from how $\OPT_1$ and $\OPT_2$
are defined.
(The players have no choices to make.)

When $i < j$, $\OPT_1(i,j)$ is attained when the first player either takes from
the left, or takes from the right.
If they were to take from the left, their value would be $\textrm{Card}[i]$ plus
whatever value they get from the leftover game on the cards $i+1$ through $j$.
Since it is the other player's turn next, this is exactly $\OPT_2(i+1,j)$.

Similarly,
if the first player were to take from the right,
their value would be $\OPT_2(i,j-1) + \textrm{Card}[j]$.
Since the player gets to choose which of these two
strategies to undertake,
she can guarantee she gets the larger value,
hence the $\max$ appearing in the definition of $\OPT_1$.

As for $\OPT_2$,
the player's value has similar formulas
(depending on whether the other player picks the leftmost or rightmost card),
but since the other player gets to pick the next move,
we need to use a $\min$ in order to satisfy the definition of $\OPT_2$,
and there is no contribution of $\textrm{Card}[i]$ and $\textrm{Card}[j]$.

\subsection*{Iterative solution} 
We can translate the above recurrence into an iterative approach as follows.
We make two 2-D arrays $A_1[i][j]$ and $A_2[i][j]$ where $1 \le i,j \le n$.
$A_1[i][j]$ will store $\OPT_1(i,j)$ for $i \le j$,
and will be undefined otherwise.
$A_2$ is defined similarly in terms of $\OPT_2$.
The dependency between the entries in $A_1$ and $A_2$ are shown in the Figure~\ref{fig:figure}. 

\begin{figure}[ht]
  \centering
  \includegraphics[width=70mm]{HW5-Problem2}
  \caption{Dependency graph of entries in $A_1$ and $A_2$}
  \label{fig:figure}
\end{figure}

Thus the entries in the 2-D arrays can be filled from bottom-to-top and left-to-right fashion. This can be accomplished with the following loops:

\begin{enumerate}
\item For $i$ = $n$ down to $1$, For $j$ = $i$ to $n$
	\begin{enumerate}
      \item If $i$ == $j$
      	\begin{enumerate}
      	\item Set $A_1[i][j]$ = $\textrm{Card}[i]$
		\item Set $A_2[i][j]$ = 0
      	\end{enumerate}
      \item Else
		\begin{enumerate}
		\item Set $A_1[i][j] = \max\{ A_2[i,j-1] + \textrm{Card}[j], A_2[i+1,j] + \textrm{Card}[i] \}$
		\item Set $A_2[i][j] = \min\{ A_1[i,j-1], A_1[i+1,j] \}$
		\end{enumerate}
    \end{enumerate}
\item The optimal score for the first player is stored in $A_1[1][n]$.
\end{enumerate}

\subsection*{Running Time} 
Using memoization, we can compute $\OPT_1$ and $\OPT_2$ efficiently.
The total number of states is $2\cdot {n+1 \choose 2} = O(n^2)$,
and each state requires only $O(1)$ operations to compute.
Hence the total time spent to compute the maximum value guaranteeable by the
first player is $O(n^2)$.

\subsection*{Recovering the winning first move} 
We can recover the optimal first move of the first player by examining
the two quantities contributing to the $\max$ defining $\OPT_1(1,n)$.
If the first quantity is larger, then the first player should take the left
card (card 1); otherwise, the first player should take the right card
(card $n$).

\end{document}