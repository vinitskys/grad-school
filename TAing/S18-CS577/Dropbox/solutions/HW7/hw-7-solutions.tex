%
\documentclass{article}
\usepackage{fullpage}

\usepackage{epsfig}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amstext}
\usepackage{amscd}
\usepackage{amsmath}
\usepackage{theorem}
\usepackage{times}
\usepackage{graphicx}
\usepackage{alltt}
\usepackage{algorithmic}
\usepackage[colorlinks]{hyperref}
\def\SPAN{\textup{span}}
\def\tu{\textup{u}}
\def\R{\mathbb{R}}
\def\bbP{\mathbb{P}}
\def\bbS{\mathbb{S}}
\def\bbQ{\mathbb{Q}}
\def\P{\mathbb{P}}
\def\S{\mathbb{S}}
\def\Q{\mathbb{Q}}
\def\N{\mathbb{N}}
\def\Z{\mathbb{Z}}
\def\E{\mathbb{E}}


\def\cQ{\mathcal{Q}}
\def\cN{\mathcal{N}}
\def\cF{\mathcal{F}}
\def\cNP{\mathcal{NP}}

\def\kl{D_{\text{KL}}}
\def\tv{D_{\text{TV}}}

\def\be{\mathbf{e}}
\def\nf{\nabla f}
\def\veps{\varepsilon}
\newtheorem{theorem}{Theorem}[section]
\newtheorem{corollary}{Corollary}[theorem]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{remark}{Remark}

\begin{document}

%\thispagestyle{empty}

\noindent
\fbox{
\parbox{\textwidth}{
\begin{Large}
{\bf CS 577: Introduction to Algorithms\hfill Homework 7 Solution}
\end{Large}\\

}}

\subsection*{Problems} 

\begin{enumerate}

%\bigskip\bigskip \hfill (\emph{See next page for problem 2}) \newpage

\item {\bf (Worth: 2 points. Page limit: 1 sheet; 2 sides)}

\begin{enumerate}
\item 
We have $n$ users on a P2P network who want to pick \emph{distinct} IDs in a distributed fashion. Each user independently picks one uniformly random $b$ bit number. Show that when $b\geq 6+2\log n$, the probability of the users picking distinct IDs  is at least $0.99$. \\

\textbf{SOLUTION:} 
%Let $A$ be the event that all users pick distinct IDs, let $\bar A$ be the complement of $A$, i.e., some users pick same IDs. Let $\bar A_i$ be the event that the $i^{th}$ user picks same ID as some other user other than $i$. Then we can upper bound $P(\bar A)$ by:
Let $A$ be the event that some users pick duplicated number. Let $A_{ij}$ be the event that user $i$ and user $j$ pick the same number. Then we can upper bound $P(A)$ by:
\begin{align*}
\begin{split}
    P( A)&=P(\cup_{i\neq j}A_{ij})\\
    &\leq \sum_{i\neq j}P(A_{ij})\\
    &= \binom{n}{2}\frac{1}{2^b}\\
    &=\frac{n(n-1)}{2^{b}\cdot 2}
\end{split}
\end{align*}
where the second line is the union bound, the third line holds since for all $i\neq j$, $P(A_{ij})$ should be the same: user $i$ can pick anything, but $j$ has to choose whatever $i$ has chosen, which gives $\frac{1}{2^b}$. The problem boils down to counting how many $A_{ij}$ exist: exactly $\binom{n}{2}$. 

Since $b\geq 6+2\log n$, we have:
\[
P( A)\leq \frac{n(n-1)}{2^{b}\cdot 2}\leq \frac{n(n-1)}{2^6\cdot2^{2\log n}\cdot 2}\leq \frac{n^2}{2^6\cdot n^2\cdot 2}\leq 0.008
\]
Finally this gives:
\[
P(\text{No collision})=1-P( A)\geq 1-0.008\geq 0.992
\]
\item
We have $n$ users with distinct IDs on a P2P network, who want to elect a leader in a distributed fashion. Each user independently picks a uniformly random $b$-bit number, %\footnote{e.g., they could do this by hashing their ID into a hash table of size $2^b$, although here we require a stronger property than 2-universality or pairwise independence.} 
and the leader is determined to be the user with the smallest number. Show that when $b\geq 8 + \log n$, the probability that a unique leader is chosen is at least $0.99$.\\

\textbf{SOLUTION:} Let $A$ be the event in which a unique leader is elected. Let $A_i$ denote the event that the unique leader picks the number $i$, where $i\in\{1,2,\ldots, 2^b-1\}$. Then we get the following equations, these are explained below:
\begin{align}\label{eq:q12}
\begin{split}
    P(A)&=P(\cup_{i=1}^{2^b-1}A_i)\\
    &=\sum_{i=1}^{2^b-1}P(A_i)\\
    &=\sum_{i=1}^{2^b-1} n\cdot\frac{1}{2^b}\cdot\left(1-\frac{i}{2^b}\right)^{n-1}\\
    &=\frac{n}{2^b}\sum_{i=1}^{2^b-1}\left(\frac{i}{2^b}\right)^{n-1}\\
    &=\frac{n}{2^{bn}}\sum_{i=1}^{2^b-1}i^{n-1}
\end{split}
\end{align}
The first equality is just by the definition of $A$ and the $A_i$'s. The second equality follows by noting that the $A_i$'s are disjoint events. Let us look at the third equality: since the leader uniquely picks number $i$, he or she only has one choice out of the $2^b$ candidates, that is how we get the term $\frac{1}{2^b}$. The rest of the users have to pick a number bigger than the one that the leader selected. Since the leader selected number $i$, the others have pick one of $(2^b-i)$ numbers. So the probability that the rest of the users (there are $n-1$ of them) do not pick the same number as the leader does is $\left(1-\frac{i}{2^b}\right)^{n-1}$. Since each of the $n$ users could be the leader, that is why we multiply $n$ in the beginning. 

Recall that $\sum_{x=m}^np(x)\geq\int_{m-1}^np(x)dx$. In particular, let $m=1$, $n=2^b-1$, and $p(x)=x^{n-1}$, we have:
\[
\sum_{i=1}^{2^b-1}i^{n-1}\geq \int_{0}^{2^b-1}i^{n-1}di = \frac{i^n}{n}\bigg\rvert_{i=0}^{2^b-1}=\frac{(2^b-1)^n}{n}
\]
substitute this result back to Eq.~\ref{eq:q12}, we get:
\[
P(A)\geq \frac{n}{2^{bn}}\frac{(2^b-1)^n}{n}=\left(  1-\frac{1}{2^b}\right)^n
\]
%Next, use the fact that $1+x\geq e^{x/(1+x)}$, when $x>-1$. In particular, let $x=-\frac{1}{2^b}>-1$, we have:
%\[
%\left(  1-\frac{1}{2^b}\right)^n \geq \exp\left( \frac{-\frac{1}{2^b}}{1-\frac{1}{2^b}}n \right)=\exp\left( -\frac{n}{2^b-1} \right)
%\]
%Substitute $b\geq 8+\log n$, we get:
%\[
%\exp\left( -\frac{n}{2^b-1} \right)\geq \exp\left( -\frac{n}{2^8n-1} \right)\geq \exp\left( -\frac{n}{2^8n-n} \right)= \exp\left( -\frac{1}{2^8-1} \right)\geq 0.99
%\]
Substitute $b\geq 8 + \log n$, we have:
\[
\left(  1-\frac{1}{2^b}\right)^n\geq \left(  1-\frac{1}{2^8n}\right)^n
\]
From here on we can proceed in two ways:

(i) Use $(1-\frac{1}{x})^x\geq 0.25$ for $x\geq 2$ (as seen in Section 1), in particular, set $x=2^8n=256n\geq 2$, we get:
\[
\left(  1-\frac{1}{2^8n}\right)^n = \left(\left(  1-\frac{1}{2^8n}\right)^{256n}\right)^{\frac{1}{256}}\geq 0.25^{1/256}>0.994
\]

(ii) Use $(1+x) \geq e^{\frac {x}{1+x}}$ (as seen in Section 2), to get:  
\[
\left(  1-\frac{1}{256n}\right)^n \geq e^{(\frac{-\frac{1}{256n}}{1 - \frac{1}{256n}})\cdot n} = ({\frac 1 e})^{\frac {n}{256n-1}} = ({\frac 1 e})^{\frac {1}{256-\frac 1 n}} \geq ({\frac 1 e})^{\frac {1}{255}} > 0.996
\]
\end{enumerate}

\newpage

\item {\bf (Worth: 3 points. Page limit: 1 sheet; 2 sides)}
\begin{enumerate}
%\item (a) Suppose you pick $k$ numbers independently and uniformly at random from the set $\{1,...,n\}$. Let $f$ be the expectation of the minimum number picked. Obtain an exact expression for $f$ in terms of $n$ and $k$. Show that $f \in \Theta(n/k)$, i.e., that  $ \alpha \cdot \frac {n} {k} \leq f \leq \beta\cdot\frac n k$ for some constants $\alpha,\beta > 0$  whenever $n,k$ are arbitrarily large.

%(Hint: You may want to use the fact that the sum  $\sum_{i=a}^{b}{p(i)}$ is bounded above by $\int_a^{b+1} p(x)dx$, and below by  $\int_{a-1}^{b} p(x)dx$, assuming $p(x)>0$ in the interval $[a,b]$.)

\item
You are given a circle of unit circumference. You pick $k$ points on
the circle independently and uniformly at random and snip the circle
at those points, obtaining $k$ different arcs. Determine the expected
length of any single arc. 

{\em (Hint: Note that the length of each arc is identically distributed, so each has the same expectation. What is the sum of their expectations?)}\\
%\item You are given $n$ points that are evenly spaced along a circle (as in a clock). You pick  $k$ points independently and uniformly at random and snip the circle at the those points, obtaining $k$ different arcs. Let $f$ be the expectation of the number of points in any arc. Find a function $g(n,k)$ such that  $ \alpha \cdot g \leq f \leq \beta\cdot g$ for some constants $\alpha,\beta > 0$  whenever $n,k$ are arbitrarily large.

\textbf{SOLUTION:} Let $X_i$ be the random variable that represents the length of the $i^{th}$ arc. Use the hint, we know $X_1,\ldots, X_k$ are identically distributed, and they have the same expectation: 
\[
\E[X_1]=\E[X_2]=\ldots=\E[X_k]
\]
Also by the linearity of expectation, we have:
\[
\sum_{i=1}^k\E[X_i]=\E[\sum_{i=1}^k X_i]=1
\]
where the second equality is due to the fact that the lengths of $k$ arcs would sum up to the circumference of the circle. This tells that $\E[X_i]=\frac{1}{k}$, for all $i\in\{1,\ldots, k\}$ .

We can also calculate this value explicitly. This approach is attached in the appendix, you're not required to understand this.
\smallskip

\item

  You are given a sorted circular linked list containing $n$ integers,
  where every element has a ``next'' pointer to the next larger
  element. (The largest element's ``next'' pointer points to the
  smallest element.) You are asked to determine whether a given target
  element belongs to the list. There are only two ways you can access
  an element of the list: (1) to follow the next pointer from a
  previously accessed element, or (2) via a given function RAND that
  returns a pointer to a uniformly random element of the list.

Develop a randomized algorithm for finding the target that makes at most $O(\sqrt n)$ comparisons in expectation and always returns the correct answer.

{\em (Hint: Your algorithm will perform some random accesses and some amount of linear search. Use part (a) to analyze the number of steps in the linear search.)}\\

\textbf{SOLUTION:} There are two ways to access an element: with RAND, and by linear search from some element that has already been accessed. Suppose we access $k$ elements using RAND. Once this is done the only option left is to choose one of these elements and begin a linear search until we reach the next element returned by RAND. Intuitively, there are $n/k$ elements on average between two elements accessed by RAND. So we get that the total number of accesses is $k+n/k$ in expectation. We will show this formally later. We can balance this out by having $k = \sqrt{n}$. \\

\noindent \textbf{Algorithm: }

\begin{algorithmic}
\STATE Let $t$ be the element we look for. Choose $\sqrt{n}$ elements of the circularly linked list by making calls to RAND. 
\STATE Order them in increasing order to form a list of elements $e_1, e_2, \ldots e_k$. 
\STATE Find $e_i$ such that $e_i \leq t < e_{i+1}$. 
\STATE If this is not possible, $t < e_1$ or $t \geq e_n$, so $t$ lies between $e_n$ and $e_1$ if it exists. Thus use $e_i = e_n$. 
\STATE Starting at $e_i$ follow the circularly linked list until $e_{i+1}$, returning true if $t$ is found. 
\end{algorithmic}

\noindent \textbf{Analysis: }

We access $\sqrt{n}$ elements in the first step by making calls to RAND. Then we check potentially all elements from $e_i$ to $e_{i+1}$. So we would like to determine the expected number of elements between $e_i$ and $e_{i+1}$. 

From (a), we know that if we choose $k$ point on a circle with unit circumference uniformly at random, then the expected length of any arc is $\frac{1}{k}$. In this problem, we have a circle with circumference of $n$, by choosing $k$ points, we would have expected length of any single arc $\frac{n}{k}$. Setting $k=\sqrt{n}$, we have the expected length of any arc is $\frac{n}{k}=\frac{n}{\sqrt n}=\sqrt n$. The expected length of any arc represents the expected number of elements between $e_i$ and $e_{i+1}$. Hence, in the worst case, we compare $t$ to every element between $e_i$ and $e_{i+1}$, which is $\sqrt n$ comparison in expectation.
% Let $X_j$ be the number of elements between $e_j$ and $e_{j+1}$. Since RAND is uniform, the $X_j's$ are identically distributed, and $E[X_j] = E[X_j']$ for all $j, j'$. Note that $n = \sum\limits_{i = 1}^k X_i$. Taking the expectation of both sides and applying linearity of expectation gives $E[n] = n = \sum\limits_{i = 1}^k E[X_i]$. Since RAND is uniform, the $X_j's$ are identically distributed, and $E[X_j] = E[X_j']$ for all $j, j'$. Furthermore $k = \sqrt{n}$. So $n = \sqrt{n} E[X_i]$ and thus $E[X_i] = \sqrt{n}$. 


\end{enumerate}

\section{Appendix}
Here we give an explicit calculation for problem 2a.
\begin{remark}
You are not required to understand this approach.
\end{remark}



First notice that if we snip the circle in the first picked point, we can ``unroll'' the circle into an unit length interval, and the problem becomes ``calculate the expected length of intervals created by $k-1$ points on $[0,1]$''.

Since each $X_i$'s are identical, we just use $X$ to represent the random variable. Recall $\E[X]=\int xp(x)dx$, where $p(x)=P(X=x)$ is the PDF (probability density function). To get PDF, we need CDF(cumulative distribution function). Let $F(x)=P(X\leq x)$ be the CDF. This is a rather hard term to calculate, as there are many possibilities that the length of certain interval is less than $x$: if any one of the $k-1$ points fall within a $x-$neighborhood, this event becomes true. So instead we calculate:
\[
P(X\geq x)=(1-x)^{k-1}\Longrightarrow P(X\leq x)=1-(1-x)^{k-1}
\]
Then we have:
\begin{align}\label{eq:pdf}
p(x)=P(X=x)=\frac{dF(x)}{dx}=(k-1)(1-x)^{k-2}
\end{align}
Then we finally have:
\[
\E[X]=\int_{-\infty}^{\infty}xp(x)dx=\int_{0}^{1}xp(x)dx=\int_{0}^{1}x(k-1)(1-x)^{k-2}dx=\frac{1}{k}
\]
The last equality can be calculated using integration by parts.

\begin{remark}
Notice Eq~\ref{eq:pdf} isn't always true. This derivation only works if CDF $F(x)$ is absolutely continuous (which I guess is true in most of the cases we would see in this class). The more general PDF is defined via Radon-Nikodym derivative, which you don't need to worry about for this class. 
\end{remark}

 



\def\excise{\item
Consider the following game at a carnival. There are 5 stationary toy turtles that look identical. You are given a bucket of toy turtles that look identical, and your goal is to stack, on top of the stationary turtles, as many turtles from the bucket as possible. 

Each of the 5 stationary turtles have a capacity for how many turtles it can carry on top of it --- 12, 15, 20, 30, 60 respectively. If you exceed the capacity of a turtle, it collapses and becomes unusable, and  you lose everything you stacked on it.

You pay \$7 to enter the game, and can leave at any point during the game with a payback of 10 cents per turtle you have stacked on the stationary ones as of the time you quit. 

Come up with a randomized algorithm that in expectation yields a profit for you.

\textbf{Extra credit (1 pt)} if your algorithm yields more than \$1 profit in expectation.

(Hint: Suppose there were only two stations, with capacity $30$ and $60$. Design an algorithm for this case. Generalize your algorithm to handle $5$ stations.)
}


\end{enumerate}
\end{document}
