\documentclass{article}
\usepackage{fullpage}
\usepackage{subfiles}

\usepackage{epsfig}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amstext}
\usepackage{amscd}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{times}
\usepackage{graphicx}
\usepackage{alltt}


\newtheorem{claim}{Claim}

\usepackage[linesnumbered,lined,boxed,commentsnumbered,ruled,vlined]{algorithm2e}

\begin{document}

\noindent
\fbox{
\parbox{\textwidth}{
\begin{Large}
{\bf CS 577: Introduction to Algorithms\hfill Homework 4 Solutions}
\end{Large}
}}

\section*{Problem 1}
(a). Let $G'=(V,E')$ be a subgraph of $G$. Suppose $G'$ is obtained by taking a spanning tree $T$ and adding one edge $e$. Since $T$ is a spanning tree, it is connected and contains no cycle. Then adding the edge $e$ into $T$ will form exactly one cycle and therefore $G'$ is \emph{special}. On the other hand, suppose $G'$ is \emph{special}, then it is connected and contains exactly one cycle. Pick an arbitrary edge $e$ in this cycle and delete it from $G$, we obtain a new graph $G_1$. Since $G_1$ is still connected and contains no cycle, $G_1$ is a spanning tree of $G$.

\par~\\
(b). We first find a minimum spanning tree $T$ of $G$. Let $e$ denote the edge with minimum weight in $E\setminus T$. We conclude that $T\cup\{e\}$ is a minimum-weight special subgraph of $G$.

\begin{algorithm}
\SetKwInOut{Input}{Input}
\Input{Graph $G=(V,E)$.}
Find a minimum spanning tree $T$ of $G$\;
Find the edge $e\in E\setminus T$ with minimum weight\;
\Return{$G':=T\cup\{e\}$.}
\caption{Pseudocode of MSS($G$)}
\end{algorithm}

\subsection*{Proof of Correctness:}
Observe that by using the characterization in part (a), our algorithm returns a special subgraph $G'$ of $G$. Now we prove that this special subgraph is optimal. 

Let $F$ be an arbitrary special subgraph of $G$. We will show that the weight of our solution $G'$ is no more than the weight of $F$. By the definition of special subgraphs, we know that $F$ is connected and contains exactly one cycle $C$. Let $f$ be the edge with maximum weight in cycle $C$. From the proof of part (a), we have $F=T'\cup\{f\}$, where $T'$ is a spanning tree of $G$. 

Since $T$ is an MST, the weight of $T$ is no larger than the weight of $T'$. Meanwhile, since $f$ has the maximum weight in the cycle $C$, we know that $f\notin T$ by the ``cycle property''. Therefore by our choice of edge $e$ in the algorithm, we have $w_e\le w_f$. Finally, we get $w_{G'}=w_T+w_{e}\le w_{T'}+w_{f}=w_{F}$, as claimed.

This implies that our algorithm successfully returns a minimum-weight special subgraph of $G$.
%We first show by induction that $T$ is the unique MST of $G$. For the basis, if $G$ contains only one vertex, the result is trivial.

%For the general graph $G$, let $v$ denote one leaf of the MST $T$. Then $T':=T\setminus\{v\}$ is an MST of the graph $G_1:=(V\setminus\{v\},E_1)$, in which vertex $v$ and all edges incident to $v$ are deleted. By induction hypothesis, we know that $T'$ is the unique MST of $G_1$. Since $T$ is an MST of $G$, the weight of the edge $e$ connected to vertex $v$ in $T$ must be $w_e=\min\{w'_e\mid e'=(u,v)\in E,u\in V\}$. Since the weights of all edges in $G$ are distinct, $T=T'\cup\{e\}$ is the unique MST of $G$.

%Then we prove that our algorithm returns a minimum-weight special subgraph of $G$. Let $G''=(V,E'')$ be a minimum-weight special subgraph of $G$. From part (a), we know that $G''$ is obtained by taking a spanning tree and adding one edge. Let $C$ denote the cycle in $G''$ and consider the edge $e_0\in C$ which has the maximum weight in cycle $C$. We show that $T_0:=(V,E''\setminus\{e_0\})$ is the MST $T$ of $G$, which will imply the correctness of our algorithm.

%Obviously, $T_0$ is a spanning tree of $G$. Suppose that $T_0$ is not the MST of $G$, there are two cases.
%\begin{itemize}
%\item the MST $T$ contains edge $e_0$: In this case, there must exist an edge $e_1:=(x,y)\in C,e_1\ne e_0$ such that $e_0$ lies on the path between $x$ and $y$ in the MST $T$, otherwise deleting the edge $e_0$ will disconnect $x$ and $y$ in the cycle $C$, a contradiction. In this way, we can get a new spanning tree of $G$ by deleting edge $e_0$ from $T$ and then adding the edge $e_1$. Since $e_0$ has maximum weight in the cycle $C$, we get a spanning tree with smaller weight than the MST $T$, a contradiction.
%\item the MST $T$ does not contain edge $e_0$: In this case, from part (a), we know that $T\cup\{e_0\}$ is a special subgraph of $G$. However, its weight is smaller than $G''=T_0\cup\{e_0\}$ since $T_0$ is not the MST $T$, which contradicts the fact that $G''$ is a minimum-weight special subgraph.
%\end{itemize}





\subsection*{Running time analysis:}
To get a minimum spanning tree $T$, we can use Kruskal algorithm with running time $O(m\log n)$, and then we can find the edge $e$ in time $O(m)$. Therefore the total running time of the algorithm is $O(m\log n)$.

\subsection*{An alternate proof of correctness:} It is possible to prove correctness using a more direct exchange argument. Consider, in particular, a supposedly optimal special subgraph $F$, and any edge $e'=(u,v)$ in our solution $G'$ that does not belong to $F$. Suppose that $e'$ is in the MST $T$. Then, there is a cut separating $u$ and $v$ (namely, the partition created by removing $e'$ from $T$) such that $e'$ is the cheapest edge crossing this cut in $G$. Then, $F$ contains an edge $f$ crossing this cut on the path in $F$ from $u$ to $v$. Then $F\setminus\{f\}\cup\{e\}$ is a special subgraph cheaper than $F$, contradicting the optimality of $F$. On the other hand, if $F=T\cup\{f\}$ for some edge $f\ne e$, then we can argue as above that $F\setminus\{f\}\cup\{e\}$ is a special subgraph cheaper than $F$.


\section*{Problem 2}
(a). We need to show that the following statements are equivalent for any subset $S$ of homeworks: 
(i) $S$ is reasonable and (ii) for all integers $t \leq n$, the number of homeworks in $S$ due within $t$ days is at most $t$. 

We first show by contrapositive that (i) implies (ii). In other words, we will show that if for some $t \leq n$, there are more than $t$ homeworks in $S$ due on or before day $t$, then $S$ cannot be reasonable. Consider any such $t$. Given that we can complete at most one homework per day, it is impossible to complete more than $t$ homeworks in $t$ days. It means that there exists at least one homework in $S$ we cannot finish on time and thus $S$ is not reasonable.
 
 Now we show that (ii) implies (i) by induction on the number of homeworks in $S$, say $k$. Specifically, given (ii) we will give a schedule to complete all homeworks in $S$ before their deadline. For the base case $k=0$, the argument is obviously correct. Suppose now there are $k+1$ homeworks in $S$. Then there must exist a homework $h\in S$ with deadline $t\ge k+1$, otherwise the number of homeworks in $S$ due within $k$ days is $k+1$, which contradicts (ii). Now consider the set of homeworks $S\setminus\{h\}$. Note that this set satisfies (ii) for the same reason that $S$ satisfies (ii): for all integers $t\le n$, the number of homeworks in $S$ due within $t$ days is at most $t$. Therefore $S\setminus\{h\}$ is reasonable by the induction hypothesis. Thus we can first do all homeworks in $S$ except $h$ within $k$ days and then finish homework $h$ in the $(k+1)$-th day. This proves that $S$ is reasonable.

 
\par~\\
(b). We present a greedy algorithm that returns a reasonable set of homeworks $S$  
 that maximizes the number of points. 
By part (a), completing the homeworks in $S$ in order of their due date gives us a schedule 
  for homeworks in $S$ that meets all deadlines. The algorithm builds a reasonable set $S$ 
  by including homeworks in decreasing order of points
  while maintaining the invariant that the set $S$ is reasonable.

\begin{algorithm}
\SetKwInOut{Input}{Input}
\Input{number of homeworks $n$; each homework's deadline $t_i$ and points $p_i$.}
Sort the homeworks by decreasing order of points\;
Let $L$ represent this sorted list\;
Initialize set $S=\emptyset$\;
\ForEach{$h\in L$}{
\If{$S\cup\{h\}$ is reasonable}{add $h$ to $S$\;}
}
\Return{$S$.}
\caption{Pseudocode of greedy}
\end{algorithm}
To test the ``If'' condition, we maintain an array $A$ that keeps track of number of 
homeworks in $S$ due on or before day $t$ for all $t$. 
That is, $A[t]$ keeps track of number of homeworks in $S$ due on or before day $t$. When a homework 
with due date $d$ is added to $S$, add $1$ to $A[t]$ for all $t \geq d$.     

\subsection*{Proof of Correctness:} 
By construction the set  $S$ that the algorithm outputs is reasonable. 
We show that $S$ also attains maximum possible points. 

We first prove this for the case in which each homework is worth a distinct number of points. 
For this case, we show that $S$ is the unique optimal solution via an exchange argument. 
Let  the homeworks in $S$ listed in decreasing order of points be  $\{h_1,h_2 \ldots h_k\}$.
Suppose that there is an optimal solution $M$ that is different from $S$ and that 
$j$ is the smallest index such that $h_j$ is not in $M$.  That is, assume there is an 
optimal solution $M = \{h_1, h_2 ,\ldots h_{j-1}, g_j, \ldots g_{k^\prime}\}$, where $g_j,\ldots,g_{k'}$ are different from $h_j$.
Note that since $M$ is reasonable, so is its subset $\{h_1,\ldots,h_{j-1},g_j\}$. Now we observe that $g_j$ cannot have larger value than $h_j$, otherwise our algorithm would have considered it before $h_j$ and add it to our solutions. Thus $g_j$ has value less than $h_j$ and in fact all the other $g$'s have fewer points than $h_j$.

We will now carry out an exchange argument. Specifically, we will exchange $h_j$ with some other homework $g$ in $M$ such that the resulting set $M\cup\{h\}\setminus\{g\}$ is both reasonable and has more points than $M$.
We first consider the set $ M^\prime = M \cup \{h_j\}$. 
This set may not be reasonable, however since $M$ was reasonable, it must hold that for 
any day $t$, the number of homeworks in  $M^\prime$ due on or before day $t$  is
$\leq t+1$. Let $t_0$ be the smallest $t$ for which the number of homeworks due on or before
day $t$ is exactly $t+1$. Note that if we delete a homework from $M^\prime$ due on or before $t_0$, then
the resultant set is reasonable. 
The key observation here is that there must exist a homework $g$, of value smaller than $h_j$, that is due before $t_0$. 
This is because, if the only homeworks due on or before $t_0$ in $M'$ are the $h_i$'s, then 
the number of homeworks due on or before $t_0$ must be no more than $t_0$
(because $\{h_1, \ldots h_j\}$ form a reasonable set). 
Therefore deleting the homework $g$ from $M^\prime$ results in a reasonable set of total value greater than $M$. 
This is a contradiction, and thus $S$ is the unique optimal set. 


Now, consider the case in which the points are no longer distinct.
In this case, we  can use the argument above to prove correctness. 
If the points are not distinct $S$ may not be the unique optimal solution, but  $S$ is still an optimal solution. 

The argument in the prior case (points distinct case) shows that we can start with an 
optimal set $M$ that contains the $j-1$ homeworks $h_1, h_2, \ldots h_{j-1}$, but not $h_j$, 
and transform it into a reasonable set of  value equal to or greater than $M$. 
Repeating this gives us an optimal solution $T$ that contains $S$. Since $T$ must be reasonable, and adding
any homework to $S$ gives a set that is not reasonable, we have $T = S$. 
This completes the proof.   
  



\subsection*{Running time analysis:} Sorting the list with respect to points takes $O(n \log n)$ time. 
To check whether the set $S$ is reasonable at any given stage, we keep track of an array of length $n$ in which
the $i$-th entry stores the number of homeworks in $S$ due on or before day $i$. Updating this array every time 
a homework is added to $S$ and checking whether $S$ remains reasonable requires $O(n)$ time. Since we need to do
this $n$ times, the entire  algorithm requires $O(n^2)$ time. Finally sorting $S$ with respect to due date requires
$O(n\log n)$ time. Therefore the algorithm requires $O(n^2)$ time.  



\end{document}

