%
\documentclass{article}
\usepackage{fullpage}
\usepackage{epsfig}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amstext}
\usepackage{amscd}
\usepackage{amsmath}
\usepackage{theorem}
\usepackage{times}
\usepackage{graphicx}
\usepackage{alltt}
\usepackage{enumerate}

\begin{document}

\noindent
\fbox{
\parbox{\textwidth}{
\begin{Large}
{\bf CS 577: Introduction to Algorithms\hfill Discussion Solutions for Week 10\\}
\end{Large}

Updated:\vspace{0.02in}
\today
\hfill {\bf} Scribe: Pranav Mehendiratta
}}

\section*{Problem 1}
	Let $X$ be the random variable representing the number of records in a
	uniformly random permutation of numbers $\{1,\ldots,n\}$. We need to find
	$E[X]$. Let $X_i$ be the indicator random variable for the event that $i$-th
	element is a record. Then, $X = \sum_i X_i$.
	Moreover, for any $i$, $E[X_i] = Pr[X_i = 1]$ is the probability that the
	$i$-th number is greater than all the numbers before it.
	To find $Pr[X_i = 1]$ we think of first randomly partitioning the $n$ numbers
	into the ``first $i$'' (as a set), the ``last $n-i$'', and then randomly
	ordering each of these sets.
	Whether the largest is placed last doesn't depend on how the numbers are
	partitioned, nor on the order of the last $n-i$.
	We can just focus on the random permutation of the first $i$ elements.
	In a uniformly random permutation of $i$ numbers, the largest number is
	uniformly distributed;
	therefore, the probablity it appears last is $\frac{1}{i}$.
	Thus, $E[X_i] = \frac{1}{i}$,
	and so $E[X] = \sum_{i=1}^n \frac{1}{i}$, the $n$-th harmonic number.



\section*{Problem 2}
\subsection*{Part (a)}

Within any row $k$, all elements larger than $M[i_0, j_0]$ and smaller than
$M[i_1, j_1]$ occur within a contiguous range.
For each $k$, let $s_k$ be the index of the first element larger than $M[i_0, j_0]$;
if this index does not exist, set $s_k = n+1$.
Similarly, let $l_k$ be the index of the first element not smaller than $M[i_1, j_1]$,
or alternatively $n+1$ if no such index exists.
For each row $k$, $s_k$ and $l_k$ are such that $M[k, j]$ for $s_k \le j < l_k$
is precisely all the elements in the $k$-th row that are larger than $M[i_0,
j_0]$ and smaller than $M[i_1, j_1]$.
The number of such elements is $l_k - s_k$ if $l_k \ge s_k$, or $0$ otherwise.
This suggests the following algorithm:
for each $k$, find $s_k$ and $l_k$,
and then compute the number of elements of $M$ between
$M[i_0,j_0]$ and $M[i_1,j_1]$ as $\sum_{k=1}^n l_k-s_k$.

We explain the process for finding $s_k$; a similar one works for $l_k$.
First we do a linear scan of row $1$ to find $s_1$.
We start with $s_1=n+1$ and decrement until $s_1=1$ or $M[1,s_1-1]$ is at most
$M[i_0,j_0]$.
For $s_2$, we know that the rows and columns are monotonically increasing,
so $M[2, s_1] > M[1, s_1]$, from which we deduce $s_2 \leq s_1$.
Therefore, to find $s_2$, it suffices to linearly scan row $2$
\emph{starting at} $s_1$, and decrementing until the true value is found.
In general, $s_{k+1} \le s_k$, and we can start a linear scan for $s_{k+1}$
at $s_k$.
With this strategy, the total work of scanning across all rows is only $O(n)$,
since the total number of decrements is at most $n$.
When finding the values of $l_k$, it similarly holds that $l_{k+1} \le l_k$,
and the same strategy works.

\paragraph{Algorithm} CountBetween ($M, i_0, j_0, i_1, j_1$)
\begin{enumerate}
\item
	Let $S[1\ldots n]$ and $L[1\ldots n]$ be arrays for storing $s_k$ and $l_k$
	for every row $k$. 
\item Initialize $s\gets n+1, l\gets n+1$.
\item For each row $k$ from $1$ to $n$: 
	\begin{enumerate}
	\item While $s > 1$ and $M[k, s-1] > M[i_0,j_0]$, decrement $s$.
	\item While $l > 1$ and $M[k, l-1] \ge M[i_1,j_1]$, decrement $l$.
	\item Set $S[k] \gets s$ and $L[k] \gets l$.
	\end{enumerate}
\item Sum the quantities $\max(0,L[k] - S[k])$ over all $k = 1\ldots n$. The result is the number of elements. 
\end{enumerate}

\paragraph{Runtime Analysis}
Each iteration of step 3 involves $O(1)$ work, plus some $O(1)$ work per
decrement to $s$ or $l$.
Since $s$ and $l$ start at $n+1$ and decrease to no less than $1$,
there are at most $O(n)$ total decrements across all iterations of step 3.
Thus step 3 involves $O(n)$ work in total.
Steps 1,2, and 4 take $O(n)$ time.
Thus the overall running time is $O(n)$.

\paragraph{Correctness}
As discussed above, it suffices to check that the algorithm correctly computes
$S[k] = s_k$ and $L[k] = l_k$ after the end of step 3.
This follows from the following claim, which can be shown by induction on $k$
by using our observation that $s_k \leq s_{k-1}$ and $l_k \leq l_{k-1}$:
for all $k=1,\ldots,n$, after the $k$-th iteration of step 3, $S[1\ldots k]$ and
$L[1\ldots k]$ store $s_1,\ldots,s_k$ and $l_1,\ldots,l_k$ respectively.


\subsection*{Part (b)}

From part (a), we know that $M[k, j]$ where $s_k \le j < l_k$ consists of
precisely the elements larger than $M[i_0, j_0]$ and smaller than $M[i_1, j_1]$
in row $k$.
For $k = 0, 1, \ldots, n$, let $t_k$ denote the total number of such entries
among the first $k$ rows of $M$.
Note that $t_0 = 0$ and $t_{k+1} = t_k + \max(0,l_k-s_k)$ for all $k=1,\ldots,n$.

We now introduce the sampling procedure.
For each row $k=1,\ldots,n$,
we think of labeling the entries $M[k, j]$ for $s_k \le j < l_k$ as $t_{k-1}+1, \ldots, t_k$,
leaving the remaining entries of $M$ unlabeled.
Note that this assigns to every entry of $M$ between $M[i_0,j_0]$ and
$M[i_1,j_1]$ a distinct number between $1$ and the number of such entries ($=
t_n$), and assigns no other labels.
\textit{i.e.}, there is a one-to-one and onto correspondence between the labels
and the entries we wish to sample.
It follows that, in order to sample a uniformly random element of $M$ between
$M[i_0,j_0]$ and $M[i_1,j_1]$,
it suffices to sample a uniformly random label $x$ from $1,\ldots,t_n$, and then
output the entry of $M$ with label $x$.
Since for any label $x$,
the entry with label $x$ can be found in $O(n)$ time (see below),
this gives the desired sampling procedure.

\paragraph{Algorithm} SampleUniformly($M, i_0, j_0, i_1, j_1$)

\begin{enumerate}
\item Construct $S[1\ldots n]$ and $L[1\ldots n]$ as in part (a).
\item Let $T[0\ldots n]$ be an array. Use $S$ and $L$ to fill in $T[k] = t_k$ for $k=0,1,\ldots,n$.
\item Generate a random number, $x$, between $1 \leq x \leq T[n]$. 
\item Let $k \in \{1,\ldots,n\}$ so that $T[k - 1] < x \leq T[k]$. 
\item Output $M[k, S[k] + x - T[k - 1] - 1]$.
\end{enumerate}

\paragraph{Runtime Analysis}
Step 1 takes $O(n)$ time by part (a).
Step 2 takes $O(n)$ time when computing $T[k+1]$ as $T[k] + \max(0,L[k]-S[k])$.
Step 4 can also be implemented in $O(n)$ time.
All other steps take constant time.
Therefore, the overall running time is $O(n)$.

\paragraph{Correctness}
After sampling $x$, the algorithm outputs the entry of $M$ labeled by $x$.
As mentioned above, there is a one-to-one and onto correspondence between the
labels and the entries of $M$ between $M[i_0,j_0]$ and $M[i_1,j_1]$.
It follows that by picking a uniformly random label, the algorithm outputs
a uniformly random entry of $M$ between $M[i_0,j_0]$ and $M[i_1,j_1]$, as desired.

\subsection*{Part (c)}

Recall that QuickSelect takes in an array $A$ and index $r \in \{1,\ldots,n\}$ and returns
the element of $A$ with rank $r$
(\textit{i.e.}, the element that would be in position $r$ after sorting $A$).
QuickSelect can be summarized as picking a pivot $p$, finding the rank of $p$,
and recursively searching either among elements larger than $p$ or among
elements less than $p$.
In randomized QuickSelect, $p$ is chosen uniformly at random, and this sampling process
is retried until the rank of $p$ lies between $|A|/4$ and $3|A|/4$.

Our algorithm will follow the same outline.
Since we are working with a totally monotone matrix instead of an array, we
implement the primitive operations differently, however.
For one, we only keep track of indices $i_0,j_0, i_1,j_1$, wherein we are
searching among entries of $M$ between $M[i_0,j_0]$ and $M[i_1,j_1]$.
We sample the pivot with a call to (a slight modification of) SampleUniformly
from part (b).
We find the rank of the pivot using CountBetween from part (a).
From here, QuickSelect is essentially the same as above.

\paragraph{Algorithm}
MatrixQuickSelect($M, i_0,  j_0, i_1, j_1, r$) finds the rank-$r$ element ($r = 1,2,\ldots$)
from among the entries of $M$ between $M[i_0,j_0]$ and $M[i_1,j_1]$.
To find the median of $M$, call MatrixQuickSelect($M, 1, 1, n, n, n^2/2$).

In the following pseudocode, we assume that SampleUniformly returns the row and
column of the sampled element (not the value stored there). The implementation of
SampleUniformly in part (b) can be trivially modified to do this.

\begin{enumerate}
\item Let $m \gets \mathrm{CountBetween}(M, i_0, j_0, i_1, j_1)$.
\item If $m \leq 10$, then return the rank-$r$ element.
\item Let $p, q \gets \mathrm{SampleUniformly}(M, i_0, j_0, i_1, j_1)$.
\item Let $r_{piv} \gets \mathrm{CountBetween}(M, i_0, j_0, p, q) + 1$.
\item If $r_{piv} <\frac{m}{4}$ or $r_{piv} > \frac{3m}{4}$, then retry from step 3.
\item If $r = r_{piv}$, then return $M[p,q]$.
\item If $r < r_{piv}$, then return $\mathrm{MatrixQuickSelect}(M, i_0, j_0, p, q, r)$.
\item If $r > r_{piv}$, then return $\mathrm{MatrixQuickSelect}(M, p, q, i_1, j_1, r - r_{piv})$.
\end{enumerate}

In step 2 (handling the base case), ``return the rank-$r$ element'' is underspecified.
In that line, there are at most ten elements of $M$ between $M[i_0,j_0]$ and $M[i_1,j_1]$.
By running steps 4 and 5 of SampleUniformly for \emph{all} values of $x$, all
elements of $M$ between $M[i_0,j_0]$ and $M[i_1,j_1]$ can be explicitly
enumerated in time $O(n)$.
From here, the element of rank $r$ is easily found.

\paragraph{Runtime Analysis}
Ignoring the work done by recursive calls, every step of the algorithm runs in
$O(n)$ time (or better).
The only loop comes from step 5 looping back to step 3.
As in the analysis of randomized quickselect, the expected number of times to
loop back is $2$.
Thus the total expected running time ignoring recursive calls is $O(n)$.

To account for recursive calls, we set up and solve a recurrence.
We note that the input size ``$n$'' does \emph{not} decrease in recursive
calls.
Instead, it is the number of entries in $M$ between $M[i_0,j_0]$ and
$M[i_1,j_1]$ that decreases with each call.
Let $m$ denote this number, and $T(m)$ the running time on inputs of size $m$.
In each recursive call, the quantity denoted by $m$ decreases by at least a
quarter;
this leads to the recurrence $T(m) = T(\frac{3m}{4}) + O(n)$,
which solves to $T(m) = O(n\log m)$.
Since $m \le n^2$, $\log m = O(\log n)$.
It follows that the algorithm runs in time $O(n\log n)$.

\paragraph{Correctness} Follow the correctness proof for QuickSelect.
CountBetween and SampleUniformly are correct by parts (a) and (b).

\end{document}
