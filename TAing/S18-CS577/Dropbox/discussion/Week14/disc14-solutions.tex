%
\documentclass{article}
\usepackage{fullpage}

\usepackage{epsfig}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amstext}
\usepackage{amscd}
\usepackage{amsmath}
\usepackage{theorem}
\usepackage{times}
\usepackage{graphicx}
\usepackage{alltt}
\usepackage{enumerate}
\usepackage{enumitem}
\usepackage{algorithmicx, algpseudocode}

\newcommand{\SubS}{\textsc{SubsetSum }}
\newcommand{\SQ}{\textsc{SquaredSum }}
\newcommand{\PP}{\textsc{PartitionProblem }}

\begin{document}

%\thispagestyle{empty}

\noindent
\fbox{
\parbox{\textwidth}{
\begin{Large}
{\bf CS 577: Introduction to Algorithms\hfill Discussion Solutions for Week 14\\}
\end{Large}

\vspace{0.02in}
\hfill {\bf} 
}}

\subsection*{Solutions}

\subsubsection*{Problem 1:}
We need to show that \SQ is $\mathrm{NP}$-hard. To do so,  we need to  
give a reduction from an $\mathrm{NP}$-hard problem to \SQ. \\

\noindent We give a reduction from \SubS to \SQ.  
Recall that the decision version of the \textsc{SubsetSum} problem is defined as follows:\\ 

\noindent \SubS : Given a collection $U$ of positive integers and a target value $t$, determine whether there is a subset $S \subseteq U$ 
 such that the sum of the integers in $S$ is exactly $t$, i.e., determine whether there is  a subset $S$ s.t.  $\sum_{s \in S} s = t$.     \\
 
\noindent Observe that  \SubS asks for an exact sum, whereas the \SQ problem merely asks for any solution with squared sum smaller than the given target. To align the two together, we observe that the smallest squared sum is achieved when the sums of the different components are equal. So, for example, when $k=2$, the only way to achieve a squared sum of $2(W/2)^2$ is if each of the parts has sum $W/2$, where $W$ is the sum of all the numbers. This suggests that if we had an instance of \SubS where the target was exactly $W/2$, we could reduce it "as is" to \SQ by giving a target of $W^2/2$. However, we  need to show that a general instance of subset sum can be reduced to one where the target is exactly $W/2$. 
We call this restricted case of \SubS the \PP.\\

\noindent \PP : Given a collection $U^\prime$ of positive integers that sum to $W$, determine whether there is a subset $S$ s.t. 
$\sum_{s \in S} = 1/2 \cdot \sum_{u\in U} u = W/2$. \\
 
\noindent We begin with a reduction from \PP to \SQ.  The reduction is based on the following basic inequality : 
For any pair of  numbers $P,Q$,
$$ 2(P^2 + Q^2) \geq (P+Q)^2.$$  
Observe that  equality holds  if and only if $P = Q$.  \\
  
\noindent Reduction from \PP to \SQ: Given an instance $U$ of \PP,  the reduced instance of \SQ is defined as follows:

The  positive integers $x_1,x_2 \ldots x_n$ are  the  elements in $U$.

 $k = 2$ and $B = W^2/2$,  where $W$ is the sum of elements $x_1,x_2 \ldots x_n$. 
 
By the inequality above, $S_1, S_2$ is a solution for this instance of \SQ 
if and only if $\sum_{x \in S_1} x = \sum_{y \in S_2} y = W/2$. The latter holds if and only if $S_1$
 is a solution to the initial instance of the \PP.
We have thus given a reduction from \PP to \SubS.  
We now show that \SubS can be reduced to \PP.  \\

\noindent Reduction from \SubS to \PP: 
Let $U,t$ be an instance of \SubS.  Let $W$ be sum of elements in $U$. The reduced instance of \PP is defined as follows: 

$U^\prime = U \cup \{W-2t\}$.

We show that  there is a subset  
of $U$ that sums to $t$ if and only if  there is a subset $S^\prime$ of $U^\prime$ s.t.  
$$\sum_{s \in S^\prime} s = 1/2 \cdot \sum_{u\in U^\prime} u = (W+W-2t)/2 = W-t.$$

Suppose there is a subset $S$ of $U$ that sums to $t$, then  $S^\prime = S \cup \{W-2t\}$ has total sum
$W - t$ which is exactly half the total sum of $U^\prime$.

For the other direction, suppose that there is a subset  $S$ of $U^\prime$ such that 
$\sum_{x \in S^\prime} x = 1/2 \cdot \sum_{y \in U^\prime} y = (W + W-2t)/2 = W - t$. 
Then $S^\prime \setminus \{W-2t\}$ is a subset of $U$ and has total sum $t$. 
This completes the proof. 

\subsubsection*{Problem 2}
\begin{itemize}
\item[(a)] Given an instance of \textsc{BoxDepth}, made up of rectangles $\{1,\ldots, n\}$, here is an algorithm that finds the maximum number of rectangles sharing a common point, by using the \textsc{Clique} function: 

Let $G$ be the graph with one vertex $v_i$ for each rectangle $i$, and with the edge $(v_i,v_j)$ iff rectangles $i$ and $j$ have a common point. 

For $k=1$ to $n$, call the \textsc{Clique} function on $G$ and $k$. Return the maximum $k$ for which \textsc{Clique}$(G,k)$ returns ``yes''. 

We leave it as an exercise to prove that a set of rectangles has  box depth $\geq k$ if and only if $G$ contains a clique on $\geq k$ vertices, therefore that the algorithm is correct. 

\item[(b)]  We give an $O(n^3)$ time algorithm for \textsc{BoxDepth}. 
The key observation is the following: the intersection of two or more rectangles is 
also a rectangle. Furthermore,  each corner of this rectangle is either an intersection point (of the sides of
 two or more rectangles) or  the corner of one of the original rectangles.   
In particular, any set of overlapping rectangles,  
must all contain a common point that is either an intersection point or the corner of some rectangle. 
Let the set of  such points be $P$. 

To find the largest value $k$ for which there are $k$ overlapping rectangles that share a common point, 
we only need to find for each point $p$ in $P$, 
the number of rectangles containing $p$ and output the maximum over all points.

The number of points in $P$ is at most $O(n^2)$. This is because the number of intersection points is 
at most $4n^2$ (every pair of rectangles intersect at at most $4$ points) and the number of corners is
at most $4n$.  
For each point in $P$, checking whether a given rectangle contains the point takes $O(1)$ time, and as a result
 finding the number of rectangles that contain the point takes at most $O(n)$ time. 
 Therefore, the algorithm described above runs in $O(n^3)$ time.  
   
  
\item[(c)] The reduction in part (a) and the algorithm in part (b) do not together imply $\mathrm{P} = \mathrm{NP}$ because the   $\mathrm{P} = \mathrm{NP}$ question can be equivalently stated as whether \textsc{SAT}, or any problem that \textsc{SAT} reduces to, in particular \textsc{Clique}, has a polynomial-time algorithm. What we showed in part (a) and part (b) says nothing about this question. What we did show in part (a) is that if \textsc{Clique} has a polynomial-time algorithm, i.e., if $\mathrm{P} = \mathrm{NP}$, then so does \textsc{BoxDepth}. We then showed in part (b) that ``if $\mathrm{P} = \mathrm{NP}$'' is an unnecessary assumption; there is, unconditionally,  a polynomial-time algorithm for \textsc{BoxDepth}.

\end{itemize}


\end{document}
