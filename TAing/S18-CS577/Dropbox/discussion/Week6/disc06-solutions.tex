%
\documentclass{article}
\usepackage{fullpage}

\usepackage{epsfig}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amstext}
\usepackage{amscd}
\usepackage{amsmath}
\usepackage{theorem}
\usepackage{times}
\usepackage{graphicx}
\usepackage{alltt}
\usepackage{enumerate}
\usepackage{enumitem}
\usepackage{algorithmicx, algpseudocode}

\begin{document}

%\thispagestyle{empty}

\noindent
\fbox{
\parbox{\textwidth}{
\begin{Large}
{\bf CS 577: Introduction to Algorithms\hfill Discussion Solutions for Week 6\\}
\end{Large}

\vspace{0.02in}
\hfill {\bf} Scribe: Zach Zhou
}}

\subsection*{Solutions}

\begin{enumerate}
\item Here are counterexamples to some natural greedy approaches:
\begin{itemize}
\item Order items by decreasing weight.\\
$w_1 = 5,\ t_1 = 1000$\\
$w_2 = 4,\ t_2 = 10$\\
$w_3 = 4,\ t_3 = 10$\\
Greedy  would schedule 1, then 2, then 3 for a cost of $5*1000 + 4*1010 + 4*1020 = 13120$.\\
Optimal cost is $4*10 + 4*20 + 5*1020 = 5220$. 
\item Order items by decreasing time.\\
We can use the same weights and times from above as a counterexample. Greedy here produces the same solution as greedy from above, and the optimal cost remains the same.
\item Order items by increasing time.\\
$w_1=100,\ t_1=100$\\
$w_2=3,\ t_2=10$\\
$w_3=2,\ t_3=20$\\
Greedy would schedule 2 then 3 then 1 for a cost of $3*10 + 2*30 + 100*130 = 13090$.\\
Optimal cost is $100*100 + 3*110 + 2*130 = 10590$.\\
\end{itemize}

\textbf{Algorithm}\\
Schedule jobs in order of decreasing $w_i/t_i$.\\

\textbf{Correctness}\\
We will prove optimality by an exchange argument.
\smallskip

Let $G$ be the solution produced by our algorithm and $O$ be any other schedule. If $O$ is not ordered by decreasing $w_i/t_i$ there must be two consecutive items $i < j$ with $w_i/t_i < w_j/t_j$. We will show that swapping these two items will improve $O$.

Before the swap, $i$ and $j$ are contributing $w_iC_i + w_jC_j = w_iC_i + w_j(C_i + t_j)$ to the cost of $O$. Once they are swapped, $j$ now completes at $C_i - t_i + t_j$ and $i$ finishes when $j$ finished previously, $C_i + t_j$. Thus, they are now contributing $w_j(C_i - t_i + t_j) + w_i(C_i + t_j)$ to the cost. Taking the difference of  contributions before and after the swap, we see that swapping reduces the cost by  $w_jt_i - w_it_j$, a quantity greater than $0$ because  $w_i/t_i < w_j/t_j$. 

Proceeding in this manner, we can transform $O$ into $G$ step-by-step, without incurring an increase in the cost during any of the steps. This means that $G$ is as good a solution as $O$. Since $O$ was arbitrary, it follows that $G$ is optimal.\\ 




\textbf{Running Time}\\
Computing $w_i/t_i$ for every job $i$ takes $O(n)$ time. Sorting the jobs takes $O(n\log n)$ time. Thus, the overall running time is in $O(n\log  n)$.\\

\item[2.(a).]
The algorithm is simple. Put as many words as possible into the first line; repeat with the remaining words starting from line 2, and so on until no words are left.  

To see why this algorithm gives an optimal solution, let $O$ be any other way of arranging the words. Denote the arrangement given by our algorithm with $S$. We now give a ``greedy stays ahead'' argument.

\emph{Claim.} For every $i$,  the penalty incurred by the first $i$ lines according to $S$  is at most the penalty incurred by the  first $i$ lines according to $O$. Further, there are at least as many words in the first $i$ lines according to $S$, as there are in the first $i$ lines according to $O$. 

This claim can be seen true by induction, with base case  $i=1$. This proves the correctness of our algorithm.

The running time of the algorithm is in  $O(n)$ because all that is done is to scan through the word list, starting a new line whenever  the next word in the current line would make the line length $ >L$.

\item[2.(b).]
For $1 \leq i \leq n$, let $w_i$ denote the $i$-th word.
We begin with some useful observations:
\begin{itemize}
\item If all $n$ words in the given input fit on a single line, then it is  optimal 
to place all words on a single line. The total slop of this solution is zero
because the last line of a paragraph (the only line in this case) does not contribute to
the slop. 
\item Now consider the case in which all $n$ words don't fit on a single line. Suppose 
we have an optimal solution\footnote{a break up of words into lines that minimizes the total slop} 
in which the first $j-1$ words are in the first line. 
Then if we delete the first line, what we are left with must necessarily be the 
optimal solution for the remaining  words .
This is because if the remaining  words had a better solution, i.e., one that resulted in 
smaller total slop, then we could just add the first line with $j-1$ words to this solution
to get a  solution for all $n$ words that incurs smaller total slop.  
\end{itemize}    
The second observation suggests that the subproblems we ought to consider are the following: 
For each $i$ determine the optimal break up for the words $\{w_i, \ldots, w_n\}$.

Define $OPT(i)$ to be the slop of the optimal solution for the words $\{w_i, \ldots w_n\}$. 
Denote by $SLOP(r,s)$ the slop of a line when the words from $\{w_r, \ldots w_s\}$ are placed in a  line 
other than the last line (assuming they fit on a single line).  
That is $SLOP(r,s) = (L - (\sum_{k=r}^{s} p_k +  (s-r) ))^3$.  
      
We now state the recursive formula for slop of the optimal solution based on the observations above. 
We have two cases 
 \begin{itemize}
\item[(i)] If  $\sum_{k = i}^{n} p_k + (n-i) \leq L$ (i.e. the words  $\{w_i \ldots w_n\}$ fit on a single line),
then $OPT(i) = 0$. This is the base case of our recurrence.    
\item [(ii)] If $\sum_{k=i}^{n} p_k + (n-1) > L$, 
then $$OPT(i) = \min_j SLOP(i,j-1) + OPT(j) $$ 
where the minimum is over $j$ s.t. the words $w_i \ldots w_{j-1}$ fit on a single line, i.e., over all $j$ s.t. $i < j$ and 
$\sum_{k=i}^{j-1} p_k + (j-1 - i) \leq L$.     
\end{itemize}

Note that the minimum possible slop for all $n$ words is $OPT(1)$. 
Below is the procedure to fill up an array $Opt[1..n]$, meant to store the values $OPT(1)..OPT(n) $. 

\begin{algorithmic}[1]
\State  $k \leftarrow  n$, $sum \leftarrow p_n$
\While{$sum < L$} {(this loop handles item (i) in the recurrence)}
\State $Opt[k] \leftarrow 0$
\State $k \leftarrow k-1$
\State $sum \leftarrow sum + p_k$
\EndWhile
\For{$i \leftarrow k \text{ to } 1$}
 \State $Opt[i] \leftarrow \min_j [SLOP(i,j-1) + Opt[j]]$, where min is over $j$ s.t. $\sum_{k=i}^{j-1} p_k + (j-1 - i) \leq L$
\label{findmin}
\EndFor
\end{algorithmic} 


\paragraph{Running Time} We fill in the $n$ elements of the $Opt$ array, where for the $i$th element, we potentially go through all the elements $i+1..n$ and for each element compute a sum involving $j-i$ terms --- altogether incurring $O(n^3)$ time assuming, as the question states, arithmetic on a pair of numbers can be done in constant time. 

Below is an $O(n^2)$-time algorithm for finding the optimal breakpoints in the paragraph. The idea is that for every $i$, there must be some $j>i$ realizing the value found at line~\ref{findmin} of the above algorithm; that $j$ is an optimal point to start the second line  in a paragraph formed by words $w_i..w_n$.

\begin{algorithmic}[1]
\State   $i = 1$
\State  $breakpoints = $ empty
\While {$Opt[i] > 0$} 
 \State find the first $j>i$  such that  $Opt[i] = SLOP(i,j-1) + Opt[j]$ 
 \State add $j-1$ to $breakpoints$
 \State $i = j$
\EndWhile 
\end{algorithmic}

\end{enumerate}

\end{document}
