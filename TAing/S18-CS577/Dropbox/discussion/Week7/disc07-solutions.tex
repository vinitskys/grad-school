%
\documentclass{article}
\usepackage{fullpage}

\usepackage{epsfig}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amstext}
\usepackage{amscd}
\usepackage{amsmath}
\usepackage{theorem}
\usepackage{times}
\usepackage{graphicx}
\usepackage{alltt}
\usepackage{enumerate}
\usepackage{enumitem}
\usepackage{algorithmicx, algpseudocode}

\begin{document}

%\thispagestyle{empty}

\noindent
\fbox{
\parbox{\textwidth}{
\begin{Large}
{\bf CS 577: Introduction to Algorithms\hfill Discussion Solutions for Week 7\\}
\end{Large}

\vspace{0.02in}
\hfill {\bf} 
}}

\subsection*{Solutions}

\subsubsection*{Problem 1:}
 We first present a pseudo-polynomial time algorithm with  running time polynomial in $n$ and $S$. 
\paragraph{A pseudo-polynomial solution. }

We begin by observing that in any given schedule, the  cost 
 for a particular month (storage plus shipment costs)  can be completely determined from the number of  trucks in storage at the beginning of the month and the number of trucks left in storage at the end of the month (or beginning of the next month).
This suggests that our subproblems be defined by two parameters - the month in question (because each month $i$ has a different demand $d_i$), and the number of trucks in storage at the beginning of each month.\\

We will build an $n \times (S + 1)$ table of solutions to subproblems. Cell $(i, s)$ of our table will contain
the value $OPT(i, s)$, which represents the minimum cost to have filled all orders for months $i, \ldots n$, when we have $s$ trucks in storage at the beginning of month $i$. Our base case corresponds to the $n$-th row of the table.
The entries of the $n$-th row are defined as follows: 
 $OPT(n,s) = K$ if $ s < d_n$,  and $OPT(n,s) = C \cdot(s - d_n)$ otherwise.
This is because if $s < d_n$, then the only way to meet the demand is to order more trucks, and it is optimal to order trucks to exactly meet the demand $d_n$. If on the other hand the number of trucks in storage is greater than or equal to the demand $d_n$, then we do not need to order trucks, but we incur the cost of storage for any extra trucks.\\ 

The remaining entries of the table are defined by the following recurrence: 


$$ OPT(i,s) = \min_{0 \leq s^\prime \leq S}  \{OPT(i+1,s^\prime) +  cost(i,s,s^\prime)\},$$

Where $cost(i,s,s^\prime)$ is the cost for month $i$, given that we started with $s$ trucks at the beginning of month $i$ and have $s^\prime$ trucks at the beginning of the subsequent month. $cost(i,s,s^\prime)$ is defined as follows: \\


$cost(i, s, s^\prime) =  $
$\begin{cases}
   
    C \cdot (s^\prime) &\text{ if } s^\prime = s-d_i \\
    K + C\cdot (s^\prime) &\text{ if }  s^\prime > s-d_i\\
    \infty &\text{ if }  s^\prime < s-d_i
  \end{cases}
$
 
The recurrence follows from the following observations: 
\begin{itemize}
\item[(1)] If $s-d_i = s^\prime$, then we have exactly $s^\prime$ trucks left in storage after the shipment for month $i$. Therefore the cost in this case is just the cost of storing $s^\prime$ trucks. 
    \item[(2)] If $s-d_i < s^\prime$, then to have $s^\prime$ trucks left in storage after shipping trucks for month $i$, we would have to place an order of $s^\prime - (s- d_i)$ trucks. This leads to the additional cost of $K$ over the cost in case $(1)$. 
    \item[(3)] If $s - d_i > s^\prime$, then we cannot have $s^\prime$ trucks left over at the beginning of month  $i+1$. To account for this, we define the cost in this case to be $\infty$.
    
\end{itemize}


 
 When the table is filled in, the cell $(1,0)$ will contain the minimum cost to fulfill  all orders. To find the actual sequence of orders that achieves this minimum,
we could store in each cell, along with that cell?s minimum cost, the order we placed that month to achieve the cost. To fill in each cell of the table above row $n$, we must look at $S$ other cells (each
cell in the row above). This gives us a running time $O(nS^2)$, which is polynomial in $n$ and $S$.

\paragraph{A fully polynomial time algorithm}
We can actually get an 
algorithm with running time polynomial in $n$ for this problem by being a little careful about which subproblems we consider. 
The key to this faster approach (in terms of the table in the solution we have outlined above) is that many of the cells in the table represent choices we would never want to make. In particular, after each month, we either want to have enough trucks left over to satisfy the next month's entire order, or we want to have no trucks left over. If we were to store some trucks between months $i - 1$ and $i$, but not enough to satisfy all of $d_i$, we'd still have to order some trucks in month $i$ and pay the order cost $K$. If we?re going to pay that order cost, there?s no point in our also paying the storage cost to store some trucks between months $i-1$ and $i$, since the order cost is the same no matter how many trucks we order.


This suggests a one-parameter solution, where we fill in a
one-dimensional array in which the entry at $i$, $OPT(i)$, represents the minimum cost to meet demands for month $i, \ldots, n$, when there are no trucks  in storage at the beginning of month $i$.  

To ease the presentation of the algorithm, we treat our input as being made up of $n+1$ months and set the demand of the $n+1$-th month to be $0$.
For our base case, we define $OPT(n+1)$  to be $0$. 
The remaining entries are defined by the  recurrence below. 
Note that the minimum cost for months 
$\{i, \ldots, n+1\}$, when we have zero trucks in storage at the beginning of month $i$, and we place the next order in month $j$, is exactly the cost of a single order in month $i$ ($K$) plus the cost of storage of trucks for months $\{i+1, \ldots, j-1\}$ plus $OPT(j)$. If we require more than $S$ trucks to meet the demands for months $\{i+1, \ldots, j-1\}$, then we define the cost to be infinity. The above suggests that the choice that is to be made when computing $OPT(i)$ is when to place the next order of trucks. This yields the following recurrence. 


$$OPT(i) = \min_{j > i} \{K +  cost(i,j) + OPT(j)\}$$
for $i < n+1,$

where

$
cost(i, j) = 
\begin{cases} 
  (\sum_{l=i+1}^{j-1} d_l\cdot (l-i)) \cdot C &\text{ if }\sum_{l=i+1}^{j-1} d_l \leq S \\
 \infty  &\text{ otherwise}     
 \end{cases}
 $
 
The minimum cost to fulfill the demands for all months is $OPT(1)$.

\textbf{Running time: }The table has $O(n)$ cells and  implemented naively, each cell would take $O(n^2)$ time to compute. This is because we have $O(n)$ choices of $j$, and the formula for cost requires  $O(j-i)	 = O(n)$ time to evaluate for any  particular $j$. However,  $cost(i,j+1)$ can be computed from $cost(i,j)$ in $O(1)$ operations, as
$cost(i,j+1) = cost(i,j)  + d_j \cdot (j - i)  C.$ This allows us to compute each cell with $O(n)$ operations, and yields a running time of $O(n^2) $ time to fill up the table and compute $OPT(1)$.

\textbf{Computing the schedule:}
 The optimal schedule can be obtained by keeping track for each $i$, of the $j$ at which the minimum is obtained. Call this $j_i$. 

We can recover from this the optimal schedule schedule by first buying the trucks
for months $1, 2, ..., j_1 - 1 $ in month $1$, then buying the trucks for months $j_1, j_1 + 1, ..., j_{(j_1+1) - 1}$ in month $j_1$,
and so on






 






\subsubsection*{Problem 2}
  
Observe that is optimal to drop off all students belonging to a particular exit the very first time the bus reaches the exit.
This tells us that when the bus is at the school, 
the choice we need to make is whether the first stop of the bus should be the nearest exit to the east of the school or the nearest exit to the west. 
Let us suppose the bus chooses to head to the east exit and drops off the children belonging to that exit. 
We now have a problem exactly like the one we started with. 
The only change is that the bus is now at the first exit to the east of the school, and the number of exits (to the east of the school) that the bus is yet to visit has gone down by one. 
This suggests a recursive formula for solving our problem.
  
We begin by defining our subproblems. Since at each stage, the bus either chooses to visit the nearest exit to the east or the nearest exit to the west, we index our subproblem by the the nearest unvisited exit to the east and the nearest unvisited exit to the west of the school. We also need to specify the current location of the bus. Observe that if the
nearest unvisited exits to the east and west are $i$ and $j$ respectively, then the last stop of the bus must have been either at the exit to the east of the school closest to $i$ or the exit to the west of the school closest to $j$. As a result we only need to specify whether the bus is to the east (E) or west (W) of the school to specify the location of the bus.\\

We define our subproblems as follows: Let $OPT(i,j,E)$ be the cost of the optimal solution when the bus is at exit $i$ + 1, the nearest unvisited exit to the east of the bus is $i$, and the nearest unvisited exit to the west of the bus is $j$. Similarly, we let $OPT(i, j,W)$ denote the cost of the optimal solution when the bus is at exit $j-1$,
the nearest unvisited exit to the east of the bus is $i$, and the nearest unvisited exit to the west of the bus is $j$.\\

We have the following recurrence\\
\newline
$
OPT(i,j,E) = min \bigg[ OPT(i-1,j,E) + (L(i+1) - L(i))(\sum_{r=1}^{i} S[r] + \sum_{t=j}^{n} S[t]),\\ \hspace*{3cm} OPT(i,j+1,W) + (L(j) - L(i+1))(\sum_{r=1}^{i} S[r] + \sum_{t=j}^{n} S[t])\bigg] \hspace{3cm}   (1)
$
\newline


$
OPT(i,j,W) = min \bigg[ OPT(i-1,j,E) + (L(i+1) - L(i))(\sum_{r=1}^{i} S[r] + \sum_{t=j}^{n} S[t]),\\ \hspace*{3cm} OPT(i,j+1,W) + (L(j) - L(i+1))(\sum_{r=1}^{i} S[r] + \sum_{t=j}^{n} S[t])\bigg]  \hspace{3cm}   (2)
$
\newline


For the base case, we have $OPT(0,n,E) = L[n] \cdot S[n]$, and $OPT(1,n+1,W) = L[n] \cdot S[1].$\\

We need a separate equation to describe the cost of the optimal solution when the bus is at the school and 
has not visited any exits. Let $i_0$ be the nearest exit to the east of the school, then $i_0+1$ 
is the nearest exit to the west. Lets denote the distance of the school from the east-most exit. We let $OPT(i_0, i_0 + 1,E) (= OPT(i_0, i_0 + 1,W))$ 
represent the optimal solution when the bus is at the school and has not visited any exits yet. We have \\

$OPT(i_0,i_0 +1,E) = OPT(i_0, i_0+1,W) = min \bigg[OPT(i_0 - 1, i_0+1,E)+(s-L(i_0))(\sum_{r=1}^{n} S[r]), \\ \hspace*{7cm} 
OPT(i_0, i_0+2,W)+(L(i_0+1)-s)(\sum_{r=1}^{n} S[r]) \bigg]
\hspace{1cm}   (3)$

We now present an iterative algorithm for the problem. Note that to compute $OPT(i, j,E),OPT(i,j,W)$ we need to have already computed $OPT(i-1,j,E)$ and $OPT(i,j+1,W)$. We build the array up in an order that ensures that this is the case.\\

\begin{algorithmic}[1]
\State $OPT(0,n,E)=L[n] \cdot S[n]$
\State $OPT(1,n+1,W)=L[n] \cdot S[1]$
\For{$i = 1 \text{  to } i_0$ }
 \For{$j = n \text{ to } i_0+1$ }
   \State   Set  $OPT(i,j,E)$ and $OPT(l,j,W)$ using (1), (2) or (3).
   
 \EndFor
\EndFor
\State \textbf{return} $OPT(i_0, i_0 + 1, E)$
\end{algorithmic}


\textbf{Running Time} -  Note that we can keep track of the number of children left on the bus so that line 5 takes $O(1)$  time for each iteration. 
The running time for the nested for loops is $O(n^2)$. So our algorithm runs in $O(n^2)$ time.





\end{document}