%
\documentclass{article}
\usepackage{fullpage}
\usepackage{colortbl}
\usepackage{epsfig}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amstext}
\usepackage{amscd}
\usepackage{amsmath}
\usepackage{amsthm} % new (see below)
%\usepackage{theorem}
\usepackage{times}
\usepackage{graphicx}
\usepackage{alltt}
\usepackage{mathtools}
\usepackage{multirow}
\usepackage{color}
\usepackage[table]{xcolor}
\definecolor{red}{RGB}{255,0,0}
\DeclarePairedDelimiter\ceil{\lceil}{\rceil}
\DeclarePairedDelimiter\floor{\lfloor}{\rfloor}


% The following were added specific to this solutions document
% Also, `theorem' was replaced by `amsthm' above.
\usepackage{mdframed}
\newtheorem{claim}{Claim}

\begin{document}

%\thispagestyle{empty}

\noindent
\fbox{
\parbox{\textwidth}{
\begin{Large}
{\bf CS 577: Introduction to Algorithms\hfill Discussion Solutions for Week 3\\}
\end{Large}

\vspace{0.02in}
\hfill {\bf} Scribe: Minh Nguyen
}}

\subsection*{Solutions}

\begin{enumerate}
\item
	\begin{enumerate}
	\item
    To get a running a running time of $O(n\log n)$ we should try to find an algorithm that gives the recurrence $$T(n)=2T(n/2)+O(n)$$
    
	A simple solution is trying to find a representative of the majority party. The idea is that if $S$ is a collection of delegates with a strict majority (more than half of the delegates belong to the same political party), and we partition $S$ into two sets $A$ and $B$, then either $A$ or $B$ must also have a strict majority. Hence we can recursively check for a representative delegate of the majority party by checking each of $A$ and $B$ for a majority representative. This may lead to a ‘false positive’ majority in $A$ or $B$ that was not a majority in $S$, but we can guard against this by introducing the majority representatives from $A$ and $B$ to all of $S$, and checking whether the result is a true majority.
    
    In detail, this leads to the following recursive algorithm, which takes as input a set $S$ of $n$ delegates:
    \begin{enumerate}
    \item Split $S$ into two sets $A$ and B of sizes $\floor*{n/2}$ and $\ceil*{n/2}$ respectively.
    \item Recurse on A to get a majority representative $m_A$, or \textit{NoMajority}.
    \item  If $m_A$ is defined,
    	\begin{enumerate}
    	\item Introduce $m_A$ to all other delegates in $S$.
        \item If $m_A$ is a representative of the majority, return $m_A$.
        \item Otherwise, continue.
    	\end{enumerate}
    \item Recurse on B to get a majority representative $m_B$, or \textit{NoMajority}.
    \item  If $m_B$ is defined,
    	\begin{enumerate}
    	\item Introduce $m_B$ to all other delegates in $S$.
        \item If $m_B$ is a representative of the majority, return $m_B$.
        \item Otherwise, continue.
    	\end{enumerate}
    \item If neither $m_A$ nor $m_B$ is a representative of a majority in $S$, return \textit{NoMajority}.
    \end{enumerate}
    Once a representative of the majority party is found, they can be introduced to every other delegate in order to determine the other members in the majority party.
    
    As-stated correctness of this algorithm hinges on the following fact:  
	\begin{claim}\label{claim:1}
	For any set $S$ of n delegates containing a strict majority party, and any partition $A$, $B$ of $S$, either $A$ or $B$ contains a strict majority party.
    \end{claim}
    \begin{proof}
     Let $k > n/2$ be the number delegates in the majority party in $S$, and let $k_A$ and $k_B$ be the corresponding count in $A$ and $B$, respectively. Since $A$ and $B$ partition $S$, $k_A + k_B$ = $k$, and $|A| + |B| = n$. If it were the case that $k_A \le |A|/2$ and $k_B \le |B|/2$, then it would follow that $$k = k_A + k_B \le |A|/2 + |B|/2 = n/2$$ which is contradicted to the assumption that $k > n/2$. Thus the claim holds.
    \end{proof}
 
    We can bound the running time as follows:
	\begin{claim}\label{claim:2}
	On input $n$ delegates containing a strict majority party, the above algorithm makes $O(n \log(n))$ introductions to determine the majority party.
	\end{claim}
    \begin{proof}
    On input n delegates, there are at most $2n$ introductions, plus those made in recursive calls. This gives the recurrence 
    $$T(n)=2T(n/2)+O(n)$$
    which we know solves to $T(n) = O(n \log(n))$.
    \end{proof}
	
    \item 
	In order to find a representative of the majority in linear time, we try to reduce the problem size by a constant factor after making a linear number of introductions. Our hope is to achieve the recurrence $T(n) = T(n/2) + O(n)$ for the number of introductions.
    
	The main idea is that if we find two delegates that are from different parties, and remove them from consideration, then the majority party maintains its majority – we might have removed one member from the majority party, but we also removed a non-majority delegate. On the other hand, if we pair up the delegates in a way that the two delegates in every pair are from the same party, then we can just take half the delegates – one from each pair – and recurse over this half. Once again, in doing so the majority is preserved. This suggests the following algorithm.
    
    \begin{enumerate}
    \item  If the number of delegates is odd, first set one, say $d$, aside.
    \item  Pair the remaining delegates, and introduce the delegates in each pair to one another.
    \item  Discard each delegate in a pair which belong to different parties, and discard one delegate from each pair which belong to the same party.
	\item  If the number of remaining delegates (not counting $d$) is odd, discard $d$; otherwise, keep $d$.
    \item  Recurse on the remaining delegates.
    \end{enumerate}
    Counting the number of introductions is straightforward: Note that at each step, at least $\floor{n/2}$ of the delegates are discarded, and $O(n)$ introductions are made. So the overall number of introductions satisfies the recurrence $T(n) \le T(n/2) + O(n)$, which solves to $O(n)$.
    
    Correctness follows from the following claim, whose proof we leave as a (tedious) exercise.
    \begin{claim}\label{claim3:}
    Let $D$ be a collection of $n$ delegates with a majority party $P$, and let $D'$ be the collection of delegates leftover after discarding. Then $P$ is a majority party in $D'$.
    \end{claim}
    \end{enumerate}
\item
Hint: To find a local minimum of an array, a simple solution is to compare middle element with its neighbors. If the middle element is not greater than any of its neighbors, then we return it. If the middle element is greater than its left neighbor, then there exists at least one local minima in left half (Why?). Thus, we recurse the algorithm on the left half. Else, we recurse on the right half. The runtime is $T(n)=T(\frac{n}{2})+O(1)=O(\log(n))$
\begin{enumerate}
\item
	A first attempt at an algorithm for this problem is to simply start at any node and repeatedly move to lesser-valued neighbor nodes until we reach a local minimum. This algorithm does work-it terminates because the values of the nodes that we visit are strictly decreasing, so we cannot visit the same node twice. Furthermore, we stop when we can no longer find a lesser-valued neighbor, meaning that termination occurs when we've reached a local minimum. However, this algorithm is not $O(n)$. We may zigzag through the grid, traversing half of the rows, so that we visit about half of the nodes.
    \vspace{5mm}
    
    Another attempt is to simply recurse on a portion of the grid, since any subgrid must have at least one local minimum (for instance, the subgrid’s global minimum). This doesn’t quite work either, since it’s possible that a local minimum of a subgrid won’t be a local minimum of the whole grid. This can occur if the local minimum of the subgrid appears along its perimeter, since it’s then possible for that node to have neighbors in the grid which don’t exist in the subgrid.
    \vspace{5mm}
    
    While the above two ideas don’t work on their own, we can draw inspiration from them to construct a working $O(n)$ algorithm. We can use divide-and-conquer to examine a decreasing sequence of grid values in gradually smaller portions of the grid while avoiding problems that arise from subgrid perimeter values. Our grid local minimum algorithm, $GLM(G)$, takes as input an $n \times n$ grid $G$ and returns a local minimum of $G$.  
    
 	$GLM(G):$
	\begin{enumerate}
	\item Manually find the minimum value in $G$ which is either on the perimeter or in the middle row or middle column. (If $n$ is even, search through both middle rows and both middle columns.)
	\item Check the neighbors of this minimum value node to determine whether it is a local minimum in the grid. If it is, then we are done. Otherwise, move to step (iii).
	\item Since the node found in step (i) is not a local minimum, it must have a neighbor of lesser value. Find such a neighbor
	\item The middle row and column chosen in step (i) divide the grid into four “quadrants”, and the neighbor found in step (iii) lies in one of these quadrants. Recurse on the quadrant containing this neighbor, including adjacent nodes from the perimeter, middle row, and middle column of the whole grid. Return the result of this recursive call.

	\end{enumerate}
	In the grid below, we search through the middle row, column, and perimeter and find that the minimum value among those cells is 2.
    \begin{table}[h]
    \centering
    \begin{tabular}{|>{\columncolor[gray]{0.8}}c|c|c|c|c|c|>{\columncolor[gray]{0.8}}c|}
        \hline
		14&\cellcolor[gray]{0.8}40&\cellcolor[gray]{0.8}5&\cellcolor[gray]{0.8}28&\cellcolor[gray]{0.8}20&\cellcolor[gray]{0.8}3&30\\\hline
        33&24&10&\cellcolor[gray]{0.8}46&4&26&16\\\hline
        47&12&19&\cellcolor[gray]{0.8}31&35&1&44\\\hline
        6&\cellcolor[gray]{0.8}39&\cellcolor[gray]{0.8}11&\cellcolor[gray]{0.8}27&\cellcolor[gray]{0.8}42&\cellcolor[gray]{0.4}2&37\\\hline
        32&36&13&\cellcolor[gray]{0.8}25&18&49&15\\\hline
        41&22&48&\cellcolor[gray]{0.8}45&9&29&21\\\hline
        23&\cellcolor[gray]{0.8}7&\cellcolor[gray]{0.8}43&\cellcolor[gray]{0.8}8&\cellcolor[gray]{0.8}34&\cellcolor[gray]{0.8}17&38\\\hline
    \end{tabular}
	\end{table}
    
    If 2 were a local minimum, we would be done. In this case, the cell with value 2 has a smaller neighbor— namely, 1—so we recurse on the quadrant of the grid containing 1, including the gray cells from the original grid.
	
    \begin{table}[h]
   	\centering
    \begin{tabular}{|>{\columncolor[gray]{0.8}}c|c|c|>{\columncolor[gray]{0.8}}c|}
    \hline
    28&\cellcolor[gray]{0.8}20&\cellcolor[gray]{0.8}3&30\\\hline
    46&4&26&16\\\hline
    31&35&1&44\\\hline
    27&\cellcolor[gray]{0.8}42&\cellcolor[gray]{0.8}2&37\\\hline
    
    \end{tabular}
    \end{table}
	

\item
	One way to prove the correctness of this algorithm is induction. For n $ \le 3$, every grid node is along the perimeter or in the middle row or column. The algorithm therefore checks every grid node and finds the global minimum. The global minimum of a grid is necessarily a local minimum.  
    \vspace{5mm}

    Now assume inductively that the algorithm works for any grid of dimension less than $n$. We must prove that the algorithm also works for a grid of dimension $n$, where $n \ge 4$. Let $G$ be our grid, and let $G'$ be the subgrid in the first recursive call. Since $G'$ has dimension less than $n$, we know by our hypothesis that the algorithm correctly finds a local minimum of $G'$ . If this local minimum is also a local minimum in $G$, then we are done.
    \vspace{5mm}
    
    We can only run into trouble if the local minimum we find in $G'$ is not a local minimum of $G$. We will now prove that this cannot happen. Let $u$ be the local minimum found in $G'$ , and suppose by way of contradiction that $u$ is not a local minimum of $G$. This means that $u$ must have a lesser-valued neighbor in $G$ which is not in $G'$ , i.e., $u$ must be along the perimeter of $G'$ . Every perimeter node of $G'$ was one of the perimeter or middle row/column nodes in $G$, so $u$ was one of the nodes checked in step (i) when the algorithm was run on $G$. Note that u could not have been the minimum value node found in step (i); if it had been, then we would have moved to one of its lesser-valued neighbors and recursed on that quadrant of $G$. Since u has no lesser-value neighbors in $G'$ , our recursive call would have been performed on another quadrant instead of $G'$ . Therefore, when performing step (i) with $G$, we found another node $v$ with value less than $u$.
    \vspace{5mm}
    
    We now proceed by making two observations. First, $u$ must be present in the subgrid for each recursive call. Otherwise, it will be eliminated from consideration somewhere along the way and cannot be returned at the end of the algorithm. Second, the values of the minima found in step (i) will not increase as we descend through the recursive calls. The minimum node found in step (i) lies along the perimeter of the subgrid on which we recurse, so when we perform step (i) in the recursive call, we will either find that that node is still the minimum or else we will find a node with an even smaller value. In either case, the value of the step (i) minimum does not increase.
    \vspace{5mm}
    
    These observations lead us to a contradiction once the recursive calls of the algorithm reach one of our base cases. From the first observation, we know that $u$ is present in this final subgrid. From the second observation, we know that the minimum node value in this grid is at most the same as the value of $v$, which itself is less than the value of $u$. Thus, $u$ does not have the minimum value in this final subgrid. The algorithm will return the global minimum of this subgrid, which will not be $u$. But this means that the algorithm does not return $u$, and so we obtain a contradiction. This completes the proof of correctness.
    
\item
	The number of nodes along the grid perimeter is $4n - 4$, and the number of additional nodes along the middle row(s) and column(s) is bounded by $4n$. (When $n$ is odd, the number of middle row/column nodes is instead about $2n$.) This means that the number of values we must check in step (i) is bounded by $8n-4$. The minimum value node we find in this step has at most 4 neighbors, and we must check at most all of them. We conclude that the number of nodes checked in steps (i) through (iii) is bounded by $(8n - 4) + 4 = 8n$.
    \vspace{5mm}
    
    Roughly speaking, the dimension of the grid is halved with each recursive call. Using the above reasoning repeatedly and tracing through the algorithm’s recursive calls, we compute that the total work for the algorithm is roughly $8[n + (n/2) + (n/4) + . . . ]$. This is bounded above by $16n$ and is therefore $O(n)$. This leads us to guess that $T(n) \le cn$ for some constant $c$.
    \vspace{5mm}
    
    If $n$ is odd, the dimension of the subgrid is $\frac{n+1}{2}$ ; else, if $n$ is even, then the dimension of the subgrid is exactly $\frac{n}{2}$ . In either case, the subgrid in the recursive call has dimension at most $\frac{n+1}{2}$. This means that a recurrence for this algorithm’s runtime is
    $$T(n)\le T(\frac{n+1}{2})+8n$$
    Substituting our runtime guess into the recurrence, we can find a constant $c$ for which $T(n) \le cn$:
	$$T(n) \le T(\frac{n+1}{2})+8n$$
    $$\le c~(\frac{n+1}{2})+8n$$
    $$\le (\frac{c}{2}+8)n+\frac{c}{2}$$
    To obtain $T(n)\le cn$, we therefor want 
    $$(\frac{c}{2}+8)n+\frac{c}{2}\le cn$$
    There are many values of $c$ which satisfy this inequality. We only need to show that one such $c$ exists, and we can afford to be lazy in our selection. For example, $c = 30$ satisfies the inequality for any $n \ge 4$. This is sufficient, since the recurrence only applies for $n \ge 4$ (the values $n \le 3$ are our base cases). It is easy to verify that $T(n) \le 30n$ for $n = 1, 2, 3$ too, and so we conclude that $T(n) = O(n)$.

\end{enumerate}

\end{enumerate}

\end{document}