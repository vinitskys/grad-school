%
\documentclass{article}
\usepackage{fullpage}

\usepackage{epsfig}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amstext}
\usepackage{amscd}
\usepackage{amsmath}
\usepackage{theorem}
\usepackage{times}
\usepackage{graphicx}
\usepackage{alltt}
\newtheorem{claim}{Claim}
\newtheorem{proof}{proof}

\usepackage{algorithmicx, algpseudocode}
\begin{document}

%\thispagestyle{empty}

\noindent
\fbox{
\parbox{\textwidth}{
\begin{Large}
{\bf CS 577: Introduction to Algorithms\hfill Discussion Solutions for Week 1\\}
\end{Large}

Updated:\vspace{0.02in}
\today
\hfill {\bf} Scribe: Gautam Prakriya
}}

\section*{Problem 1}
The algorithm for this problem is a modification of Dijkstra's algorithm. For each vertex $v$, besides keeping track of the tentative distance $d(v)$ from the source $s$, we also keep  track of the tentative number of edges $\ell(v)$ in the best path from the source. 

As in Dijkstra's algorithm we use a priority queue, the only difference is that the key of each vertex $v$ is now a pair of values $(d(v),\ell(v))$. 
Comparison between keys is defined as follows: 
$(d(v), \ell(v)) < (d(u),\ell(u))$ iff either $d(v) < d(u)$, or $d(v) = d(u)$, and $\ell(v) < \ell(u)$. 

The details of the algorithm are provided below:
\begin{algorithmic} 
\State Initialize $d(s):= 0$, $\ell(s):= 0$. 
\State For each vertex
$v \neq s$, set $d(v):=\infty$, $\ell(v):= \infty$. 
\State For each vertex $v$, set $\textrm{prev}(v):= \textrm{Null}$.
\State Initialize priority queue $Q$ by inserting 
each vertex  $v$ with key $(d(v),\ell(v))$. 
\While{$Q$ is not empty} 
    \State $u \leftarrow$ Get($Q$) 
    \For{each edge $(u,v)$}
       \If{($d_(v) > d(u) + w(u,v)$) OR ($d(v) = d(u) + w(u,v)$ AND $\ell(v) > \ell(u) + 1$)}
       \State $\textrm{prev}(v):= u$.
       \State Update key of $v$ in $Q$ as follows:
       \State $d(v):= d(u) + w(u,v)$
       \State $\ell(v) := \ell(u) + 1$
       \EndIf
        \EndFor
    \EndWhile 
\end{algorithmic}            


The proof of correctness and running time analysis are identical to Dijkstra's algorithm, and we leave these details as an exercise. 


\section*{Problem 2}
The key insight for this problem is the following: The bottleneck distance between a pair of vertices $s,t$ in a connected graph $G$ is equal to 
the bottleneck distance between $s$ and $t$ in any minimum spanning tree of $G$. 

This immediately gives us an algorithm for the problem because the bottleneck distances between a pair of vertices $u,v$ in a tree
is just the maximum weight edge in the unique path from $u$ to $v$.
The only caveat is that the input graph $G$ may be disconnected. In this case, we find an MST for each connected component of $G$. In fact Kruskal's algorithm does indeed return a forest made up of the minimum spanning trees for each connected component (do you see why?)

We now spell out the algorithm for the problem. 

\begin{itemize}
    \item[Input:] An undirected graph $G$ with edge weights. 
    \item[Output:] The bottleneck distance $b(u,v)$ for every
    pair of verices $u,v$ in $G$. 
     
    \item[(1)] Use Kruskal's algorithm to find a forest $F$ made up of minimum spanning trees of each connected component of $G$. 
    \item[(2)] Initialize $b(u,v) = \infty$ for every pair $(u,v)$ of vertices. 
    \item[(3)] For each vertex $u$, run  BFS on $F$ with root $u$. 
    \begin{itemize}
                   \item[] Each time a new vertex $v$ is visited, update $b(u,v)$ using the relation $b(u,v) = \max(b(u,p), w(p,v))$, where $p$ is the parent of $v$ and $w(p,v)$ is the weight of the edge $(p,v)$.
    \end{itemize}
   \end{itemize}
   
\paragraph{Proof of correctness:} We leave it as an exercise to show that step (3) correctly
computes the bottleneck distances in a forest. 
All that is left to prove is the following statement: 

\begin{claim} Let $G = (V,E)$ be a connected graph. Let $T$ be a MST of $G$.  
For every pair of vertices $u,v$, the bottleneck distance of  $u,v$  in $T$ is equal to to the bottleneck distance of $u,v$ in $G$.
\end{claim}
\textbf{Proof: } 
Let $b_T(u,v)$ denote  the bottleneck distance of a pair of vertices $u,v$ in $T$, and $b_G(u,v)$ denote the bottleneck distance of $u,v$ in $G$. 

The proof follows from the  cut property of MSTs:-
For any partition $S,\overline{S}$ of the vertex set $V$, 
every minimum spanning tree of $G$  contains a min-weight edge
from the set $E(S,\bar{S})$ - the set of edges with one endpoint in $S$ and the other in $\overline{S}$.

Now, suppose there is a pair of vertices $u,v$ such that
$b_T(u,v) > b_G(u,v)$. We employ the cut property to arrive at a contradiction. Let $P$ be the $u,v$ path in $T$.
Let $e$ denote the max-weight edge in $P$.
The weight of $e$, $w(e) = b_T(u,v)$.
Deleting $e$ from $T$ partitions the vertex set into two 
connected components $S, \overline{S}$ one containing $u$ and the other containing $v$. 

Let $P^\prime$ denote the $u$-$v$ path in $G$ that realizes the bottleneck distance $b_G(u,v)$. That is every edge in $P^\prime$ has weight at most $b_G(u,v)$. 
$P^\prime$ must contain an edge that crosses the partition $S,\overline{S}$. That is there is an edge $e^\prime$ in $P\prime$  that lies in $E(S,\overline{S})$. 

By the above, we have the following sequence of inequalities. 
$$w(e) = b_T(u,v) > b_G(u,v) \geq w(e^\prime).$$

Now, the only edge in the MST $T$ from $E(S,\overline{S})$ is $e$.  By the above inequalities $e$ is not a  min-weight edge of 
$E(S,\overline{S})$. This is contradicts the cut property. 



\paragraph{Running Time:} Kruskal's algorithm can be implemented in $O(|E| \log |V|)$ time. For each $u$, the 
BFS takes $O(|V| + |E|) = O(|V|)$ time ($F$ is a forest). Therefore the entire algorithm takes $O(|V|^2 + |E| \log |V|)$ time.

 





\end{document}
