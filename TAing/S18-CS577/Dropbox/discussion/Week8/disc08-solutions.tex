%
\documentclass{article}
\usepackage{fullpage}

\usepackage{epsfig}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amstext}
\usepackage{amscd}
\usepackage{amsmath}
\usepackage{theorem}
\usepackage{times}
\usepackage{graphicx}
\usepackage{alltt}
\usepackage{enumerate}
\usepackage{enumitem}
\usepackage{algorithmicx, algpseudocode}

\begin{document}

%\thispagestyle{empty}

\noindent
\fbox{
\parbox{\textwidth}{
\begin{Large}
{\bf CS 577: Introduction to Algorithms\hfill Discussion Solutions for Week 8\\}
\end{Large}

\vspace{0.02in}
\hfill {\bf} 
}}



\subsection*{Problem 1:}
In class we saw an $O(nW)$ time algorithm for this problem, where $W$ is the capacity of the knapsack.
The algorithm was based on the following recurrence:
$$OPT(i, w) = \max \{(v_i + OPT(i-1, w - w_i)), OPT(i-1, w)\}.$$ 
Here $OPT(i,w)$ represents the maximum value we can attain with a subset of the items $\{1,\ldots, i\}$, subject to the constraint that the total weight of the subset is at most $w$.

Our goal in this problem is to design an $O(nV)$ algorithm to find the maximum attainable value of the knapsack instance, where $V$ is the sum of the values of all items. To achieve this, we use a common trick that is very useful in a
variety of situations.  
At a high level, the idea is to swap the quantity we are optimizing with the quantity that defines the constraint. 

That is, we define $OPT^\prime(i,v)$ to be the minimum weight of a subset of the items $\{1 , \ldots, i\}$, subject to the constraint that
the total value of the subset is at least $v$. Before we give a recurrence for this, let us see how it helps us solve the problem. We claim that the largest $v$ for which $OPT^\prime(n,v) \leq W$ is the optimal value  for the knapsack instance. Any smaller $v$ is clearly not optimal, while by the definition of $OPT$, it is impossible to get any larger $v$ and still satisfy the weight constraint.
\smallskip

Then we will discuss how to compute $OPT^\prime(i,v)$. Note that we can choose to either include item $i$ in the subset or to not include it. If we choose to include it, the optimal weight we can achieve is $w_i + OPT^\prime(i-1,\max(0, v-v_i))$. We are using $\max(0, v - v_i)$ here to handle the case that $v - v_i < 0$. On the other hand, if we do not include item $i$, the optimal weight we can achieve is $ OPT^\prime(i-1,v)$. This gives us the following 
recurrence: 

$$OPT^\prime(i,v) = \min [ w_i + OPT^\prime(i-1,\max(0,v-v_i)), OPT^\prime(i-1,v)].$$

We use the recurrence to build up the the solutions $OPT^\prime$ in a bottom up fashion. 
For the base case, set $OPT^\prime(0,v)$ to $0$ if $v = 0$ and to $\infty$ otherwise.
\begin{algorithmic}
\For{$i = 1 \ldots n$}
\For{$v = 0 \ldots V$}
\State $OPT^\prime(i, v) = \min [ w_i + OPT^\prime(i-1,\max(0, v-v_i)), OPT^\prime(i-1,v)]$
\EndFor
\EndFor
\end{algorithmic}

\paragraph{Running Time} Filling up the array takes $O(nV)$ time. This is because  there are $n(V+1)$ iterations, and each iteration takes constant time. Computing the largest $v$ for which $OPT^\prime(n,v) \leq W$ takes $O(V)$ time. 

\subsection*{Problem 2:}
Comparing the best choice of $S$ which contains the first investment with the best choice of $S$ which does not contain the first investment and choosing the better one is a valid way to find the best choice of $S$ overall. But the problem is that the given definition of $OPT$ does not match this strategy perfectly. Namely, choosing $\{1\} \cup OPT(2, r - 1)$ does not necessarily yield the best choice of $S$ which contains the first investment. It finds the set $S \subseteq [2, \cdots,n]$ maximizing $\frac{\sum_i p_i}{\sum_i c_i}$, whereas we want the subset maximizing $\frac{p_1 + \sum_i p_i}{c_1 + \sum_i c_i}$; however, in general, these are not equivalent.
\smallskip

Another way of thinking about this is as follows. Let $S^*$ denote the overall optimal solution with $r$ investments and suppose that this set contains investment $1$. Let $T = S^*\setminus\{ 1\}$ be all of the investments in $S^*$ except $1$. Then $T$ contains $r-1$ investments over $[2, \cdots,n]$. Let $T^*$ denote the optimal solution corresponding to $OPT(2, r - 1)$, that is, the best $r-1$ investments over $[2, \cdots,n]$. In general, $T$ is not the same as $T^*$. Although $T^*$ achieves a better ratio than $T$ (owing to its optimality over $[2, \cdots,n]$), $T^*\cup \{1\}$ might achieve a worse ratio than $T\cup\{1\} = S^*$ because of the way the ratios are defined. This is what our example below exhibits.  

Consider the following example. Let $p_1 = 100, p_2 = 100, p_3 = 1$ and $c_1 = 10, c_2 = 50, c_3 = 1$, and let $r = 2$. In this case, $OPT(2, r - 1)$ or $T^*$ is $\{2\}$. So, $\{1\} \cup OPT(2, r - 1) = \{1, 2\}$, which yields to $(100 + 100) / (10 + 50) \approx 3.33$. But $\{1, 3\}$ is the optimal answer with return ratio of $(100 + 1) / (10 + 1) \approx 9.18$, and it includes the first investment.




\subsection*{Problem 3:}
Given $n$ precincts $P_1, P_2, \cdots, P_n$, each containing $m$ registered voters, then there are $mn$ voters in total. $n$ precincts are required to be divided into 2 districts $D_1$ and $D_2$, and each district contains $\frac{n}{2}$ precincts, $|D_1| = \frac{n}{2}$ and $|D_2| = \frac{n}{2}$. Furthermore, each district has $\frac{mn}{2}$ registered voters. We can do the following discussion on either party A or party B, without loss of generality, we will focus on party A in this problem.
\smallskip

For party A, given $v_1, v_2, \cdots, v_n$ registered voters in precincts $P_1, P_2, \cdots, P_n$. We have $V = \sum_{i = 1}^nv_i$ voters for party A in total, and the numbers of voters for party A in district $D_1$ and $D_2$ are $V_{1} = \sum_{P_i \in D_1}v_i$ and $V_{2} = \sum_{P_i \in D_2}v_i$, respectively. Note that $V = V_1 + V_2$. Party A can hold a majority in both two districts, \textit{iff} $V_1 \geq \frac{mn}{4} + 1$ and $V_2 \geq \frac{mn}{4} + 1$. With $V_2 = V - V_1$, we have, 
$$V_2 = V - V_1 \geq \frac{mn}{4} + 1$$
$$\frac{mn}{4} + 1 \leq V_1 \leq V - \frac{mn}{4} - 1$$
\smallskip

The discussion above indicates that:
\begin{itemize}
  \item The total number of voters for party A should satisfy $V \geq \frac{mn}{2} + 2$, otherwise, the given set is not susceptible to gerrymandering.
  \item To guarantee that both districts have more than (or equal to) $\frac{mn}{4} + 1$ voters for party A, for district $D_1$, the number of voters for party A should satisfy $V_1 \in [\frac{mn}{4} + 1, V - \frac{mn}{4} - 1]$.
\end{itemize}

First of all, we can determine if one of the parties has greater than or equal to $\frac{mn}{2} + 2$ voters in total in linear time. Then we can check whether or not such a district $D_1$ exists by the following steps.

\smallskip

We define subproblems $G(i,k,v)$ as follows. For precincts $\{P_1, P_2, \cdots, P_i\}$, if we can find a subset of precincts $D$ that contains $k$ precincts and the number of voters for party A in this subset is $v$, we will set $G(i, k, v) = true$; otherwise it will be false. First of all, we'll explain how to obtain the final answer with $G$. Given a table of $G$, we can determine whether there exists a district $D_1$ that contains $\frac{n}{2}$ precincts, and the number of voters for party A is $\frac{mn}{4} + 1 \leq V_1 \leq V - \frac{mn}{4} - 1$ by checking all the $G(n, \frac{n}{2}, v)$, for $v \in [\frac{mn}{4} + 1, V - \frac{mn}{4} - 1]$. If there exist one such element that is true, then the given set of precincts is susceptible to gerrymandering.
\smallskip

Next, we'll see how to derive a recursive equation for $G$. Note that $G(i, k, v)$ can be true when we can find a subset of $k-1$ precincts with $v - v_i$ voters for party A among $\{P_1, P_2, \cdots, P_{i - 1}\}$, or we can find a subset of $k$ precincts with $v$ voters for part A in $\{P_1, P_2, \cdots, P_i\}$. That is, when $G(i-1,k-1,v-v_i) = true$ or $G(i-1,k,v) = true$. For the base case, when $i = 1$ and $k = 1$, $G(1, 1, v)$ is true \textit{iff} $v = v_1$. All the other cases will be false, and we can get the following recursive equations based on this observation.
\smallskip

\textbf{Recursive Equation: }

$G(i, k, v) = 
\begin{cases} 
true, \text{    if }i = 1, k  = 1, \text{and }v = v_1\\
true, \text{    if } G(i - 1, k - 1, v - v_i) \vee G(i - 1, k, v)\\
false, \text{   otherwise}
 \end{cases}
$

Where $i \in [1, n]$, $k \in [1, \frac{n}{2}]$ and $v \in [\frac{mn}{4} + 1, V - \frac{mn}{4} - 1]$. And note that $\vee$ is a logical OR ( $\mid\mid$ in most programming languages).
\smallskip

\textbf{Evaluation Order:}
Suppose that we are using a 3-dimensional table/array to represent $G(i, k, v)$, then the array can be filled in the increasing order of $i$, and any order for $k$ and $v$.
\smallskip

\textbf{Running Time:}
It takes $O(n)$ time to determine whether the total number of party A (or B) is larger than or equal to $\frac{mn}{2} + 2$. For the recursive equation, each subproblem ($G(i, k, v)$) can be solved in constant time. For the number of subproblems, we have $i \in [1, \cdots n]$, $k \in [1, \cdots, n/2]$ and $v \in [\frac{mn}{4} + 1, V - \frac{mn}{4} - 1]$; thus, it is bounded by $n \cdot \frac{n}{2} \cdot O(V - \frac{mn}{2})$, which leads to the running time of $O(n^3m)$. For the final step to determine the solution, it costs $O(nm)$ to check one single row where $i = n$ and $k = \frac{n}{2}$. Thus, the total running time of the algorithm is $O(n^3m)$.


\end{document}
