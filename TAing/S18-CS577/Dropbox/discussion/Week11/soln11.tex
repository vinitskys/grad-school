\documentclass{article}
\usepackage{fullpage}
\usepackage[linesnumbered,lined,boxed,commentsnumbered,ruled,vlined]{algorithm2e}
\usepackage{epsfig}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amstext}
\usepackage{amscd}
\usepackage{amsmath}
\usepackage{times}
\usepackage{graphicx}
\usepackage{alltt}
\usepackage{amsthm}

\newtheorem{lemma}{Lemma}
\newtheorem{claim}{Claim}

\begin{document}

%\thispagestyle{empty}

\noindent
\fbox{
\parbox{\textwidth}{
\begin{Large}
{\bf CS 577: Introduction to Algorithms\hfill Discussion Problems for Week 11}
\end{Large}\\
}}

\section*{Problem 1}  

We start with the following claim:
\begin{claim}\label{claim:p1}
For any non-zero vector $x\in\{0,1\}^k$, $\Pr{}_M[Mx=0]=1/2^\ell$,
where $M$ is a uniformly random $\{0,1\}$ matrix, and arithmetic operations are
done modulo 2.
\end{claim}
Given the claim, it follows for any two different vectors $x,y\in\{0,1\}^k$,
the probability that $x$ and $y$ hash to the same value is
\begin{align*}
\Pr[h_M(x)=h_M(y)]=\Pr[Mx=My]=\Pr[M(x-y)=0]=1/2^\ell=1/m.
\end{align*}
By definition, this family is a universal family of hash functions.

We now prove the claim:
\begin{proof}[Proof of Claim~\ref{claim:p1}]
Note that it is equivalent to independently sampling each element in $M$ from
$\{0,1\}$ with equal probability $1/2$. Since $x\ne 0$, there exists a position
$1\le j\le k$ such that $x_j=1$. In this way, for any $1\le i\le\ell$ and fixed
$M_{i,1},\ldots ,M_{i,j-1},M_{i,j+1},\ldots,M_{i,k}$, there is exactly one
choice for $M_{i,j}$ to make the $i$-th element of $Mx$ be 0, i.e,
\begin{align*}
M_{i,j}=-\sum\limits_{j'\ne j}{M_{i,j'}x_{j'}},
\end{align*}
where the operation is performed modulo $2$. Thus we have $\Pr[(Mx)_i=0]=1/2$. Hence,
\begin{align*}
\Pr[Mx=0]=\Pr\left[\bigwedge\limits_{i=1}^{\ell}{\{(Mx)_i=0\}}\right]=\prod\limits_{i=1}^{\ell}{\Pr[(Mx)_i=0]}=1/2^\ell.
\end{align*}
\end{proof}

%\newpage
\section*{Problem 2}
A key observation is that the presence of a directed cycle in the final residual network suggests that there was an alternate way to push through a maximum amount of flow. More specifically, the maximum flow through a network is unique if and only if the final residual network has no directed cycle with positive residual capacity. This leads to the following algorithm, which takes as input a flow network $(G,c,s,t)$ with source $s$ and sink $t$:

\begin{algorithm}
\SetKwInOut{Input}{Input}
\Input{A flow network $(G,c,s,t)$ with $G=(V,E)$ acyclic.}
Find a maximum flow $f^*$ and the final residual network $G_{f^*}$\;
Ensure any zero-capacity edges are removed from $G_{f^*}$\;
Run a cycle-detection algorithm to see if there is a directed cycle in $G_{f^*}$\;
%\ForEach {$e=(u,v)\in E$ with $f^*_e>0$}{
%Run a BFS from $u$ to determine whether $G_{f^*}\setminus\{e\}$ has a $u\to v$ path\;
\If{a cycle exists}{\Return{$\mathsf{FALSE}$} (max flow is not unique)\;}
%}
\Return{$\mathsf{TRUE}$} (max flow is unique).
\caption{}
\end{algorithm}
In Step 1, there exists an $O(|V||E|)$ time deterministic algorithm to find the
maximum flow.
Step 2 can be implemented in $O(|V| + |E|)$ time with DFS.
(cf. solutions to problem 1 in week 4.)
Thus the overall running time is $O(|V||E|)$.

To prove the correctness of the algorithm, it suffices to prove the following lemma.
\begin{lemma}
Let $f^*$ be a maximum flow of the network $(G,c,s,t)$.
Then $G$ has multiple distinct maximum flows if and only if its final residual
network $G_{f^*}$ has a directed cycle with positive residual capacity.
\begin{proof} \ 
\begin{itemize}
\item[\underline{$\Leftarrow$}]
	Let $C$ be a cycle in $G_{f^*}$ with positive residual capacity.
	Since $C$ has positive residual capacity,
	we can, starting from $f^*$, push positive flow around $C$,
	to get a different flow $f$ which is still a feasible flow in $G$.
	Moreover, since pushing flow in a cycle has no effect on the net flow leaving $s$,
	$f$ has the same value as $f^*$; $f$ is thus a maximum flow.
	Since $f$ is distinct from $f^*$, it follows that $f^*$ is not unique.

\item[\underline{$\Rightarrow$}]
	Let $f$ be a maximum flow distinct from $f^*$.
	To construct the cycle in $G_{f^*}$, we study how $f^*$ differs from $f$.
	Let $\Delta \doteq f - f^*$ be the function from edges to numbers given by
	$\Delta(e) = f(e) - f^*(e)$.
	We claim the following:
	\begin{claim} \label{claim:p2}
	For any pair of vertices $u,v \in V$ with an edge $e = (u,v) \in E$,
	\begin{itemize}
	\item
		If $\Delta(e) > 0$, then the edge $e$ in $G_{f^*}$ has positive residual
		capacity.
	\item
		If $\Delta(e) < 0$, then $e$'s ``back'' edge in $G_{f^*}$ (from $v$ to $u$)
		has positive residual capacity.
	\end{itemize}
	\end{claim}
	\begin{proof}
		If $\Delta(e) = f(e) - f^*(e) > 0$, then since the capacity on $e$, $c(e)$,
		is at least $f(e)$, it follows that $c(e) > f^*(e)$.
		Thus there is positive residual capacity along $e$ in $G_{f^*}$.
		If instead $\Delta(e) = f(e) - f^*(e) < 0$, then in particular $f^*(e) > 0$,
		so there is positive residual capacity in the backward direction along $e$
		in $G_{f^*}$.
	\end{proof}

	Now observe that, since $f^*$ and $f$ both satisfy flow conservation at every
	vertex $v \in V\setminus\{s,t\}$, it follows that $\Delta$ does as well.
	(That is, the sum of $\Delta(e)$ over edges $e$ entering $v$ is equal to the
	sum of $\Delta(e)$ over edges $e$ leaving $v$.)
	Moreover, since $f^*$ and $f$ have the same value, we have flow conservation
	at $s$ and $t$ as well. The calculation at $t$ is below; the one for $s$ is
	similar.
	\begin{align*}
		\sum_{e\text{ entering }t} \Delta(e) - \sum_{e\text{ exiting }t} \Delta(e)
		&= \sum_{e\text{ entering }t} (f^*(e) - f(e)) - \sum_{e\text{ exiting }t} (f^*(e) - f(e)) \\
		&= \sum_{e\text{ entering }t} f^*(e) - \sum_{e\text{ entering }t} f(e) - \sum_{e\text{ exiting }t} f^*(e) + \sum_{e\text{ exiting }t} f(e) \\
		&= \left(\sum_{e\text{ entering }t} f^*(e) - \sum_{e\text{ exiting }t} f^*(e)\right) - \left(\sum_{e\text{ entering }t} f(e) - \sum_{e\text{ exiting }t} f(e) \right) \\
		&= |f^*| - |f| = 0
	\end{align*}

	We can now construct a cycle with positive residual capacity in $G_{f^*}$ as
	follows.
	We will construct an infinite sequence $w_0, w_1, \ldots$ of vertices such
	that for every $i$, there is an edge from $w_i$ to to $w_{i+1}$ in $G_{f^*}$
	with positive residual capacity.
	Since an infinite sequence of vertices must eventually repeat a vertex,
	there must be a cycle in $G_{f^*}$ with positive residual capacity.

	To construct the sequence, first note that since $f^* \ne f$,
	there is some edge $e_0 \in E$ with $\Delta(e_0) \ne 0$.
	It follows that there is a vertex $w_0$ (the head or tail of $e_0$) such that
	either
	(1) some edge $e \in E$ leaving $w_0$ has $\Delta(e) > 0$, or else
	(2) some edge $e \in E$ entering $w_0$ has $\Delta(e) < 0$.
	That (1) or (2) holds will let us find $w_1$.
	Moreover, $w_1$ will satisfy (1) or (2), which lets us find $w_2$.
	Continuing in this way, we find $w_3$, then $w_4$, and so on.

	Thus, for each $i = 0,1,\ldots$, suppose we have found $w_i$ satisfying (1) or (2).
	The conservation law for $\Delta$ implies that there is another edge $e'$
	either entering $w_i$ with $\Delta(e') < 0$,
	or else exiting $w_i$ with $\Delta(e') > 0$.
	Either way, by Claim~\ref{claim:p2}, we get an edge leaving $w_i$ in
	$G_{f^*}$ with positive residual capacity.
	We set $w_{i+1}$ to be the opposite endpoint of this edge.
	Note that the existence of $e'$ shows that $w_{i+1}$ satisfies (1) or (2).
	\qedhere
\end{itemize}
\end{proof}
\end{lemma}

\end{document}
