%
\documentclass{article}
\usepackage{fullpage}

\usepackage{epsfig}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amstext}
\usepackage{amscd}
\usepackage{amsmath}
\usepackage{amsthm} % new (see below)
%\usepackage{theorem}
\usepackage{times}
\usepackage{graphicx}
\usepackage{alltt}

% The following were added specific to this solutions document
% Also, `theorem' was replaced by `amsthm' above.
\usepackage{mdframed}
\newtheorem{claim}{Claim}

\begin{document}

%\thispagestyle{empty}

\noindent
\fbox{
\parbox{\textwidth}{
\begin{Large}
{\bf CS 577: Introduction to Algorithms\hfill Discussion Solutions for Week 2\\}
\end{Large}

\vspace{0.02in}
\hfill {\bf} Scribe: Andrew Morgan
}}

\subsection*{Solutions}

\begin{enumerate}
\item
	Let $A[1\ldots n]$ and $B[1\ldots n]$ be the two sorted input lists, and $k$
	the given index.
	The goal is to find the $k$-th smallest number in the union of $A$ and $B$
	(denoted $A\cup B$).
	We focus on the case where $k = n$ is a power of 2
	(\textit{i.e.}, finding the median of the union of $A$ and $B$ when we can
	always split them into two equal halves).
	A reduction from the general case follows this.

	\paragraph{Algorithm}
	We now describe how to find the $n$-th smallest element among
	$A[1\ldots n] \cup B[1\ldots n]$ where we moreover assume $n$ is a power of $2$.
	The algorithm works by comparing the median of $A$ with the median of
	$B$---either way the comparison resolves, we are able to eliminate half of
	$A$ and half of $B$ from the search.
	This gives rise to the following recursive function, named
	$median(A[1\ldots n], B[1\ldots n])$.
	\begin{enumerate}
	\item If $n = 1$, then return whichever of $A[1]$ or $B[1]$ is smaller.
	\item Otherwise ($n > 1$), in which case do the following:
	\begin{enumerate}
		\item Set $a \gets A[n/2]$ and $b \gets B[n/2]$.
		\item If $a > b$, then compute and return $median(A[1\ldots n/2], B[n/2+1\ldots n])$.
		\item If $a < b$, then compute and return $median(A[n/2+1\ldots n], B[1\ldots n/2])$.
	\end{enumerate}
	\end{enumerate}
	Note that $median$ is only defined when $A$ and $B$ have the same length which is
	moreover a power of $2$, so
	we need to ensure that the recursive calls in steps (b.ii) and (b.iii) meet
	that guarantee.
	And indeed, the ranges $1\ldots n/2$ and $n/2+1 \ldots n$ both contain $n/2$
	indices.

	\paragraph{Correctness}
	We claim that for each $n$, for all input arrays $A$ and $B$ of length $n$,
	$median(A,B)$ returns the $n$-th smallest element of $A \cup B$.
	We argue this claim by induction on $n$.
	\begin{description}
	\item[Base case]
		In the case where $n = 1$, the smallest of $A[1\ldots 1] \cup B[1\ldots 1]$
		is the smaller of $A[1]$ and $B[1]$, so the algorithm is correct.
	\item[Inductive step]
		Let $A$ and $B$ be two sorted lists of length $n > 1$.
		Let $a = A[n/2]$ and $b = B[n/2]$ be as in the algorithm.
		We consider only the case $a > b$;
		the case where $a < b$ has the same argument with $A$ and $B$ swapped.
		We make the following claim:
		\begin{claim}\label{claim:1}
			Each of the numbers in $A[n/2+1\ldots n]$ are larger than at least $n$
			numbers in $A\cup B$, and also each of the numbers in $B[1\ldots n/2]$
			are smaller than at least $n+1$ numbers in $A\cup B$.
		\end{claim}
		\begin{proof}
			Each of the numbers in $A[n/2+1\ldots n]$ is larger than all the numbers
			in $A[1\ldots n/2]$.
			This gives us $n/2$ of the claimed $n$ numbers.
			Moreover, since $a > b$, each number in $A[n/2+1\ldots n]$ is larger than
			$b$, which is at least each of the numbers in $B[1\ldots n/2]$.
			Thus each number in $A[n/2+1\ldots n]$ is larger than all the numbers in
			$B[1\ldots n/2]$.
			This gives us the remaining $n/2$ of the claimed numbers.
			Showing that the numbers in $B[1\ldots n/2]$ are less than some $n+1$
			numbers follows a similar argument:
			they are all less than all the numbers in $B[n/2+1\ldots n]$ as well as
			all the numbers in $A[n/2\ldots n]$ since $B[n/2] = b < a = A[n/2]$.
		\end{proof}
		Given the claim, we now conclude that the $n$-th smallest number in
		$A\cup B$ cannot lie in $A[n/2+1\ldots n]\cup B[1\ldots n/2]$, since it is
		larger than precisely $n-1 < n$ other numbers, and smaller than precisely
		$n < n+1$ other numbers.
		Thus the $n$-th smallest number in $A\cup B$ must lie in $A[1\ldots n/2] \cup B[n/2+1\ldots n]$.
		Moreover, it must be the $n/2$-th smallest number in
		$A[1\ldots n/2] \cup B[n/2+1\ldots n]$ since we have eliminated exactly $n/2$
		numbers smaller than the $n$-th.
		By the inductive hypothesis applied at length $n/2$ (which is less than $n$),
		the recursive call to $median$ returns the answer we are looking for.
		So the algorithm is correct.
	\end{description}

	\paragraph{Running Time Analysis}
	The algorithm as defined copies entire arrays around, which would be
	prohibitively expensive.
	However, since all the arrays involved are contiguous subarrays of $A$ or
	$B$, we can just pass around pointers to the beginning and end of the
	subarrays and avoid copying around arrays.\footnote{%
		The notion of a `slice' as in C++, Go, and Rust (and possibly many other
		languages) provides a useful abstraction for this trick.
	}
	This entails some straightforward (if quite tedious) modification to the
	calculations of indices in the algorithm. We omit the details.

	With the above modification in place,
	the algorithm does $O(1)$ work outside the work done in recursive calls,
	makes one recursive call on arrays that are half the size.
	This gives the recurrence $T(n) = T(n/2) + O(1)$ with base case $T(1) = O(1)$.
	This solves to $T(n) = O(\log n)$.

	\paragraph{Reduction to the case where $k = n$ is a power of $2$}
	We first reduce to the case where $k \le n$, then to the case where $k = n$,
	and then finally to the case where $k = n$ are both a power of $2$.

	\begin{mdframed}
	\textsc{Remark.}
	These reductions are thought of in terms of modifying $A$ and $B$,
	but the literal modifications would be too expensive to implement directly.
	Instead, the modifications are made to how $A$ and $B$ are \emph{accessed}.
	In programming terms, one can imagine implementing the above algorithm such
	that it only accesses $A$ and $B$ through some abstract interface.
	The interface takes in indices $i$ and $j$, and returns whether
	$A[i] < B[j]$.
	Then the following modifications provide an implementation of this interface
	that makes the algorithm work even for general $k$ and $n$.

	Consequently the algorithm for the general case is to (i) set up the
	interface for accessing $A$ and $B$ and update $k$ and $n$ according to the
	following instructions, and then (ii) run the algorithm above with the
	updated values of $k$ and $n$.
	The modifications are chosen such that the end result after the modifications
	will still be the solution to the original problem.
	\end{mdframed}

	In the case where $k > n$, we reverse $A$ and $B$ and the results of all
	comparisons between numbers, and then replace $k$ by $2n - k + 1$.
	A number is the $i$-th smallest if and only if there are exactly $i-1$
	smaller and $2n-i$ larger numbers,
	so reversing all comparisons transforms the $k$-th smallest into the
	$(2n-k+1)$-th smallest.
	When $k > n$, $2n - k + 1 \le n$, so we have indeed reduced to the case of $k \le n$.
	
	To reduce to the case of $k = n$ from $k \le n$,
	note that the $k$-th smallest element of $A \cup B$ must occur among
	$A[1\ldots k] \cup B[1\ldots k]$:
	every other number in $A$ and $B$ is larger than $k$ other numbers, so cannot
	be the $k$-th smallest.
	Moreover, the $k$-th largest element in $A[1\ldots n]\cup B[1\ldots n]$
	is still the $k$-th largest element in $A[1\ldots k]\cup B[1\ldots k]$
	as the $k-1$ numbers smaller than it are still present in the latter set.
	Thus we can just focus on finding the $k$-th largest number in
	$A[1\ldots k] \cup B[1\ldots k]$,
	\textit{i.e.}, just update $n$ to be $n = k$.

	Finally, to get to the case where $k = n$ is a power 2,
	let $n'$ be the first power of 2 at least $n$.
	Select $n' - n$ numbers smaller than $\min(A[1],B[1])$ and prepend them (in order)
	to the front of $A$,
	and similarly add $n' - n$ numbers larger than $\max(A[n],B[n])$ to the end of $B$.
	This makes $A$ and $B$ have length $n'$,
	and the $n$-th smallest element before the transformation becomes the $n'$-th
	smallest after.

	The transformations done in all these reductions have the combined effect of
	blowing up $n$ by at most a factor of two, and blowing up the cost of each
	query to $A$ and $B$ by a factor of $O(1)$.
	Therefore the running time for the general case is $O(1)\cdot O(\log(2n)) = O(\log n)$.

\item
\begin{enumerate}
\item
	Since the elements in a cityscape are arranged in order from left to right, a
	natural thought is to merge two cityscapes from left to right using a similar
	idea to the merging process of \textit{mergesort}.
	As we do this, we can use the height of whichever skyline is taller as the
	height of the combined skyline.
	Thus, during the merge we will have the following state:
	\begin{itemize}
	\item $i_A$, the index of the `current' tuple in $A$
	\item $i_B$, the index of the	`current' tuple in $B$
	\item For the first cityscape, its current height $height_A$
	\item For the second cityscape, its current height $height_B$
	\end{itemize}

	\paragraph{Algorithm}
	A precise description of the algorithm follows.
	It takes as input a cityscape $A$ and cityscape $B$, and outputs a new
	cityscape $S$.
	\begin{enumerate}
	\item
		Initialize $S$ to be the empty cityscape.
		Initialize $height_A$ and $height_B$ to be 0,
		and $i_A$ and $i_B$ to point to the first elements of $A$ and $B$ respectively.
	\item
		Repeat the following until both $i_A$ and $i_B$ point past the end of $A$ and $B$:
		\begin{enumerate}
		\item
			Set $next\_event \gets \min( A[i_A].x, B[i_B].x )$.\\
			(If $i_A$ points past the end of $A$, treat $A[i_A].x$ as $+\infty$, and similar wrt $B$.)
		\item
			If $A[i_A].x = next\_event$, then update $height_A \gets A[i_A].y$. \\
			If $B[i_B].x = next\_event$, then update $height_B \gets B[i_B].y$.
		\item
			Append $(next\_event, \max(height_A, height_B))$ to $S$.
		\item
			If $A[i_A].x = next\_event$, then increment $i_A$. \\
			If $B[i_B].x = next\_event$, then increment $i_B$.
		\end{enumerate}
	\item
		Finally, run through $S$ and remove adjacent pairs $(x_1,y_1)$, $(x_2, y_2)$ where
		$y_1 = y_2$.
	\end{enumerate}

	\paragraph{Correctness}
	To show the above algorithm is correct, we appeal to the correctness of the
	merge subroutine in mergesort to conclude that $S$ is indeed a skyline,
	\textit{i.e.}, it consists of a sequence of tuples $(x,y)$ where the
	$x$-values appear in increasing order.
	It remains to show that $S$ is the correct skyline,
	which is to say that at any point $t$, the height of the skyline represented
	by $S$ at position $t$
	equals the maximum of the heights of the skylines for $A$ and $B$ at position
	$t$.
	We note that it suffices to show that this holds with respect to the value of $S$
	prior to step (iii), since it defines the same skyline as $S$ after step (iii).

	To argue that,
	fix $t$ and let $(x_S, y_S)$ be the tuple in $S$ for which $x_S$ is largest
	subject to $x_S \le t$.
	The skyline defined by $S$ has height $y_S$ at position $t$.

	When $(x_S, y_S)$ was added to $S$ in some iteration of step (ii),
	we had $y_S = \max( height_A, height_B )$ for some values of $height_A$ and $height_B$.
	Specifically, $height_A$ was the value of $y_A$ in the tuple $(x_A, y_A)$ in
	$A$ for which $x_A$ is largest subject to $x_A \le x_S$;
	$height_B$ was similarly set in terms of $B$.

	Now, for every $x$-coordinate of a tuple in $A$, there is a tuple with the same
	$x$-coordinate in $S$.
	Thus, since $x_S$ is maximal subject to $x_S \le t$,
	every $x$-coordinate of a tuple in $A$ is either at most $x_S$ or larger than $t$.
	It follows that $height_A$ is indeed the actual height of the skyline defined by $A$
	at position $t$.
	Similarly, $height_B$ really is the height of the skyline described by $B$ at
	position $t$.

	Therefore $y_S = \max(height_A, height_B)$ is indeed the correct height for
	the skyline defined by $S$ at position $t$.

	\paragraph{Running Time}
	If $A$ has $n_1$ elements and $B$ has $n_2$ elements,
	we can bound the running time by $O(n_1 + n_2)$ by a similar argument as for
	the merge subroutine of mergesort.
	Step (i) takes $O(1)$ time.
	Each iteration of the loop in step (ii) takes $O(1)$ time, and it runs at most once
	for each element of $A$ and $B$.
	Thus it takes $O(n_1 + n_2)$ time in total.
	Step (iii) takes time proportional to the size of $S$,
	\textit{i.e.}, $O(n_1 + n_2)$ time.
	The total running time of the algorithm is the sum of these three quantities,
	which is $O(n_1 + n_2)$.

\item
	We use divide and conquer (and combine) to solve this problem, with the
	algorithm from part (a) serving as the ``combine'' step in this process.
	The algorithm looks very similar to mergesort.

	\paragraph{Algorithm}
	We devise the following algorithm $Find\_Cityscape(B)$ where $B$ is a list of
	buildings.
\begin{enumerate}
\item
	For the base case where there is only one building in $B$ ($|B| = 1$), we can
	get the cityscape directly.
	It is just the coordinates of its top-left and bottom-right corners.
	This takes $O(1)$ time.
\item
	In the case where there is more than one building in $B$ ($|B| > 1$),
	divide the buildings in $B$ into two lists $B_1$ and $B_2$ of roughly equal
	size.
	Compute $Find\_Cityscape(B_1)$ and $Find\_Cityscape(B_2)$.
	Merge the two cityscapes obtained by using the algorithm described in part
	(a) and return the result.
	Merging takes linear time.
\end{enumerate}

	\paragraph{Correctness}
	The correctness of the above algorithm follows analogously to the correctness
	argument for mergesort, with the merge procedure (and its proof of correctness)
	replaced by the solution to part (a).

	More precisely, we prove the algorithm is correct by induction on the number of
	buildings in the input.
	In the base case, $|B| = 1$, and the cityscape computed is correct.
	For the inductive step, $|B| > 1$, and the algorithm enters its recursive case,
	and constructs $B_1$ and $B_2$.
	Both $B_1$ and $B_2$ contain fewer buildings than $B$,
	so by the inductive hypothesis,
	the two calls $Find\_Cityscape(B_1)$ and $Find\_Cityscape(B_2)$ are correct,
	so return the cityscapes made by $B_1$ and $B_2$ respectively.
	By the correctness of the merge procedure in part (a),
	the merge of these two cityscapes is the cityscape for $B_1 \cup B_2 = B$.
	It follows that $Find\_Cityscape(B)$ correctly computes the cityscape for $B$.

	\paragraph{Running Time}
	Let $T(n)$ be the time it takes to find the cityscape of $n$ buildings.
	The recursive case of the algorithm spends $O(n)$ work outside recursive calls,
	and makes $2$ recursive calls on inputs of size $n/2$.
	This gives a recurrence of $T(n) = 2\cdot T(n/2) + O(n)$,
	with a base case of $T(1) = O(1)$.
	Just as with mergesort, this solves to $T(n) = O(n\log n)$.

\end{enumerate}

\end{enumerate}

\end{document}
