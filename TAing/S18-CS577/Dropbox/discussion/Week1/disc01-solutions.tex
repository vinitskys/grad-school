%
\documentclass{article}
\usepackage{fullpage}

\usepackage{epsfig}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amstext}
\usepackage{amscd}
\usepackage{amsmath}
\usepackage{theorem}
\usepackage{times}
\usepackage{graphicx}
\usepackage{alltt}

\begin{document}

%\thispagestyle{empty}

\noindent
\fbox{
\parbox{\textwidth}{
\begin{Large}
{\bf CS 577: Introduction to Algorithms\hfill Discussion Solutions for Week 1\\}
\end{Large}

Updated:\vspace{0.02in}
\today
\hfill {\bf} Scribe: Sam Vinitsky
}}

\subsection*{Solutions}

\begin{enumerate}
\item We will prove these individually by induction:
\begin{itemize}
\item (i) and (ii) $\implies$ (iii): Let $s(n)$ be the proposition that any graph $G$ with $n$ vertices that is connected and acyclic must have exactly $n-1$ edges. The base case, $s(3)$, is true because if a graph on three vertices has $\leq1$ edge it cannot be connected, and if it has $\geq3$ edges it must have a cycle. Thus it has $n-1 =2$ edges. Suppose $s(n)$ is true for some arbitrary $n \geq 3$. Consider a graph $G$ with $n+1$ vertices that is connected and acyclic. We wish to show that $G$ has exactly $n$ edges. Since $G$ is acyclic, some vertex $v$ must have degree 1. Remove this vertex to get a new graph $G'$ with $n$ vertices. Note that $G'$ is connected and acyclic (otherwise $G$ couldn't have been). Then by the inductive hypothesis $s(n)$, $G'$ must have exactly $n-1$ edges. $G$ is just $G'$ with an additional edge connecting to $v$, and thus $G$ has exactly $n$ edges as desired.

\item (ii) and (iii) $\implies$ (ii): Let $s(n)$ be the proposition that any graph $G$ with $n$ vertices that is acyclic and has $n-1$ edges must also be connected. The base case, $s(3)$, is true, since in the graph with 3 vertices if there are no cycles and 2 edges, then every vertex is connected. Suppose that $s(n)$ is true for some arbitrary $n \geq 3$. Consider an acyclic graph $G$ with $n+1$ vertices and $n$ edges. We wish to show that $G$ is connected. Since $G$ is acyclic, there must be some vertex $v$ of degree 1. Remove this vertex to get a new graph $G'$ with $n$ vertices and $n-1$ edges. Then $G'$ must be acyclic (otherwise $G$ has a cycle). Then $G'$ is an acyclic graph on $n$ vertices and $n-1$ edges, so by the inductive hypothesis $G$ is connected. $G$ is just $G'$ with an additional edge $e=(u,v)$ connecting to $v$ (where $u \in G'$). Since $G'$ was connected, every vertex in $G'$ in connected to $u$, and is thus connected to $v$. Therefore $G$ must be connected as well. 

\item (i) and (iii) $\implies$ (ii). Let $s(n)$ be the proposition that any connected graph $G=(V,E)$ with $n$ vertices that has $n-1$ edges must also be acyclic. The base case, $s(3)$, is true, because if a graph has 3 nodes and 2 edges and is connected, the edges must not create a cycle. Suppose that $s(n)$ is true for some arbitrary $n$. Consider a graph $G=(V,E)$ with  $|V|=n+1$ vertices that is connected and has $|E|=n$ edges. 

Does $G$ contain a vertex of degree 1? If not, then every vertex in $G$ has degree $\geq2$, and when we sum  the degrees of all the vertices in $G$ we get 
\[ \sum_{u\in V} \text{deg}(u) \geq 2 |V| ,\]
which contradicts with the fact that for any graph, in particular for $G$, we have
\[ \sum_{u\in V} \text{deg}(u) = 2|E|.\]
   So there must be some vertex in $G$ of degree 1. By removing this vertex, we get a new graph $G'$ that is  connected and contains $n-1$ edges. By the inductive hypothesis, we know that $G'$ must then be acyclic. Adding back on the vertex $v$ to get $G$, we cannot introduce a cycle since $v$ only has one neighbor. Therefore $G$ is acyclic as desired. 

\end{itemize}

(Fun fact: a graph satisfying all three of these properties is called a \textit{tree}. A graph satisfying just (ii) and (iii) is called a \textit{forest}.)

\item 
\begin{enumerate}
\item We wish to solve for $T(n)$ in $T(n) = 2T(n-1) + 1$. We will do so by repeatedly replacing the recurrence on the right hand side. We call this ``unrolling the recurrence''. Note that $T(n-1) = 2T(n-2) + 1$ by the recurrence relation. Then:

\begin{align*}
T(n) &= 2T(n -1) + 1 \\
T(n) &= 2(2T(n-2) +1) + 1 \\
T(n) &= 4T(n-2) + 2 + 1 \\
T(n) &= 4T(n-2) + 3
\end{align*}

By the recurrence relation, $T(n-2) = 2T(n-3) + 1$. Plugging this in, we get:

\begin{align*}
T(n) &= 4(2T(n-3) + 1) + 3 \\
T(n) &= 8T(n-3) + 7
\end{align*}

Extrapolating this, we see that $T(n) = 2^i T(n-i) + 2^i-1$. Therefore when $i=n$, we have $T(n) = 2^n T(0) + 2^n-1$. Since $T(0) = 0$, we have $T(n) = 2^n-1$. We leave it as an exercise to verify the correctness of this last equality  using induction.   


\item We wish to solve for $T(n)$ in $T(n) = T(\frac{3}{4}n) + n$. As in problem 2a, we will repeatedly replace the recurrence on the right hand side. Note that $T(\frac{3}{4}n) = T(\big(\frac{3}{4}\big)^2 n) + \frac{3}{4}n$ by the recurrence relation. Then:

\begin{align*}
T(n) &= T(\tfrac{3}{4}n) + n \\
T(n) &= (T((\tfrac{3}{4})^2n) + \tfrac{3}{4}n) + n \\
T(n) &= T((\tfrac{3}{4})^2 n) + (1 + \tfrac{3}{4}) n
\end{align*}

By the recurrence relation, $T((\tfrac{3}{4})^2 n) = T((\tfrac{3}{4})^3 n ) + (\tfrac{3}{4})^2 n$.

\begin{align*}
T(n) &= T((\tfrac{3}{4})^2 n) + (1 + \tfrac{3}{4}) n \\
T(n) &= T((\tfrac{3}{4})^3 n) + (1 + \tfrac{3}{4} + (\tfrac{3}{4})^2) n
\end{align*}

Extrapolating this, we see that $T(n) = T((\frac{3}{4})^i n) + \big( \sum_{k=0}^{i-1} (\frac{3}{4})^k \big) n$. We need the quantity inside the $T(\cdot)$ on the right to be 1, since this is the base case for our recurrence. This means we need $(\frac{3}{4})^i n = 1$, meaning $(\frac{3}{4})^i = \frac{1}{n}$, and thus $i = \log_{3/4} (\frac{1}{n})$. Then $T(n) = T(1) + \big( \sum_{k=0}^{\log_{3/4}(1/n)} (\frac{3}{4})^k \big) n$. We now need to deal with this messy sum. Since every term is positive, we know it must lie between $\sum_{k=0}^0 (\frac{3}{4})^k = 1$ and $\sum_{k=0}^\infty \frac{3}{4}^k = \frac{1}{1-3/4} = 4$ (since this is a geometric series). This means that:

\begin{align*}
T(1) + \sum_{k=0}^0 (\frac{3}{4})^k n = T(1) + n \leq &T(n) \\
&T(n) \leq T(1) + \big( \sum_{k=0}^{\infty} (\frac{3}{4})^k \big) n = T(1) + 4n
\end{align*}

Recall that $T(1) = 2$. Then $n + 2 \leq T(n) \leq 4n +2$, and thus $T(n) \in \Theta(n)$.

\item We will prove this by induction on $n$. Since $f(n) \in O(n^3)$, there exist some $c$ such that $f(n) \leq c n^3 +c$ for all $n \geq 0$. We will prove that there exists some $d>0$ such that $R(n) \leq dn^4 +d$ for all $n\geq 0$. This would imply $R(n) \in O(n^4)$. We will let the inductive proof guide our choice of $d$, increasing its value as needed. First, we can choose $d$ large enough so that $d > R(0)$. This takes care of the base case of $n=0$. For the inductive step, assume $R(n) \leq dn^4 + d$ for some $n$. Then 

\begin{align*}
R(n+1) &\leq R(n) +f(n) \\
&\leq dn^4 + cn^3
\end{align*}

We want this last expression to be no more than $d(n+1)^4 +d$. If our earlier choice of $d$ does not satisfy $d \geq c$ already, then we can increase it further to make sure that happens; this way we get
\begin{align*}
R(n+1) & \leq dn^4 + cn^3 \\
& \leq d(n^4 + 4n^3)\\ 
& \leq d(n+1)^4 
\end{align*}

To recap, if  $d= \max(c,R(0))$ then   $R(n) \leq dn^4 + d$ for all $n \geq 0$.  This implies $R(n) \in O(n^4)$. 

\end{enumerate}

\end{enumerate}

\end{document}
