%
\documentclass{article}
\usepackage{fullpage}
\usepackage{amsmath,amsfonts,amssymb}
\usepackage{mathtools}
\DeclarePairedDelimiter\floor{\lfloor}{\rfloor}
\DeclarePairedDelimiter{\ceil}{\lceil}{\rceil}
\usepackage{epsfig}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amstext}
\usepackage{amscd}
\usepackage{amsmath}
\usepackage{theorem}
\usepackage{times}
\usepackage{graphicx}
\usepackage{alltt}
\newtheorem{claim}{Claim}
\newtheorem{proof}{proof}

\usepackage{algorithmicx, algpseudocode}
\begin{document}

%\thispagestyle{empty}

\noindent
\fbox{
\parbox{\textwidth}{
\begin{Large}
{\bf CS 577: Introduction to Algorithms\hfill Discussion Solutions for Week 12\\}
\end{Large}

}}

\section*{Problem 1}
The problem is a selection problem with a minimization objective. Therefore, we convert this to a problem of finding the min-cut. We set up our graph in the following way - 

\begin{itemize} {
\item  Each product is represented as a node in the graph and is connected to the source ($s$) with an edge having capacity equal to its respective $p$ cost (pineapple's version) and is also connected to the sink ($t$) with an edge having capacity equal to its respective $n$ cost (nanosoft's version)

\item  Each node is connected to every other node with an edge having capacity equal to the incompatibility cost between those two products. 

}\end{itemize}

In this setup finding the min $s-t$ cut $(S,T)$ corresponds to finding the minimum cost up purchasing the products. For each product, we buy pineapple's version if the product node is in $T$ and nanosoft's version if the product node is in $S$. \newline

We also make some basic observations - 
\begin{itemize} {
\item We purchase one version of each product - If we don't purchase any version of a product $i$, then $s$ and $t$ are still connected via s $\xrightarrow[\text{$p_i$}]{\text{}}$ i $\xrightarrow[\text{$n_i$}]{\text{}}$  t.  Since we use an $s-t$ cut this is not possible.

\item For any two products with different versions we account for the incompatibility cost - Let us say we purchase product $i$ from Pineapple and product $j$ from Nanosoft and we don't account for the incompatibility cost. Then $s$ and $t$ are still connected via s $\xrightarrow[\text{$p_j$}]{\text{}}$ j $\xrightarrow[\text{$c_i$}]{\text{}}$ i $\xrightarrow[\text{$n_i$}]{\text{}}$ t. Again, this is not possible due to the $s-t$ cut.

}\end{itemize}



We now argue there exists a $s-t$ cut $(S,T)$ with a capacity C, if and only if there is a way to purchase products such that the total cost is C. We prove the forward direction first. Assume we buy all the products in S from pineapple and all the products in T from nanosoft. Then, the cost of buying these products is  $\sum\limits_{i\in S}p_i $ + $\sum\limits_{j\in T}n_j $ + $\sum c(i,j) $ , this is exactly equal to the capacity of the $(S,T)$ cut. Now in the opposite direction, let there exist a $s-t$ cut $(S,T)$ with capacity $C$, then for a product $i$ we buy $i$ from Pineapple if $i$ $\in$ $S$ else we buy from Nanosoft. Similar to the above sum, this will amount to a cost of $C$. The claim combined with the two observations we made, prove that our algorithm is correct.








\section*{Problem 2}

We begin with two observations. First, we can forget about  the integral part of each element in the matrix$A$, and focus only on the fractional part. This is because a rounding preserves row- and column-sums in the simplified matrix iff it also preserves the sums in the original matrix. So from here on $0 \leq A[i,j] \leq 1$ for each $i,j$.  

Second, if any of the rows or columns in $A$ sum to a non-integer, then a rounding does not exist. This is because the rounding is supposed to preserve the row- and column-sums, and after the rounding those sums are integers. Therefore, we first calculate the sum all the rows and columns and return infeasible if any of them are not integral. So from here on all row- and column-sums of $A$ are integers.  

Next, we reduce the problem to a  matching-style flow problem as follows. 

\begin{itemize} {
\item We setup  a bipartite graph, with a node on the left side for each row in $A$ (row nodes), and a node on the right side for each column in $A$  (column nodes). 

\item For every row node $i$ and column node $j$, we introduce the edge $(i,j)$ of capacity equal to $1$ iff $A[i][j]$ is not an integer. This represents the fact that we can round numbers up as long as they are not already integers.

\item We add a source node $s$ and connect it to all row nodes. The capacity of edge $(s,i)$ equals the sum of row $i$. 

\item We add a sink node $t$ and connect all  column nodes to it. The capacity of edge $(j,t)$ equals the sum of col $j$.

}\end{itemize}


We claim that  there is a rounding of $A$ iff there is an integral flow in this network that saturates all edges leaving $s$ and all edges entering $t$. 

First off, notice that the flow saturating all edges leaving $s$ versus saturating all edges entering $t$ are actually equivalent conditions. This is because if all $s$-leaving edges are saturated, then the flow has value equal to the sum of all rows, which equals the sum of all columns, and which implies that all $t$-entering edges are also saturated. The converse direction follows similarly. 

\smallskip

Now  suppose that a valid rounding exists. Then construct a flow by sending $1$ unit along edge $(i,j)$ if $A[i][j]$ is rounded up; otherwise send no flow along $(i,j)$. Observe that in row $i$, the number elements that are rounded up must equal to the sum of row $i$; this implies that the flow we just defined saturates all the edges leaving $s$ (hence all edges entering $t$). 

\smallskip

For the opposite direction, suppose that we have an integral flow that  saturates all $s$-leaving edges and all $t$-entering edges. Round $A[i][j]$ up if the edge $(i,j)$ carries flow; round it down otherwise. Consider a row node $i$; because the flow saturates the edge $(s,i)$, and because of flow conservation, we know that the number of $j$ such that $(i,j)=1$ is exactly the sum of row $i$, meaning that the rounding preserves all  row-sums. A similar argument applies to  column sums. 


\smallskip

Although not required for the solution, we derive another interesting fact. Namely, if the sum of the  elements of all rows and columns are integers then there will always be a rounding that exists for the matrix. The proof is as follows - in the graph we setup, we initially send flow equal to the capacity of the edge to all the row nodes. Then for a row node $i$ and a column node $j$ we will send the flow equal to the fractional amount of $A[i][j]$ from node $i$ to node $j$. Note that doing this will saturate the edges from source to the row nodes and also corresponds to a flow equal to the sum of all the fractional values (we can do this for any matrix with integral sums). But since all the capacities are integral, if a flow exists with some value, there must also exist a integral flow (flow through all edges being integers) with the same flow amount. This, in our graph means there will be a flow with the same values such that the values are either only 0 or 1. This implies that a rounding must exist. 








\end{document}
